package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;
import java.util.Optional;

/**
 * Created by kakim on 02.06.2017.
 */
public class StartAction extends PlayerAction {
    private static final String QUALIFIER = "startAction";
    public static final String TYPE=ACTION_BASE_NAMESPACE +"/"+QUALIFIER;
    public static final QName NAME = attribute(TYPE, "name");
    public static final QName LOCATION = attribute(TYPE, "location");

    public StartAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, String name, Location location) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, NAME, name);
        if (location != null)
            putAttribute(oid, LOCATION, location);

    }

    public StartAction(String oid, String causeOid, String sourceActorId, String name, Location location) {
        this(oid, causeOid, sourceActorId, null, name, location);
    }

    public String getName() {
        return getValue(NAME, String.class).get();
    }

    public Optional<Location> getLocation() {
        return getValue(LOCATION, Location.class);
    }


}
