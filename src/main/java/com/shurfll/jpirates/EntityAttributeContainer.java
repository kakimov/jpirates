package com.shurfll.jpirates;

import com.shurfll.jpirates.entity.AttributeHelper;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.utils.PrettyPrint;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class EntityAttributeContainer {

    private static final String TO_STRING_FORMAT="%s{%s}";

    private Map<QName, EntityAttribute> attributes = new HashMap<>();

    public Map<QName, EntityAttribute> getAttributes(){
        return attributes;
    }

    public void putAttribute(EntityAttribute attribute) {
        attributes.put(attribute.getName(), attribute);
    }

    public void putAttribute(String entityOid, QName qName, Object o) {
        if (o != null)
            attributes.put(qName, AttributeHelper.createAttribute(entityOid, qName, AttributeHelper.resolveType(o), o));
    }

    public Optional<EntityAttribute> getAttribute(QName name) {
        return Optional.ofNullable(attributes.get(name));
    }

    public <T> Optional<T> getValue(QName name, Class<T> clazz) {
        return Optional.ofNullable(attributes.get(name))
                .flatMap(EntityAttribute::getValue)
                .map(EntityAttributeValue::getValue)
                .map(clazz::cast);
    }

    @Override
    public String toString() {
        return String.format(TO_STRING_FORMAT,getClass().getSimpleName(),PrettyPrint.printEntityAttributeContainer(this));
    }
}
