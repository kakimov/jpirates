package com.shurfll.jpirates.event.action;

import com.shurfll.jpirates.event.Event;

import javax.xml.namespace.QName;
import java.util.Map;
import java.util.Optional;

/**
 * Some action a player is about to do
 * Created by shurfll on 22.05.17.
 */
public abstract class Action extends Event {

    protected static final String ACTION_QUALIFIER = "action";
    protected static final String ACTION_META_QUALIFIER = "meta";
    protected static final String ACTION_BASE_NAMESPACE = BASE_EVENT_NAMESPACE + "/" + ACTION_QUALIFIER;
    protected static final String ACTION_META_NAMESPACE = ACTION_BASE_NAMESPACE + "/" + ACTION_META_QUALIFIER;

    public static final QName SOURCE_ACTOR_ID = attribute(ACTION_BASE_NAMESPACE,"sourceActorId");


    public Action(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta) {
        super(oid, causeOid);
        putAttribute(oid, SOURCE_ACTOR_ID, sourceActorId);
        if (meta != null) {
            meta.forEach((qName, o) -> putAttribute(oid, qName, o));
        }
    }

    public Action(String oid, String causeOid, String sourceActorId) {
        this(oid, causeOid, sourceActorId, null);
    }

    public void addMeta(QName qName, Object value) {
        putAttribute(getOid(), qName, value);
    }

    public void addMeta(String name, Object value) {
        putAttribute(getOid(), meta(name), value);
    }

    public Optional<Object> getMeta(QName qName, Class clazz) {
        return getValue(qName, clazz);
    }

    public <T> Optional<T> getMeta(String name) {
        return getValue(meta(name), Object.class).map(o -> (T) o);
    }

    public String getSourceActorId() {
        return getValue(SOURCE_ACTOR_ID, String.class).get();
    }




    public static QName meta(String name) {
        return attribute(ACTION_META_NAMESPACE, name);
    }
}
