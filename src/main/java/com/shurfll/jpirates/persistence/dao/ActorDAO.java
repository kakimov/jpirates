package com.shurfll.jpirates.persistence.dao;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.ActorType;

import java.util.List;
import java.util.Optional;

public interface ActorDAO {
    Optional<Actor> getByActorId(String actorId);

    Optional<Actor> getByOid(String oid);

    Actor add(Actor actor);

    Optional<Actor> findActorByName(String name);

    <T> List<T> getAllByType(ActorType type);

    List<String> getActorsAtLocation(Location location);

    List<Actor> getAll();

    void delete(String oid);
}
