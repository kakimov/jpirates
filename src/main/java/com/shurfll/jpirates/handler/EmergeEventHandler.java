package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.EmergeEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.functions.Function3;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmergeEventHandler implements EventHandler<EmergeEvent> {

    private Reactor reactor;
    private PlayersManager playersManager;

    @Autowired
    public EmergeEventHandler(Reactor reactor, PlayersManager playersManager) {
        this.reactor = reactor;
        this.playersManager = playersManager;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(EmergeEvent.class, this);
    }

    @Override
    public Flowable<Event> handle(EmergeEvent event) {
        return createNotificationEvents(event);
    }

    private Flowable<Event> createNotificationEvents(EmergeEvent event) {
        Player player = playersManager.getPlayerFlowable(event.getTargetActorId()).ofType(Player.class).blockingFirst();

        // notify new neighbours that you have sailed
        List<Object> newNeighbours = findNeighbours(event.getTargetActorId(), event.getLocation());
        Flowable<Event> newNeighboursEvents = notifyNeighboursAboutEvent(event, newNeighbours, Collections.singletonList(player), this::createMeetEvent);

        Flowable<Event> playersEvent = Flowable.empty();
        if (newNeighbours.size() > 0) {
            playersEvent = Flowable.just(createMeetEvent(event, player, newNeighbours));
        }

        return Flowable.merge(playersEvent, newNeighboursEvents);
    }


    private Event createMeetEvent(Event event, Actor target, List<Object> objects) {
        return EventFactory.meetEvent(event, target, objects);
    }

    private Flowable<Event> notifyNeighboursAboutEvent(Event event,
                                                         List<Object> neighbours,
                                                         List<Object> objects,
                                                         Function3<Event, Actor, List<Object>, Event> eventGenerator) {
        return Flowable.fromIterable(neighbours).ofType(Actor.class).map(player -> eventGenerator.apply(event, player, objects));
    }


    private List<Object> findNeighbours(String playerId, Location location) {
        List<Object> objects = new ArrayList<>();
        objects.addAll(playersManager.getNeighboursAtLocation(playerId, location));
        return objects;
    }

}
