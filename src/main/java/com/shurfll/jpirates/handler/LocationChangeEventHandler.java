package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.LocationChangeEvent;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Function3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class LocationChangeEventHandler implements EventHandler<LocationChangeEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationChangeEventHandler.class);

    private Reactor reactor;
    private PlayersManager playersManager;

    @Autowired
    public LocationChangeEventHandler(Reactor reactor, PlayersManager playersManager) {
        this.reactor = reactor;
        this.playersManager = playersManager;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(LocationChangeEvent.class, this);
    }

    @Override
    public Flowable<Event> handle(LocationChangeEvent event) {
        return Flowable.fromIterable(createNotificationEvents(event));
    }

    private List<Event> createNotificationEvents(LocationChangeEvent event) {
        Actor player = playersManager.getPlayerFlowable(event.getTargetActorId()).blockingFirst();
        // notify old neighbours that you have sailed away
        List<Object> oldNeighbours = findNeighbours(event.getTargetActorId(), event.getPrevLocation());
        List<Event> oldNeighboursEvents = notifyNeighboursAboutEvent(event, oldNeighbours, Collections.singletonList(player), this::createSailAwayEvent);
        // notify new neighbours that you have sailed up
        List<Object> newNeighbours = findNeighbours(event.getTargetActorId(), event.getCurrentLocation());
        List<Event> newNeighboursEvents = notifyNeighboursAboutEvent(event, newNeighbours, Collections.singletonList(player), this::createMeetEvent);

        Event locationChangeEvent = EventFactory.infoEvent(event, player.getActorId(), LocalizationKeys.LOCATION_CHANGED_EVENT, LocationHelper.printLocation(event.getCurrentLocation()));
        List<Event> playerEvents = new ArrayList<>();
        playerEvents.add(locationChangeEvent);
        if (newNeighbours.size() > 0) {
            playerEvents.add(createMeetEvent(event, player, newNeighbours));
        }

        List<Event> resultEvents = new ArrayList<>();
        resultEvents.addAll(playerEvents);
        resultEvents.addAll(oldNeighboursEvents);
        resultEvents.addAll(newNeighboursEvents);
        return resultEvents;
    }

    private Event createSailAwayEvent(LocationChangeEvent event, Actor target, List<Object> objects) {
        return EventFactory.sailAwayEvent(event, target, objects, event.getDirection());
    }

    private Event createMeetEvent(LocationChangeEvent event, Actor target, List<Object> objects) {
        return EventFactory.meetEvent(event, target, objects);
    }

    private List<Event> notifyNeighboursAboutEvent(LocationChangeEvent event,
                                                         List<Object> neighbours,
                                                         List<Object> objects,
                                                         Function3<LocationChangeEvent, Actor, List<Object>, Event> eventGenerator) {
        List<Event> result = new ArrayList<>();
        for(Object neighbour : neighbours){
            if(neighbour instanceof Actor){
                try {
                    result.add(eventGenerator.apply(event, (Actor)neighbour, objects));
                }catch (Exception e){
                    LOGGER.error("unable to notify neighbour: {}",neighbour,e);
                }
            }
        }
        return result;
    }


    private List<Object> findNeighbours(String playerId, Location location) {
        List<Object> objects = new ArrayList<>();
        objects.addAll(playersManager.getNeighboursAtLocation(playerId, location));
        return objects;
    }
}
