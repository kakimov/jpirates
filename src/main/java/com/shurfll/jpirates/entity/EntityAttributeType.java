package com.shurfll.jpirates.entity;

public enum EntityAttributeType {
    LONG, STRING, LOCATION, CONFIGURATION, LEVELUPMODE, DIRECTION, GOLDSPENDING, TREASURE, FIRE_RESULT, ACTOR, MAP, COLLECTION
}
