package com.shurfll.jpirates.event.action;

import javax.xml.namespace.QName;
import java.util.Map;

public abstract class PlayerAction extends Action {
    public PlayerAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta) {
        super(oid, causeOid, sourceActorId, meta);
    }

    public PlayerAction(String oid, String causeOid, String sourceActorId) {
        super(oid, causeOid, sourceActorId);
    }
}
