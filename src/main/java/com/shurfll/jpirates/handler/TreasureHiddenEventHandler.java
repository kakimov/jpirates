package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.effect.AbsoluteActionBlockingEffect;
import com.shurfll.jpirates.effect.EffectManager;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.TreasureHiddenEvent;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;

@Component
public class TreasureHiddenEventHandler implements EventHandler<TreasureHiddenEvent> {

    private Reactor reactor;

    private PlayersManager playersManager;

    private EffectManager effectManager;


    @Autowired
    public TreasureHiddenEventHandler(Reactor reactor, PlayersManager playersManager, EffectManager effectManager) {
        this.reactor = reactor;
        this.playersManager = playersManager;
        this.effectManager = effectManager;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(TreasureHiddenEvent.class, this);
    }


    @Override
    public Flowable<Event> handle(TreasureHiddenEvent event) {
        return playersManager.getPlayersFlowable()
                .filter(this::shouldNotifyUser)
                .map(actor -> EventFactory.infoEvent(event, actor.getActorId(), LocalizationKeys.SEARCH_HIDDEN, LocationHelper.printLocation(event.getLocation())));
    }

    private boolean shouldNotifyUser(Actor actor){
        return effectManager.getEffects(actor.getActorId()).stream().noneMatch(effect -> effect instanceof AbsoluteActionBlockingEffect);
    }
}
