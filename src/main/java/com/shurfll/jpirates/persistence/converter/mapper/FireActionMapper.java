package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.FireAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class FireActionMapper extends AbstractEventMapper {

    public FireActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return FireAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return FireAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.fireAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, FireAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, FireAction.TARGET_ID)
                , optionalAttribute(e, FireAction.TARGET_NAME));
    }


}
