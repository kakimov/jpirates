package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.MessageAction;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class MessageActionService implements ActionService<MessageAction> {

    @Autowired
    private PlayersManagerImpl playersManager;

    @Override
    public boolean canHandle(Action a) {
        return a instanceof MessageAction;
    }


    @Override
    public Flowable<Event> doAction(MessageAction action) {
        return message(action);
    }

    private Flowable<Event> message(MessageAction action) {
        return playersManager.getPlayerFlowable(action.getSourceActorId())
                .flatMap(actor -> processActor(action, actor));
    }

    private Flowable<Event> processActor(MessageAction action, Actor actor) {
        return processActorWithLocation(action, actor);

    }

    private Flowable<Event> processActorWithLocation(MessageAction action, Actor actor) {
        List<Actor> neighbours = getNeighbours(actor.getActorId());
        return Flowable.fromIterable(neighbours).map(target -> createMessageEvent(action, actor, target, action.getMessage()));
    }


    private List<Actor> getNeighbours(String actorId) {
        return playersManager.getNeighbours(actorId);
    }

    private Event createMessageEvent(MessageAction action, Actor source, Actor target, String message) {
        return EventFactory.infoEvent(action, target.getActorId(), LocalizationKeys.MESSAGE, source.getName(), message);
    }

}
