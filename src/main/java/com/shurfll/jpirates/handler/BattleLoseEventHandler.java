package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.effect.Effect;
import com.shurfll.jpirates.effect.EffectFactory;
import com.shurfll.jpirates.effect.EffectManager;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.AttributeChangeMode;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.BattleLoseEvent;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.xml.namespace.QName;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Component
public class BattleLoseEventHandler extends AbstractConfigurableSelfrefisteringEventHandler<BattleLoseEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BattleLoseEventHandler.class);

    private static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "battleLoseEventHandler";

    public static final QName PARAM_TIMEOUT = new QName(HANDLER_URI, "timeout");
    public static final QName PARAM_REST_PLACE = new QName(HANDLER_URI, "restPlace");

    private EffectManager effectManager;

    private PlayersManager playersManager;

    private ObjectFactory objectFactory;

    protected Long timeoutParam;
    protected Location restPlaceParam;

    @Autowired
    public BattleLoseEventHandler(Reactor reactor, ConfigurationManager configurationManager, EffectManager effectManager, PlayersManager playersManager, ObjectFactory objectFactory) {
        super(reactor, configurationManager);
        this.effectManager = effectManager;
        this.playersManager = playersManager;
        this.objectFactory = objectFactory;
    }

    @Override
    protected String getHandlerUri() {
        return HANDLER_URI;
    }

    @Override
    public Class getEventClass() {
        return BattleLoseEvent.class;
    }

    @Override
    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_TIMEOUT, Long.class) && ConfigurationHelper.hasParam(nullableConfig, PARAM_REST_PLACE, Location.class);
    }

    @Override
    protected void configure(Optional<Configuration> nullableConfig) {
        LOGGER.debug("Configure: {}", nullableConfig);
        timeoutParam = ConfigurationHelper.getParamValue(nullableConfig, PARAM_TIMEOUT, Long.class, Commons.ACTION_BATTLE_LOSE_DEFAULT_DELAY);
        restPlaceParam = ConfigurationHelper.getParamValue(nullableConfig, PARAM_REST_PLACE, Location.class, Commons.restPlace());
        LOGGER.info("Configuration updated. timeout={}, restPlace={}", timeoutParam, restPlaceParam);
    }

    @Override
    public Flowable<Event> handle(BattleLoseEvent battleLoseEvent) {
        LOGGER.debug("Handle event: {}", battleLoseEvent);
        Effect effect = EffectFactory.absoluteActionBlockingEffect(battleLoseEvent);
        effectManager.addEffect(battleLoseEvent.getTargetActorId(), effect);
        Flowable<Event> before = doBeforeBlocking(battleLoseEvent);
        Flowable<Event> after = doAfterBlocking(battleLoseEvent);
        return before.concatWith(after.delay(timeoutParam, TimeUnit.MILLISECONDS))
                .doOnTerminate(() -> effectManager.removeEffect(battleLoseEvent.getTargetActorId(), effect));
    }

    private Flowable<Event> doAfterBlocking(BattleLoseEvent battleLoseEvent) {
        return playersManager.getPlayerFlowable(battleLoseEvent.getTargetActorId())
                .flatMap(actor -> {
                    if (actor instanceof Player) {
                        return Flowable.concat(awakeEvent(battleLoseEvent), wakeUpAndMoveToRandomLocation(battleLoseEvent));
                    } else {
                        //playersManager.removePlayer(battleLoseEvent.getTargetOid());
                        return Flowable.empty();
                    }
                });
    }

    private Flowable<Event> doBeforeBlocking(BattleLoseEvent event) {
        Location location = restPlaceParam;
        InfoEvent infoEvent = EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.BATTLE_LOSE);
        StateChangeEvent stateChangeEvent = EventFactory.stateChangeEventReplace(event, event.getTargetActorId(), ActorStateAttributes.ACTOR_LOCATION, location);
        return Flowable.just(infoEvent, stateChangeEvent);
    }

    private Flowable<Event> awakeEvent(BattleLoseEvent event) {
        return Flowable.just(EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.BATTLE_LOSE_AWAKE_EVENT));
    }

    private Flowable<Event> wakeUpAndMoveToRandomLocation(BattleLoseEvent event) {
        Location location = objectFactory.generateLocation();
        StateChangeEvent stateChangeEvent = EventFactory.stateChangeEventReplace(event, event.getTargetActorId(), ActorStateAttributes.ACTOR_LOCATION, location);
        stateChangeEvent.getChanges().put(ActorStateAttributes.ACTOR_STATUS,new StateChangeEvent.Change("UP", AttributeChangeMode.REPLACE));
        return Flowable.just(stateChangeEvent, emerge(event, location));
    }

    private Event emerge(BattleLoseEvent event, Location location) {
        return EventFactory.emergeEvent(event, event.getTargetActorId(), location);
    }
}
