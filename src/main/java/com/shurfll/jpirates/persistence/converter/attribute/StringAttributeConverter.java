package com.shurfll.jpirates.persistence.converter.attribute;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StringAttributeConverter extends SimpleTypedValueConverter {
    @Autowired
    public StringAttributeConverter(ValueConverter converter) {
        super(EntityAttributeType.STRING.name(), converter);
    }
}
