package com.shurfll.jpirates.telegram.keyboard;

import java.util.List;

/**
 * Created by kakim on 18.06.2017.
 */
public class ButtonsKeyboard implements Keyboard {
    private List<String> buttons;

    public ButtonsKeyboard(List<String> buttons) {
        this.buttons = buttons;
    }

    public List<String> getButtons() {
        return buttons;
    }

    public void setButtons(List<String> buttons) {
        this.buttons = buttons;
    }
}
