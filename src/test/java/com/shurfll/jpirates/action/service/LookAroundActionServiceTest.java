package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.LookAroundActionService;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.LookAroundEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import io.reactivex.Flowable;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalMatchers;

import io.reactivex.subscribers.TestSubscriber;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.shurfll.jpirates.Commons.loc;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by kakim on 29.05.2017.
 */
public class LookAroundActionServiceTest {

    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";

    private static final String STATIC_PLAYER_ID = "staticid";
    private static final String STATIC_PLAYER_NAME = "staticname";

    private static final Location LOCATION = loc(0L, 0L);
    private static final Location STATIC_LOCATION = loc(0L, 1L);

    private LookAroundActionService service;
    private PlayersManager playersManager;
    private TestSubscriber subscriber;
    private TestSubscriber staticSubscriber;

    @Before
    public void setUp() {
        subscriber = new TestSubscriber();
        staticSubscriber = new TestSubscriber();
        playersManager = mock(PlayersManager.class);
        service = new LookAroundActionService(playersManager);
    }


    @Test
    public void test001_lookAroundAction() {
        Player player = new Player(PLAYER_ID, PLAYER_NAME);
        Player staticPlayer = new Player(STATIC_PLAYER_ID, STATIC_PLAYER_NAME);
        when(playersManager.getLocation(PLAYER_ID)).thenReturn(Optional.ofNullable(LOCATION));
        when(playersManager.getLocation(STATIC_PLAYER_ID)).thenReturn(Optional.ofNullable(STATIC_LOCATION));
        when(playersManager.getPlayerFlowable(PLAYER_ID)).thenReturn(Flowable.just(player));
        when(playersManager.getNeighboursAtLocation(eq(PLAYER_ID), AdditionalMatchers.not(eq(STATIC_LOCATION)))).thenReturn(Collections.emptyList());
        when(playersManager.getNeighboursAtLocation(eq(PLAYER_ID), eq(STATIC_LOCATION))).thenReturn(Collections.singletonList(staticPlayer));

        service.doAction(EventFactory.lookAroundAction(null, PLAYER_ID)).subscribe(subscriber);

        subscriber.awaitTerminalEvent();


        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, event -> TestEventHelper.infoEventMatched(event, PLAYER_ID, "event.info.custom", "На EAST от вас находятся: staticname. "));
    }


    private boolean neighboursMatch(LookAroundEvent event, Map<Location, List<Object>> expectedNeighbours) {
        return event.getVicinity().entrySet().stream().allMatch(entry -> locationMatch(entry.getValue(), expectedNeighbours.get(entry.getKey())))
                &&
                expectedNeighbours.entrySet().stream().allMatch(entry -> locationMatch(entry.getValue(), event.getVicinity().get(entry.getKey())));
    }

    private boolean locationMatch(List<Object> actual, List<Object> expected) {
        return actual.stream().allMatch(expected::contains) && expected.stream().allMatch(actual::contains);
    }

}