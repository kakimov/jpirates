package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.SearchAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class SearchActionMapper extends AbstractEventMapper {

    public SearchActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return SearchAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return SearchAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.searchAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, SearchAction.SOURCE_ACTOR_ID));
    }


}
