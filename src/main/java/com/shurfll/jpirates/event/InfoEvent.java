package com.shurfll.jpirates.event;

import javax.xml.namespace.QName;

/**
 * Created by shurfll on 26.05.17.
 */
public class InfoEvent extends PersonalEvent {
    private static final String QUALIFIER = "infoEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName MESSAGE = attribute(TYPE, "message");
    private static final QName ARGUMENTS = attribute(TYPE, "args");

    public InfoEvent(String oid, String causeOid, String targetActorId, String message, Object[] args) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, MESSAGE, message);
        putAttribute(oid, ARGUMENTS, args);
    }

    public String getMessage() {
        return getValue(MESSAGE, String.class).get();
    }

    public Object[] getArgs() {
        return getValue(ARGUMENTS, Object[].class).get();
    }

}
