package com.shurfll.jpirates.telegram;

import com.shurfll.jpirates.event.InfoEvent;

/**
 * Created by kakim on 17.06.2017.
 */
public interface TelegramManager {
    public void sendEvent(InfoEvent event);
    public void setPlayerBot(TelegramBot playerBot);
}
