package com.shurfll.jpirates.event.system;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.PersonalEvent;

import javax.xml.namespace.QName;

public class ActorStartedEvent extends PersonalEvent {
    private static final String QUALIFIER = "actorStartedEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;

    private static final QName ACTOR = attribute(TYPE, "actor");
    private static final QName LOCATION = attribute(TYPE, "location");

    public ActorStartedEvent(String oid, String causeOid, String targetActorId, Actor actor, Location location) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, ACTOR, actor);
        putAttribute(oid, LOCATION, location);
    }

    public Location getLocation() {
        return getValue(LOCATION, Location.class).get();
    }

    public Actor getActor() {
        return getValue(ACTOR, Actor.class).get();
    }

}
