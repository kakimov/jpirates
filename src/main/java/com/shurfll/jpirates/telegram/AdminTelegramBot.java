package com.shurfll.jpirates.telegram;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Qualifier("admin")
public class AdminTelegramBot extends AbstractTelegramBot {

    @Value("${admin.bot.username}")
    private String botUsername;
    @Value("${admin.bot.token}")
    private String botToken;

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
