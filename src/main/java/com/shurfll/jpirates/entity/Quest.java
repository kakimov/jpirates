package com.shurfll.jpirates.entity;

import javax.xml.namespace.QName;

public class Quest {
    private QName type;
    private String questId;
    private String parentQuestId;
    private String name;
    private String description;
    private String command;

    public Quest(QName type, String questId, String parentQuestId, String name, String description, String command) {
        this.type = type;
        this.questId = questId;
        this.parentQuestId = parentQuestId;
        this.name = name;
        this.description = description;
        this.command = command;
    }

    public QName getType() {
        return type;
    }

    public void setType(QName type) {
        this.type = type;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getParentQuestId() {
        return parentQuestId;
    }

    public void setParentQuestId(String parentQuestId) {
        this.parentQuestId = parentQuestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "Quest{" +
                "type=" + type +
                ", questId='" + questId + '\'' +
                ", parentQuestId='" + parentQuestId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", command='" + command + '\'' +
                '}';
    }
}
