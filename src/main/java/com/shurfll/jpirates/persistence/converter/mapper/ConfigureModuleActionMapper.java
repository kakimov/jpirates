package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.admin.ConfigureModuleAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class ConfigureModuleActionMapper extends AbstractEventMapper {

    public ConfigureModuleActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return ConfigureModuleAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return ConfigureModuleAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.configureModuleAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, ConfigureModuleAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, ConfigureModuleAction.CONFIGURATION));
    }


}
