package com.shurfll.jpirates.action;

public class ActionCommons {
    public static final String ACTION_NAMESPACE = "http://jpirates.org/action";

    public static final String ACTION_DELAY_META_KEY = "actionDelay";

}
