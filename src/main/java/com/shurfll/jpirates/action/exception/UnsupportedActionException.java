package com.shurfll.jpirates.action.exception;

import com.shurfll.jpirates.ActorException;
import com.shurfll.jpirates.event.action.Action;

/**
 * Created by kakim on 26.10.2017.
 */
public class UnsupportedActionException extends ActorException {
    private Action causeAction;

    public UnsupportedActionException(String actorId, Action causeAction) {
        super(actorId);
        this.causeAction = causeAction;
    }

    public Action getCauseAction() {
        return causeAction;
    }

    public void setCauseAction(Action causeAction) {
        this.causeAction = causeAction;
    }
}
