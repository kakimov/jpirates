package com.shurfll.jpirates.effect;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


import java.util.List;

public interface EffectHandlerManager {
    Flowable<Event> calculateEffects(Action action, List<Effect> effect);
}
