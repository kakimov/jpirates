package com.shurfll.jpirates.action.service.admin;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.AdminActionService;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.admin.ConfigureModuleAction;
import com.shurfll.jpirates.event.action.admin.NewsAction;
import com.shurfll.jpirates.manager.PlayersManager;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class NewsActionService extends AdminActionService<NewsAction> {

    private PlayersManager playersManager;

    @Autowired
    public NewsActionService(PlayersManager playersManager) {
        this.playersManager = playersManager;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof NewsAction;
    }

    @Override
    public Flowable<Event> doAction(NewsAction action) {
        return playersManager.getPlayersFlowable().map(actor -> EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.GAME_NEWS, action.getText()));
    }
}
