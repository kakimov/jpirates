package com.shurfll.jpirates.manager;

import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.persistence.dao.ActorDAO;
import com.shurfll.jpirates.persistence.dao.StateDAO;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class StateManagerImpl implements StateManager {

    private Map<String, State> worldState = new ConcurrentHashMap<>();

    private StateDAO stateDAO;
    private ActorDAO actorDAO;

    @Autowired
    public StateManagerImpl(StateDAO stateDAO, ActorDAO actorDAO) {
        this.stateDAO = stateDAO;
        this.actorDAO = actorDAO;
    }

    @Override
    public Flowable<State> getState(String entityOid) {
        return Flowable.just(stateDAO.getState(entityOid));
    }

    @Override
    public Flowable<State> getStateByActorId(String actorId) {
        Actor actor = actorDAO.getByActorId(actorId).get();
        return Flowable.just(stateDAO.getStateByOwnerOid(actor.getOid()));
    }

    @Override
    public void updateState(State state) {
        stateDAO.updateState(state);
    }


    @Override
    public void deleteState(String oid) {
        worldState.remove(oid);
    }
}
