package com.shurfll.jpirates.action.service.admin;

import com.shurfll.jpirates.action.service.AdminActionService;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.admin.ConfigureModuleAction;
import io.reactivex.Flowable;
import org.springframework.stereotype.Component;


@Component
public class ConfigureModuleActionService extends AdminActionService<ConfigureModuleAction> {

    private ConfigurationManager configurationManager;

    public ConfigureModuleActionService(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof ConfigureModuleAction;
    }

    @Override
    public Flowable<Event> doAction(ConfigureModuleAction action) {
        configurationManager.updateConfiguration(action.getConfiguration());
        return Flowable.empty();
    }
}
