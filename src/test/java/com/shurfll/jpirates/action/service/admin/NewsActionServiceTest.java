package com.shurfll.jpirates.action.service.admin;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class NewsActionServiceTest {

    private PlayersManager playersManager;
    private NewsActionService service;
    private Actor aPlayer;
    private String aPlayerOid="aOid";
    private Actor bPlayer;
    private String bPlayerOid="bOid";
    private String NEWS_TEXT="Breaking news: news!";

    @Before
    public void init(){
        playersManager = mock(PlayersManager.class);
        service = new NewsActionService(playersManager);
        aPlayer = new Player(aPlayerOid, "a");
        bPlayer = new Player(bPlayerOid, "B");
    }

    @Test
    public void canHandle() {
        assertTrue(service.canHandle(EventFactory.newsAction(null, "test", "test")));
        assertFalse(service.canHandle(EventFactory.moveAction(null, "test", Direction.EAST)));
    }

    @Test
    public void doAction() {
        when(playersManager.getPlayersFlowable()).thenReturn(Flowable.just(aPlayer, bPlayer));
        TestSubscriber subscriber = TestSubscriber.create();
        service.doAction(EventFactory.newsAction(null, "test", NEWS_TEXT)).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> checkNewsMessage(e, aPlayerOid));
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> checkNewsMessage(e, bPlayerOid));
    }

    private boolean checkNewsMessage(InfoEvent e,String targetOid){
        return LocalizationKeys.GAME_NEWS.equals(e.getMessage()) && e.getTargetActorId().equals(targetOid);
    }
}