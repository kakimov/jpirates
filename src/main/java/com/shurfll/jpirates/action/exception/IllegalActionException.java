package com.shurfll.jpirates.action.exception;

import com.shurfll.jpirates.ActorException;

/**
 * Created by kakim on 28.10.2017.
 */
public class IllegalActionException extends ActorException {

    private String causeMessage;

    public IllegalActionException(String actorId, String causeMessage) {
        super(actorId);
        this.causeMessage = causeMessage;
    }

    public String getCauseMessage() {
        return causeMessage;
    }

    public void setCauseMessage(String causeMessage) {
        this.causeMessage = causeMessage;
    }
}
