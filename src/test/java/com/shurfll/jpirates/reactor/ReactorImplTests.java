package com.shurfll.jpirates.reactor;

import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.TestEvent;
import com.shurfll.jpirates.event.Event;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import lombok.NoArgsConstructor;
import org.junit.Before;
import org.junit.Test;


public class ReactorImplTests {

    private static String EVENT_ID = "testevent";
    private static String EVENT_CAUSE_ID = "cause_id";

    private static String TRIGGER_ID = "trigger_id";
    private static String SUCCESS_ID = "SUCCESS_id";

    private ReactorImpl reactor;

    @Before
    public void init() {
        reactor = new ReactorImpl();
    }

    @Test
    public void test001_submitEventAndObserve() throws Exception {
        TestSubscriber subscriber = TestSubscriber.create();
        Event triggerEvent = new TestEvent(TRIGGER_ID, null);
        Event finalEvent = new TestEvent(SUCCESS_ID, null);
        Flowable<Event> events = reactor.submitEventAndObserve(new TestEvent(EVENT_ID, EVENT_CAUSE_ID));
        events.subscribe(subscriber);
        reactor.submitEvent(triggerEvent);
        TestEventHelper.checkEventProduced(subscriber, Event.class, e -> triggerEvent == e);
    }

    @Test
    public void test002_processEventsAfterException(){
        reactor.registerHandler(TestEventA.class, event -> Flowable.empty());
        reactor.registerHandler(TestEventB.class, event -> {
            throw new RuntimeException("exception in handler");
        });
        TestSubscriber subscriber = TestSubscriber.create();
        reactor.events().subscribe(subscriber);
        reactor.init();

        reactor.submitEvent(new TestEventA("1","2"));
        subscriber.awaitCount(1);
        reactor.submitEvent(new TestEventB("error", "error"));
        subscriber.awaitCount(2);
        reactor.submitEvent(new TestEventA("2","2"));
        subscriber.awaitCount(3);

    }

    private class TestEventA extends Event{
        public TestEventA(String oid, String causeOid) {
            super(oid, causeOid);
        }
    }

    private class TestEventB extends Event{
        public TestEventB(String oid, String causeOid) {
            super(oid, causeOid);
        }
    }
}