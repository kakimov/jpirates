package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Treasure;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.TreasureFoundEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;


import javax.annotation.PostConstruct;
import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TreasureFoundEventHandler implements EventHandler<TreasureFoundEvent> {

    private PlayersManager playersManager;

    private Reactor reactor;


    @Autowired
    public TreasureFoundEventHandler(PlayersManager playersManager, Reactor reactor) {
        this.playersManager = playersManager;
        this.reactor = reactor;
    }


    @PostConstruct
    public void init() {
        reactor.registerHandler(TreasureFoundEvent.class, this);
    }

    @Override
    public Flowable<Event> handle(TreasureFoundEvent event) {
        Assert.notNull(event, "event can't be null");
        Assert.notNull(event.getTreasure(), "treasure can't be null");
        Assert.notNull(event.getLocation(), "location can't be null");
        return playersManager.getPlayerFlowable(event.getTargetActorId())
                .flatMap(actor -> Flowable.concat(changeWinnerState(event, actor, event.getTreasure())
                        , notifyWinner(event, actor, event.getTreasure())
                        , notifyLosers(event, actor, event.getLocation(), event.getTreasure())));
    }

    private Flowable<Event> changeWinnerState(Event event, Actor actor, Treasure treasure) {
        Map<QName,Object> changes = treasure.getAttributes().entrySet().stream()
                .filter(e -> e.getValue().getValue().isPresent())
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getValue().get().getValue()));
        changes.put(ActorStateAttributes.ACTOR_EXPERIENCE, Commons.DEFAULT_TREASURE_FOUND_EXPERIENCE);
        return Flowable.just(EventFactory.stateChangeEventModify(event, actor.getActorId(), changes));
    }

    private Flowable<Event> notifyWinner(Event event, Actor actor, Treasure treasure) {
        return Flowable.just(EventFactory.infoEvent(event, actor.getActorId(), LocalizationKeys.TREASURE_ITEM_OBTAINED, convertTreasure(treasure)));
    }

    private Flowable<Event> notifyLosers(Event event, Actor winner, Location location, Treasure treasure) {
        return Flowable.fromIterable(playersManager.getNeighboursAtLocation(winner.getActorId(), location))
                .map(loser -> EventFactory.infoEvent(event, loser.getActorId(), LocalizationKeys.SEARCH_ANOTHER_FOUND, winner.getName()));

    }

    private String convertTreasure(Treasure treasure){
        List<String> items = new ArrayList<>();
        if(treasure.getAttributes() == null){
            return "";
        }
        for(Map.Entry<QName, EntityAttribute> attr :  treasure.getAttributes().entrySet()){
            // TODO Localization of treasure items
            items.add(attr.getKey().getLocalPart()+" - " + attr.getValue().getValue().map(v -> v.getValue()));
        }
        return String.join(",", items);
    }


}
