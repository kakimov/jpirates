package com.shurfll.jpirates;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.PersonalEvent;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import org.reactivestreams.Subscriber;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Maintain the list of active subscribers
 */
@Component
public class SubscriptionManagerImpl implements SubscriptionManager{

    private Map<String, Subscriber> subscribers = new ConcurrentHashMap<>();

    private PublishProcessor<Event> events = PublishProcessor.create();

    public Flowable<PersonalEvent> personalEventsFlowable(String playerId){
        return events.ofType(PersonalEvent.class).filter(event -> event.getTargetActorId().equals(playerId));
    }
}
