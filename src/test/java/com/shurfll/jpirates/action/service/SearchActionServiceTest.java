package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.SearchActionService;
import com.shurfll.jpirates.component.treasure.TreasureManager;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Treasure;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.TreasureFoundEvent;
import com.shurfll.jpirates.event.action.player.SearchAction;
import com.shurfll.jpirates.manager.PlayersManager;
import org.junit.Before;
import org.junit.Test;
import io.reactivex.subscribers.TestSubscriber;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class SearchActionServiceTest {

    private static final String ACTOR_ID = "actor_oid";

    private PlayersManager manager;
    private TreasureManager treasureManager;
    private SearchActionService service;

    @Before
    public void setUp() throws Exception {
        manager = mock(PlayersManager.class);
        treasureManager = mock(TreasureManager.class);
        service = new SearchActionService(manager,treasureManager);
    }

    @Test
    public void test001_playerHasNoLocation() {
        when(manager.getLocation(eq(ACTOR_ID))).thenReturn(Optional.empty());
        TestSubscriber subscriber = TestSubscriber.create();
        service.doAction(searchAction()).subscribe(subscriber);
        subscriber.assertNoValues();
        subscriber.assertTerminated();
    }

    @Test
    public void test002_noTreasureAtLocation() {
        Location location = new Location(1L, 1L);
        when(manager.getLocation(eq(ACTOR_ID))).thenReturn(Optional.of(location));
        when(treasureManager.discoverTreasures(eq(location))).thenReturn(Optional.empty());
        TestSubscriber subscriber = TestSubscriber.create();
        service.doAction(searchAction()).subscribe(subscriber);
        infoEventProduced(subscriber, ACTOR_ID, LocalizationKeys.SEARCH_NOTHING);
        subscriber.assertComplete();
    }

    @Test
    public void test003_TreasureAtLocation() {
        Location location = new Location(1L, 1L);
        Treasure treasure = new Treasure(null, null);
        when(manager.getLocation(eq(ACTOR_ID))).thenReturn(Optional.of(location));
        when(treasureManager.discoverTreasures(eq(location))).thenReturn(Optional.of(treasure));
        TestSubscriber subscriber = TestSubscriber.create();
        service.doAction(searchAction()).subscribe(subscriber);
        treasureFoundEventProduced(subscriber, ACTOR_ID);
        subscriber.assertComplete();
    }

    private void treasureFoundEventProduced(TestSubscriber<Event> subscriber, String targetId) {
        TestEventHelper.checkEventProduced(subscriber, TreasureFoundEvent.class, event -> treasureFoundMatch((TreasureFoundEvent) event, targetId));
    }

    private boolean treasureFoundMatch(TreasureFoundEvent event, String targetId) {
        return targetId.equals(event.getTargetActorId());
    }

    private void infoEventProduced(TestSubscriber<Event> subscriber, String targetId, String messageKey) {
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, event -> infoEventMatch((InfoEvent) event, targetId, messageKey));
    }

    private boolean infoEventMatch(InfoEvent event, String targetId, String messageKey) {
        return targetId.equals(event.getTargetActorId()) && messageKey.equals(event.getMessage());
    }

    private SearchAction searchAction() {
        return new SearchAction(null, null, ACTOR_ID);
    }
}