package com.shurfll.jpirates.effect;

public abstract class Effect {
    private String oid;
    private String causeOid;
    private Boolean blocking;


    public Effect(String oid, String causeOid, Boolean blocking) {
        this.oid = oid;
        this.causeOid = causeOid;
        this.blocking = blocking;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getCauseOid() {
        return causeOid;
    }

    public void setCauseOid(String causeOid) {
        this.causeOid = causeOid;
    }

    public Boolean getBlocking() {
        return blocking;
    }

    public void setBlocking(Boolean blocking) {
        this.blocking = blocking;
    }
}
