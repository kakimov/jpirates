package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.LevelUpAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class LevelUpActionMapper extends AbstractEventMapper {

    public LevelUpActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return LevelUpAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return LevelUpAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.levelUpAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, LevelUpAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, LevelUpAction.MODE));
    }


}
