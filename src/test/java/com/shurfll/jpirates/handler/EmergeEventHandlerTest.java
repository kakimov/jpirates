package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.MeetEvent;
import com.shurfll.jpirates.event.system.EmergeEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class EmergeEventHandlerTest {

    private static final String PLAYER_ID="id";
    private static final String PLAYER_NAME="name";
    private static final Location LOCATION = new Location(-7l,5l);
    private static final String PLAYER_A_ID="id_a";
    private static final String PLAYER_A_NAME="name_a";
    private static final String PLAYER_B_ID="id_b";
    private static final String PLAYER_B_NAME="name_b";
    private static final Player PLAYER = new Player(PLAYER_ID, PLAYER_NAME);
    private static final Player NEIGHTBOUR_A = new Player(PLAYER_A_ID, PLAYER_A_NAME);
    private static final Player NEIGHTBOUR_B = new Player(PLAYER_B_ID, PLAYER_B_NAME);
    private static final List<Actor> NEIGHBOURS = Arrays.asList(NEIGHTBOUR_A,NEIGHTBOUR_B);

    private EmergeEventHandler handler;
    private Reactor reactor;
    private PublishProcessor<Event> subject;
    private PlayersManager playersManager;
    private TestSubscriber<Event> subscriber;

    @Before
    public void init(){
        subject = PublishProcessor.create();
        reactor = mock(Reactor.class);
        when(reactor.events()).thenReturn(subject);
        playersManager = mock(PlayersManager.class);
        handler = new EmergeEventHandler(reactor,playersManager);
        subscriber = TestSubscriber.create();
    }

    @Test
    public void handle() {
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID))).thenReturn(Flowable.just(PLAYER));
        when(playersManager.getNeighboursAtLocation(eq(PLAYER_ID),eq(LOCATION))).thenReturn(NEIGHBOURS);
        EmergeEvent event = EventFactory.emergeEvent(null,PLAYER_ID, LOCATION);
        handler.handle(event).subscribe(subscriber);
        TestEventHelper.checkEventProduced(subscriber,MeetEvent.class, e -> TestEventHelper.meetEventMatched(e,PLAYER_ID, NEIGHBOURS));
        TestEventHelper.checkEventProduced(subscriber,MeetEvent.class, e -> TestEventHelper.meetEventMatched(e,PLAYER_A_ID, Collections.singletonList(PLAYER)));
    }
}