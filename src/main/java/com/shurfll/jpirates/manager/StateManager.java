package com.shurfll.jpirates.manager;

import com.shurfll.jpirates.entity.State;
import io.reactivex.Flowable;

public interface StateManager {
    Flowable<State> getState(String entityOid);

    Flowable<State> getStateByActorId(String actorId);

    void updateState(State state);

    void deleteState(String oid);
}
