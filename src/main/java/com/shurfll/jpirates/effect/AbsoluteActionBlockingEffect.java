package com.shurfll.jpirates.effect;

public class AbsoluteActionBlockingEffect extends Effect{

    public AbsoluteActionBlockingEffect(String oid, String causeOid) {
        super(oid, causeOid, true);
    }
}
