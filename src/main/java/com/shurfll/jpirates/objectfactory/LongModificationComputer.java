package com.shurfll.jpirates.objectfactory;

import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class LongModificationComputer implements ModificationComputer {
    @Override
    public boolean canCompute(EntityAttribute attribute) {
        return attribute.getType().equals(EntityAttributeType.LONG);
    }

    @Override
    public Object compute(EntityAttribute attribute, Object value) {
        return applyPostprocessor(attribute.getPostProcessors(), attribute.getValue()
                .map(EntityAttributeValue::getValue)
                .map(o -> (Long) o + (Long) value)
                .orElse((Long) value));
    }

    private Object applyPostprocessor(List<Function<Object, Object>> functions, Object value) {
        if (functions == null) return value;
        Object result = value;
        for (Function<Object, Object> func : functions) {
            result = func.apply(result);
        }
        return result;
    }
}
