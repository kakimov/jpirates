package com.shurfll.jpirates.objectfactory;

import com.shurfll.jpirates.entity.State;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import static com.shurfll.jpirates.ActorHelper.*;


public class ActorHealthPostProcessor implements PostProcessorProvider {

    private State state;

    public ActorHealthPostProcessor(State state) {
        this.state = state;
    }

    @Override
    public  List<Function<Object, Object>> provide() {
        return Arrays.asList(o -> Math.max((Long)o, 0L), o -> Math.min((Long)o, getActorTotalHealth(state)));
    }
}
