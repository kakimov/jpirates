package com.shurfll.jpirates.action;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.exception.AnotherActionExecutingException;
import com.shurfll.jpirates.action.exception.IllegalActionException;
import com.shurfll.jpirates.action.exception.InterruptedException;
import com.shurfll.jpirates.action.exception.UnsupportedActionException;
import com.shurfll.jpirates.action.interruption.ActionInterruptionRules;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.effect.Effect;
import com.shurfll.jpirates.effect.EffectFactory;
import com.shurfll.jpirates.effect.EffectManager;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.PersonalEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.AdminAction;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.flowables.GroupedFlowable;
import io.reactivex.processors.PublishProcessor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * The purpose of the class is to implement the time laps of a command execution
 * Created by kakim on 02.06.2017.
 */

@Component
public class ActionExecutorImpl implements ActionExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionExecutorImpl.class);

    @Autowired
    private List<ActionService> playerActionServices;


    private Reactor reactor;

    private ActionInterruptionRules actionInterruptionRights;

    private EffectManager effectManager;

    private Map<String, Action> activeActions = new ConcurrentHashMap<>();

    @Autowired
    public ActionExecutorImpl(Reactor reactor
            , ActionInterruptionRules actionInterruptionRights
            , EffectManager effectManager) {
        this.reactor = reactor;
        this.actionInterruptionRights = actionInterruptionRights;
        this.effectManager = effectManager;
    }


    @Override
    public Flowable<Event> execute(Action action) {
        return Flowable.just(action)
                .doOnNext(a -> MDC.put("actorId", a.getSourceActorId()))
                .doOnNext(a -> LOGGER.debug("Prepare to execute action: {}", a))
                .groupBy(AdminAction.class::isInstance)
                .flatMap(this::processAdminActions)
                .flatMap(effectManager::handleAction)
                .groupBy(event -> event instanceof Action)
                .flatMap(group -> group.getKey() ? group.ofType(Action.class).flatMap(this::prepareAndExecuteAction) : group)
                .doOnNext(event -> LOGGER.info("Event {} produced by action {}", event.toString(), action.getOid()))
                .onErrorReturn((t) -> translateException(action, t));
    }

    private Flowable<Action> processAdminActions(GroupedFlowable<Boolean, Action> group) {
        if (group.getKey()) {
            group.flatMap(this::executeAction).subscribe(reactor::submitEvent);
            return Flowable.empty();
        } else {
            return group;
        }
    }

    private Flowable<Event> prepareAndExecuteAction(Action action) {
        Effect effect = putBlockingEffect(action);

        PublishProcessor<InterruptionCompletionEvent> processor = PublishProcessor.create();
        Flowable<Event> eventFlow =  Flowable.just(action)
                .flatMap(a -> prepareAction(a))
                .doOnNext(a -> LOGGER.debug("Execute action: {}", a))
                .groupBy(event -> event instanceof Action)
                .flatMap(group -> group.getKey() ? group.ofType(Action.class).flatMap(this::executeAction) : group)
                .doOnTerminate(() -> processor.onNext(new InterruptionCompletionEvent()));
        return Flowable.merge(eventFlow,listenForInterruption(action, processor))
                .doOnError(e -> handleError(action, e, effect))
                .doOnTerminate(() -> handleTermination(action,effect,processor));

    }

    private void handleError(Action action, Throwable e, Effect effect) {
        if (e instanceof InterruptedException) {
            LOGGER.debug("Interrupt action chain {} and release lock for actor: {} because of exception: {}", action.getOid(), action.getSourceActorId(), e);
        } else {
            LOGGER.error("Unpredicted exception", e);
        }
    }

    private void handleTermination(Action action, Effect effect, PublishProcessor<InterruptionCompletionEvent> interruptionListener) {
        effectManager.removeEffect(action.getSourceActorId(), effect);
        interruptionListener.onComplete();
        LOGGER.debug("Terminate action chain {} and release lock for actor: {}", action.getOid(), action.getSourceActorId());
    }

    private Flowable<Event> listenForInterruption(Action action, Flowable<InterruptionCompletionEvent> breakCondition) {
        Flowable<Event> interruptingEvents =  reactor.events()
                .ofType(PersonalEvent.class)
                .filter(event -> actionInterruptionRights.isInterruptedByEvent(action, event))
                .flatMap(event -> Flowable.error(new InterruptedException(action.getSourceActorId(), event)));
        return interruptingEvents.takeUntil(breakCondition);
    }

    /**
     * Check if there other action that blocks user interface
     *
     * @param action
     * @return
     */
    private Effect putBlockingEffect(Action action) {
        Effect effect = EffectFactory.absoluteActionBlockingEffect(action);
        effectManager.addEffect(action.getSourceActorId(), effect);
        return effect;
    }

    /**
     * Performs "before action" logic. Returns Events Flowable
     *
     * @param action
     * @return
     */
    private Flowable<Event> prepareAction(Action action) {
        Long timeout = (Long) action.getMeta(ActionCommons.ACTION_DELAY_META_KEY).orElse(0L);
        Flowable<Long> timer = Flowable.timer(timeout, TimeUnit.MILLISECONDS);

        Flowable<Event> beforeActionEvents = playerActionServices.stream()
                .filter(s -> s.canHandle(action))
                .findFirst()
                .map(service -> service.doBeforeAction(action))
                .orElse(Flowable.empty());

        return Flowable.concat(beforeActionEvents, timer.map(aLong -> action));
    }

    private Flowable<Event> executeAction(Action action) {
        Optional<ActionService> actionService = playerActionServices.stream().filter(s -> s.canHandle(action)).findFirst();
        if (actionService.isPresent()) {
            return actionService.get()
                    .doAction(action)
                    .onErrorReturn(e -> Flowable.empty());
        } else {
            return Flowable.error(new UnsupportedActionException(action.getSourceActorId(), action));
        }
    }

    private Event translateException(Action action, Throwable throwable) throws RuntimeException {
        if (throwable instanceof InterruptedException) {
            return EventFactory.infoEvent(((InterruptedException) throwable).getCauseEvent(), ((InterruptedException) throwable).getActorId(), LocalizationKeys.EXCEPTION_ACTION_INTERRUPTED);
        } else if (throwable instanceof UnsupportedActionException) {
            return EventFactory.infoEvent(((UnsupportedActionException) throwable).getCauseAction(), ((UnsupportedActionException) throwable).getActorId(), LocalizationKeys.EXCEPTION_ACTION_IS_UNSUPPORTED, new Object[]{((UnsupportedActionException) throwable).getCauseAction()});
        } else if (throwable instanceof AnotherActionExecutingException) {
            return EventFactory.infoEvent(((AnotherActionExecutingException) throwable).getCauseAction(), ((AnotherActionExecutingException) throwable).getActorId(), LocalizationKeys.EXCEPTION_ACTION_IN_PROGRESS);
        } else if (throwable instanceof IllegalActionException) {
            return EventFactory.infoEvent(null, ((IllegalActionException) throwable).getActorId(), ((IllegalActionException) throwable).getCauseMessage());
        } else {
            LOGGER.error("Unpredictable error during action: {}: ", action, throwable);
            return EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.CUSTOM_MESSAGE, throwable.getMessage());
        }
    }

    public List<ActionService> getPlayerActionServices() {
        return playerActionServices;
    }

    public void setPlayerActionServices(List<ActionService> playerActionServices) {
        this.playerActionServices = playerActionServices;
    }

    @AllArgsConstructor
    @Getter
    private class InterruptionEvent{
        private Event cause;
    }

    @AllArgsConstructor
    private class InterruptionCompletionEvent{
    }

}
