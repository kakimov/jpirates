package com.shurfll.jpirates.reactor;

import com.shurfll.jpirates.event.Event;
import io.reactivex.Flowable;

@FunctionalInterface
public interface EventHandler<T extends Event> {

    public Flowable<Event> handle(T t);
}
