package com.shurfll.jpirates.helper;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shurfll on 22.05.17.
 */
public class LocationHelper {

    private static final String LOCATION_TEMPLATE="%d северной широты и %d западной долготы";

    public static Location changeLocation(final Location location, final Direction direction) {
        return moveInDirection(location,direction);

    }

    private static Location moveInDirection(Location currentLocation, Direction direction){
        Location newLocation = new Location(currentLocation);
        switch (direction){
            case EAST:{
                newLocation.setLongitude(newLocation.getLongitude() + Commons.MOVE_STEP_DELTA);
                break;
            }
            case WEST:{
                newLocation.setLongitude(newLocation.getLongitude() - Commons.MOVE_STEP_DELTA);
                break;
            }
            case SOUTH:{
                newLocation.setLatitude(newLocation.getLatitude() - Commons.MOVE_STEP_DELTA);
                break;
            }
            case NORTH:{
                newLocation.setLatitude(newLocation.getLatitude() + Commons.MOVE_STEP_DELTA);
                break;
            }
        }
        return newLocation;
    }

    public static String printLocation(Location l){
        return String.format(LOCATION_TEMPLATE,l.getLatitude(),l.getLongitude());
    }

    public static List<Location> getVicinity(Location location){
        List<Location> vicinities = new ArrayList<>(9);
        for(long i = location.getLatitude()-1; i <= location.getLatitude() + 1; i ++){
            for(long j = location.getLongitude()-1; j <= location.getLongitude()+1; j++){
                vicinities.add(Commons.loc(i,j));
            }
        }
        return vicinities;
    }

    public static String printRelativeDirection(Location base, Location target){
        return getRelativeDirection(base,target).name();
    }

    public static Direction getRelativeDirection(Location base, Location target){
        if(base.equals(target)){
            throw new IllegalArgumentException("the base and the target are the same");
        }
        if(base.getLatitude() == target.getLatitude()){
            return base.getLongitude() > target.getLongitude() ? Direction.WEST : Direction.EAST;
        }else if (base.getLongitude() == target.getLongitude()){
            return base.getLatitude() > target.getLatitude() ? Direction.SOUTH : Direction.NORTH;
        }else if (base.getLongitude() > target.getLongitude() && base.getLatitude() > target.getLatitude()){
            return Direction.SOUTH_WEST;
        }else if (base.getLongitude() > target.getLongitude() && base.getLatitude() < target.getLatitude()){
            return Direction.NORTH_WEST;
        }else if (base.getLongitude() < target.getLongitude() && base.getLatitude() > target.getLatitude()){
            return Direction.SOUTH_EAST;
        }else if (base.getLongitude() < target.getLongitude() && base.getLatitude() < target.getLatitude()){
            return Direction.NORTH_EAST;
        }
        throw new IllegalArgumentException("can't determine relative position for args: base " + base + " target " + target);
    }
}
