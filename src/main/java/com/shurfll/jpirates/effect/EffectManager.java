package com.shurfll.jpirates.effect;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


import java.util.List;

public interface EffectManager {
    Flowable<Event> handleAction(Action action);

    void addEffect(String targetId, Effect effect);

    void removeEffect(String targetId, Effect effect);

    List<Effect> getEffects(String targetId);
}
