package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.GoldSpending;
import org.springframework.stereotype.Component;

@Component
public class GoldSpendingValueMapper extends AbstractValueMapper {
    public GoldSpendingValueMapper() {
        super(EntityAttributeType.GOLDSPENDING, GoldSpending.class, o -> gson.toJson(o), s -> gson.fromJson(s, GoldSpending.class));
    }
}
