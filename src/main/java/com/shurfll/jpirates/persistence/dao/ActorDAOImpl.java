package com.shurfll.jpirates.persistence.dao;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.ActorType;
import com.shurfll.jpirates.persistence.converter.ActorConverter;
import com.shurfll.jpirates.persistence.converter.value.LocationValueMapper;
import com.shurfll.jpirates.persistence.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ActorDAOImpl implements ActorDAO {

    private ActorRepository actorRepository;
    private ActorConverter actorConverter;
    private LocationValueMapper valueMapper;

    @Autowired
    public ActorDAOImpl(ActorRepository actorRepository, ActorConverter actorConverter, LocationValueMapper valueMapper) {
        this.actorRepository = actorRepository;
        this.actorConverter = actorConverter;
        this.valueMapper = valueMapper;
    }

    @Override
    public Optional<Actor> getByActorId(String actorId) {
        return actorRepository.findByActorId(actorId)
                              .map(actorConverter::convert);
    }

    @Override
    public Optional<Actor> getByOid(String oid) {
        return actorRepository.findById(oid)
                              .map(actorConverter::convert);
    }

    @Override
    public Actor add(Actor actor) {
        return actorConverter.convert(actorRepository.saveAndFlush(actorConverter.convert(actor)));
    }

    @Override
    public Optional<Actor> findActorByName(String name) {
        return actorRepository.findByName(name)
                .map(actorConverter::convert);
    }

    @Override
    public List<Actor> getAll() {
        return actorRepository.findAll().stream().map(actorConverter::convert).collect(Collectors.toList());
    }

    @Override
    public <T> List<T> getAllByType(ActorType type) {
        return actorRepository.findAllByType(type.name()).stream().map(actorConverter::convert).map(a -> (T) a).collect(Collectors.toList());
    }

    @Override
    public List<String> getActorsAtLocation(Location location) {
        return actorRepository.findAllAtLocation(valueMapper.serialize(location));
    }

    @Override
    public void delete(String oid) {
        actorRepository.deleteById(oid);
    }
}
