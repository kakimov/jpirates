package com.shurfll.jpirates.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "state")
public class StateEntity extends AttributeContainer{

    @Column(length = 32)
    private String ownerOid;


    public StateEntity() {
    }

    public StateEntity(String oid, String ownerOid, List<Attribute> attributes) {
        super(oid,attributes);
        this.ownerOid = ownerOid;
    }

    public String getOwnerOid() {
        return ownerOid;
    }

    public void setOwnerOid(String ownerOid) {
        this.ownerOid = ownerOid;
    }
}
