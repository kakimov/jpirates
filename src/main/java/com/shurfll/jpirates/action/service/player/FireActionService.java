package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ConfigurableActionService;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.FireAction;
import com.shurfll.jpirates.helper.BattleHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.xml.namespace.QName;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class FireActionService extends ConfigurableActionService<FireAction> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FireActionService.class);

    private final Map<String, Long> fireTimestamps = new HashMap<>();

    private PlayersManager playersManager;

    private StateManager stateManager;

    private BattleHelper battleHelper;

    private static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "FireActionService";

    private Long paramFireDelay = null;

    public static final QName PARAM_FIRE_DELAY = new QName(HANDLER_URI, "fireDelay");

    @Autowired
    public FireActionService(ConfigurationManager configurationManager, PlayersManager playersManager, StateManager stateManager , BattleHelper battleHelper) {
        super(configurationManager);
        this.playersManager = playersManager;
        this.stateManager = stateManager;
        this.battleHelper = battleHelper;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof FireAction;
    }

    @Override
    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_FIRE_DELAY, Long.class);
    }

    @Override
    protected void configure(Optional<Configuration> nullableConfig) {
        LOGGER.debug("Configuration update: {}", nullableConfig);
        paramFireDelay = ConfigurationHelper.getParamValue(nullableConfig, PARAM_FIRE_DELAY, Long.class, Commons.FIRE_DEFAULT_DELAY);
        LOGGER.info("Configuration updated. fireDelay={}", paramFireDelay);
    }

    @Override
    protected String getHandlerUri() {
        return HANDLER_URI;
    }

    @Override
    public Flowable<Event> doAction(FireAction action) {
        return fireInternalPrepareAndExecute(action);
    }

    private Flowable<Event> fireInternalPrepareAndExecute(FireAction action) {
        String actorId = action.getSourceActorId();
        String targetId = action.getTargetId();
        return Flowable.zip(playersManager.getPlayerFlowable(actorId, actorId), playersManager.getPlayerFlowable(actorId, targetId), (winner, loser) -> fireInternalExecute(action, winner, loser))
                .flatMap(events -> events);
    }

    private Flowable<Event> fireInternalExecute(FireAction action, Actor actor, Actor target) {
        return Flowable.combineLatest(stateManager.getStateByActorId(actor.getActorId())
                            , stateManager.getStateByActorId(target.getActorId())
                            , (s1, s2) -> fireInternalExecute(action,actor,target,s1,s2))
                            .flatMap(events -> Flowable.fromIterable(events));

    }

    private List<Event> fireInternalExecute(FireAction action, Actor actor, Actor target, State actorState, State targetState){
        List<Event> eventList = new ArrayList<>();
        if (!ActorHelper.isSameLocation(actorState, targetState)) {
            eventList.add(EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.BATTLE_PLAYER_OUT_OF_SIGHT, new Object[]{target.getName()}));
        } else if (!timeToFireEnough(fireTimestamps.getOrDefault(actor.getActorId(), 0L))) {
            eventList.add(EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.BATTLE_GUNS_NOT_READY, new Object[]{timeToFireLeft(fireTimestamps.getOrDefault(actor.getActorId(), 0L))}));
        } else {
            FireResult result = battleHelper.fire(actor.getActorId());

            fireTimestamps.put(actor.getActorId(), System.currentTimeMillis());
            eventList.add(EventFactory.damageDeliveredEvent(action, actor, result));
            eventList.add(EventFactory.damageReceivedEvent(action, actor.getActorId(), target.getActorId(), result));
            eventList.add(EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.DAMAGE_DELIVERED, result.getDamageMade()));
            eventList.add(EventFactory.infoEvent(action, target.getActorId(), LocalizationKeys.DAMAGE_RECEIVED, result.getDamageMade()));
        }
        return eventList;
    }

    private Long timeToFireLeft(Long beforeTime) {
        return TimeUnit.MILLISECONDS.toSeconds(paramFireDelay - timeDiff(System.currentTimeMillis(), beforeTime));
    }

    private boolean timeToFireEnough(Long beforeTime) {
        return timeDiff(System.currentTimeMillis(), beforeTime) >= paramFireDelay;
    }

    private Long timeDiff(Long currentTime, Long beforeTime) {
        return currentTime - beforeTime;
    }

    public BattleHelper getBattleHelper() {
        return battleHelper;
    }

    public void setBattleHelper(BattleHelper battleHelper) {
        this.battleHelper = battleHelper;
    }
}
