package com.shurfll.jpirates.event.action.admin;

import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.event.action.AdminAction;

import javax.xml.namespace.QName;
import java.util.Map;

public class ConfigureModuleAction extends AdminAction {
    private static final String QUALIFIER = "configureModuleAction";
    public static final String TYPE = ACTION_BASE_NAMESPACE + "/" + QUALIFIER;

    public static final QName CONFIGURATION = attribute(TYPE, "configuration");

    public ConfigureModuleAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, Configuration configuration) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, CONFIGURATION, configuration);
    }

    public ConfigureModuleAction(String oid, String causeOid, String sourceActorId, Configuration configuration) {
        this(oid, causeOid, sourceActorId, null, configuration);
    }

    public Configuration getConfiguration() {
        return getValue(CONFIGURATION, Configuration.class).get();
    }

}
