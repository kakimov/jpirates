package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.TreasureFoundEvent;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;

import javax.xml.namespace.QName;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

public class TreasureFoundEventHandlerTest {

    private static final String ACTOR_ID = "actor_id";
    private static final Location ACTOR_LOCATION = new Location(0L, 0L);

    private static final String COMPETITOR_1_ID = "comp_1";
    private static final String COMPETITOR_2_ID = "comp_2";

    private static final Actor competitor1 = new Player(COMPETITOR_1_ID, COMPETITOR_1_ID);
    private static final Actor competitor2 = new Player(COMPETITOR_2_ID, COMPETITOR_2_ID);

    private PlayersManager manager;

    private TreasureFoundEventHandler handler;

    private Reactor reactor;

    @Before
    public void setUp() throws Exception {
        reactor = mock(Reactor.class);
        manager = mock(PlayersManager.class);
        when(manager.getPlayerFlowable(eq(ACTOR_ID))).thenReturn(Flowable.just(new Player(ACTOR_ID, ACTOR_ID)));
        when(manager.getNeighboursAtLocation(eq(ACTOR_ID), eq(ACTOR_LOCATION))).thenReturn(Collections.emptyList());
        handler = new TreasureFoundEventHandler(manager, reactor);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test000_null_event() {
        handler.handle(null).subscribe();
    }

    @Test(expected = NoSuchElementException.class)
    public void test001_null_treasure() {
        TreasureFoundEvent event = event(null);
        handler.handle(event).subscribe();
    }

    @Test
    public void test002_empty_treasure() {
        TestSubscriber subscriber = TestSubscriber.create();
        TreasureFoundEvent event = event(new Treasure(null, Collections.EMPTY_MAP));
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, event1 -> TestEventHelper.infoEventMatched(event1, ACTOR_ID, LocalizationKeys.TREASURE_ITEM_OBTAINED));
        subscriber.assertNoErrors();
    }

    @Test
    public void test003_various_treasure_items() {
        TestSubscriber subscriber = TestSubscriber.create();
        Map<QName, EntityAttribute> treasureItems = createGoldItem(10L);
        treasureItems.putAll(createCustomItem(new QName("custom_ns", "custom"),"test"));
        treasureItems.putAll(createTreasureItems(ActorStateAttributes.ACTOR_EXPERIENCE, EntityAttributeType.LONG,Commons.DEFAULT_TREASURE_FOUND_EXPERIENCE));
        TreasureFoundEvent event = event(new Treasure(null, treasureItems));
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, ev -> TestEventHelper.stateChangeEventMatched( ev, ACTOR_ID, convertTreasureItemsToChanges(treasureItems)));
    }

    @Test(expected = NoSuchElementException.class)
    public void test004_null_location() {
        TreasureFoundEvent event = event(null);
        handler.handle(event).subscribe();
    }

    @Test
    public void test005_no_competitors_around() {
        TestSubscriber subscriber = TestSubscriber.create();
        Map<QName, EntityAttribute> treasureItems = createGoldItem(10L);
        treasureItems.putAll(createTreasureItems(ActorStateAttributes.ACTOR_EXPERIENCE,EntityAttributeType.LONG, Commons.DEFAULT_TREASURE_FOUND_EXPERIENCE));
        TreasureFoundEvent event = event(new Treasure(null, treasureItems));
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertValueCount(2);
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, ev -> TestEventHelper.stateChangeEventMatched(ev, ACTOR_ID, convertTreasureItemsToChanges(treasureItems)));
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, ev -> TestEventHelper.infoEventMatched(ev, ACTOR_ID, LocalizationKeys.TREASURE_ITEM_OBTAINED));
    }

    @Test
    public void test005_several_competitors_around() {
        List<Actor> competitors = new ArrayList<>();
        competitors.add(competitor1);
        competitors.add(competitor2);
        when(manager.getNeighboursAtLocation(eq(ACTOR_ID), eq(ACTOR_LOCATION))).thenReturn(competitors);
        TestSubscriber subscriber = TestSubscriber.create();
        Map<QName, EntityAttribute> treasureItems = createGoldItem(10L);
        TreasureFoundEvent event = event(new Treasure(null, treasureItems));
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, ev -> TestEventHelper.infoEventMatched(ev, COMPETITOR_1_ID, LocalizationKeys.SEARCH_ANOTHER_FOUND));
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, ev -> TestEventHelper.infoEventMatched(ev, COMPETITOR_2_ID, LocalizationKeys.SEARCH_ANOTHER_FOUND));
    }


    private Map<QName, EntityAttribute> createTreasureItems(QName name, EntityAttributeType type,Object value) {
        Map<QName, EntityAttribute> items = new HashMap<>();
        items.put(name, AttributeHelper.createAttribute(null,name,type,value));
        return items;
    }

    private Map<QName, EntityAttribute> createGoldItem(Object value) {
        return createTreasureItems(GoodsAttributes.GOLD,EntityAttributeType.LONG, value);
    }

    private Map<QName, EntityAttribute> createCustomItem(QName customName, Object value) {
        return createTreasureItems(customName, EntityAttributeType.STRING, value);
    }


    private TreasureFoundEvent event(Treasure treasure) {
        return event(ACTOR_LOCATION, treasure);
    }

    private TreasureFoundEvent event(Location location, Treasure treasure) {
        return event(ACTOR_ID, location, treasure);
    }

    private TreasureFoundEvent event(String actorId, Location location, Treasure treasure) {
        TreasureFoundEvent event = new TreasureFoundEvent(null, null, actorId, location, treasure);
        return event;
    }

    private Map<QName, Object> convertTreasureItemsToChanges(Map<QName,EntityAttribute> treasures){
        return treasures.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getValue().get().getValue()));
    }

}