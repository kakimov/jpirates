package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.QuestAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class QuestActionMapper extends AbstractEventMapper {

    public QuestActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return QuestAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return QuestAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.questAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, QuestAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, QuestAction.QUEST_ID));
    }


}
