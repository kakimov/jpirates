package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.apache.http.util.Asserts;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class SailActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/sail) (.+)";

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }



    @Override
    public Optional<Action> resolve(List<Message> messages) {
        Asserts.check(messages.size() == 1, "Inappropriate messages size: " + messages);
        Message message = messages.get(0);
        Direction direction = getDirection(message);
        return Optional.of(EventFactory.moveAction(null, String.valueOf(message.getChatId()), direction));
    }

    @Override
    public Optional<InfoEvent> validate(List<Message> messages) {
        if (messages.size() > 1)
            return Optional.of(EventFactory.infoEvent(null, String.valueOf(messages.get(0).getChatId()), LocalizationKeys.CUSTOM_MESSAGE, "Wrong arguments of sail action: " + messages.stream().map(message -> message.toString()).collect(Collectors.joining())));
        Message message = messages.get(0);
        try {
            getDirection(message);
            return Optional.empty();
        }catch (IllegalArgumentException e){
            return Optional.of(EventFactory.infoEvent(null, String.valueOf(messages.get(0).getChatId()), LocalizationKeys.CUSTOM_MESSAGE, "Invalid argument" +message.getText()));
        }
    }

    private Direction getDirection(Message message){
        Optional<String> directionStr = getDirectionStr(message.getText().orElseThrow(() -> new IllegalArgumentException("message.getText is empty")));
        return parseDirection(directionStr.orElseThrow(() -> new IllegalArgumentException("Can't determine direction from message: " + message.getText().get())));
    }

    private Optional<String> getDirectionStr(String text){
        Matcher matcher = COMMAND_PATTERN.matcher(text);
        return matcher.find() && matcher.groupCount() == 2 ? Optional.ofNullable(matcher.group(2)) : Optional.empty();
    }

    private Direction parseDirection(String direction) {
        return Direction.valueOf(direction.toUpperCase());
    }
}
