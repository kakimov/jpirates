package com.shurfll.jpirates.persistence.repository;

import com.shurfll.jpirates.persistence.StateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StateRepository extends JpaRepository<StateEntity,String>, JpaSpecificationExecutor<StateEntity> {
    @Query("select b from StateEntity b join b.attributes where b.ownerOid = :ownerOid")
    Optional<StateEntity> findByOwnerOid(@Param("ownerOid") String ownerOid);

}
