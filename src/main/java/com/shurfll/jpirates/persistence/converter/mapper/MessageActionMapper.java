package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.MessageAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class MessageActionMapper extends AbstractEventMapper {

    public MessageActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return MessageAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return MessageAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.messageAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, MessageAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, MessageAction.MESSAGE));
    }


}
