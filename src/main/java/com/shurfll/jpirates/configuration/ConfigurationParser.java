package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.Configuration;

public interface ConfigurationParser {
    public Configuration parseConfiguration(String value);
}
