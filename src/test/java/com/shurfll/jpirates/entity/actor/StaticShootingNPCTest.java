package com.shurfll.jpirates.entity.actor;

import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.event.*;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.FireAction;
import io.reactivex.observers.BaseTestConsumer;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Timed;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class StaticShootingNPCTest {

    private static final String NPC_ID = "npc_id";
    private static final String NPC_NAME = "npc_name";
    private static final String ANOTHER_NPC_ID = "another_npc_id";
    private static final String ANOTHER_NPC_NAME = "another_npc_name";
    private static final String PLAYER_ID = "player_id";
    private static final String PLAYER_NAME = "player_name";
    private static final String PLAYER2_ID = "player_id_2";
    private static final String PLAYER2_NAME = "player_name_2";

    private StaticShootingNPC npc;
    private NPC anotherNpc;
    private PublishProcessor<Event> events;
    private TestSubscriber<Action> subscriber;
    private Player player;
    private Player player2;

    @Before
    public void setup() {
        events = PublishProcessor.create();
        npc = new StaticShootingNPC(NPC_ID, NPC_NAME,  events, 100L,0L);
        anotherNpc = new NPC(ANOTHER_NPC_ID, ANOTHER_NPC_NAME);
        subscriber = TestSubscriber.create();
        npc.getActions().subscribe(subscriber);
        player = new Player(PLAYER_ID, PLAYER_NAME);
        player2 = new Player(PLAYER2_ID, PLAYER2_NAME);
    }


    @Test
    public void test001_noActionsWithNoMeetEvents() throws Exception {
        subscriber.assertNoValues();
        subscriber.assertNotComplete();
    }

    @Test
    public void test002_ignoreNonPlayers() throws Exception{
        events.onNext(createMeetEvent(anotherNpc));
        subscriber.await(100, TimeUnit.MILLISECONDS);
        subscriber.assertNoValues();
        subscriber.assertNoErrors();
        subscriber.assertNotComplete();
    }

    @Test
    public void test003_ignoreEventForDifferentTarget() throws Exception{
        events.onNext(createMeetEvent(anotherNpc, npc));
        subscriber.await(100, TimeUnit.MILLISECONDS);
        subscriber.assertNoValues();
        subscriber.assertNoErrors();
        subscriber.assertNotComplete();
    }

    @Test
    public void test004_handleBattleLoseEvent() {
        events.onNext(createBattleLoseEvent());
        subscriber.awaitTerminalEvent();
        subscriber.assertComplete();
    }

    @Test
    public void test005_ignoreBattleLoseEventForAnotherNPC() {
        events.onNext(createBattleLoseEvent(new NPC(ANOTHER_NPC_ID, ANOTHER_NPC_NAME)));
        subscriber.assertNoValues();
        subscriber.assertNotComplete();
    }

    @Test
    public void test006_shootAtMeetEvent() {
        events.onNext(createMeetEvent(npc,player));
        subscriber.awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS);
        events.onNext(createBattleLoseEvent());
        subscriber.awaitTerminalEvent();
        subscriber.assertValueCount(1);
        subscriber.assertValue(EventFactory.fireAction(null, NPC_ID, PLAYER_ID, PLAYER_NAME));
    }

    @Test
    public void test007_shootAtCorrectInterval() {
        TestSubscriber<Timed<Action>> timestampedSubscriber = TestSubscriber.create();
        npc.getActions().timestamp().subscribe(timestampedSubscriber);
        Long startTime = System.currentTimeMillis();
        events.onNext(createMeetEvent(player));
        subscriber.awaitCount(3, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 400);
        events.onNext(createBattleLoseEvent());
        subscriber.awaitTerminalEvent();
        subscriber.assertComplete();

//        timestampedSubscriber.values().stream()
//                .reduce(this::checkTimestamps).getByActorId();
    }

    @Test
    public void test008_changeTargetAtWin() {
        FireAction fire1 = EventFactory.fireAction(null, NPC_ID, player.getActorId(), player.getName());
        FireAction fire2 = EventFactory.fireAction(null, NPC_ID, player2.getActorId(), player2.getName());
        events.onNext(createMeetEvent(player));
        events.onNext(createMeetEvent(player2));
        subscriber.awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 1000);
        subscriber.assertValue(fire1);
        // npc should stop firing to first target and switch to the second
        events.onNext(createBattleWonEvent(player, 10L));
        subscriber.awaitCount(2, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 1000);
        subscriber.assertValues(fire1, fire2);

    }

    @Test
    public void test009_changeTargetAtPlayerLeave() {
        FireAction fire1 = EventFactory.fireAction(null, NPC_ID, player.getActorId(), player.getName());
        FireAction fire2 = EventFactory.fireAction(null, NPC_ID, player2.getActorId(), player2.getName());
        events.onNext(createMeetEvent(player));
        events.onNext(createMeetEvent(player2));
        subscriber.awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 100);
        subscriber.assertValue(fire1);
        // npc should stop firing to first target and switch to the second
        events.onNext(createSailAwayEvent(player));
        subscriber.awaitCount(2, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 1000);
        subscriber.assertValues(fire1, fire2);
    }

    @Test
    public void test010_keepOrderOfTargets() {
        FireAction fire1 = EventFactory.fireAction(null, NPC_ID, player.getActorId(), player.getName());
        FireAction fire2 = EventFactory.fireAction(null, NPC_ID, player2.getActorId(), player2.getName());
        events.onNext(createMeetEvent(player));
        events.onNext(createMeetEvent(player2));
        subscriber.awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 100);
        subscriber.assertValue(fire1);
        // npc should stop firing to first target and switch to the second
        events.onNext(createSailAwayEvent(player));
        subscriber.awaitCount(2,BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 1000);
        subscriber.assertValues(fire1, fire2);
        events.onNext(createMeetEvent(player));
        subscriber.awaitCount(3, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 1000);
        subscriber.assertValues(fire1, fire2, fire2);
    }

    @Test
    public void test011_stopAtBattleWonWithNoTargets() {
        FireAction fire1 = EventFactory.fireAction(null, NPC_ID, player.getActorId(), player.getName());
        events.onNext(createMeetEvent(player));
        subscriber.awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 100);
        subscriber.assertValue(fire1);
        // npc should stop firing to first target and switch to the second
        events.onNext(createBattleWonEvent(player, 10L));
        subscriber.awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 1000);
        subscriber.assertValueCount(1);
    }


    private Timed<Action> checkTimestamps(Timed<Action> a1, Timed<Action> a2) {
        assertTrue("Time of emitting should not be less then 100ms", a2.time() - a1.time() >= 900);
        return a2;
    }

    private MeetEvent createMeetEvent(Object neighbour) {
        return createMeetEvent(new Object[]{neighbour});
    }

    private MeetEvent createMeetEvent(Object[] neighbours) {
        return createMeetEvent(npc, neighbours);
    }

    private MeetEvent createMeetEvent(Actor target, Actor neighbour) {
        return createMeetEvent(target, new Object[]{neighbour});
    }

    private MeetEvent createMeetEvent(Actor target, Object[] neighbours) {
        return EventFactory.meetEvent(null, target, Arrays.asList(neighbours));
    }

    private SailAwayEvent createSailAwayEvent(Object neighbour) {
        return createSailAwayEvent(new Object[]{neighbour});
    }

    private SailAwayEvent createSailAwayEvent(Object[] neighbours) {
        return createSailAwayEvent(npc, neighbours);
    }

    private SailAwayEvent createSailAwayEvent(Actor target, Object[] neighbours) {
        return EventFactory.sailAwayEvent(null, target, Arrays.asList(neighbours), Direction.EAST);
    }

    private BattleWonEvent createBattleWonEvent(Actor loser, Long goldEarned) {
        return createBattleWonEvent(npc, loser, goldEarned);
    }

    private BattleWonEvent createBattleWonEvent(Actor winner, Actor loser, Long goldEarned) {
        return EventFactory.battleWonEvent(null, winner, loser, goldEarned);
    }

    private BattleLoseEvent createBattleLoseEvent() {
        return createBattleLoseEvent(npc);
    }

    private BattleLoseEvent createBattleLoseEvent(Actor loser) {
        return EventFactory.battleLoseEvent(null, loser);
    }

}