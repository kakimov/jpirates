package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.SpendGoldActionService;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;


import javax.xml.namespace.QName;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.shurfll.jpirates.Commons.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpendGoldActionServiceTest {

    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";

    private SpendGoldActionService service;
    private StateManager stateManager;
    private ObjectFactory objectFactory;
    private ConfigurationManager configurationManager;
    private TestSubscriber subscriber;

    @Before
    public void setUp() {
        objectFactory = new ObjectFactory(Collections.emptyList());
        subscriber = new TestSubscriber();
        stateManager = mock(StateManager.class);
        configurationManager = mock(ConfigurationManager.class);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.empty());
        when(configurationManager.configurationChange(anyString())).thenReturn(Flowable.empty());
        service = new SpendGoldActionService(configurationManager, stateManager);
        service.init();
        subscriber = TestSubscriber.create();
    }


    @Test
    public void test001_spendGold_heal_enough_money() {
        State state = createState(PLAYER_ID, loc(0L, 0L), 1L, DEFAULT_HEALTH, DEFAULT_REPAIR_UPGRADE_COST);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));
        service.doAction(EventFactory.spendGoldAction(null, PLAYER_ID, GoldSpending.HEAL)).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertValueCount(2);
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.SPEND_GOLD_FULL_REPAIR));
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, e -> TestEventHelper.stateChangeEventMatched(e, PLAYER_ID, stateChangeDiff(DEFAULT_REPAIR_UPGRADE_COST, ActorStateAttributes.ACTOR_HEALTH, DEFAULT_HEALTH - 1)));
    }

    @Test
    public void test002_spendGold_heal_not_enough_money() {
        Long playersGold = DEFAULT_REPAIR_UPGRADE_COST - 1;
        State actorState = createState(PLAYER_ID, loc(0L, 0L), 1L, DEFAULT_HEALTH, playersGold);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(actorState));
        service.doAction(EventFactory.spendGoldAction(null, PLAYER_ID, GoldSpending.HEAL)).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertValueCount(1);
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.SPEND_GOLD_NOT_ENOUGH_GOLD));
    }

    private State createState(String entityOid, Location location) {
        return objectFactory.createState(null,entityOid, location);
    }

    private State createState(String entityOid, Location location, Long health, Long totalHealth, Long gold) {
        State state = createState(entityOid, location);
        ActorHelper.setActorHeath(objectFactory, state, health);
        ActorHelper.setActorTotalHeath(objectFactory, state, totalHealth);
        ActorHelper.setActorGold(objectFactory, state, gold);
        return state;
    }

    private Map<QName, Object> stateChangeDiff(Long cost, QName name, Object value) {
        Map<QName, Object> items = new HashMap<>();
        items.put(GoodsAttributes.GOLD, -1 * cost);
        items.put(name, value);
        return items;
    }

}