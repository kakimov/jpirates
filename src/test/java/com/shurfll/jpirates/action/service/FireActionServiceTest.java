package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.FireActionService;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.*;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.helper.BattleHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import io.reactivex.Flowable;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;

import javax.xml.namespace.QName;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.shurfll.jpirates.Commons.loc;
import static org.mockito.Mockito.*;

/**
 * Created by kakim on 29.05.2017.
 */
public class FireActionServiceTest {

    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";

    private static final String STATIC_PLAYER_ID = "staticid";
    private static final String STATIC_PLAYER_NAME = "staticname";

    private ObjectFactory objectFactory;

    private FireActionService service;

    private PlayersManager playersManager;
    private StateManager stateManager;

    private TestSubscriber subscriber;
    private TestSubscriber staticSubscriber;
    private BattleHelper battleHelper;
    private ConfigurationManager configurationManager;

    private Long staticGold = 30L;

    @Before
    public void setUp() {
        objectFactory = new ObjectFactory(Collections.EMPTY_LIST);
        playersManager = mock(PlayersManager.class);
        stateManager = mock(StateManager.class);
        subscriber = new TestSubscriber();
        staticSubscriber = new TestSubscriber();
        battleHelper = mock(BattleHelper.class);
        configurationManager = mock(ConfigurationManager.class);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.empty());
        when(configurationManager.configurationChange(anyString())).thenReturn(Flowable.empty());

        service = new FireActionService(configurationManager, playersManager, stateManager, battleHelper);
        Player player = new Player(PLAYER_ID, PLAYER_NAME);
        Player staticPlayer = new Player(STATIC_PLAYER_ID, STATIC_PLAYER_NAME);
        when(playersManager.getPlayerFlowable(anyString(), eq(PLAYER_ID))).thenReturn(Flowable.just(player));
        when(playersManager.getPlayerFlowable(anyString(), eq(STATIC_PLAYER_ID))).thenReturn(Flowable.just(staticPlayer));

        Location sameLocation = loc(0L, 0L);
        State staticState = createState(STATIC_PLAYER_ID, sameLocation, 1L, 0L, 100000L, staticGold);
        State state = createState(PLAYER_ID, sameLocation, 100L, 100L, 10L);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));
        when(stateManager.getStateByActorId(eq(STATIC_PLAYER_ID))).thenReturn(Flowable.just(staticState));

        when(battleHelper.fire(eq(PLAYER_ID))).thenReturn(new FireResult(1L));
        service.init();
    }


    @Test
    public void test001_fireAction() {
        service.doAction(EventFactory.fireAction(null, PLAYER_ID, STATIC_PLAYER_ID, STATIC_PLAYER_NAME)).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, DamageDeliveredEvent.class, event -> damageDeliveredEventMatch((DamageDeliveredEvent) event, PLAYER_ID));
        TestEventHelper.checkEventProduced(subscriber, DamageReceivedEvent.class, event -> damageReceivedEventMatch((DamageReceivedEvent) event, STATIC_PLAYER_ID));

        subscriber.assertComplete();
    }


    @Test
    public void test002_fireAction_kill_other() {
        Map<QName, Object> loserChanges = new HashMap<>();
        loserChanges.put(GoodsAttributes.GOLD, -staticGold);
        loserChanges.put(ActorStateAttributes.ACTOR_HEALTH, -1L);

        Map<QName, Object> winnerChanges = new HashMap<>();
        winnerChanges.put(GoodsAttributes.GOLD, staticGold);
        winnerChanges.put(ActorStateAttributes.ACTOR_EXPERIENCE, Commons.DEFAULT_BATTLE_WON_EXPERIENCE);

        service.doAction(EventFactory.fireAction(null, PLAYER_ID, STATIC_PLAYER_ID, STATIC_PLAYER_NAME)).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        //winner events
        TestEventHelper.checkEventProduced(subscriber, DamageDeliveredEvent.class, event -> damageDeliveredEventMatch((DamageDeliveredEvent) event, PLAYER_ID));

        //loser events
        TestEventHelper.checkEventProduced(subscriber, DamageReceivedEvent.class, event -> damageReceivedEventMatch((DamageReceivedEvent) event, STATIC_PLAYER_ID));

        subscriber.awaitTerminalEvent();
        subscriber.assertComplete();
    }

    private boolean damageDeliveredEventMatch(DamageDeliveredEvent event, String targetId) {
        return targetId.equals(event.getTargetActorId());
    }

    private boolean damageReceivedEventMatch(DamageReceivedEvent event, String targetId) {
        return targetId.equals(event.getTargetActorId());
    }




    private State createState(String entityOid, Location location, Long healthLeft, Long healthRepairingAmount, Long healthRepairingSpeed) {
        State specialState = objectFactory.createState(null,entityOid, location);
        ActorHelper.setActorHeath(objectFactory, specialState, healthLeft);
        ActorHelper.setActorHealthRepairingAmount(objectFactory, specialState, healthRepairingAmount);
        ActorHelper.setActorHealthRepairingSpeed(objectFactory, specialState, healthRepairingSpeed);
        return specialState;
    }

    private State createState(String entityOid, Location location, Long healthLeft, Long healthRepairingAmount, Long healthRepairingSpeed, Long gold) {
        State state = createState(entityOid, location, healthLeft, healthRepairingAmount, healthRepairingSpeed);
        ActorHelper.setActorGold(objectFactory, state, gold);
        return state;
    }


}