package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import java.util.Map;


@Component
public class StateChangeEventHandler extends AbstractSelfregisteringEventHandler<StateChangeEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateChangeEventHandler.class);

    private ObjectFactory objectFactory;

    private StateManager stateManager;

    @Autowired
    public StateChangeEventHandler(Reactor reactor, StateManager stateManager, ObjectFactory objectFactory) {
        super(reactor);
        this.objectFactory = objectFactory;
        this.stateManager = stateManager;
    }

    @Override
    public Class getEventClass() {
        return StateChangeEvent.class;
    }

    @Override
    public Flowable<Event> handle(StateChangeEvent event) {
        return stateManager.getStateByActorId(event.getTargetActorId())
                .flatMap(state -> {
                            if (event.getChanges().isEmpty()) return Flowable.empty();
                            return Flowable.just(doChanges(state, event))
                                    .doOnError(t -> LOGGER.error("Unable to process event: {}, excaption: {}. The state will be rollbacked", event, t))
                                    .onErrorReturn(t -> state)
                                    .doOnNext(calculatedState -> LOGGER.debug("The new state calculated: {}", calculatedState))
                                    .map(calculatedState -> {
                                        stateManager.updateState(calculatedState);
                                        return EventFactory.stateChangedEvent(event, event.getTargetActorId());
                                    });
                        }
                );
    }

    private State doChanges(State state, StateChangeEvent event) {
        event.getChanges().entrySet().stream().forEach(entry -> doSingleChange(state, entry));
        return state;
    }

    private void doSingleChange(State state, Map.Entry<QName, StateChangeEvent.Change> entry) {
        try {
            objectFactory.updateEntityAttribute(state, entry.getKey(), entry.getValue().getMode(), entry.getValue().getValue());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
