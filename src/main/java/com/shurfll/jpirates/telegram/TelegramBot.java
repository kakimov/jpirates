package com.shurfll.jpirates.telegram;

import io.reactivex.Flowable;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public interface TelegramBot{

    public Flowable<Update> messagesFlowable();

    public Integer sendMessage(Long chatId, String message, ReplyKeyboard replyKeyboard) throws TelegramApiException;

    public void removeKeyboard(Long chatId, Integer messageId);

}
