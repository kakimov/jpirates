package com.shurfll.jpirates.effect;

import com.shurfll.jpirates.IDGenerator;
import com.shurfll.jpirates.event.Event;

import java.util.Optional;

public class EffectFactory {
    public static final AbsoluteActionBlockingEffect absoluteActionBlockingEffect(Event causeEvent) {
        return new AbsoluteActionBlockingEffect(generateID(), getCauseEventOid(causeEvent));
    }

    private static String generateID() {
        return IDGenerator.generateID();
    }

    private static String getCauseEventOid(Event event) {
        return Optional.ofNullable(event).map(event1 -> event1.getOid()).orElse(null);
    }
}
