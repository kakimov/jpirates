package com.shurfll.jpirates.persistence.converter;

import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.ActorType;
import com.shurfll.jpirates.entity.actor.NPC;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.persistence.ActorEntity;
import org.springframework.stereotype.Component;

@Component
public class ActorConverter {

    public Actor convert(ActorEntity entity){
        if(ActorType.PLAYER.equals(ActorType.valueOf(entity.getType()))) {
            return new Player(entity.getOid(), entity.getActorId(), entity.getName());
        }else if(ActorType.NPC.equals(ActorType.valueOf(entity.getType()))){
            return new NPC(entity.getOid(), entity.getActorId(), entity.getName());
        }
        throw new IllegalArgumentException("Unknown ActorEntity type: " + entity);
    }

    public ActorEntity convert(Actor actor){
        return new ActorEntity(actor.getOid(),actor.getActorId(),actor.getType().name(),actor.getName());
    }


}
