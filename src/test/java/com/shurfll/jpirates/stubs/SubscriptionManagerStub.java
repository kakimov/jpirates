package com.shurfll.jpirates.stubs;

import com.shurfll.jpirates.SubscriptionManager;
import com.shurfll.jpirates.event.PersonalEvent;

import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;


public class SubscriptionManagerStub implements SubscriptionManager {

    private PublishProcessor<PersonalEvent> publishProcessor = PublishProcessor.create();

    @Override
    public Flowable<PersonalEvent> personalEventsFlowable(String playerId) {
        return publishProcessor;
    }

    public void pushEvent(PersonalEvent event) {
        publishProcessor.onNext(event);
    }
}
