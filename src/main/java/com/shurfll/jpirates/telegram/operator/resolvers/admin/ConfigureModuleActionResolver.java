package com.shurfll.jpirates.telegram.operator.resolvers.admin;

import com.shurfll.jpirates.configuration.ConfigurationParser;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
@Qualifier("admin")
public class ConfigureModuleActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/config) (.+)";

    private ConfigurationParser parser;

    @Autowired
    public ConfigureModuleActionResolver(ConfigurationParser parser) {
        this.parser = parser;
    }

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        if (messages.size() > 1)
            throw new IllegalArgumentException("Wrong arguments of fire action: " + messages.stream().map(message -> message.toString()).collect(Collectors.joining()));
        Message message = messages.get(0);
        Matcher matcher = COMMAND_PATTERN.matcher(message.getText().get());
        if (matcher.find()) {
            String configuration = matcher.group(2);
            return Optional.of(EventFactory.configureModuleAction(null, String.valueOf(message.getChatId()), parser.parseConfiguration(configuration)));
        } else {
            return Optional.empty();
        }

    }
}
