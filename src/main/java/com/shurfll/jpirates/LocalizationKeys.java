package com.shurfll.jpirates;

public interface LocalizationKeys {
    String REPAIR_NO_NEED = "event.repair.no_need_in_repair";
    String REPAIR_COMPLETED = "event.repair.repair_completed";
    String REPAIR_STEP = "event.repair.step";
    String BATTLE_LOSE_AWAKE_EVENT="event.battlelose.awake";
    String BATTLE_PLAYER_OUT_OF_SIGHT="event.battle.player_out_of_sight";
    String BATTLE_GUNS_NOT_READY="event.battle.guns_not_ready";
    String SPEND_GOLD_NOT_ENOUGH_GOLD="event.spendgold.not_enough_gold";
    String SPEND_GOLD_FULL_REPAIR="event.spendgold.full_repair";
    String SPEND_GOLD_DONE="event.spendgold.done";

    String MOVE_EVENT_INTENT="event.move.intent";
    String START_INTRO="event.start.intro";
    String START_INTRO_COMMANDS="event.start.intro_commands";
    String SEARCH_STARTING="event.search.start_searching";
    String TREASURE_ITEM_OBTAINED="event.obtain_treasure_item";
    String SEARCH_NOTHING="event.search_nothing";
    String SEARCH_ANOTHER_FOUND="event.search.another_found";
    String SEARCH_HIDDEN="event.treasure_hidden";

    String ACTION_IS_BLOCKED="event.action_blocked";
    String BATTLE_LOSE="event.battle_lose";
    String BATTLE_WON="event.battle_won";
    String LOCATION_CHANGED_EVENT="event.location_changed_event";
    String DAMAGE_DELIVERED="event.battle.damage_delivered";
    String DAMAGE_RECEIVED="event.battle.damage_received";
    String DAMAGE_DELIVERED_ALREADT_DOWN = "event.battle.damage_delivered_already_down";
    String MEET="event.meet";
    String SAIL_AWAY="event.sail_away";

    String MESSAGE="event.message";

    String CUSTOM_MESSAGE="event.info.custom";

    String NAME_CONFLICK="start_action.name_conflict";
    String ACTOR_ID_CONFLICK="start_action.actor_id_conflick";


    String LEVEL_UP="event.level_up";
    String LEVEL_UP_DONE="event.level_up.done";
    String LEVEL_UP_NO_POINTS="event.level_up.no_points";

    String GAME_NEWS="event.game_news";

    String EXCEPTION_ACTION_INTERRUPTED="exception.action_interrupted";
    String EXCEPTION_ACTION_IS_UNSUPPORTED="exception.action_is_unsupported";
    String EXCEPTION_ACTION_IN_PROGRESS="exception.action_in_progress";

}
