package com.shurfll.jpirates.effect.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.effect.AbsoluteActionBlockingEffect;
import com.shurfll.jpirates.effect.Effect;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;
import org.springframework.stereotype.Component;


@Component
public class AbsoluteActionBlockingEffectHandler implements EffectHandler<AbsoluteActionBlockingEffect> {


    @Override
    public boolean canHandle(Effect effect) {
        return effect instanceof AbsoluteActionBlockingEffect;
    }

    @Override
    public Flowable<Event> handle(Action action, AbsoluteActionBlockingEffect effect) {
        return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.ACTION_IS_BLOCKED));
    }
}
