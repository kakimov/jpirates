package com.shurfll.jpirates.telegram.translator;

import com.shurfll.jpirates.telegram.keyboard.ButtonsKeyboard;
import com.shurfll.jpirates.telegram.keyboard.CallbackButtonsKeyboard;
import com.shurfll.jpirates.telegram.keyboard.Keyboard;
import com.shurfll.jpirates.telegram.keyboard.TextKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class KeyboardHelper {

    public static Optional<ReplyKeyboard> getKeyboard(Keyboard keyboard){
        if(keyboard instanceof TextKeyboard){
            return Optional.of(new ReplyKeyboardRemove());
        }else if(keyboard instanceof ButtonsKeyboard){
            ReplyKeyboardMarkup telegramKeyboard = new ReplyKeyboardMarkup();
            telegramKeyboard.setKeyboard(prepareRows((ButtonsKeyboard)keyboard));
            telegramKeyboard.setOneTimeKeyboard(true);
            telegramKeyboard.setResizeKeyboard(true);
            return Optional.of(telegramKeyboard);
        }else if(keyboard instanceof CallbackButtonsKeyboard){
            InlineKeyboardMarkup telegramKeyboard = new InlineKeyboardMarkup();
            telegramKeyboard.setKeyboard(prepareRows((CallbackButtonsKeyboard) keyboard));
            return Optional.of(telegramKeyboard);
        }
        return Optional.of(new ReplyKeyboardRemove());
    }

    private static List<KeyboardRow> prepareRows(ButtonsKeyboard keyboard){
        List<KeyboardRow> rows = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.addAll(keyboard.getButtons().stream().map(s -> new KeyboardButton(s)).collect(Collectors.toList()));
        rows.add(row);
        return rows;
    }

    private static List<List<InlineKeyboardButton>> prepareRows(CallbackButtonsKeyboard keyboard){
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        row.addAll(keyboard.getItems().stream().map(KeyboardHelper::convert).collect(Collectors.toList()));
        rows.add(row);
        return rows;
    }

    private static InlineKeyboardButton convert(CallbackButtonsKeyboard.CallbackButton button){
        InlineKeyboardButton keyboardButton = new InlineKeyboardButton(button.getText());
        keyboardButton.setCallbackData(button.getCommand());
        return keyboardButton;
    }
}
