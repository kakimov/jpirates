package com.shurfll.jpirates.entity;

public enum AttributeChangeMode {
    MODIFY, REPLACE
}
