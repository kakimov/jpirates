package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.action.Action;

import javax.xml.namespace.QName;
import java.util.Map;

public class TestAction extends Action {

    private TestEvent event;

    public TestAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, TestEvent event) {
        super(oid, causeOid, sourceActorId, meta);
        this.event = event;
    }

    public TestAction(String oid, String causeOid, String sourceActorId, TestEvent event) {
        super(oid, causeOid, sourceActorId);
        this.event = event;
    }

    public TestEvent getEvent() {
        return event;
    }

    public void setEvent(TestEvent event) {
        this.event = event;
    }
}