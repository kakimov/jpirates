package com.shurfll.jpirates.quest;

import com.shurfll.jpirates.entity.Quest;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.Optional;

public interface QuestTypeResolver {
    String COMMON_QUEST_TYPE_RESOLVER_NS = "http://jpirates.shurfll.com/quest/";

    QName getType();

    default boolean canResolve(Quest quest) {
        return Optional.ofNullable(quest)
                .map(q -> q.getType())
                .map(qName -> qName.equals(getType()))
                .orElse(false);
    }

    List<Quest> resolve(String actorId, String questId);
}
