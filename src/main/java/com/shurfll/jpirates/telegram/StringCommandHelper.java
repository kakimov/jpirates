package com.shurfll.jpirates.telegram;

import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.FireAction;
import com.shurfll.jpirates.event.action.player.MoveAction;
import org.telegram.telegrambots.api.objects.Message;

import java.util.Optional;

/**
 * Created by shurfll on 28.05.17.
 */
public class StringCommandHelper {
    public static Action parseAction(Message botMessage) throws UnsupportedOperationException {
        String message = botMessage.getText().toLowerCase();
        String[] args = message.split("\\s");
        if (message.startsWith("/sail")) {
            return parseMoveAction(getFromId(botMessage), args);
        } else if (message.startsWith("/start")) {
            return EventFactory.startAction(null, getFromId(botMessage), getFromName(botMessage), null);
        } else if (message.startsWith("/fire")) {
            return parseFireAction(botMessage.getFrom().getId().toString(), args);
        } else if (message.startsWith("/status")) {
            return EventFactory.statusAction(null, getFromId(botMessage));
        }
        throw new UnsupportedOperationException("Cannot determine command");

    }

    private static String getFromId(Message message) {
        return String.valueOf(message.getChatId());
    }

    private static String getFromName(Message message) {
        String username = message.getFrom().getUserName();
        return Optional.ofNullable(username).orElse(message.getFrom().getFirstName());
    }

    private static FireAction parseFireAction(String actorId, String[] args) {
        if (args.length == 2) {
            return EventFactory.fireAction(null, actorId, null, args[1]);
        }
        throw new IllegalArgumentException("Illegal format of /fire command. For example: /fire playerId");
    }

    private static MoveAction parseMoveAction(String actorId, String[] args) {
        if (args.length == 2) {
            return EventFactory.moveAction(null, actorId, Direction.valueOf(args[1]));
        }
        throw new IllegalArgumentException("Illegal format of /sail command. For example: /sail WEST 3");
    }

}
