package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;

import javax.annotation.PostConstruct;

public abstract class AbstractSelfregisteringEventHandler<T extends Event> implements EventHandler<T> {

    private Reactor reactor;

    public AbstractSelfregisteringEventHandler(Reactor reactor) {
        this.reactor = reactor;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(getEventClass(), this);
    }

    public abstract Class getEventClass();

    protected Reactor getReactor() {
        return reactor;
    }
}
