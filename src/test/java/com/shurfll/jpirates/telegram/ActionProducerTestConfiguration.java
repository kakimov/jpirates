package com.shurfll.jpirates.telegram;

import com.shurfll.jpirates.SubscriptionManager;
import com.shurfll.jpirates.action.ActionExecutor;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.stubs.SubscriptionManagerStub;
import com.shurfll.jpirates.stubs.TelegramBotStub;
import io.reactivex.Flowable;
import org.hamcrest.Matchers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

/**
 * Created by kakim on 25.10.2017.
 */
@Profile("action_producer_test")
@Configuration
public class ActionProducerTestConfiguration {

    @Bean
    @Qualifier("player")
    @Primary
    public TelegramBot playerBot() {
        return new TelegramBotStub();
    }

    @Bean
    @Qualifier("admin")
    @Primary
    public TelegramBot adminBot() {
        return new TelegramBotStub();
    }

    @Bean
    @Primary
    public SubscriptionManager subscriptionManager() {
        return new SubscriptionManagerStub();
    }

    @Bean
    @Primary
    public ActionExecutor actionExecutor() {
        ActionExecutor executor =  Mockito.mock(ActionExecutor.class);
        Mockito.when(executor.execute(ArgumentMatchers.any(Action.class))).thenReturn(Flowable.empty());
        return executor;
    }

}
