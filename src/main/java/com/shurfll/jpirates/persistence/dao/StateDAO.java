package com.shurfll.jpirates.persistence.dao;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;

import java.util.List;

public interface StateDAO {
    State getState(String entityOid);

    State getStateByOwnerOid(String actorId);

    State updateState(State state);

    List<State> getActorsAtLocation(Location location);
}
