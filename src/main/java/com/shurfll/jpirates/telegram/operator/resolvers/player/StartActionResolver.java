package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class StartActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/start)";

    private PlayersManager playersManager;

    @Autowired
    public StartActionResolver(PlayersManager playersManager) {
        this.playersManager = playersManager;
    }

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public boolean canResolve(List<Message> messages) {
        boolean result = super.canResolve(messages);
        return result && messages.size() > 1;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        Message message1 = messages.get(0);
        Message message2 = messages.get(1);
        return Optional.of(EventFactory.startAction(null, String.valueOf(message1.getChatId()), message2.getText().get(), null));
    }

    @Override
    public Optional<InfoEvent> makeResponse(List<Message> messages) {
        if (messages.size() == 1) {
            return Optional.of(EventFactory.infoEvent(null, String.valueOf(messages.get(0).getChatId()), LocalizationKeys.START_INTRO));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<InfoEvent> validate(List<Message> messages) {
        String actorId = String.valueOf(messages.get(0).getChatId());
        return playersManager.getPlayerFlowable(actorId)
                .map(actor -> Optional.of(EventFactory.infoEvent(null,actorId, LocalizationKeys.ACTOR_ID_CONFLICK, actor.getName())))
                .onErrorReturn(t -> Optional.empty())
                .blockingFirst();
    }
}
