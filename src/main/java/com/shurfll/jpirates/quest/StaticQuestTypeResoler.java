package com.shurfll.jpirates.quest;

import com.shurfll.jpirates.entity.Quest;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StaticQuestTypeResoler implements QuestTypeResolver {

    public static final QName TYPE = new QName(COMMON_QUEST_TYPE_RESOLVER_NS,"static");

    @Override
    public QName getType() {
        return TYPE;
    }

    @Override
    public List<Quest> resolve(String actorId, String questId) {
        return QuestManager.QUESTS.stream()
                .filter(quest -> questId == null ? quest.getParentQuestId() == null : questId.equalsIgnoreCase(quest.getParentQuestId()))
                .collect(Collectors.toList());
    }
}
