package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.system.ActorStartedEvent;
import com.shurfll.jpirates.event.system.EmergeEvent;
import com.shurfll.jpirates.reactor.Reactor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import io.reactivex.subscribers.TestSubscriber;

public class ActorStartedEventHandlerTest {

    private ActorStartedEventHandler handler;
    private static final String PLAYER_ID="id";
    private static final String PLAYER_NAME="name";
    private static final Player PLAYER = new Player(PLAYER_ID,PLAYER_NAME);
    private static final Location LOCATION = new Location(-1l,3l);
    private TestSubscriber<Event> subscriber;

    @Before
    public void init(){
        handler = new ActorStartedEventHandler(Mockito.mock(Reactor.class));
        subscriber = TestSubscriber.create();
    }

    @Test
    public void test001() {
        ActorStartedEvent event = EventFactory.actorStartedEvent(null, PLAYER, LOCATION);
        handler.handle(event).subscribe(subscriber);
        TestEventHelper.checkEventProduced(subscriber,InfoEvent.class, e -> TestEventHelper.infoEventMatched(e,PLAYER_ID,LocalizationKeys.START_INTRO_COMMANDS));
        TestEventHelper.checkEventProduced(subscriber,InfoEvent.class, e -> TestEventHelper.infoEventMatched(e,PLAYER_ID,LocalizationKeys.LOCATION_CHANGED_EVENT));
        TestEventHelper.checkEventProduced(subscriber,EmergeEvent.class, e -> TestEventHelper.emergeEventMatched(e,PLAYER_ID,LOCATION));

    }
}