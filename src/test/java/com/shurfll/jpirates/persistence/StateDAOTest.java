package com.shurfll.jpirates.persistence;

import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.persistence.converter.EntityHelper;
import com.shurfll.jpirates.persistence.dao.StateDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.namespace.QName;
import java.util.Arrays;

import static com.shurfll.jpirates.persistence.converter.EntityHelper.*;


@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ComponentScan("com.shurfll.jpirates.persistence")
public class StateDAOTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private StateDAO dao;

    private ObjectFactory objectFactory;

    @Before
    public void init() {
        objectFactory = new ObjectFactory();
    }

    @Test
    public void test001_saveState() {

        State state  = state(entAttr(ActorStateAttributes.ACTOR_HEALTH, null,10L));
        String id = dao.updateState(state).getOid();
        StateEntity persistedState = entityManager.find(StateEntity.class, id);
        stateAttrCheck(persistedState, entry(ActorStateAttributes.ACTOR_HEALTH, "10"));
    }

    @Test
    public void test002_updateState() {
        //save
        StateEntity stateEntity = EntityHelper.state(null,null, attr(ActorStateAttributes.ACTOR_HEALTH, EntityAttributeType.LONG, "10"));
        stateEntity = entityManager.persist(stateEntity);
        //modify
        State stateToUpdate = dao.getState(stateEntity.getOid());
        stateToUpdate.getAttributes().get(ActorStateAttributes.ACTOR_HEALTH).getValue().get().setValue("11");
        dao.updateState(stateToUpdate);
        //getByActorId & check
        StateEntity persistedState = entityManager.find(StateEntity.class, stateEntity.getOid());
        stateEntityAttrCheck(persistedState, entry(ActorStateAttributes.ACTOR_HEALTH, "11"));
    }

    private EntityAttribute entAttr(QName name,String stateOid, Object value) {
        return objectFactory.createAttribute(stateOid, name, value);
    }

    private State state( EntityAttribute... attrs) {
        return objectFactory.createState(null,null,Arrays.asList(attrs));
    }

}
