package com.shurfll.jpirates.action;

import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;

import java.util.concurrent.TimeUnit;


public class TestLongActionService implements ActionService<TestLongAction> {
    @Override
    public boolean canHandle(Action a) {
        return a instanceof TestLongAction;
    }

    @Override
    public Flowable<Event> doAction(TestLongAction testAction) {
        return testAction.getEvent() != null ? Flowable.timer(testAction.getTimeout(), TimeUnit.MILLISECONDS).map(t -> testAction.getEvent()) : Flowable.empty();
    }
}