package com.shurfll.jpirates.persistence.converter.attribute;

import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.AttributeContainer;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;

public class SimpleTypedValueConverter extends AbstractAttributeValueConverter {

    private ValueConverter valueConverter;

    public SimpleTypedValueConverter(String type, ValueConverter converter) {
        super(type);
        this.valueConverter=converter;
        setSerializer(this::serializeAttribute);
        setDeserializer(this::deserializeAttribute);
    }

    private EntityAttribute deserializeAttribute(Attribute attr) {
        EntityAttributeType type = EntityAttributeType.valueOf(attr.getType());
        return deserializeAttribute(attr,type, a -> valueConverter.deserialize(type, attr.getValue()));
    }



    private Attribute serializeAttribute(AttributeContainer owner,EntityAttribute attr) {
        return serializeAttribute(owner, attr,  val -> valueConverter.serialize(val.getValue()));
    }



}
