package com.shurfll.jpirates.entity;

/**
 * Created by kakim on 04.06.2017.
 */
public class FireResult {
    private Long damageMade;

    public FireResult(Long damageMade) {
        this.damageMade = damageMade;
    }

    public Long getDamageMade() {
        return damageMade;
    }

    public void setDamageMade(Long damageMade) {
        this.damageMade = damageMade;
    }

    @Override
    public String toString() {
        return "FireResult{" +
                "damageMade=" + damageMade +
                '}';
    }
}
