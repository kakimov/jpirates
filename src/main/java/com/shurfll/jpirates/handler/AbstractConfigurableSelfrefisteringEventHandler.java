package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.reactor.Reactor;

import javax.annotation.PostConstruct;
import java.util.Optional;

public abstract class AbstractConfigurableSelfrefisteringEventHandler<T extends Event> extends AbstractSelfregisteringEventHandler<T> {

    private ConfigurationManager configurationManager;

    public AbstractConfigurableSelfrefisteringEventHandler(Reactor reactor, ConfigurationManager configurationManager) {
        super(reactor);
        this.configurationManager = configurationManager;
    }

    @PostConstruct
    public void init() {
        super.init();
        configure(configurationManager.getConfiguration(getHandlerUri()));
        configurationManager.configurationChange(getHandlerUri())
                .map(Optional::ofNullable)
                .filter(this::validateConfiguration)
                .subscribe(this::configure);
    }

    protected abstract boolean validateConfiguration(Optional<Configuration> nullableConfig);

    protected abstract void configure(Optional<Configuration> config);

    protected abstract String getHandlerUri();


}
