package com.shurfll.jpirates.event.system;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.event.LocationChangeEvent;
import com.shurfll.jpirates.event.PersonalEvent;

import javax.xml.namespace.QName;

/**
 * The difference with {@link LocationChangeEvent} is that {@linkplain EmergeEvent} doesn't have a before location, or direction
 * An actor emerges in location
 * Created by shurfll on 25.05.17.
 */
public class EmergeEvent extends PersonalEvent {

    private static final String QUALIFIER = "emergeEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName LOCATION = attribute(TYPE, "location");

    public EmergeEvent(String oid, String causeOid, String targetActorId, Location location) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, LOCATION, location);
    }

    public Location getLocation() {
        return getValue(LOCATION, Location.class).get();
    }

}
