package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import org.springframework.stereotype.Component;

@Component
public class StringValueMapper extends AbstractValueMapper {
    public StringValueMapper() {
        super(EntityAttributeType.STRING, String.class, Object::toString, s -> gson.fromJson(s, String.class));
    }
}
