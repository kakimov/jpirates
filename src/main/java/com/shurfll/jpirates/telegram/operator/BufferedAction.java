package com.shurfll.jpirates.telegram.operator;

import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.TelegramManager;
import com.shurfll.jpirates.telegram.operator.resolvers.ActionResolver;
import io.reactivex.FlowableOperator;
import io.reactivex.FlowableSubscriber;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.util.comparator.Comparators;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by k.akimov on 09.06.2017.
 */
public class BufferedAction implements FlowableOperator<Action, Message> {

    private ActionFactory actionFactory;
    private TelegramManager telegramManager;

    public BufferedAction(ActionFactory actionFactory, TelegramManager telegramManager) {
        this.actionFactory = actionFactory;
        this.telegramManager = telegramManager;
    }

    @Override
    public Subscriber<? super Message> apply(Subscriber<? super Action> subscriber) throws Exception {
        Subscriber s = new BufferWhileSubscriber(subscriber);
        return s;
    }


    final class BufferWhileSubscriber implements FlowableSubscriber<Message>, Subscription {

        private final Subscriber<? super Action> child;
        private Subscription s;


        private Map<String, List<Message>> buffers = new HashMap<>();

        /**
         * @param child
         */
        public BufferWhileSubscriber(Subscriber<? super Action> child) {
            this.child = child;
        }

        @Override
        public void onNext(Message t) {
            List<Message> buffer = populateAndPrepareContainer(t);
            Optional<ActionResolver> resolver = actionFactory.findResolver(buffer);

            resolver.ifPresent(r -> resolveMessages(r
                    , buffer
                    , (action, event) -> {
                        action.ifPresent(child::onNext);
                        event.ifPresent(telegramManager::sendEvent);
                        flushContainer(t);
                    }
                    , (event) -> event.ifPresent(telegramManager::sendEvent)
                    , (event) -> {
                        event.ifPresent(telegramManager::sendEvent);
                        flushContainer(t);
                    }));
            if (!resolver.isPresent()) {
                buffer.clear();
            }

        }

        private List<Message> populateAndPrepareContainer(Message m) {
            List<Message> buffer = buffers.get(String.valueOf(m.getChatId()));
            if (buffer == null) {
                buffer = new ArrayList<>();
                buffers.put(String.valueOf(m.getChatId()), buffer);
            }

            buffer.add(m);

            if (shouldFlushBuffer(buffer)) {
                buffer.clear();
                buffer.add(m);
            }

            return buffer;
        }

        private boolean shouldFlushBuffer(List<Message> messages) {
            Message lastMessage = messages.get(messages.size() - 1);
            List<ActionResolver> tailResolvers = actionFactory.findResolvers(Collections.singletonList(lastMessage));
            List<ActionResolver> headResolvers = actionFactory.findResolvers(messages);
            Optional<ActionResolver> head = headResolvers.stream().sorted(Comparator.comparing(ActionResolver::getPriority)).findFirst();
            Optional<ActionResolver> tail = tailResolvers.stream().sorted(Comparator.comparing(ActionResolver::getPriority)).findFirst();
            if (!head.isPresent()) {
                // в случае, если мы не можем собрать команду среди накопленных сообщений
                return true;
            }
            if (!tail.isPresent()) {
                // в случае, если новое сообщение не может быть рассмотренно как команда
                return false;
            }
            if (tail.get().getPriority() > head.get().getPriority()) {
                // если приоритет у резолвера нового сообщения ниже, чем приоритет головного - должны дать работать головному резолверу
                return false;
            }
            if (tail.get().getPriority() <= head.get().getPriority()) {
                // если приоритет у резолвера нового сообщения выше или равен, чем приоритет головного - должны сбрасывать и начинать накапливать новую команду
                return true;
            }

            throw new IllegalArgumentException("Невозможно вычислить ActionResolver для команды: " + messages.stream().map(Message::getText).map(o -> o.orElse(null)).collect(Collectors.joining(",")));
        }

        private void resolveMessages(ActionResolver resolver, List<Message> buffer, BiConsumer<Optional<Action>, Optional<InfoEvent>> doOnResolve, Consumer<Optional<InfoEvent>> doIfAcceptable, Consumer<Optional<InfoEvent>> doIfUnacceptable) {
            Optional<InfoEvent> validationResult = resolver.validate(buffer);
            if (validationResult.isPresent()) {
                doIfUnacceptable.accept(validationResult);
            } else if (resolver.canResolve(buffer)) {
                doOnResolve.accept(resolver.resolve(buffer), resolver.makeResponse(buffer));
            } else {
                doIfAcceptable.accept(resolver.makeResponse(buffer));
            }

        }

        private void flushContainer(Message m) {
            buffers.put(String.valueOf(m.getChatId()), null);
        }

        @Override
        public void onError(Throwable e) {
            child.onError(e);
        }

        @Override
        public void onComplete() {
            child.onComplete();
        }

        @Override
        public void onSubscribe(Subscription s) {
            this.s = s;
            child.onSubscribe(this);
        }

        @Override
        public void request(long l) {
            s.request(l);
        }

        @Override
        public void cancel() {
            s.cancel();
        }
    }
}
