package com.shurfll.jpirates.telegram;

import com.shurfll.jpirates.entity.actor.ActorType;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import com.shurfll.jpirates.quest.QuestManager;
import com.shurfll.jpirates.telegram.event.QuestEvent;
import com.shurfll.jpirates.telegram.keyboard.ButtonsKeyboard;
import com.shurfll.jpirates.telegram.operator.ActionFactoryImpl;
import com.shurfll.jpirates.telegram.operator.BufferedAction;
import com.shurfll.jpirates.telegram.operator.resolvers.ActionResolver;
import com.shurfll.jpirates.telegram.translator.EventTranslatorManager;
import com.shurfll.jpirates.telegram.translator.KeyboardHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by kakim on 31.05.2017.
 */
@Component
public class TelegramManagerImpl implements TelegramManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramManagerImpl.class);

    @Autowired
    private EventTranslatorManager eventTranslatorManager;

    @Autowired
    @Qualifier("player")
    private List<ActionResolver> playerActionResolvers;

    @Autowired(required = false)
    @Qualifier("admin")
    private List<ActionResolver> adminActionResolvers;

    @Autowired
    @Qualifier("player")
    private TelegramBot playerBot;

    @Autowired
    @Qualifier("admin")
    private TelegramBot adminBot;

    @Autowired
    private PlayersManagerImpl playersManager;

    @Autowired
    private ActionProducer actionProducer;

    @Autowired
    private QuestManager questManager;

    private Set<String> telegramChats = new HashSet<>();

    private Map<Long, String> activeMenu = new HashMap<>();

    @Override
    public void sendEvent(InfoEvent event) {
        try {
            if (playersManager.getPlayerByActorId(event.getTargetActorId()).map(actor -> ActorType.PLAYER.equals(actor.getType())).orElse(false)) {
                LOGGER.debug("Send message to player: {}", event);
                sendMessage(Long.valueOf(event.getTargetActorId()), event);
            }
        }catch (Exception e){
            LOGGER.error("Unable to send event: {}", event, e);
        }
    }

    @PostConstruct
    public void init() {
        playerBot.messagesFlowable()
                .doOnNext(this::registerTelegramChat)
                .map(message -> new Message(message))
                .doOnNext(message -> LOGGER.debug("Message caught: {}", message))
                .lift(new BufferedAction(new ActionFactoryImpl(playerActionResolvers), this))
                .subscribe(actionProducer::submitAction, t -> LOGGER.error("Unexpected error",t));

        adminBot.messagesFlowable()
                .doOnNext(this::registerTelegramChat)
                .map(message -> new Message(message))
                .doOnNext(message -> LOGGER.debug("Message caught: {}", message))
                .lift(new BufferedAction(new ActionFactoryImpl(adminActionResolvers), this))
                .subscribe(actionProducer::submitAction, t -> LOGGER.error("Unexpected error",t));
    }

    private void registerTelegramChat(Update update) {
        if (update.hasMessage()) {
            telegramChats.add(String.valueOf(update.getMessage().getChatId()));
        }
    }

    private void sendMessage(Long chatId, Event event) throws TelegramApiException {
        Optional<ButtonsKeyboard> keyboard = null;
        if (event instanceof QuestEvent) {
            String questId = ((QuestEvent) event).getQuestId();
            keyboard = questManager.getMenu(((QuestEvent) event).getTargetActorId(), questId)
                    .map(menu -> QuestManager.createButtons(menu.getQuests()));
            activeMenu.put(chatId, questId);
        } else {
            keyboard = questManager.getMenu(String.valueOf(chatId), activeMenu.get(chatId))
                    .map(menu -> QuestManager.createButtons(menu.getQuests()));
        }
        Integer messageId = playerBot.sendMessage(chatId, eventTranslatorManager.translate(event), keyboard.flatMap(KeyboardHelper::getKeyboard).orElse(null));
    }


    private void handleKeyboards(Long chatId, Integer mesId) {
        playerBot.removeKeyboard(chatId, mesId);
    }

    @Override
    public void setPlayerBot(TelegramBot playerBot) {
        this.playerBot = playerBot;
    }
}
