package com.shurfll.jpirates;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.MeetEvent;
import com.shurfll.jpirates.event.system.EmergeEvent;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import io.reactivex.subscribers.TestSubscriber;


import javax.xml.namespace.QName;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Created by kakim on 10.10.2017.
 */
public class TestEventHelper {
    public static <T extends Event> String dumpEvents(List<T> events) {
        return events.stream().map(Event::toString).collect(Collectors.joining(","));
    }

    public static <T extends Event, U extends T>  void checkEventProduced(TestSubscriber<T> subscriber, Class<U> clazz, Function<U, Boolean> matcher) {
        checkEventProduced(subscriber.values(),clazz, matcher);
    }

    public static <T extends Event, U extends T> void checkEventProduced(List<T> values, Class<U> clazz, Function<U, Boolean> matcher) {
        List<Event> events = values.stream()
                .filter(clazz::isInstance)
                .map(clazz::cast)
                .filter(matcher::apply)
                .collect(Collectors.toList());
        assertFalse(events.size() < 1, "Cannot find event of type: " + clazz + " current events: " + dumpEvents(values));
        assertFalse(events.size() > 1, "Too many events of type: " + clazz + " events: " + dumpEvents(values));
    }

    public static void checkEventsProduced(TestSubscriber<Event> subscriber, Class<? extends Event> clazz, Function<Event, Boolean> matcher, Long eventCount) {
        List<Event> events = subscriber.values().stream()
                .filter(clazz::isInstance)
                .map(clazz::cast)
                .filter(matcher::apply)
                .collect(Collectors.toList());
        assertFalse(events.size() < eventCount, "Cannot find proper amount '" + eventCount + "' event of type: " + clazz + " current events: " + dumpEvents(subscriber.values()));
    }

    public static boolean infoEventMatched(Event event, String actorId, String messageKey) {
        return event instanceof InfoEvent && actorId.equals(((InfoEvent) event).getTargetActorId()) && messageKey.equals(((InfoEvent) event).getMessage());
    }

    public static boolean infoEventMatched(Event event, String actorId, String messageKey, Object... args) {
        return infoEventMatched(event, actorId, messageKey) && Arrays.equals(args, ((InfoEvent) event).getArgs());
    }

    public static boolean emergeEventMatched(Event event, String actorId, Location location) {
        return event instanceof EmergeEvent && actorId.equals(((EmergeEvent) event).getTargetActorId()) && location.equals(((EmergeEvent) event).getLocation());
    }

    public static boolean meetEventMatched(Event event, String actorId, List<Actor> neighbours) {
        return event instanceof MeetEvent && actorId.equals(((MeetEvent) event).getTargetActorId())
                && neighbours.containsAll(((MeetEvent) event).getObject())
                && ((MeetEvent) event).getObject().containsAll(neighbours);
    }

    public static boolean stateChangeEventMatched(Event event, String actorId, Map<QName, Object> changes) {
        return event instanceof StateChangeEvent && actorId.equals(actorId) && changesMatch((StateChangeEvent) event, changes);
    }

    public static boolean stateChangeEventMatched(Event event, String actorId, QName name, Object value) {
        return event instanceof StateChangeEvent && actorId.equals(actorId) && ((StateChangeEvent) event).getChanges().size() == 1 && changeMatch(((StateChangeEvent) event).getChanges().entrySet().iterator().next(), name, value);
    }

    private static boolean changesMatch(StateChangeEvent event, Map<QName, Object> changes) {
        return changes.entrySet().stream().allMatch(treasureChange -> event.getChanges().entrySet().stream().anyMatch(change -> changeMatch(change, treasureChange)))
                && event.getChanges().entrySet().stream().allMatch(change -> changes.entrySet().stream().anyMatch(treasureChange -> changeMatch(change, treasureChange)));
    }

    private static boolean changeMatch(Map.Entry<QName, StateChangeEvent.Change> eventChange, Map.Entry<QName, Object> treasureChange) {
        return eventChange.getKey().equals(treasureChange.getKey()) && eventChange.getValue().getValue().equals(treasureChange.getValue());
    }

    private static boolean changeMatch(Map.Entry<QName, StateChangeEvent.Change> eventChange, QName name, Object value) {
        return eventChange.getKey().equals(name) && eventChange.getValue().getValue().equals(value);
    }
}
