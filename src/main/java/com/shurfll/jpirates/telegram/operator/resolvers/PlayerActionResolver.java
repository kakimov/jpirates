package com.shurfll.jpirates.telegram.operator.resolvers;

import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("player")
public abstract class PlayerActionResolver extends ActionResolver {
}
