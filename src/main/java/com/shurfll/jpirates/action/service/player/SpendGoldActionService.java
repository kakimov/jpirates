package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ConfigurableActionService;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.GoldSpending;
import com.shurfll.jpirates.entity.GoodsAttributes;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.SpendGoldAction;
import com.shurfll.jpirates.manager.StateManager;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class SpendGoldActionService extends ConfigurableActionService<SpendGoldAction> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpendGoldActionService.class);

    private static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "SpendGoldActionService";

    private StateManager stateManager;

    public static final QName PARAM_HEAL_COST = new QName(HANDLER_URI, "healCost");
    private Long paramHealCost = null;
    public static final QName PARAM_FIREPOWER = new QName(HANDLER_URI, "firepowerCost");
    private Long paramFirepower = null;

    public SpendGoldActionService(ConfigurationManager configurationManager, StateManager stateManager) {
        super(configurationManager);
        this.stateManager = stateManager;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof SpendGoldAction;
    }

    @Override
    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_HEAL_COST, Long.class)
                && ConfigurationHelper.hasParam(nullableConfig, PARAM_FIREPOWER, Long.class);
    }

    @Override
    protected void configure(Optional<Configuration> nullableConfig) {
        LOGGER.debug("Configuration update: {}", nullableConfig);
        paramHealCost = ConfigurationHelper.getParamValue(nullableConfig, PARAM_HEAL_COST, Long.class, Commons.DEFAULT_REPAIR_UPGRADE_COST);
        paramFirepower = ConfigurationHelper.getParamValue(nullableConfig, PARAM_FIREPOWER, Long.class, Commons.DEFAULT_FIRE_POWER_UPGRADE_COST);
        LOGGER.info("Configuration updated. paramHealCost={}, paramFirepower={}", paramHealCost, paramFirepower);
    }

    @Override
    protected String getHandlerUri() {
        return HANDLER_URI;
    }


    @Override
    public Flowable<Event> doAction(SpendGoldAction action) {
        return spendGoldInternal(action);
    }

    private Flowable<Event> spendGoldInternal(SpendGoldAction action) {
        switch (action.getGoldSpending()) {
            case HEAL: {
                return repairHealth(action);
            }
            case FIRE_POWER: {
                return firePower(action);
            }
            default: {
                return Flowable.error(new IllegalArgumentException("Unknown GoldSpending type: " + action.getGoldSpending()));
            }
        }
    }

    private Flowable<Event> repairHealth(SpendGoldAction action) {
        return stateManager.getStateByActorId(action.getSourceActorId())
                .flatMap(actorState -> {
                    Long actorGold = ActorHelper.getActorGold(actorState);
                    if (actorGold < paramHealCost) {
                        return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.SPEND_GOLD_NOT_ENOUGH_GOLD));
                    } else {
                        Map<QName, Object> items = new HashMap<>();
                        items.put(GoodsAttributes.GOLD, paramHealCost * -1);
                        items.put(ActorStateAttributes.ACTOR_HEALTH, ActorHelper.getActorTotalHealth(actorState) - ActorHelper.getActorHealth(actorState));
                        Event stateChangeEvent = EventFactory.stateChangeEventReplace(action, action.getSourceActorId(), items);
                        Event infoEvent = EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.SPEND_GOLD_FULL_REPAIR, new Object[]{paramHealCost});
                        return Flowable.just(stateChangeEvent, infoEvent);
                    }
                });
    }

    private Flowable<Event> firePower(SpendGoldAction action) {
        return stateManager.getStateByActorId(action.getSourceActorId())
                .flatMap(actorState -> {
                    Long actorGold = ActorHelper.getActorGold(actorState);
                    if (actorGold < paramFirepower) {
                        return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.SPEND_GOLD_NOT_ENOUGH_GOLD));
                    } else {
                        Map<QName, Object> items = new HashMap<>();
                        items.put(GoodsAttributes.GOLD, paramFirepower * -1);
                        items.put(ActorStateAttributes.ACTOR_FIRE_POWER, 1L);
                        Event stateChangeEvent = EventFactory.stateChangeEventModify(action, action.getSourceActorId(), items);
                        Event infoEvent = EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.SPEND_GOLD_DONE, new Object[]{GoldSpending.FIRE_POWER, paramFirepower});
                        return Flowable.just(stateChangeEvent, infoEvent);
                    }
                });

    }
}
