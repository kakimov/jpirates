package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.event.action.Action;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("player")
public abstract class PlayerActionService<T  extends Action> implements ActionService<T> {
}
