package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.Configuration;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ConfigurationManagerImpl implements ConfigurationManager {

    private ConfigurationDAO configurationDAO;

    @Autowired
    public ConfigurationManagerImpl(ConfigurationDAO configurationDAO) {
        this.configurationDAO = configurationDAO;
    }

    @Override
    public Flowable<Configuration> configurationChange(String moduleId) {
        return configurationDAO.updates();
    }

    @Override
    public Optional<Configuration> getConfiguration(String moduleId) {
        return configurationDAO.getConfiguration(moduleId);
    }

    @Override
    public void updateConfiguration(Configuration configuration) {
        configurationDAO.updateConfiguration(configuration);
    }
}
