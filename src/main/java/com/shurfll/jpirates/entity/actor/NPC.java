package com.shurfll.jpirates.entity.actor;

import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


public class NPC extends Actor {


    public NPC(String actorId, String name) {
        super(ActorType.NPC, actorId, name);
    }

    public NPC(String oid,String actorId, String name) {
        super(ActorType.NPC,oid, actorId, name);
    }

    @Override
    public String toString() {
        return getName();
    }

    public Flowable<Action> getActions(){
        return Flowable.empty();
    }
}
