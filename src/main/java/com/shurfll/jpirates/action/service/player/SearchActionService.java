package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.component.treasure.TreasureManager;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Treasure;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.SearchAction;
import com.shurfll.jpirates.manager.PlayersManager;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.Optional;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class SearchActionService implements ActionService<SearchAction> {


    private PlayersManager playersManager;

    private TreasureManager treasureManager;

    @Autowired
    public SearchActionService(PlayersManager playersManager, TreasureManager treasureManager) {
        this.playersManager = playersManager;
        this.treasureManager = treasureManager;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof SearchAction;
    }

    @Override
    public Flowable<Event> doBeforeAction(SearchAction action) {
        return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.SEARCH_STARTING));
    }

    @Override
    public Flowable<Event> doAction(SearchAction action) {
        return Flowable.just(playersManager.getLocation(action.getSourceActorId()))
                .filter(Optional::isPresent).map(Optional::get)
                .flatMap(location -> {
                            Optional<Treasure> treasure = treasureManager.discoverTreasures(location);
                            return doActionInternal(action, treasure, location);
                        }
                );
    }

    private Flowable<Event> doActionInternal(SearchAction action, Optional<Treasure> treasure, Location location) {
        if (treasure.isPresent()) {
            return Flowable.just(EventFactory.treasureFoundEvent(action, action.getSourceActorId(), treasure.get(), location));
        } else {
            return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.SEARCH_NOTHING));
        }
    }

}
