package com.shurfll.jpirates.persistence.converter;

import com.shurfll.jpirates.TestInstruments;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.admin.ConfigureModuleAction;
import com.shurfll.jpirates.event.action.player.*;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.mapper.AbstractEventMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static com.shurfll.jpirates.persistence.converter.EntityHelper.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class EventConverterTest {
    private static final String RESOURCE_DIR = "/persistence/converter";


    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.persistence.converter")
    static class TestConverterConfiguration{}

    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.objectfactory")
    static class TestObjectFactoryConfiguration{}

    private EventConverter converter;

    @Autowired
    private List<AbstractEventMapper> mappers;

    @Autowired
    private EntityAttributeConverter entityAttributeConverter;

    @Before
    public void init() {
        converter = new EventConverter(mappers, entityAttributeConverter);
    }

    @Test
    public void test001_startAction_from_entity() {
        StartAction event = (StartAction) converter.convert(entity(StartAction.TYPE,
                strAttr(StartAction.SOURCE_ACTOR_ID, "actorId")
                , strAttr(StartAction.NAME, "testName"))).get();
        assertEquals("testName", event.getName());
        assertEquals("actorId", event.getSourceActorId());
    }

    @Test
    public void test002_startAction_to_entity() {
        StartAction action = EventFactory.startAction("actionOid", "causeOid", "actorOid", "actorName", null);
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, StartAction.TYPE);
        entityAttrCheck(entity,  entry(StartAction.SOURCE_ACTOR_ID,"actorOid")
                                ,entry(StartAction.NAME, "actorName"));
    }

    @Test
    public void test003_fireAction_from_entity() {
        FireAction event = (FireAction) converter.convert(entity(FireAction.TYPE,
                strAttr(FireAction.SOURCE_ACTOR_ID, "actorId"),
                strAttr(FireAction.TARGET_NAME, "name"),
                strAttr(FireAction.TARGET_ID, "id"))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertEquals("id", event.getTargetId());
        assertEquals("name", event.getTargetName().get());
    }

    @Test
    public void test004_fireAction_to_entity() {
        FireAction action = EventFactory.fireAction("actionOid", "causeOid", "actorOid", "targetId", "targetName");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, FireAction.TYPE);
        entityAttrCheck(entity,  entry(FireAction.SOURCE_ACTOR_ID,"actorOid")
                                ,entry(FireAction.TARGET_ID, "targetId")
                                ,entry(FireAction.TARGET_NAME, "targetName"));
    }

    @Test
    public void test005_levelUpAction_from_entity() {
        LevelUpAction event = (LevelUpAction) converter.convert(entity(LevelUpAction.TYPE
                , strAttr(LevelUpAction.SOURCE_ACTOR_ID, "actorId")
                , attr(LevelUpAction.MODE, EntityAttributeType.LEVELUPMODE, LevelUpMode.HEALTH.name()))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertEquals(LevelUpMode.HEALTH, event.getLevelUpMode());
    }

    @Test
    public void test006_levelUpAction_to_entity() {
        LevelUpAction action = EventFactory.levelUpAction("actionOid", "causeOid", "actorOid", LevelUpMode.HEALTH);
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, LevelUpAction.TYPE);
        entityAttrCheck(entity,  entry(LevelUpAction.SOURCE_ACTOR_ID,"actorOid")
                                ,entry(LevelUpAction.MODE, "\"HEALTH\""));
    }

    @Test
    public void test007_lookAroundAction_from_entity() {
        LookAroundAction event = (LookAroundAction) converter.convert(entity(LookAroundAction.TYPE
                , strAttr(LookAroundAction.SOURCE_ACTOR_ID, "actorId"))).get();
        assertEquals("actorId", event.getSourceActorId());
    }

    @Test
    public void test008_lookAroundAction_to_entity() {
        LookAroundAction action = EventFactory.lookAroundAction("actionOid", "causeOid", "actorOid");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, LookAroundAction.TYPE);
        entityAttrCheck(entity,  entry(LookAroundAction.SOURCE_ACTOR_ID,"actorOid"));
    }

    @Test
    public void test009_messageAction_from_entity() {
        MessageAction event = (MessageAction) converter.convert(entity(MessageAction.TYPE
                , strAttr(MessageAction.SOURCE_ACTOR_ID, "actorId")
                , strAttr(MessageAction.MESSAGE, "message"))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertEquals("message", event.getMessage());
    }

    @Test
    public void test010_messageAction_to_entity() {
        MessageAction action = EventFactory.messageAction("actionOid", "causeOid", "actorOid","testMessage");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, MessageAction.TYPE);
        entityAttrCheck(entity,  entry(MessageAction.SOURCE_ACTOR_ID,"actorOid")
                                ,entry(MessageAction.MESSAGE,"testMessage"));
    }

    @Test
    public void test011_moveAction_from_entity() {
        MoveAction event = (MoveAction) converter.convert(entity(MoveAction.TYPE
                , strAttr(MoveAction.SOURCE_ACTOR_ID, "actorId")
                , attr(MoveAction.DIRECTION, EntityAttributeType.DIRECTION, Direction.NORTH.name()))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertEquals(Direction.NORTH, event.getDirection());
    }

    @Test
    public void test012_moveAction_to_entity() {
        MoveAction action = EventFactory.moveAction("actionOid", "causeOid", "actorOid",Direction.EAST);
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, MoveAction.TYPE);
        entityAttrCheck(entity,  entry(MoveAction.SOURCE_ACTOR_ID,"actorOid")
                ,entry(MoveAction.DIRECTION,"\"EAST\""));
    }

    @Test
    public void test013_questAction_from_entity() {
        QuestAction event = (QuestAction) converter.convert(entity(QuestAction.TYPE
                , strAttr(QuestAction.SOURCE_ACTOR_ID, "actorId")
                , strAttr(QuestAction.QUEST_ID, "questId"))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertEquals("questId", event.getQuestId());
    }

    @Test
    public void test014_questAction_to_entity() {
        QuestAction action = EventFactory.questAction("actionOid", "causeOid", "actorOid","questId");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, QuestAction.TYPE);
        entityAttrCheck(entity,  entry(QuestAction.SOURCE_ACTOR_ID,"actorOid")
                ,entry(QuestAction.QUEST_ID,"questId"));
    }

    @Test
    public void test015_repairAction_from_entity() {
        RepairAction event = (RepairAction) converter.convert(entity(RepairAction.TYPE
                , strAttr(RepairAction.SOURCE_ACTOR_ID, "actorId"))).get();
        assertEquals("actorId", event.getSourceActorId());
    }

    @Test
    public void test016_repairAction_to_entity() {
        RepairAction action = EventFactory.repairAction("actionOid", "causeOid", "actorOid");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, RepairAction.TYPE);
        entityAttrCheck(entity,  entry(RepairAction.SOURCE_ACTOR_ID,"actorOid"));
    }

    @Test
    public void test017_searchAction_from_entity() {
        SearchAction event = (SearchAction) converter.convert(entity(SearchAction.TYPE
                , strAttr(SearchAction.SOURCE_ACTOR_ID, "actorId"))).get();
        assertEquals("actorId", event.getSourceActorId());
    }

    @Test
    public void test018_searchAction_to_entity() {
        SearchAction action = EventFactory.searchAction("actionOid", "causeOid", "actorOid");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, SearchAction.TYPE);
        entityAttrCheck(entity,  entry(SearchAction.SOURCE_ACTOR_ID,"actorOid"));
    }

    @Test
    public void test019_spendGoldAction_from_entity() {
        SpendGoldAction event = (SpendGoldAction) converter.convert(entity(SpendGoldAction.TYPE
                , strAttr(SpendGoldAction.SOURCE_ACTOR_ID, "actorId")
                , attr(SpendGoldAction.MODE, EntityAttributeType.GOLDSPENDING, GoldSpending.FIRE_POWER.name()))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertEquals(GoldSpending.FIRE_POWER, event.getGoldSpending());
    }

    @Test
    public void test020_spendGoldAction_to_entity() {
        SpendGoldAction action = EventFactory.spendGoldAction("actionOid", "causeOid", "actorOid",GoldSpending.HEAL);
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, SpendGoldAction.TYPE);
        entityAttrCheck(entity,  entry(SpendGoldAction.SOURCE_ACTOR_ID,"actorOid")
                                ,entry(SpendGoldAction.MODE,"\"HEAL\""));
    }

    @Test
    public void test021_statusAction_from_entity() {
        StatusAction event = (StatusAction) converter.convert(entity(StatusAction.TYPE
                , strAttr(StatusAction.SOURCE_ACTOR_ID, "actorId"))).get();
        assertEquals("actorId", event.getSourceActorId());
    }

    @Test
    public void test022_statusGoldAction_to_entity() {
        StatusAction action = EventFactory.statusAction("actionOid", "causeOid", "actorOid");
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, StatusAction.TYPE);
        entityAttrCheck(entity,  entry(SpendGoldAction.SOURCE_ACTOR_ID,"actorOid"));
    }


    @Test
    public void test023_configureModuleAction_from_entity() {
        ConfigureModuleAction event = (ConfigureModuleAction) converter.convert(entity(ConfigureModuleAction.TYPE
                , strAttr(ConfigureModuleAction.SOURCE_ACTOR_ID, "actorId")
                , attr(ConfigureModuleAction.CONFIGURATION, EntityAttributeType.CONFIGURATION, TestInstruments.readResource(RESOURCE_DIR, "configuration.json")))).get();
        assertEquals("actorId", event.getSourceActorId());
        assertTrue(event.getConfiguration() != null);
    }

    @Test
    public void test024_configureModuleAction_to_entity() {
        ConfigureModuleAction action = EventFactory.configureModuleAction("actionOid", "causeOid", "actorOid", new Configuration("test"));
        EventEntity entity = converter.convert(action).get();
        entityBaseCheck(entity, ConfigureModuleAction.TYPE);
        entityAttrCheck(entity,  entry(ConfigureModuleAction.SOURCE_ACTOR_ID,"actorOid")
                                ,entry(ConfigureModuleAction.CONFIGURATION,"{\"moduleUri\":\"test\",\"attributes\":[]}"));
    }



    private void entityBaseCheck(EventEntity entity, String type) {
        assertEquals("actionOid", entity.getOid());
        assertEquals("causeOid", entity.getCauseOid());
        assertEquals(type, entity.getType());
    }


}