package com.shurfll.jpirates.telegram.event;

import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.telegram.keyboard.Keyboard;

/**
 * Created by kakim on 18.06.2017.
 */
public class QuestEvent extends InfoEvent {

    private String questId;

    private Keyboard keyboardMode;

    public QuestEvent(String oid, String causeOid, String targetActorId, String questId, String message, Keyboard keyboardMode) {
        super(oid, causeOid, targetActorId, message, null);
        this.questId = questId;
        this.keyboardMode = keyboardMode;
    }


    public Keyboard getKeyboardMode() {
        return keyboardMode;
    }

    public void setKeyboardMode(Keyboard keyboardMode) {
        this.keyboardMode = keyboardMode;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    @Override
    public String toString() {
        return super.toString()+", keyboardMode="+keyboardMode;
    }
}
