package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.StatusAction;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class StatusActionService implements ActionService<StatusAction> {

    private PlayersManager playersManager;
    private StateManager stateManager;

    @Autowired
    public StatusActionService(PlayersManager playersManager, StateManager stateManager) {
        this.playersManager = playersManager;
        this.stateManager = stateManager;
    }

    private static final String STATUS_MESSAGE = "Ваш статус: имя '%s', здоровье '%d' из '%d', золота '%d'. Вы находитесь в точке %s. Вы накопили %d опыта. Ваш уровень %d, у вас есть %d очков улучшений персонажа";

    @Override
    public boolean canHandle(Action a) {
        return a instanceof StatusAction;
    }


    @Override
    public Flowable<Event> doAction(StatusAction action) {
        return status(action);
    }

    private Flowable<Event> status(StatusAction action) {
        return Flowable.zip(playersManager.getPlayerFlowable(action.getSourceActorId())
                , stateManager.getStateByActorId(action.getSourceActorId())
                , (actor, state) -> statusEvent(action, actor, state));
    }

    private Event statusEvent(StatusAction action, Actor actor, State state) {
        return EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.CUSTOM_MESSAGE, String.format(STATUS_MESSAGE, getArguments(actor, state)));
    }

    private Object[] getArguments(Actor actor, State state) {
        // Ваш статус: имя '%s', здоровье '%d' из '%d', золота '%d'. Вы находитесь в точке %s. Вы накопили %d опыта
        return new Object[]{actor.getName()
                , ActorHelper.getActorHealth(state)
                , ActorHelper.getActorTotalHealth(state)
                , ActorHelper.getActorGold(state)
                , LocationHelper.printLocation(ActorHelper.getActorLocation(state))
                , ActorHelper.getActorExperience(state)
                , ActorHelper.getActorLevel(state)
                , ActorHelper.getActorLevelUpPoints(state)};
    }

}
