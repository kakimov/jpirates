package com.shurfll.jpirates.persistence.converter;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.persistence.StateEntity;
import com.shurfll.jpirates.persistence.converter.mapper.StateMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.shurfll.jpirates.persistence.converter.EntityHelper.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class StateConverterTest {


    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.persistence.converter")
    static class TestConverterConfiguration{}

    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.objectfactory")
    static class TestObjectFactoryConfiguration{}

    private StateConverter stateConverter;

    @Autowired
    private EntityAttributeConverter entityAttributeConverter;

    @Before
    public void init() {
        ObjectFactory objectFactory = new ObjectFactory();
        StateMapper mapper = new StateMapper(entityAttributeConverter, objectFactory);
        stateConverter = new StateConverter(entityAttributeConverter, mapper);
    }

    @Test
    public void test001_convert_to_entity() {
        State state = new ObjectFactory().createState(null,"actorOid", Commons.loc(1L, 1L));
        StateEntity entity = stateConverter.convert(state);
        assertEquals("actorOid", entity.getOwnerOid());
        stateEntityAttrCheck(entity, entry(ActorStateAttributes.ACTOR_LOCATION, "{\"latitude\":\"1\",\"longitude\":\"1\"}"));
    }

    @Test
    public void test002_convert_from_entity() {
        StateEntity entity = state(null,"actorOid", attr(ActorStateAttributes.ACTOR_LOCATION, EntityAttributeType.LOCATION, "{\"latitude\":\"1\",\"longitude\":\"1\"}"));
        State state = stateConverter.convert(entity);
        assertEquals("actorOid", state.getOwnerOid());

        assertEquals(Commons.loc(1L, 1L), state.getAttribute(ActorStateAttributes.ACTOR_LOCATION).get().getValue().get().getValue());
    }
}