package com.shurfll.jpirates.action.interruption.rule;

import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.MoveAction;
import org.springframework.stereotype.Component;

import static com.shurfll.jpirates.action.interruption.rule.RuleUtils.*;

@Component
public class MoveActionInterruptionRule extends ActionInterruptionRule<MoveAction> {

    public MoveActionInterruptionRule() {
        addInterruptionRule((a,e) -> isPersonalEvent(e) && isEventTargetMe(a,e) && isDamageReceivedEvent(e));
    }

    @Override
    public boolean canHandle(Action action) {
        return action instanceof MoveAction;
    }

}
