package com.shurfll.jpirates.persistence.converter.attribute;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DirectionAttributeConverter extends SimpleTypedValueConverter {
    @Autowired
    public DirectionAttributeConverter(ValueConverter converter) {
        super(EntityAttributeType.DIRECTION.name(), converter);
    }
}
