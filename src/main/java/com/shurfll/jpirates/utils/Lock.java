package com.shurfll.jpirates.utils;

public class Lock {
    volatile private boolean locked = false;
    volatile private Thread owner;

    public Lock() {
    }

    public Lock(boolean locked) {
        this.locked = locked;
        owner = Thread.currentThread();
    }

    public static Lock newLock(){
        return new Lock(true);
    }

    public synchronized void lock(){
        locked = true;
    }

    public synchronized void unlock(){
        locked = false;
    }

    public synchronized boolean isLocked(){
        return locked && !owner.equals(Thread.currentThread());
    }
}
