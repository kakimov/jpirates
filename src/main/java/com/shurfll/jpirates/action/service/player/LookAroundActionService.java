package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.LookAroundAction;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class LookAroundActionService implements ActionService<LookAroundAction> {

    private PlayersManager playersManager;

    private static final String YOUT_LOCATION_MESSAGE = "В ваших ближайших окрестностях находятся: %s. ";
    private static final String YOUT_NEIGHBOURS_MESSAGE = "На %s от вас находятся: %s. ";
    private static final String NOBODY_HERE = "Вокруг вас никого нет";

    @Autowired
    public LookAroundActionService(PlayersManager playersManager) {
        this.playersManager = playersManager;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof LookAroundAction;
    }

    @Override
    public Flowable<Event> doAction(LookAroundAction action) {
        return lookAroundInternal(action);
    }

    private Flowable<Event> lookAroundInternal(LookAroundAction action) {
        return playersManager.getPlayerFlowable(action.getSourceActorId())
                .flatMap(actor -> processActor(action, actor));
    }

    private Flowable<Event> processActor(LookAroundAction action, Actor actor) {
        return playersManager.getLocation(actor.getActorId())
                .map(location -> processActorWithLocation(action, actor, location))
                .map(Flowable::just)
                .orElse(Flowable.empty());
    }

    private Event processActorWithLocation(LookAroundAction action, Actor actor, Location location) {
        List<Location> locations = LocationHelper.getVicinity(location);
        Map<Location, List<Object>> vicinity = new HashMap<>();
        locations.forEach(loc -> vicinity.put(loc, findNeighbours(actor.getActorId(), loc)));
        return EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.CUSTOM_MESSAGE, translate(vicinity, location));
    }

    private List<Object> findNeighbours(String playerId, Location location) {
        List<Object> objects = new ArrayList<>();
        objects.addAll(playersManager.getNeighboursAtLocation(playerId, location));
        return objects;
    }


    public String translate(Map<Location, List<Object>> vicinity, Location actorLocation) {
        StringBuilder sb = new StringBuilder();
        List<Object> closeFriends = vicinity.get(actorLocation);
        if (!CollectionUtils.isEmpty(closeFriends)) {
            sb.append(String.format(YOUT_LOCATION_MESSAGE, Commons.objectsToString(vicinity.get(actorLocation))));
        }
        vicinity.entrySet().stream()
                .filter(entry -> !CollectionUtils.isEmpty(entry.getValue()))
                .filter(entry -> !entry.getKey().equals(actorLocation))
                .forEach(entry ->
                        sb.append(String.format(YOUT_NEIGHBOURS_MESSAGE
                                , LocationHelper.printRelativeDirection(actorLocation, entry.getKey())
                                , Commons.objectsToString(entry.getValue()))));
        String res = sb.toString();
        return res.isEmpty() ? NOBODY_HERE : res;
    }

}
