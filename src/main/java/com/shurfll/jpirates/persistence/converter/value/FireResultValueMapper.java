package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.FireResult;
import org.springframework.stereotype.Component;

@Component
public class FireResultValueMapper extends AbstractValueMapper {
    public FireResultValueMapper() {
        super(EntityAttributeType.FIRE_RESULT, FireResult.class, o -> gson.toJson(o), s -> gson.fromJson(s, FireResult.class));
    }
}
