package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.StateEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class StateMapper extends AbstractMapper<State, StateEntity> {

    private ObjectFactory objectFactory;

    public StateMapper(EntityAttributeConverter attributeConverter, ObjectFactory objectFactory) {
        super(attributeConverter);
        this.objectFactory = objectFactory;
    }

    @Override
    protected String getMapperType() {
        return State.class.getName();
    }

    @Override
    protected Class getMapperClass() {
        return State.class;
    }

    @Override
    public StateEntity toEntity(State entity) {
        StateEntity stateEntity =  new StateEntity(entity.getOid(), entity.getOwnerOid(), null);
        List<Attribute> attributes = convertEventAttributes(stateEntity, entity);
        stateEntity.setAttributes(attributes);
        return stateEntity;
    }

    @Override
    public State fromEntity(StateEntity entity) {
        return objectFactory.createState(entity.getOid(), entity.getOwnerOid(), convertAttributes(entity));
    }
}
