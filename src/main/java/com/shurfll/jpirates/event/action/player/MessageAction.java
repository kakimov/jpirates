package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Created by kakim on 02.06.2017.
 */
public class MessageAction extends PlayerAction {

    private static final String QUALIFIER = "messageAction";
    public static final String TYPE=ACTION_BASE_NAMESPACE +"/"+QUALIFIER;

    public static final QName MESSAGE = attribute(TYPE, "message");

    public MessageAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, String message) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, MESSAGE, message);
    }

    public MessageAction(String oid, String causeOid, String sourceActorId, String message) {
        this(oid, causeOid, sourceActorId, null, message);
    }

    public String getMessage() {
        return getValue(MESSAGE, String.class).get();
    }

}
