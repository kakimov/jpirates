package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import javax.xml.namespace.QName;

import static org.junit.Assert.assertEquals;

public class FileConfigurationDAOTest {

    private FileConfigurationDAO dao;
    private ConfigurationParser parser;
    private ObjectFactory objectFactory;

    public static final String MODULE_URI = "http://test.com";

    public static final String PARAMNAME = "param_long";

    @Before
    public void init() throws Exception {
        objectFactory = new ObjectFactory();
        parser = new ConfigurationParserImpl(objectFactory);
        dao = new FileConfigurationDAO(parser);
        dao.setConfigs(new Resource[]{new FileSystemResource(getClass().getResource("/config/testconfig.config").getPath())});
        dao.init();
    }

    @Test
    public void test001_getConfiguration() {
        Configuration configuration = dao.getConfiguration(MODULE_URI).orElseThrow(() -> new RuntimeException("failed"));
        assertEquals(MODULE_URI, configuration.getModuleUri());
        assertAttribute(configuration, new QName(MODULE_URI, PARAMNAME), 7L);
    }

    private void assertAttribute(Configuration conf, QName name, Object value) {
        EntityAttribute attr = conf.getAttributes().get(name);
        assertEquals(value, attr.getValue().get().getValue());
    }
}