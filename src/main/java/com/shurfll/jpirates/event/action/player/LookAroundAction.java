package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Created by kakim on 22.06.2017.
 */
public class LookAroundAction extends PlayerAction {
    private static final String QUALIFIER = "lookAroundAction";
    public static final String TYPE=ACTION_BASE_NAMESPACE +"/"+QUALIFIER;

    public LookAroundAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta) {
        super(oid, causeOid, sourceActorId, meta);
    }

    public LookAroundAction(String oid, String causeOid, String sourceActorId) {
        this(oid, causeOid, sourceActorId, null);
    }

}
