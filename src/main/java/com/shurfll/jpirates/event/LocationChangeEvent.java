package com.shurfll.jpirates.event;

import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.Location;

import javax.xml.namespace.QName;

/**
 * Created by shurfll on 25.05.17.
 */
public class LocationChangeEvent extends PersonalEvent {
    private static final String QUALIFIER = "locationChangeEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName PREVIOUS_LOCATION = attribute(TYPE, "previous");
    private static final QName CURRENT_LOCATION = attribute(TYPE, "current");
    private static final QName DIRECTION = attribute(TYPE, "direction");


    public LocationChangeEvent(String oid, String causeOid, String targetActorId, Location prevLocation, Location currentLocation, Direction direction) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, PREVIOUS_LOCATION, prevLocation);
        putAttribute(oid, CURRENT_LOCATION, currentLocation);
        putAttribute(oid, DIRECTION, direction);
    }

    public Location getPrevLocation() {
        return getValue(PREVIOUS_LOCATION, Location.class).get();
    }

    public Location getCurrentLocation() {
        return getValue(CURRENT_LOCATION, Location.class).get();
    }

    public Direction getDirection() {
        return getValue(DIRECTION, Direction.class).get();
    }

}
