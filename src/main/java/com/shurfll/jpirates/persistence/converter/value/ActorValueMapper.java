package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.actor.Actor;
import org.springframework.stereotype.Component;

@Component
public class ActorValueMapper extends AbstractValueMapper {
    public ActorValueMapper() {
        super(EntityAttributeType.ACTOR, Actor.class, o -> gson.toJson(o), s -> gson.fromJson(s, Actor.class));
    }

    @Override
    public boolean isApplicable(Class classType) {
        return Actor.class.isAssignableFrom(classType);
    }
}
