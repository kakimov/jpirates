package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.LevelUpMode;
import org.springframework.stereotype.Component;

@Component
public class LevelUpValueMapper extends AbstractValueMapper {
    public LevelUpValueMapper() {
        super(EntityAttributeType.LEVELUPMODE, LevelUpMode.class,  o -> gson.toJson(o), s -> gson.fromJson(s, LevelUpMode.class));
    }
}
