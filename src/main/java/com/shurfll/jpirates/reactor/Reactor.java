package com.shurfll.jpirates.reactor;

import com.shurfll.jpirates.event.Event;
import io.reactivex.Flowable;


import java.util.function.Predicate;


/**
 * Event-driver bus
 */
public interface Reactor {

    public Flowable<Event> events();

    public Flowable<Event> events(Predicate<Event> predicate);

    public void submitEvent(Event event);

    public Flowable<Event> submitEventAndObserve(Event event);

    public <T extends Event> void registerHandler(Class<T> eventClass, EventHandler<T> handler);

    public void unregisterHandler(Class eventClass, EventHandler handler);

    public void unregisterHandler(EventHandler handler);
}
