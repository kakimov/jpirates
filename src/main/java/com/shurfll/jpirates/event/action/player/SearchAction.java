package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Created by shurfll on 22.05.17.
 */
public class SearchAction extends PlayerAction {
    private static final String QUALIFIER = "searchAction";
    public static final String TYPE=ACTION_BASE_NAMESPACE +"/"+QUALIFIER;

    public SearchAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta) {
        super(oid, causeOid, sourceActorId, meta);
    }

    public SearchAction(String oid, String causeOid, String sourceActorId) {
        this(oid, causeOid, sourceActorId, null);
    }

}
