package com.shurfll.jpirates.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface TestComplexRepository extends JpaRepository<TestComplexEntity, String> {
}
