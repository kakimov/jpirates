package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.action.Action;

/**
 * Created by kakim on 23.10.2017.
 */
public interface ActionMetaProcessor {
    void calculateMeta(Action action);
}
