package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.StartAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class StartActionMapper extends AbstractEventMapper {

    public StartActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return StartAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return StartAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.startAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, StartAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, StartAction.NAME)
                , optionalAttribute(e, StartAction.LOCATION));
    }


}
