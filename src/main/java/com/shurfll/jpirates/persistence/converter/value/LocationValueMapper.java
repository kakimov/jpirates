package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.Location;
import org.springframework.stereotype.Component;

@Component
public class LocationValueMapper extends AbstractValueMapper {
    public LocationValueMapper() {
        super(EntityAttributeType.LOCATION, Location.class, o -> gson.toJson(o), s -> gson.fromJson(s, Location.class));
    }
}
