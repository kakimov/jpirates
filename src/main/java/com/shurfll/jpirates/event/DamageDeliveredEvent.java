package com.shurfll.jpirates.event;

import com.shurfll.jpirates.entity.FireResult;

import javax.xml.namespace.QName;

/**
 * Created by kakim on 04.06.2017.
 */
public class DamageDeliveredEvent extends PersonalEvent {
    private static final String QUALIFIER = "damageDeliveredEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName FIRE_RESULT = attribute(TYPE, "fireResult");

    public DamageDeliveredEvent(String oid, String causeOid, String targetActorId, FireResult fireResult) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, FIRE_RESULT, fireResult);
    }

    public FireResult getFireResult() {
        return getValue(FIRE_RESULT, FireResult.class).get();
    }

}
