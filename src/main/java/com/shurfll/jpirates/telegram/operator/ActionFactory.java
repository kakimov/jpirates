package com.shurfll.jpirates.telegram.operator;

import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.ActionResolver;

import java.util.List;
import java.util.Optional;

/**
 * Created by k.akimov on 09.06.2017.
 */
public interface ActionFactory {
	public Optional<ActionResolver> findResolver(List<Message> messages);

	public List<ActionResolver> findResolvers(List<Message> messages);

	public Optional<Action> map(List<Message> messages);
}
