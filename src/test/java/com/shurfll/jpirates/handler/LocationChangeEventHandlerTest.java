package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.LocationChangeEvent;
import com.shurfll.jpirates.event.MeetEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.shurfll.jpirates.TestEventHelper.dumpEvents;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

public class LocationChangeEventHandlerTest {

    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";

    private static final String STATIC_PLAYER_ID = "staticid";
    private static final String STATIC_PLAYER_NAME = "staticname";

    private static final Location OLD_LOCATION = Commons.loc(0L, 0L);
    private static final Location NEW_LOCATION = Commons.loc(0L, 1L);
    private static final Direction DIRECTION = Direction.EAST;

    private LocationChangeEventHandler handler;

    private Reactor reactor;
    private PlayersManager playersManager;
    private TestSubscriber subscriber;

    @Before
    public void init() {
        reactor = mock(Reactor.class);
        playersManager = mock(PlayersManager.class);
        handler = new LocationChangeEventHandler(reactor, playersManager);
        subscriber = TestSubscriber.create();
    }

    @Test
    public void test001_handle() {
        Player player = new Player(PLAYER_ID, PLAYER_NAME);
        Player staticPlayer = new Player(STATIC_PLAYER_ID, STATIC_PLAYER_NAME);
        playersManager.addPlayer(staticPlayer, NEW_LOCATION);
        playersManager.addPlayer(player, OLD_LOCATION);
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID))).thenReturn(Flowable.just(player));
        when(playersManager.getNeighboursAtLocation(eq(PLAYER_ID), any(Location.class))).thenReturn(Collections.singletonList(staticPlayer));
        LocationChangeEvent event = EventFactory.locationChangeEvent(null,PLAYER_ID,OLD_LOCATION,NEW_LOCATION,DIRECTION);

        handler.handle(event).subscribe(subscriber);
        meetEventProduced(subscriber, PLAYER_ID, staticPlayer);
        meetEventProduced(subscriber, STATIC_PLAYER_ID, player);
        subscriber.assertComplete();
    }



    private void meetEventProduced(TestSubscriber<Event> subscriber, final String targetId, final Object object) {
        List<Event> events = subscriber.values().stream()
                .filter(MeetEvent.class::isInstance)
                .map(MeetEvent.class::cast)
                .filter(e -> equalsMeetEvent(e, targetId, object))
                .collect(Collectors.toList());
        assertFalse(events.size() < 1, "Cannot find event: " + dumpEvents(subscriber.values()));
        assertFalse(events.size() > 1, "Too many events: " + dumpEvents(subscriber.values()));
    }

    private boolean equalsMeetEvent(MeetEvent event, String targetId, Object object) {
        return equalsMeetEvent(event, targetId, Collections.singletonList(object));
    }

    private boolean equalsMeetEvent(MeetEvent event, String targetId, List<Object> objectList) {
        return targetId.equals(event.getTargetActorId())
                && event.getObject().containsAll(objectList) && objectList.containsAll(event.getObject());
    }
}