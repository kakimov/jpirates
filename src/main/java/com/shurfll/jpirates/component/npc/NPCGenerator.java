package com.shurfll.jpirates.component.npc;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.NPC;
import com.shurfll.jpirates.entity.actor.StaticShootingNPC;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.BiConsumer;

@Component
public class NPCGenerator {

    @Autowired
    private ObjectFactory objectFactory;

    @Autowired
    private Reactor reactor;

    @Autowired
    private PlayersManagerImpl playersManager;

    private Random random = new Random();

    protected List<String> names;

    @PostConstruct
    public void init() throws IOException {
        names = IOUtils.readLines(getClass().getResourceAsStream(getResourceFileName()), "UTF-8");
    }

    public Optional<NPC> generate(BiConsumer<Actor, State> addActorFunc, Flowable<Event> events) {
        Location loc = createLocation();

        Optional<NPC> npc = createNPC(events);
        npc.ifPresent(npc1 -> {
            State state = objectFactory.createDefaultNPCState(npc1.getActorId(), loc);
            addActorFunc.accept(npc1, state);
        });
        return npc;
    }

    private Optional<NPC> createNPC(Flowable<Event> events) {
        List<NPC> npcs = playersManager.getAllNPC();
        for(String name : names){
            if(npcs.stream().map(Actor::getName).anyMatch(name::equals)){
                // if between already located NPC there is a NPC with such name
                continue;
            }else{
                return Optional.of(new StaticShootingNPC("NPC_" + System.currentTimeMillis(), name, events, Commons.FIRE_NPC_DEFAULT_DELAY, Commons.FIRE_NPC_DEFAULT_START_DELAY));
            }
        }
        return Optional.empty();
    }

    public NPC initNPC(NPC npc, Flowable<Event> events) {
        return new StaticShootingNPC(npc.getActorId(), npc.getName(), events, Commons.FIRE_NPC_DEFAULT_DELAY, Commons.FIRE_NPC_DEFAULT_START_DELAY);
    }

    private Location createLocation() {
        Location location = Commons.generateLocation();
        return location;
    }

    public String getResourceFileName() {
        return "/npc/names.txt";
    }

}
