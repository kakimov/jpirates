package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.StatusActionService;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import io.reactivex.Flowable;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;

import static com.shurfll.jpirates.Commons.generateLocation;
import static com.shurfll.jpirates.Commons.loc;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by kakim on 29.05.2017.
 */

public class StatusActionServiceTest {

    private static final String PLAYER_OID = "oid";
    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";
    private Player player;
    private State state;
    private StatusActionService service;
    private PlayersManager playersManager;
    private StateManager stateManager;
    private Location location = loc(10l,10l);

    private TestSubscriber subscriber;

    @Before
    public void setUp() {
        ObjectFactory objectFactory = new ObjectFactory();
        player = new Player(PLAYER_OID, PLAYER_ID, PLAYER_NAME);
        state = objectFactory.createDefaultPlayerState("randomOid", generateLocation());
        subscriber = new TestSubscriber();
        playersManager = mock(PlayersManager.class);
        stateManager = mock(StateManager.class);
        service = new StatusActionService(playersManager,stateManager);
    }


    @Test
    public void test001_statusAction() {
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID))).thenReturn(Flowable.just(player));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));
        service.doAction(EventFactory.statusAction(null, PLAYER_ID)).subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, event -> TestEventHelper.infoEventMatched(event, PLAYER_ID, LocalizationKeys.CUSTOM_MESSAGE));
    }


}