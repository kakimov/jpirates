package com.shurfll.jpirates.persistence.converter;

import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.persistence.StateEntity;
import com.shurfll.jpirates.persistence.converter.mapper.AbstractMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateConverter {
    private EntityAttributeConverter attributeConverter;
    private AbstractMapper<State,StateEntity> mapper;

    @Autowired
    public StateConverter(EntityAttributeConverter attributeConverter, AbstractMapper<State, StateEntity> mapper) {
        this.attributeConverter = attributeConverter;
        this.mapper = mapper;
    }

    public State convert(StateEntity entity){
        return mapper.fromEntity(entity);
    }

    public StateEntity convert(State state){
        return mapper.toEntity(state);
    }
}
