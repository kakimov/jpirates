package com.shurfll.jpirates.entity;

import javax.xml.namespace.QName;

public interface GoodsAttributes {
    String GOODS_NS = "jpirates/goods/common";

    QName GOLD = new QName(GOODS_NS, "gold");
}
