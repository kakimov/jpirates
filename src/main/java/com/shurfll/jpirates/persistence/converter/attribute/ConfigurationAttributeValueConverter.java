package com.shurfll.jpirates.persistence.converter.attribute;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.StreamSupport;

import static com.shurfll.jpirates.entity.EntityAttributeType.CONFIGURATION;

@Component
public class ConfigurationAttributeValueConverter extends AbstractComplexAttributeValueConverter {

    @Autowired
    public ConfigurationAttributeValueConverter(ValueConverter valueConverter) {
        super(CONFIGURATION.name(), valueConverter);
        setDeserializer(attr -> deserializeAttribute(attr, CONFIGURATION, this::convertAttrToConfiguration));
        setSerializer((owner, attr) -> serializeAttribute(owner, attr, this::convertConfigurationToAttr));
    }


    private Configuration convertAttrToConfiguration(Attribute attr) {
        JsonObject json = gson.fromJson(attr.getValue(), JsonObject.class);
        Configuration conf = new Configuration(json.getAsJsonPrimitive("moduleUri").getAsString());
        StreamSupport.stream(json.getAsJsonArray("attributes").spliterator(), true)
                .map(JsonElement::getAsJsonObject)
                .map(j -> convertJsonEntityAttribute(j))
                .forEach(e -> conf.getAttributes().put(e.getKey(), e.getValue()));
        return conf;

    }

    private String convertConfigurationToAttr(EntityAttributeValue value) {
        Configuration conf = (Configuration) value.getValue();
        JsonObject object = new JsonObject();
        object.addProperty("moduleUri", conf.getModuleUri());
        JsonArray attributes = new JsonArray();
        object.add("attributes", attributes);
        conf.getAttributes().entrySet().stream()
                .map(entry -> convertEntityAttributeToJson(entry.getValue()))
                .forEach(attributes::add);
        return gson.toJson(object);
    }
}
