package com.shurfll.jpirates.persistence.converter.attribute;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.entity.Treasure;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.StreamSupport;

import static com.shurfll.jpirates.entity.EntityAttributeType.TREASURE;

@Component
public class TreasureAttributeValueConverte extends AbstractComplexAttributeValueConverter {

    private ValueConverter valueConverter;

    @Autowired
    public TreasureAttributeValueConverte( ValueConverter valueConverter) {
        super(TREASURE.name(), valueConverter);
        setDeserializer(attr -> deserializeAttribute(attr, TREASURE, this::convertAttrToTreasure));
        setSerializer((owner, attr) -> serializeAttribute(owner,attr, this::convertTreasureToAttr));
    }


    private Treasure convertAttrToTreasure(Attribute attr) {
        JsonObject json = gson.fromJson(attr.getValue(), JsonObject.class);
        Treasure treasure = new Treasure(json.getAsJsonPrimitive("oid").getAsString());
        StreamSupport.stream(json.getAsJsonArray("attributes").spliterator(), true)
                .map(JsonElement::getAsJsonObject)
                .map(j -> convertJsonEntityAttribute(j))
                .forEach(e -> treasure.getAttributes().put(e.getKey(), e.getValue()));
        return treasure;
    }


    private String convertTreasureToAttr(EntityAttributeValue value) {
        Treasure treasure = (Treasure) value.getValue();
        JsonObject object = new JsonObject();
        object.addProperty("oid", treasure.getOid());
        JsonArray attributes = new JsonArray();
        object.add("attributes", attributes);
        treasure.getAttributes().entrySet().stream()
                .map(entry -> convertEntityAttributeToJson(entry.getValue()))
                .forEach(attributes::add);
        return gson.toJson(object);
    }



}
