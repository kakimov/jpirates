package com.shurfll.jpirates.action.exception;

import com.shurfll.jpirates.ActorException;
import com.shurfll.jpirates.event.action.Action;

/**
 * Created by kakim on 26.10.2017.
 */
public class AnotherActionExecutingException extends ActorException {
    private Action causeAction;
    private Action blockingAction;

    public AnotherActionExecutingException(String actorId, Action causeAction, Action blockingAction) {
        super(actorId);
        this.causeAction = causeAction;
        this.blockingAction = blockingAction;
    }

    public Action getCauseAction() {
        return causeAction;
    }

    public void setCauseAction(Action causeAction) {
        this.causeAction = causeAction;
    }

    public Action getBlockingAction() {
        return blockingAction;
    }

    public void setBlockingAction(Action blockingAction) {
        this.blockingAction = blockingAction;
    }
}
