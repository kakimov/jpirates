package com.shurfll.jpirates.event;

import javax.xml.namespace.QName;
import java.util.List;

/**
 * Created by shurfll on 26.05.17.
 */
public class MeetEvent extends PersonalEvent {
    private static final String QUALIFIER = "meetEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName OBJECTS = attribute(TYPE, "objects");


    public MeetEvent(String oid, String causeOid, String targetActorId, List<Object> object) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, OBJECTS, object);
    }

    public List<Object> getObject() {
        return getValue(OBJECTS, List.class).get();
    }

}
