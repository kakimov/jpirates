package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.entity.GoldSpending;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class SpendGoldActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/upgrade) (.+)";

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }


    @Override
    public Optional<Action> resolve(List<Message> messages) {
        if (messages.size() > 1)
            throw new IllegalArgumentException("Wrong arguments of spendGold action: " + messages.stream().map(message -> message.toString()).collect(Collectors.joining()));
        Message message = messages.get(0);
        return message.getText().map(COMMAND_PATTERN::matcher)
                .flatMap(matcher -> matcher.find() && matcher.groupCount() == 2 ? parseMode(matcher.group(2)) : Optional.empty())
                .map(mode -> EventFactory.spendGoldAction(null, String.valueOf(message.getChatId()), mode));
    }


    private Optional<GoldSpending> parseMode(String m) {
        try {
            return Optional.of(GoldSpending.valueOf(m.toUpperCase()));
        } catch (RuntimeException e) {
            return Optional.empty();
        }
    }
}
