package com.shurfll.jpirates;

import com.shurfll.jpirates.component.npc.NPCManager;
import com.shurfll.jpirates.component.treasure.TreasureManager;
import com.shurfll.jpirates.reactor.Reactor;
import com.shurfll.jpirates.telegram.ActionProducer;
import com.shurfll.jpirates.telegram.TelegramManager;
import com.shurfll.jpirates.telegram.translator.EventTranslatorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.telegram.telegrambots.ApiContextInitializer;

/**
 * Created by shurfll on 22.05.17.
 */
@SpringBootApplication
//extends SpringBootServletInitializer
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static final Long CONSOLE_PLAYER_ID = -1L;

    static {
        ApiContextInitializer.init();
    }

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(Main.class);
//    }

    public static void main(String[] args) {
        internalInit(args);
    }

    private static void internalInit(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Main.class, args);
        EventTranslatorManager translatorManager = context.getBean(EventTranslatorManager.class);
        TelegramManager telegramManager = context.getBean(TelegramManager.class);
        ActionProducer actionProducer = context.getBean(ActionProducer.class);
        Reactor reactor = context.getBean(Reactor.class);

        NPCManager manager = context.getBean(NPCManager.class);
        manager.init();

        LOGGER.info("JPirates started!");

        /*reactor.events()
                .ofType(PersonalEvent.class)
                .filter(Main::filterConsoleEvents)
                .map(translatorManager::translate)
                .subscribe(s -> System.out.println(s));

        List<ActionResolver> resolvers = context.getBeansOfType(ActionResolver.class).values().stream().collect(Collectors.toList());

        consoleReader()
                .lift(new BufferedAction(new ActionFactoryImpl(resolvers), telegramManager))
                .subscribe(actionProducer::submitAction);*/

    }

    /*private static boolean filterConsoleEvents(PersonalEvent e) {
        try {
            return CONSOLE_PLAYER_ID.equals(Long.valueOf(e.getTargetActorId()));
        } catch (Exception ex) {
            return false;
        }
    }

    private static Flowable<Message> consoleReader() {
        return Flowable.create(subscriber -> {
            System.out.println("Enter your command");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                try {
                    String command = "";
                    while (!(command = br.readLine()).equals("/exit")) {
                        try {
                            subscriber.onNext(new Message(CONSOLE_PLAYER_ID, command));
                        } catch (UnsupportedOperationException | IllegalArgumentException e) {
                            subscriber.onError(e);
                        }
                    }
                } finally {
                    br.close();
                }
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }*/
}
