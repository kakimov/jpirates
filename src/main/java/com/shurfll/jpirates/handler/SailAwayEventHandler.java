package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.SailAwayEvent;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SailAwayEventHandler implements EventHandler<SailAwayEvent> {

    private Reactor reactor;


    @Autowired
    public SailAwayEventHandler(Reactor reactor) {
        this.reactor = reactor;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(SailAwayEvent.class, this);
    }

    @Override
    public Flowable<Event> handle(SailAwayEvent event) {
        return Flowable.just(EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.SAIL_AWAY, translateObjects(event.getObject()), event.getDirection()));
    }

    private String translateObjects(List<Object> objects) {
        return objects.stream().map(Object::toString).collect(Collectors.joining(", "));
    }
}
