package com.shurfll.jpirates.action.interruption.rule;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

public abstract class ActionInterruptionRule<T extends Action>{


    protected List<BiPredicate<T,Event>> predicates = new ArrayList<>();

    protected void addInterruptionRule(BiPredicate<T,Event> p){
        predicates.add(p);
    }

    public abstract boolean canHandle(Action action);
    public boolean isInterruptedBy(T action, Event event){
        return predicates.stream().filter(p -> p.test(action,event) == true).findAny().isPresent();
    };
}