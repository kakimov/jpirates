package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.StartActionService;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.StartAction;
import com.shurfll.jpirates.event.system.ActorStartedEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import io.reactivex.subscribers.TestSubscriber;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by kakim on 29.05.2017.
 */
public class StartActionServiceTest {

    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";
    private static final Actor actor = new Player(PLAYER_ID,PLAYER_NAME);
    private static final Location location = new Location(-5l,-3l);


    private StartActionService service;
    private PlayersManager playersManager;
    private ObjectFactory objectFactory;
    private TestSubscriber subscriber;

    @Before
    public void setUp() {
        subscriber = new TestSubscriber();
        playersManager = Mockito.mock(PlayersManager.class);
        objectFactory = Mockito.spy(new ObjectFactory(Collections.emptyList()));
        service = new StartActionService(playersManager, objectFactory);
    }


    @Test
    public void test001_start_null_location() {
        ArgumentCaptor<Player> playerCaptor = ArgumentCaptor.forClass(Player.class);
        ArgumentCaptor<Location> locationCaptor = ArgumentCaptor.forClass(Location.class);
        when(playersManager.hasPlayer(eq(PLAYER_NAME))).thenReturn(false);
        when(objectFactory.generateLocation()).thenReturn(location);
        StartAction action = EventFactory.startAction(null,PLAYER_ID,PLAYER_NAME,null);
        service.doAction(action).subscribe(subscriber);
        verify(objectFactory,times(1)).generateLocation();
        verify(playersManager,times(1)).hasPlayer(PLAYER_NAME);
        verify(playersManager,times(1)).addPlayer(playerCaptor.capture(),locationCaptor.capture());
        assertEquals(actor.getActorId(), playerCaptor.getValue().getActorId());
        assertEquals(actor.getName(), playerCaptor.getValue().getName());
        assertEquals(location, locationCaptor.getValue());
        TestEventHelper.checkEventProduced(subscriber, ActorStartedEvent.class, event -> startEventMatch(event,PLAYER_ID,location));
    }


    @Test
    public void test002_start_notnull_location() {
        ArgumentCaptor<Player> playerCaptor = ArgumentCaptor.forClass(Player.class);
        ArgumentCaptor<Location> locationCaptor = ArgumentCaptor.forClass(Location.class);
        when(playersManager.hasPlayer(eq(PLAYER_NAME))).thenReturn(false);
        StartAction action = EventFactory.startAction(null,PLAYER_ID,PLAYER_NAME,location);
        service.doAction(action).subscribe(subscriber);
        verify(objectFactory,never()).generateLocation();
        verify(playersManager,times(1)).hasPlayer(PLAYER_NAME);
        verify(playersManager,times(1)).addPlayer(playerCaptor.capture(),locationCaptor.capture());
        assertEquals(actor.getActorId(), playerCaptor.getValue().getActorId());
        assertEquals(actor.getName(), playerCaptor.getValue().getName());
        assertEquals(location, locationCaptor.getValue());
        TestEventHelper.checkEventProduced(subscriber, ActorStartedEvent.class, event -> startEventMatch(event,PLAYER_ID,location));
    }

    private boolean startEventMatch(Event event, String targetId, Location location) {
        return event instanceof ActorStartedEvent && targetId.equals(((ActorStartedEvent) event).getTargetActorId()) && location.equals(((ActorStartedEvent) event).getLocation());
    }

}