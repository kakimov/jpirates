package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.MoveActionService;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.LocationChangeEvent;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Optional;
import java.util.function.Predicate;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class MoveActionServiceTest {

    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";
    private static final Location OLD_LOCATION = Commons.loc(0L, 0L);
    private static final Location NEW_LOCATION = Commons.loc(0L, -1L);
    private static final Direction DIRECTION = Direction.WEST;


    private ObjectFactory objectFactory;

    private MoveActionService service;

    private PlayersManager playersManager;

    private StateManager stateManager;

    private TestSubscriber subscriber;

    private Reactor reactor;

    private ConfigurationManager configurationManager;


    @Before
    public void setUp() {
        objectFactory = mock(ObjectFactory.class);
        subscriber = TestSubscriber.create();
        reactor = mock(Reactor.class);
        playersManager = mock(PlayersManager.class);
        stateManager = mock(StateManager.class);
        configurationManager = mock(ConfigurationManager.class);
        service = new MoveActionService(configurationManager, playersManager,stateManager, reactor);
    }


    @Test
    public void test001_moveAction() {
        Player player = new Player(PLAYER_ID, PLAYER_NAME);
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID), eq(PLAYER_ID))).thenReturn(Flowable.just(player));
        when(playersManager.getLocation(eq(PLAYER_ID))).thenReturn(Optional.of(OLD_LOCATION));

        StateChangedEvent stateChangedEvent = EventFactory.stateChangedEvent(null, PLAYER_ID);
        PublishProcessor<Event> subject = PublishProcessor.create();
        when(reactor.events(any(Predicate.class))).thenReturn(subject);

        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                subject.onNext(invocationOnMock.getArgument(0));
                return null;
            }
        }).when(reactor).submitEvent(any(Event.class));
        service.doAction(EventFactory.moveAction(null, PLAYER_ID, DIRECTION))
                .subscribe(subscriber);
        subject.onNext(stateChangedEvent);
        subscriber.awaitTerminalEvent();
        subscriber.assertComplete();
        TestEventHelper.checkEventProduced(subscriber, LocationChangeEvent.class, e -> locationChangeEventMatch(e, PLAYER_ID, OLD_LOCATION, NEW_LOCATION));

    }

    @Test
    public void test() throws Exception {
        PublishProcessor<Integer> ints = PublishProcessor.create();

        Flowable obs = ints.doOnNext(i -> System.out.println("caught: " + i)).replay();
        ints.onNext(1);
        ints.onNext(2);
        Thread.sleep(1000L);
        obs.subscribe(o -> System.out.println("subscribed: " + o));

    }


    private boolean locationChangeEventMatch(Event event, String targetId, Location oldLocation, Location newLocation) {
        return event instanceof LocationChangeEvent && targetId.equals(((LocationChangeEvent) event).getTargetActorId()) && newLocation.equals(((LocationChangeEvent) event).getCurrentLocation()) && oldLocation.equals(((LocationChangeEvent) event).getPrevLocation());
    }
}