package com.shurfll.jpirates;

/**
 * Created by kakim on 28.10.2017.
 */
public class ActorException extends Exception {
    private String actorId;


    public ActorException(String actorId) {
        this.actorId = actorId;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }
}
