package com.shurfll.jpirates.component.npc;

import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.NPC;
import com.shurfll.jpirates.event.BattleLoseEvent;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import com.shurfll.jpirates.reactor.Reactor;
import com.shurfll.jpirates.telegram.ActionProducer;
import com.shurfll.jpirates.telegram.TelegramManagerImpl;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;

@Component
public class NPCManagerImpl implements NPCManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(NPCManagerImpl.class);

    private static final String HANDLER_URI = "NPCManager_handlerUri";

    private static final int MAX_NPC_COUNT = 5;

    @Autowired
    private PlayersManagerImpl playersManager;

    @Autowired
    private NPCGenerator npcGenerator;

    @Autowired
    private Reactor reactor;

    @Autowired
    private ActionProducer actionProducer;


    public void init() {

        reactor.registerHandler(BattleLoseEvent.class, this::handleBattleLose);

        List<NPC> npcs = playersManager.getAllNPC();

        for (NPC npc : npcs) {
            initNPC(init(npc));
        }

        for (int i = npcs.size(); i < MAX_NPC_COUNT; i++) {
            createAndLocateNPC();
        }

    }

    private void initNPC(NPC npc) {
        init(npc)
                .getActions().subscribe(actionProducer::submitAction, t -> LOGGER.error("Unexpected error", t));
    }

    private void createAndLocateNPC() {
        generate()
                .ifPresent(npc -> npc.getActions()
                        .subscribe(actionProducer::submitAction, t -> LOGGER.error("Unexpected error", t)));

    }

    private Optional<NPC> generate() {
        return npcGenerator.generate(playersManager::addActor, reactor.events());
    }

    private NPC init(NPC npc) {
        return npcGenerator.initNPC(npc, reactor.events());
    }


    private Flowable<Event> handleBattleLose(BattleLoseEvent battleLoseEvent) {
        Optional<Actor> target = playersManager.getPlayerByActorId(battleLoseEvent.getTargetActorId());
        if (target.isPresent() && target.get() instanceof NPC) {
            playersManager.removePlayer(battleLoseEvent.getTargetActorId());
            createAndLocateNPC();
        }
        return Flowable.empty();
    }

}
