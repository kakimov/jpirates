package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.Configuration;
import io.reactivex.Flowable;

import java.util.Optional;

public interface ConfigurationManager {
    Flowable<Configuration> configurationChange(String moduleId);
    Optional<Configuration> getConfiguration(String moduleId);
    void updateConfiguration(Configuration configuration);
}
