package com.shurfll.jpirates.entity;

/**
 * Created by kakim on 22.06.2017.
 */
public enum LevelUpMode {
    HEALTH, SPEED, ACCURACY
}
