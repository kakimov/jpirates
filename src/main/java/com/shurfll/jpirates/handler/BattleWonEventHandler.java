package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.event.BattleWonEvent;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;

@Component
public class BattleWonEventHandler implements EventHandler<BattleWonEvent> {

    private Reactor reactor;


    @Autowired
    public BattleWonEventHandler(Reactor reactor) {
        this.reactor = reactor;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(BattleWonEvent.class, this);
    }

    @Override
    public Flowable<Event> handle(BattleWonEvent event) {
        return Flowable.just(EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.BATTLE_WON, event.getLoserName(), event.getGoldEarned()));
    }
}
