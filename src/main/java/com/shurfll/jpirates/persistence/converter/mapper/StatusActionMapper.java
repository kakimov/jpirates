package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.StatusAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class StatusActionMapper extends AbstractEventMapper {

    public StatusActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return StatusAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return StatusAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.statusAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, StatusAction.SOURCE_ACTOR_ID));
    }


}
