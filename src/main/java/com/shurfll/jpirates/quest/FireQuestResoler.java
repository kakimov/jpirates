package com.shurfll.jpirates.quest;

import com.shurfll.jpirates.entity.Quest;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FireQuestResoler implements QuestTypeResolver {

    public static final QName TYPE = new QName(COMMON_QUEST_TYPE_RESOLVER_NS, "fire");


    private static final QName PRODUCED_TYPE = new QName(COMMON_QUEST_TYPE_RESOLVER_NS, "fireCommand");

    @Autowired
    private PlayersManagerImpl playersManager;

    @Override
    public QName getType() {
        return TYPE;
    }

    @Override
    public List<Quest> resolve(String actorId, String questId) {
        return playersManager.getNeighbours(actorId).stream()
                .map(actor -> new Quest(PRODUCED_TYPE, null, questId, actor.getName(), null, "/fire " + actor.getName())).collect(Collectors.toList());
    }
}
