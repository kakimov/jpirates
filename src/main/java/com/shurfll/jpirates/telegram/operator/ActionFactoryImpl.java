package com.shurfll.jpirates.telegram.operator;

import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.ActionResolver;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by k.akimov on 09.06.2017.
 */
public class ActionFactoryImpl implements ActionFactory{

	private List<ActionResolver> resolvers;

	public ActionFactoryImpl(List<ActionResolver> resolvers) {
		this.resolvers = resolvers;
	}

	@Override
	public Optional<ActionResolver> findResolver(List<Message> messages){
		return resolvers.stream()
				.sorted(Comparator.comparing(ActionResolver::getPriority))
				.filter(resolver -> resolver.canResolve(messages.get(0)))
				.findFirst();
	}

	@Override
	public List<ActionResolver> findResolvers(List<Message> messages){
		return resolvers.stream()
				.sorted(Comparator.comparing(ActionResolver::getPriority))
				.filter(resolver -> resolver.canResolve(messages.get(0)))
				.collect(Collectors.toList());
	}

	@Override
	public Optional<Action> map(List<Message> messages){
		return findResolver(messages)
				.flatMap(r -> r.resolve(messages));
	}
}
