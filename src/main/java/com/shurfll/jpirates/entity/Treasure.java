package com.shurfll.jpirates.entity;


import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;

public class Treasure {
    private String oid;
    private Map<QName, EntityAttribute> attributes = new HashMap<>();

    public Treasure(String oid) {
        this.oid = oid;
    }

    public Treasure(String oid, Map<QName, EntityAttribute> attributes) {
        this.oid = oid;
        this.attributes = attributes;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Map<QName, EntityAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<QName, EntityAttribute> attributes) {
        this.attributes = attributes;
    }
}
