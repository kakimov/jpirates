package com.shurfll.jpirates.entity;

import com.shurfll.jpirates.EntityAttributeContainer;

/**
 * Current state of a player
 * Created by shurfll on 22.05.17.
 */

public class State extends EntityAttributeContainer {

    private String oid;
    private String ownerOid;
    private Long lastFireTimestamp = 0l;



    public State(String oid, String ownerOid) {
        this.oid = oid;
        this.ownerOid = ownerOid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getOwnerOid() {
        return ownerOid;
    }

    public void setOwnerOid(String ownerOid) {
        this.ownerOid = ownerOid;
    }

    public Long getLastFireTimestamp() {
        return lastFireTimestamp;
    }

    public void setLastFireTimestamp(Long lastFireTimestamp) {
        this.lastFireTimestamp = lastFireTimestamp;
    }

}
