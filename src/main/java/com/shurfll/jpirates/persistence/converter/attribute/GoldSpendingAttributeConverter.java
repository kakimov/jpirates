package com.shurfll.jpirates.persistence.converter.attribute;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GoldSpendingAttributeConverter extends SimpleTypedValueConverter {
    @Autowired
    public GoldSpendingAttributeConverter(ValueConverter converter) {
        super(EntityAttributeType.GOLDSPENDING.name(), converter);
    }
}
