package com.shurfll.jpirates.action.service.player;


import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.ActionCommons;
import com.shurfll.jpirates.action.TypedActionMetaProcessor;
import com.shurfll.jpirates.action.service.ConfigurableActionService;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.MoveAction;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.ReplayProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.xml.namespace.QName;
import java.util.Optional;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class MoveActionService extends ConfigurableActionService<MoveAction> implements TypedActionMetaProcessor<MoveAction> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MoveActionService.class);

    private static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "MoveActionService";

    private PlayersManager playersManager;

    private StateManager stateManager;

    private Reactor reactor;

    private Long paramMoveDelay = null;

    public static final QName PARAM_MOVE_DELAY = new QName(HANDLER_URI, "moveDelay");

    @Autowired
    public MoveActionService(ConfigurationManager configurationManager, PlayersManager playersManager, StateManager stateManager, Reactor reactor) {
        super(configurationManager);
        this.playersManager = playersManager;
        this.stateManager = stateManager;
        this.reactor = reactor;
    }

    @Override
    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_MOVE_DELAY, Long.class);
    }

    @Override
    protected void configure(Optional<Configuration> nullableConfig) {
        LOGGER.debug("Configuration update: {}", nullableConfig);
        paramMoveDelay = ConfigurationHelper.getParamValue(nullableConfig, PARAM_MOVE_DELAY, Long.class, Commons.ACTION_DEFAULT_DELAY);
        LOGGER.info("Configuration updated. moveDelay={}", paramMoveDelay);
    }

    @Override
    protected String getHandlerUri() {
        return HANDLER_URI;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof MoveAction;
    }


    @Override
    public void calculateMeta(MoveAction action) {
        State state = stateManager.getStateByActorId(action.getSourceActorId()).blockingFirst();
        Long delay = Commons.ACTION_DEFAULT_DELAY;
        Long skill = ActorHelper.getActorSailingSkill(state);
        Long skillAdvance = Math.min((delay / 2), skill) * 1000;
        action.addMeta(ActionCommons.ACTION_DELAY_META_KEY, delay - skillAdvance);
    }

    @Override
    public Flowable<Event> doBeforeAction(MoveAction moveAction) {
        return Flowable.just(EventFactory.infoEvent(moveAction, moveAction.getSourceActorId(), LocalizationKeys.MOVE_EVENT_INTENT, moveAction.getDirection()));
    }


    @Override
    public Flowable<Event> doAction(MoveAction action) {
        return playersManager.getPlayerFlowable(action.getSourceActorId(), action.getSourceActorId())
                .flatMap(player -> processChangeLocation(action, player));
    }

    private Flowable<Event> processChangeLocation(MoveAction action, Actor player) {
        Optional<Location> currentLocation = playersManager.getLocation(player.getActorId());
        if (!currentLocation.isPresent()) return Flowable.empty();

        Location newLocation = LocationHelper.changeLocation(currentLocation.get(), action.getDirection());
        StateChangeEvent event = EventFactory.stateChangeEventReplace(action, player.getActorId(), ActorStateAttributes.ACTOR_LOCATION, newLocation);
        Event locationChangeEvent = createLocationChangeEvent(action, player, currentLocation.get(), newLocation);

        ReplayProcessor<Event> subject = ReplayProcessor.create();
        // TODO не нужно ли сделать takeUntil ?
        Flowable<Event> Flowable = reactor.events(e1 -> e1 instanceof StateChangedEvent && event.getOid().equals(e1.getCauseOid()))
                .take(1)
                .map(event1 -> locationChangeEvent);
        Flowable.subscribe(subject);

        reactor.submitEvent(event);
        return subject;
    }

    private Event createLocationChangeEvent(MoveAction action, Actor target, Location currentLocation, Location
            newLocation) {
        return EventFactory.locationChangeEvent(action, target.getActorId(), currentLocation, newLocation, action.getDirection());
    }


}
