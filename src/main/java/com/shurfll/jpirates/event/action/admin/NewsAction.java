package com.shurfll.jpirates.event.action.admin;

import com.shurfll.jpirates.event.action.AdminAction;

import javax.xml.namespace.QName;
import java.util.Map;

public class NewsAction extends AdminAction {

    private static final String QUALIFIER = "notification";
    public static final String TYPE = ACTION_BASE_NAMESPACE + "/" + QUALIFIER;
    public static final QName TEXT = attribute(TYPE, "text");

    public NewsAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, String text) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, TEXT, text);
    }

    public NewsAction(String oid, String causeOid, String sourceActorId, String text) {
        this(oid, causeOid, sourceActorId, null, text);
    }

    public String getText(){
        return getValue(TEXT,String.class).get();
    }
}
