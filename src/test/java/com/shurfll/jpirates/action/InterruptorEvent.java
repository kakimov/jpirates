package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.PersonalEvent;

/**
 * Created by kakim on 27.10.2017.
 */
public class InterruptorEvent extends PersonalEvent {
    public InterruptorEvent(String oid, String causeOid, String targetActorId) {
        super(oid, causeOid, targetActorId);
    }


}
