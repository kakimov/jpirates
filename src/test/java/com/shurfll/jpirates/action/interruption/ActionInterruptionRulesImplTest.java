package com.shurfll.jpirates.action.interruption;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.FireResult;
import com.shurfll.jpirates.event.action.player.MoveAction;
import com.shurfll.jpirates.event.action.player.RepairAction;
import com.shurfll.jpirates.event.action.player.SearchAction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.*;
import static com.shurfll.jpirates.event.EventFactory.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class ActionInterruptionRulesImplTest {

    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.action.interruption")
    static class TestConverterConfiguration{}

    private static final String OLGA="olga";
    private static final String IGOR="igor";

    @Autowired
    private ActionInterruptionRules rules;

    @Test
    public void test001_moveAction_interrupt() {
        // InfoEvent shouldn't interrupt moveAction
        assertFalse(rules.isInterruptedByEvent(move(OLGA), infoEvent(null, IGOR, LocalizationKeys.DAMAGE_DELIVERED)));
        assertFalse(rules.isInterruptedByEvent(move(IGOR), infoEvent(null, IGOR, LocalizationKeys.DAMAGE_DELIVERED)));

        // DamageReceivedEvent shouldn't interrupt moveAction
        assertTrue(rules.isInterruptedByEvent(move(OLGA), damageReceivedEvent(null, null, OLGA, new FireResult(1L))));
        assertFalse(rules.isInterruptedByEvent(move(IGOR), damageReceivedEvent(null, null, OLGA, new FireResult(1L))));
    }

    @Test
    public void test002_repairAction_interrupt() {
        // InfoEvent shouldn't interrupt moveAction
        assertFalse(rules.isInterruptedByEvent(repair(OLGA), infoEvent(null, IGOR, LocalizationKeys.DAMAGE_DELIVERED)));
        assertFalse(rules.isInterruptedByEvent(repair(IGOR), infoEvent(null, IGOR, LocalizationKeys.DAMAGE_DELIVERED)));

        // DamageReceivedEvent shouldn't interrupt moveAction
        assertTrue(rules.isInterruptedByEvent(repair(OLGA), damageReceivedEvent(null, null, OLGA, new FireResult(1L))));
        assertFalse(rules.isInterruptedByEvent(repair(IGOR), damageReceivedEvent(null, null, OLGA, new FireResult(1L))));
    }

    @Test
    public void test003_searchAction_interrupt() {
        // InfoEvent shouldn't interrupt moveAction
        assertFalse(rules.isInterruptedByEvent(search(OLGA), infoEvent(null, IGOR, LocalizationKeys.DAMAGE_DELIVERED)));
        assertFalse(rules.isInterruptedByEvent(search(IGOR), infoEvent(null, IGOR, LocalizationKeys.DAMAGE_DELIVERED)));

        // DamageReceivedEvent shouldn't interrupt moveAction
        assertTrue(rules.isInterruptedByEvent(search(OLGA), damageReceivedEvent(null, null, OLGA, new FireResult(1L))));
        assertFalse(rules.isInterruptedByEvent(search(IGOR), damageReceivedEvent(null, null, OLGA, new FireResult(1L))));
    }

    private MoveAction move(String actorId){
        return moveAction(null, actorId, Direction.NORTH);
    }

    private RepairAction repair(String actorId){
        return repairAction(null, actorId);
    }

    private SearchAction search(String actorId){
        return searchAction(null, actorId);
    }





}