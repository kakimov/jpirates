package com.shurfll.jpirates.action;

import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


public class TestActionService implements ActionService<TestAction> {
    @Override
    public boolean canHandle(Action a) {
        return a instanceof TestAction;
    }

    @Override
    public Flowable<Event> doAction(TestAction testAction) {
        return testAction.getEvent() != null ? Flowable.just(testAction.getEvent()) : Flowable.empty();
    }
}