package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.AttributeChangeMode;
import com.shurfll.jpirates.entity.GoodsAttributes;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.DamageReceivedEvent;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.reactor.Reactor;
import com.shurfll.jpirates.utils.Lock;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class DamageReceivedEventHandler extends AbstractSelfregisteringEventHandler<DamageReceivedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DamageReceivedEventHandler.class);

    private PlayersManager playersManager;

    private StateManager stateManager;

    private ConcurrentHashMap<String, Lock> targetsMonitor = new ConcurrentHashMap<>();

    @Autowired
    public DamageReceivedEventHandler(Reactor reactor, PlayersManager playersManager, StateManager stateManager) {
        super(reactor);
        this.playersManager = playersManager;
        this.stateManager = stateManager;
    }

    @Override
    public Class getEventClass() {
        return DamageReceivedEvent.class;
    }

    @Override
    public Flowable<Event> handle(DamageReceivedEvent event) {
        try {
            lock(event.getTargetActorId());
        }catch (InterruptedException e){
            unlock(event.getTargetActorId());
            return Flowable.error(e);
        }
        return Flowable.zip(playersManager.getPlayerFlowable(event.getSourceActorId()),
                playersManager.getPlayerFlowable(event.getTargetActorId()),
                stateManager.getStateByActorId(event.getTargetActorId()),
                (actor, target, targetState) -> processInternal(event, actor, target, targetState))
                .flatMap(events -> Flowable.fromIterable(events))
                .doOnError(e -> unlock(event.getTargetActorId()));

    }


    private void waitAndUnlock(StateChangeEvent event){
        getReactor().events(e1 -> e1 instanceof StateChangedEvent && event.getOid().equals(e1.getCauseOid()))
                .take(1)
                .doOnNext(e -> unlock(event.getTargetActorId()))
                .subscribe();
    }

    private void lock(String actorId) throws InterruptedException{
        Lock lock = targetsMonitor.computeIfAbsent(actorId, s -> Lock.newLock());
        if(lock.isLocked()){
            // спим
            while(true){
                LOGGER.debug("Actor state: {} is locked, thread {} is sleeping", actorId, Thread.currentThread().getName());
                Thread.sleep(100);
                if(!targetsMonitor.get(actorId).isLocked()){
                    break;
                }
            }
        }
        lock.lock();
        LOGGER.debug("Lock {} by thread {}", actorId, Thread.currentThread().getName());
    }

    private void unlock(String lockId){
        if(targetsMonitor.get(lockId).isLocked()){
            targetsMonitor.get(lockId).unlock();
        }
        LOGGER.debug("Unlock {} by thread {}", lockId, Thread.currentThread().getName());
    }


    private List<Event> processInternal(DamageReceivedEvent event, Actor actor, Actor target, State targetState) {
        List<Event> eventList = new ArrayList<>();
        Map<QName, Object> actorChanges = new HashMap<>();
        Map<QName, Object> targetChanges = new HashMap<>();

        if(!ActorHelper.isUpStatus(targetState)){
            return Collections.singletonList(EventFactory.infoEvent(event, actor.getActorId(), LocalizationKeys.DAMAGE_DELIVERED_ALREADT_DOWN));
        }

        Long targetHealth = targetState.getValue(ActorStateAttributes.ACTOR_HEALTH, Long.class).orElse(0L);

        targetChanges.put(ActorStateAttributes.ACTOR_HEALTH, event.getFireResult().getDamageMade() * -1);
        Long health = targetHealth - event.getFireResult().getDamageMade();
        StateChangeEvent targetChangeEvent = null;
        if (health <= 0) {
            Long goldEarned = ActorHelper.getActorGold(targetState);
            actorChanges.put(GoodsAttributes.GOLD, goldEarned);
            actorChanges.put(ActorStateAttributes.ACTOR_EXPERIENCE, Commons.DEFAULT_BATTLE_WON_EXPERIENCE);
            targetChanges.put(GoodsAttributes.GOLD, goldEarned * -1);
            targetChangeEvent = EventFactory.stateChangeEventModify(event, target.getActorId(), targetChanges);
            targetChangeEvent.getChanges().put(ActorStateAttributes.ACTOR_STATUS,new StateChangeEvent.Change("DOWN", AttributeChangeMode.REPLACE));
            eventList.add(targetChangeEvent);
            eventList.add(EventFactory.stateChangeEventModify(event, actor.getActorId(), actorChanges));
            eventList.add(EventFactory.battleWonEvent(event, actor, target, goldEarned));
            eventList.add(EventFactory.battleLoseEvent(event, target));
        } else {
            targetChangeEvent = EventFactory.stateChangeEventModify(event, target.getActorId(), targetChanges);
            eventList.add(targetChangeEvent);

        }
        waitAndUnlock(targetChangeEvent);
        return eventList;
    }
}
