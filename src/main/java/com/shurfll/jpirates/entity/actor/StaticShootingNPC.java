package com.shurfll.jpirates.entity.actor;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.action.service.player.FireActionService;
import com.shurfll.jpirates.event.*;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.FireAction;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class StaticShootingNPC extends NPC {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticShootingNPC.class);

    private LinkedHashMap<String, Actor> targets = new LinkedHashMap<>();
    private Long shootingTimeout = Commons.FIRE_NPC_DEFAULT_DELAY;
    private Long startDelay = Commons.FIRE_NPC_DEFAULT_START_DELAY;
    private Flowable<Action> actions;


    public StaticShootingNPC(String id, String name,Flowable<Event> events, Long shootingTimeout, Long startDelay) {
        super(null,id, name);
        this.shootingTimeout = shootingTimeout;
        this.startDelay = startDelay;
        actions = events.ofType(PersonalEvent.class)
                .filter(event -> event.getTargetActorId().equals(id))
                .doOnNext(event -> LOGGER.debug("Caught event: {}", event))
                .doOnNext(this::registerEvent)
                .filter(this::isTriggeringMeetEvent)
                .doOnNext(event -> LOGGER.debug("Prepare to shoot at: {}", ((MeetEvent)event).getObject()))
                .flatMap(this::shootingSequence)
                .doOnNext(shootAction -> LOGGER.debug("Shoot at: {}", ((FireAction)shootAction).getTargetName()))
                .takeUntil(lose(events));

    }

    private Flowable<Action> shootingSequence(Event event){
        return Flowable.interval(getInitialDelay(),shootingTimeout, TimeUnit.MILLISECONDS)
                .filter(aLong -> !targets.isEmpty())
                .map(this::shoot);
    }

    private long getInitialDelay() {
        return startDelay == 0 ? 0 : new Random().nextInt(startDelay.intValue());
    }

    /**
     * If this event should trigger a firing sequence
     * @param event
     * @return
     */
    private boolean isTriggeringMeetEvent(Event event){
        return event instanceof MeetEvent && targets.size()==1;
    }

    private void registerEvent(Event event){
        if(event instanceof MeetEvent) handleMeet((MeetEvent)event);
        if(event instanceof SailAwayEvent) handleSailAway((SailAwayEvent)event);
        if(event instanceof BattleWonEvent) handleBattleWon((BattleWonEvent) event);
    }

    public Flowable<Action> getActions() {
        return actions;
    }

    private Action shoot(long tick){
        return EventFactory.fireAction(null, this, targets.entrySet().iterator().next().getValue());
    }

    private Flowable<BattleLoseEvent> lose(Flowable<Event> events){
        return events.ofType(BattleLoseEvent.class).filter(event -> getActorId().equals(event.getTargetActorId()));
    }

    private Flowable<Event> handleMeet(MeetEvent event) {
        event.getObject().stream().filter(Player.class::isInstance).map(Player.class::cast).forEach(p -> targets.put(p.getActorId(),p));
        return Flowable.empty();
    }

    private Flowable<Event> handleSailAway(SailAwayEvent event) {
        event.getObject().stream().filter(Player.class::isInstance).map(Player.class::cast).map(Player::getActorId).forEach(targets::remove);
        return Flowable.empty();
    }

    private Flowable<Event> handleBattleWon(BattleWonEvent event) {
        targets.remove(event.getLoserId());
        return Flowable.empty();
    }

}
