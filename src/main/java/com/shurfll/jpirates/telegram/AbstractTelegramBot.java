package com.shurfll.jpirates.telegram;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

public abstract class AbstractTelegramBot implements TelegramBot {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTelegramBot.class);


    private static TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

    private TelegramLongPollingBot botIternal;

    public void init(FlowableEmitter subscriber) {
        try {
            botIternal = new TelegramLongPollingBotInternal(subscriber);
            telegramBotsApi.registerBot(botIternal);
        } catch (TelegramApiRequestException e) {
            subscriber.onError(e);
        }
    }

    public Integer sendMessage(Long chatId, String message, ReplyKeyboard replyKeyboard) throws TelegramApiException{
        SendMessage botMessage = new SendMessage() // Create a SendMessage object with mandatory fields
                .setChatId(chatId)
                .setReplyMarkup(replyKeyboard)
                .setText(message)
                .setParseMode("Markdown");

        Message mess = botIternal.sendMessage(botMessage); // Call method to send the message
        return mess.getMessageId();

    }

    public void removeKeyboard(Long chatId, Integer messageId) {
        try {
            EditMessageReplyMarkup editMessageReplyMarkup = new EditMessageReplyMarkup();
            editMessageReplyMarkup.setChatId(chatId);
            editMessageReplyMarkup.setMessageId(messageId);
            editMessageReplyMarkup.setReplyMarkup(new InlineKeyboardMarkup());
            botIternal.editMessageReplyMarkup(editMessageReplyMarkup);
        } catch (TelegramApiException e) {
            LOGGER.error("Unable to send message", e);
        }
    }

    @Override
    public Flowable<Update> messagesFlowable() {
        return Flowable.create(s -> init(s), BackpressureStrategy.BUFFER);
    }

    public abstract String getBotUsername();

    public abstract String getBotToken();

    private class TelegramLongPollingBotInternal extends TelegramLongPollingBot {
        private FlowableEmitter<Update> subscriber;

        public TelegramLongPollingBotInternal(FlowableEmitter<Update> subscriber) {
            this.subscriber = subscriber;
        }


        @Override
        public String getBotToken() {
            return AbstractTelegramBot.this.getBotToken();
        }

        @Override
        public void onUpdateReceived(Update update) {
            // We check if the update has a message and the message has text
            if ((hasMessage(update) || hasCallbackQuery(update)) && subscriber != null) {
                subscriber.onNext(update);
            }
        }

        private boolean hasMessage(Update u) {
            return u.hasMessage() && u.getMessage().hasText();
        }

        private boolean hasCallbackQuery(Update u) {
            return u.hasCallbackQuery() && u.getCallbackQuery().getData() != null;
        }

        @Override
        public String getBotUsername() {
            return AbstractTelegramBot.this.getBotUsername();
        }

        @Override
        public void onClosing() {
            if (subscriber != null) {
                subscriber.onComplete();
            }
        }
    }

}
