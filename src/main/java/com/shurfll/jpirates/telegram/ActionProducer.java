package com.shurfll.jpirates.telegram;

import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by k.akimov on 08.06.2017.
 */
@Component
public class ActionProducer {

	private PublishProcessor<Action> publishProcessor;

	@PostConstruct
	public void init(){
		publishProcessor = publishProcessor.create();
	}

	public Flowable<Action> actionFlowable(){
		return publishProcessor;
	}

	public void submitAction(Action action){
		publishProcessor.onNext(action);
	}
}
