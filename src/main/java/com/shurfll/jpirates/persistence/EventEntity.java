package com.shurfll.jpirates.persistence;

import java.util.List;

public class EventEntity extends AttributeContainer{

    private String causeOid;
    private String type;


    public EventEntity(String oid, String causeOid, String type, List<Attribute> attributes) {
        super(oid,attributes);
        this.causeOid = causeOid;
        this.type = type;
    }

    public EventEntity(String type, List<Attribute> attributes) {
        this(null,null,type,attributes);
    }

    public String getCauseOid() {
        return causeOid;
    }

    public String getType() {
        return type;
    }

}
