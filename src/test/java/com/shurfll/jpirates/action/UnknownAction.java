package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.action.Action;

import javax.xml.namespace.QName;
import java.util.Map;

public class UnknownAction extends Action {

    public UnknownAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta) {
        super(oid, causeOid, sourceActorId, meta);
    }

    public UnknownAction(String oid, String causeOid, String sourceActorId) {
        super(oid, causeOid, sourceActorId);
    }
}