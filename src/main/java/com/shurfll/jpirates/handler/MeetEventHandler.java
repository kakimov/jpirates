package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.MeetEvent;
import com.shurfll.jpirates.reactor.EventHandler;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MeetEventHandler implements EventHandler<MeetEvent> {

    private Reactor reactor;


    @Autowired
    public MeetEventHandler(Reactor reactor) {
        this.reactor = reactor;
    }

    @PostConstruct
    public void init() {
        reactor.registerHandler(MeetEvent.class, this);
    }

    @Override
    public Flowable<Event> handle(MeetEvent event) {
        return Flowable.just(EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.MEET, translateObjects(event.getObject())));
    }

    private String translateObjects(List<Object> objects) {
        return objects.stream().map(Object::toString).collect(Collectors.joining(", "));
    }
}
