package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.EntityAttributeType;
import org.springframework.stereotype.Component;

@Component
public class DirectionValueMapper extends AbstractValueMapper {
    public DirectionValueMapper() {
        super(EntityAttributeType.DIRECTION, Direction.class, o -> gson.toJson(o), s -> gson.fromJson(s, Direction.class));
    }
}
