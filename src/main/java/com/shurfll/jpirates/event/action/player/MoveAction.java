package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Created by shurfll on 22.05.17.
 */
public class MoveAction extends PlayerAction {

    private static final String QUALIFIER = "moveAction";
    public static final String TYPE = ACTION_BASE_NAMESPACE + "/" + QUALIFIER;

    public static final QName DIRECTION = attribute(TYPE, "direction");

    public MoveAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, Direction direction) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, DIRECTION, direction);
    }

    public MoveAction(String oid, String causeOid, String sourceActorId, Direction direction) {
        this(oid, causeOid, sourceActorId, null, direction);
    }

    public Direction getDirection() {
        return getValue(DIRECTION, Direction.class).get();
    }

}
