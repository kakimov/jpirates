package com.shurfll.jpirates;

import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.objectfactory.LongModificationComputer;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;

import static org.junit.Assert.*;

public class LongModificationComputerTest {

    private LongModificationComputer computer;
    private ObjectFactory objectFactory;

    @Before
    public void init(){
        computer = new LongModificationComputer();
        objectFactory = new ObjectFactory(null);
    }

    @Test
    public void canCompute() {
        EntityAttribute attrLong = objectFactory.createAttribute(null, new QName("test"), EntityAttributeType.LONG, 1L);
        EntityAttribute attrLoc = objectFactory.createAttribute(null, new QName("test"), EntityAttributeType.LOCATION, Commons.loc(0L,0L));
        assertTrue(computer.canCompute(attrLong));
        assertFalse(computer.canCompute(attrLoc));
    }

    @Test
    public void computeLong() {
        EntityAttribute attrLong = objectFactory.createAttribute(null, new QName("test"), EntityAttributeType.LONG, 1L);
        assertEquals(2L, computer.compute(attrLong,1L));
        assertEquals(-1L, computer.compute(attrLong,-2L));


        EntityAttribute attrPostProcess = objectFactory.createAttribute(null, new QName("test"), EntityAttributeType.LONG, 1L,o -> Math.max((Long)o,0L));
        assertEquals(2L, computer.compute(attrPostProcess,1L));
        assertEquals(0L, computer.compute(attrPostProcess,-2L));
    }
}