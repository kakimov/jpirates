package com.shurfll.jpirates.persistence.converter;

import com.google.gson.*;
import com.shurfll.jpirates.TestInstruments;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.converter.attribute.AbstractAttributeValueConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class EntityAttributeConverterTest {

    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.persistence.converter")
    static class TestConverterConfiguration{}

    @org.springframework.context.annotation.Configuration
    @ComponentScan(basePackages = "com.shurfll.jpirates.objectfactory")
    static class TestObjectFactoryConfiguration{}

    private static final String RESOURCE_DIR = "/persistence/converter";

    private EntityAttributeConverter converter;

    private Gson gson = new GsonBuilder().create();

    @Autowired
    private List<AbstractAttributeValueConverter> attributeConverters;

    @Before
    public void init() {
        converter = new EntityAttributeConverter(attributeConverters);
    }


    @Test
    public void test001_deserialize_primitives() {
        convertorValidator("testString")
                .apply(o -> attr(EntityAttributeType.STRING, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate("testString");

        convertorValidator("10")
                .apply(o -> attr(EntityAttributeType.LONG, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(10L);
    }

    @Test
    public void test002_deserialize_collection() throws Exception {
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "collection.json"))
                .apply(o -> attr(EntityAttributeType.COLLECTION, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(result -> {
                    List resultList = (List) result;
                    Assert.assertEquals(10L, resultList.get(0));
                    Assert.assertEquals("testString", resultList.get(1));
                    Assert.assertEquals(new Location(1L, 2L), resultList.get(2));
                });
    }

    @Test
    public void test003_deserialize_map() throws Exception {
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "map.json"))
                .apply(o -> attr(EntityAttributeType.MAP, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(result -> {
                    Map resultList = (Map) result;
                    Assert.assertEquals(10L, resultList.get("0"));
                    Assert.assertEquals("testString", resultList.get("1"));
                    Assert.assertEquals(new Location(1L, 2L), resultList.get("2"));
                });
    }

    @Test
    public void test004_deserialize_configuration() throws Exception {
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "configuration.json"))
                .apply(o -> attr(EntityAttributeType.CONFIGURATION, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(result -> {
                    Configuration conf = (Configuration)result;
                    Assert.assertEquals("testModule", conf.getModuleUri());

                    Map<QName, EntityAttribute> attrs  = conf.getAttributes();
                    QName attrName = new QName("testNamespace","name");
                    Assert.assertFalse(attrs.isEmpty());
                    Assert.assertTrue(attrs.containsKey(attrName));
                    EntityAttribute attr = attrs.get(attrName);
                    Assert.assertEquals(attrName,attr.getName());
                    Assert.assertEquals("entityId",attr.getEntityId());
                    Assert.assertEquals("attrId",attr.getId());
                    Assert.assertEquals(EntityAttributeType.LONG,attr.getType());

                    List<EntityAttributeValue> attrValues = attr.getValues();
                    Assert.assertEquals(1, attrValues.size());
                    EntityAttributeValue attrValue = attrValues.get(0);
                    Assert.assertEquals("valueId",attrValue.getId());
                    Assert.assertEquals("attrId",attrValue.getAttributeId());
                    Assert.assertEquals(10L,attrValue.getValue());
                });
    }

    @Test
    public void test005_deserialize_levelUpMode(){
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "levelUpMode.json"))
                .apply(o -> attr(EntityAttributeType.LEVELUPMODE, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(LevelUpMode.HEALTH);
    }

    @Test
    public void test006_deserialize_direction(){
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "direction.json"))
                .apply(o -> attr(EntityAttributeType.DIRECTION, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(Direction.EAST);
    }

    @Test
    public void test007_deserialize_goldSpending(){
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "goldSpending.json"))
                .apply(o -> attr(EntityAttributeType.GOLDSPENDING, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(GoldSpending.FIRE_POWER);
    }

    @Test
    public void test008_deserialize_treasure() throws Exception{
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "treasure.json"))
                .apply(o -> attr(EntityAttributeType.TREASURE, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(result -> {
                    Treasure treasure = (Treasure) result;
                    Assert.assertEquals("treasureOid", treasure.getOid());

                    Map<QName, EntityAttribute> attrs  = treasure.getAttributes();
                    QName attrName = new QName("testNamespace","name");
                    Assert.assertFalse(attrs.isEmpty());
                    Assert.assertTrue(attrs.containsKey(attrName));
                    EntityAttribute attr = attrs.get(attrName);
                    Assert.assertEquals(attrName,attr.getName());
                    Assert.assertEquals("entityId",attr.getEntityId());
                    Assert.assertEquals("attrId",attr.getId());
                    Assert.assertEquals(EntityAttributeType.LONG,attr.getType());

                    List<EntityAttributeValue> attrValues = attr.getValues();
                    Assert.assertEquals(1, attrValues.size());
                    EntityAttributeValue attrValue = attrValues.get(0);
                    Assert.assertEquals("valueId",attrValue.getId());
                    Assert.assertEquals("attrId",attrValue.getAttributeId());
                    Assert.assertEquals(10L,attrValue.getValue());
                });
    }

    @Test
    public void test009_deserialize_fireResult(){
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "fireResult.json"))
                .apply(o -> attr(EntityAttributeType.FIRE_RESULT, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(o -> {
                    Assert.assertEquals((Long)1L, ((FireResult)o).getDamageMade());
                });
    }


    @Test
    public void test010_deserialize_actor(){
        convertorValidator(TestInstruments.readResource(RESOURCE_DIR, "actor.json"))
                .apply(o -> attr(EntityAttributeType.ACTOR, o))
                .apply(o -> converter.deserializeEntityAttribute((Attribute) o))
                .apply(this::unpackEntityAttribute)
                .validate(o -> {
                    Actor actor =(Actor)o;
                    Assert.assertEquals("testId", actor.getActorId());
                    Assert.assertEquals("testName", actor.getName());
                });
    }

    @Test
    public void test011_serialize_primitives(){
        convertorValidator(entityAttribute(EntityAttributeType.STRING,"testValue"))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate("testValue");

        convertorValidator(entityAttribute(EntityAttributeType.LONG,10L))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate("10");
    }

    @Test
    public void test012_serialize_collection(){
        Object value = new Object[]{ 10L,"testString", new Location(1L,2L)};
        convertorValidator(entityAttribute(EntityAttributeType.COLLECTION, value))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "collection.json"), (String)result));
    }

    @Test
    public void test013_serialize_map(){
        Map<String, Object> map = new HashMap<>();
        map.put("0", 10L);
        map.put("1", "testString");
        map.put("2", new Location(1L,2L));
        convertorValidator(entityAttribute(EntityAttributeType.MAP, map))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "map.json"), (String)result));
    }

    @Test
    public void test014_serialize_configuration(){
        Configuration conf = new Configuration("testModule");
        QName name = new QName("testNamespace", "name");
        EntityAttribute attribute = AttributeHelper.createAttribute("entityId", "attrId", name, EntityAttributeType.LONG, AttributeHelper.createAttributeValue("valueId","attrId",  10L));
        conf.getAttributes().put(name,attribute );
        convertorValidator(entityAttribute(EntityAttributeType.CONFIGURATION, conf))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "configuration.json"), (String)result));
    }

    @Test
    public void test015_serialize_levelUpMode(){
        convertorValidator(entityAttribute(EntityAttributeType.LEVELUPMODE, LevelUpMode.HEALTH))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "levelUpMode.json"), (String)result));
    }

    @Test
    public void test016_serialize_direction(){
        convertorValidator(entityAttribute(EntityAttributeType.DIRECTION, Direction.EAST))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "direction.json"), (String)result));
    }

    @Test
    public void test017_serialize_goldSpending(){
        convertorValidator(entityAttribute(EntityAttributeType.GOLDSPENDING, GoldSpending.FIRE_POWER))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "goldSpending.json"), (String)result));
    }

    @Test
    public void test018_serialize_treasure() {
        Treasure treasure = new Treasure("treasureOid");
        QName name = new QName("testNamespace", "name");
        EntityAttribute attribute = AttributeHelper.createAttribute("entityId", "attrId", name, EntityAttributeType.LONG, AttributeHelper.createAttributeValue("valueId","attrId",  10L));
        treasure.getAttributes().put(name,attribute );

        convertorValidator(entityAttribute(EntityAttributeType.TREASURE,treasure))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "treasure.json"), (String)result));
    }

    @Test
    public void test019_serialize_fireResult(){
        convertorValidator(entityAttribute(EntityAttributeType.FIRE_RESULT,new FireResult(1l)))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "fireResult.json"), (String)result));
    }


    @Test
    public void test020_serialize_actor(){
        convertorValidator(entityAttribute(EntityAttributeType.ACTOR,new Player("testId", "testName")))
                .apply(o -> converter.serializeEntityAttribute(null,(EntityAttribute) o))
                .apply(this::unpackAttribute)
                .validate(result -> compareStringObjects(TestInstruments.readResource(RESOURCE_DIR, "actor.json"), (String)result));
    }

    private void compareStringObjects(String expected, String actual){
        JsonParser parser = new JsonParser();
        JsonElement jsonExpected = parser.parse(expected);
        JsonElement jsonActual = parser.parse(actual);
        Assert.assertEquals(jsonExpected, jsonActual);
    }

    public static boolean compareJson(JsonElement json1, JsonElement json2) {
        boolean isEqual = true;
        // Check whether both jsonElement are not null
        if(json1 !=null && json2 !=null) {

            // Check whether both jsonElement are objects
            if (json1.isJsonObject() && json2.isJsonObject()) {
                Set<Map.Entry<String, JsonElement>> ens1 = ((JsonObject) json1).entrySet();
                Set<Map.Entry<String, JsonElement>> ens2 = ((JsonObject) json2).entrySet();
                JsonObject json2obj = (JsonObject) json2;
                if (ens1 != null && ens2 != null && (ens2.size() == ens1.size())) {
                    // Iterate JSON Elements with Key values
                    for (Map.Entry<String, JsonElement> en : ens1) {
                        isEqual = isEqual && compareJson(en.getValue() , json2obj.get(en.getKey()));
                    }
                } else {
                    return false;
                }
            }

            // Check whether both jsonElement are arrays
            else if (json1.isJsonArray() && json2.isJsonArray()) {
                JsonArray jarr1 = json1.getAsJsonArray();
                JsonArray jarr2 = json2.getAsJsonArray();
                if(jarr1.size() != jarr2.size()) {
                    return false;
                } else {
                    int i = 0;
                    // Iterate JSON Array to JSON Elements
                    for (JsonElement je : jarr1) {
                        isEqual = isEqual && compareJson(je , jarr2.get(i));
                        i++;
                    }
                }
            }

            // Check whether both jsonElement are null
            else if (json1.isJsonNull() && json2.isJsonNull()) {
                return true;
            }

            // Check whether both jsonElement are primitives
            else if (json1.isJsonPrimitive() && json2.isJsonPrimitive()) {
                if(json1.equals(json2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if(json1 == null && json2 == null) {
            return true;
        } else {
            return false;
        }
        return isEqual;
    }


    private Object unpackEntityAttribute(Object ea){
        return ((EntityAttribute)ea).getValue().map(val -> val.getValue()).orElseThrow(() -> new IllegalArgumentException("no value"));
    }

    private Object unpackAttribute(Object ea){
        return ((Attribute)ea).getValue();
    }

    private static Attribute attr(EntityAttributeType type, Object value) {
        return new Attribute(null, null, "testNamespace", "testName", type.name(), (String) value);
    }

    private static EntityAttribute entityAttribute(EntityAttributeType type, Object value) {
        return AttributeHelper.createAttribute(null, null, new QName("test","test"), type, value);
    }

    private static ConvertorValidator convertorValidator(Object obj) {
        return new ConvertorValidator(obj);
    }

    private static class ConvertorValidator {
        private Object result;

        public ConvertorValidator(Object init) {
            this.result = init;
        }

        public ConvertorValidator apply(Function<Object, Object> function) {
            return new ConvertorValidator(function.apply(result));
        }

        public void validate(Object expected) {
            Assert.assertEquals(expected, result);
        }

        public void validate(Consumer<Object> matcher) {
            matcher.accept(result);
        }
    }

}