package com.shurfll.jpirates.event.action;

import javax.xml.namespace.QName;
import java.util.Map;

public abstract class AdminAction extends Action {
    public AdminAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta) {
        super(oid, causeOid, sourceActorId, meta);
    }

    public AdminAction(String oid, String causeOid, String sourceActorId) {
        super(oid, causeOid, sourceActorId);
    }
}
