package com.shurfll.jpirates.action.interruption.rule;

import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.RepairAction;
import org.springframework.stereotype.Component;

import static com.shurfll.jpirates.action.interruption.rule.RuleUtils.*;

@Component
public class RepairActionInterruptionRule  extends ActionInterruptionRule<RepairAction>{

    public RepairActionInterruptionRule() {
        addInterruptionRule((a, e) -> isPersonalEvent(e) && isEventTargetMe(a,e) && isDamageReceivedEvent(e));
    }

    @Override
    public boolean canHandle(Action action) {
        return action instanceof RepairAction;
    }


}
