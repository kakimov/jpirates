package com.shurfll.jpirates.objectfactory;

import com.shurfll.jpirates.entity.EntityAttribute;

public interface ModificationComputer {
    boolean canCompute(EntityAttribute attribute);
    Object compute(EntityAttribute attribute, Object value);
}
