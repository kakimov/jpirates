package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.effect.AbsoluteActionBlockingEffect;
import com.shurfll.jpirates.effect.EffectManager;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.BattleLoseEvent;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.system.EmergeEvent;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class BattleLoseEventHandlerTest {

    private BattleLoseEventHandler handler;


    private EffectManager effectManager;
    private PlayersManager playersManager;
    private Reactor reactor;
    private ConfigurationManager configurationManager;
    private PublishProcessor<Event> publisher;
    private ObjectFactory objectFactory;
    private Location restPlace = new Location(10l, 10l);

    @Before
    public void init() {
        effectManager = mock(EffectManager.class);
        playersManager = mock(PlayersManager.class);
        reactor = mock(Reactor.class);
        publisher = PublishProcessor.create();
        when(reactor.events()).thenReturn(publisher);
        objectFactory = spy(new ObjectFactory(Collections.emptyList()));
        configurationManager = mock(ConfigurationManager.class);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.empty());
        when(configurationManager.configurationChange(anyString())).thenReturn(Flowable.empty());
        handler = new BattleLoseEventHandler(reactor, configurationManager, effectManager, playersManager, objectFactory);
    }

    @Test
    public void test001_init_noconfig() {
        handler.init();
        assertEquals(Commons.ACTION_BATTLE_LOSE_DEFAULT_DELAY, handler.timeoutParam);
    }

    @Test
    public void test002_init_config_without_attr() {
        Configuration config = config(new QName("unknown", "name"), EntityAttributeType.LONG, 1L);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.of(config));
        handler.init();
        assertEquals(Commons.ACTION_BATTLE_LOSE_DEFAULT_DELAY, handler.timeoutParam);
    }

    @Test
    public void test003_init_config_with_attr() {
        Configuration config = config(BattleLoseEventHandler.PARAM_TIMEOUT, EntityAttributeType.LONG, 1L);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.of(config));
        handler.init();
        assertEquals(Long.valueOf(1l), handler.timeoutParam);
    }

    @Test
    public void test004_update_config() {
        Configuration config = config(BattleLoseEventHandler.PARAM_TIMEOUT, EntityAttributeType.LONG, 1L);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.of(config));
        PublishProcessor<Configuration> configurationPublishProcessor = PublishProcessor.create();
        when(configurationManager.configurationChange(anyString())).thenReturn(configurationPublishProcessor);
        handler.init();
        assertEquals(Long.valueOf(1l), handler.timeoutParam);
        Location loc = Commons.generateLocation();
        Configuration conf = config(BattleLoseEventHandler.PARAM_TIMEOUT, EntityAttributeType.LONG, 2L);
        config(conf,BattleLoseEventHandler.PARAM_REST_PLACE,EntityAttributeType.LOCATION, loc);
        configurationPublishProcessor.onNext(conf);
        assertEquals(Long.valueOf(2l), handler.timeoutParam);
        assertEquals(loc, handler.restPlaceParam);
    }

    @Test
    public void test005_handle() {
        Configuration config = config(BattleLoseEventHandler.PARAM_TIMEOUT, EntityAttributeType.LONG, 1L);
        addAttribute(config, BattleLoseEventHandler.PARAM_REST_PLACE, EntityAttributeType.LOCATION, restPlace);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.of(config));
        final String targetOid = "targetOid";
        when(playersManager.getPlayerFlowable(eq(targetOid))).thenReturn(Flowable.just(new Player(targetOid, targetOid)));
        handler.init();
        BattleLoseEvent event = new BattleLoseEvent("oid", "cause_oid", targetOid);
        Location randomLocation = new Location(5l, 9l);
        when(objectFactory.generateLocation()).thenReturn(randomLocation);
        TestSubscriber<Event> subscriber = TestSubscriber.create();
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        verify(effectManager, times(1)).addEffect(eq(targetOid), any(AbsoluteActionBlockingEffect.class));
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, ev -> isLocationStateChangeEvent((StateChangeEvent) ev, targetOid, restPlace));
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, ev -> isLocationStateChangeEvent((StateChangeEvent) ev, targetOid, randomLocation));
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, ev -> isAwakeEvent((InfoEvent) ev, targetOid));
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, ev -> isBattleLoseEvent((InfoEvent) ev, targetOid));
        TestEventHelper.checkEventProduced(subscriber, EmergeEvent.class, ev -> isEmergeEvent(ev, targetOid, randomLocation));

    }

    private boolean isBattleLoseEvent(InfoEvent ev, String targetOid) {
        return ev.getTargetActorId().equals(targetOid) && LocalizationKeys.BATTLE_LOSE.equals(ev.getMessage());
    }

    private boolean isAwakeEvent(InfoEvent ev, String targetOid) {
        return ev.getTargetActorId().equals(targetOid) && LocalizationKeys.BATTLE_LOSE_AWAKE_EVENT.equals(ev.getMessage());
    }

    private boolean isLocationStateChangeEvent(StateChangeEvent ev, String targetOid, Location location) {
        return ev.getTargetActorId().equals(targetOid) && locationChangeEvent(ev, location);
    }

    private boolean isEmergeEvent(Event ev, String targetOid, Location location) {
        return ev instanceof EmergeEvent && ((EmergeEvent) ev).getTargetActorId().equals(targetOid) && ((EmergeEvent) ev).getLocation().equals(location);
    }

    private Configuration config(QName attr, EntityAttributeType type, Object value) {
        Configuration conf = new Configuration("configOid");
        conf.getAttributes().put(attr, AttributeHelper.createAttribute("configOid", attr, type, value));
        return conf;
    }

    private Configuration config(Configuration conf,QName attr, EntityAttributeType type, Object value) {
        conf.getAttributes().put(attr, AttributeHelper.createAttribute("configOid", attr, type, value));
        return conf;
    }

    private void addAttribute(Configuration conf, QName attr, EntityAttributeType type, Object value) {
        conf.getAttributes().put(attr, AttributeHelper.createAttribute("configOid", attr, type, value));
    }

    private boolean locationChangeEvent(StateChangeEvent event, Location location) {
        return event.getChanges().entrySet().stream().anyMatch(entry -> ActorStateAttributes.ACTOR_LOCATION.equals(entry.getKey()) && location.equals(entry.getValue().getValue()));
    }
}