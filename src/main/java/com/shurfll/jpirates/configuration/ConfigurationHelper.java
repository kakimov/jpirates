package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.AttributeHelper;
import com.shurfll.jpirates.entity.Configuration;

import javax.xml.namespace.QName;
import java.util.Optional;

public class ConfigurationHelper {
    public static <V> V getParamValue(Optional<Configuration> conf, QName attr, Class<V> clazz, V defaultValue) {
        return conf.map(configuration -> configuration.getAttributes())
                .flatMap(attrs -> AttributeHelper.getValue(attrs, attr, clazz))
                .orElse(defaultValue);
    }

    public static <V> Optional<V> getParamValue(Optional<Configuration> conf, QName attr, Class<V> clazz) {
        return conf.map(configuration -> configuration.getAttributes())
                .flatMap(attrs -> AttributeHelper.getValue(attrs, attr, clazz));
    }

    public static <V> boolean hasParam(Optional<Configuration> conf, QName attr, Class<V> clazz) {
        return getParamValue(conf, attr, clazz).isPresent();
    }
}
