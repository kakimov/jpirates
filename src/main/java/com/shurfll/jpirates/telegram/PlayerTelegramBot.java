package com.shurfll.jpirates.telegram;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Qualifier("player")
public class PlayerTelegramBot extends AbstractTelegramBot {

    @Value("${player.bot.username}")
    private String botUsername;
    @Value("${player.bot.token}")
    private String botToken;

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
