package com.shurfll.jpirates.persistence;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "actor",indexes = {
        @Index(columnList = "type", name = "idx_actor_type")
})
public class ActorEntity {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(length = 32)
    private String oid;

    private String actorId;

    private String name;

    @Column(length = 32)
    private String type;

    public ActorEntity() {
    }

    public ActorEntity(String oid, String actorId,String type, String name) {
        this.oid = oid;
        this.actorId = actorId;
        this.type = type;
        this.name = name;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
