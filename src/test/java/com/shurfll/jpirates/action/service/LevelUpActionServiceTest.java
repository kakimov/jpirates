package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.LevelUpActionService;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.player.LevelUpAction;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class LevelUpActionServiceTest {
    private LevelUpActionService service;
    private ConfigurationManager configurationManager;
    private StateManager stateManager;
    private TestSubscriber<Event> subscriber;
    private PublishProcessor<Configuration> configurations;
    private ObjectFactory objectFactory;
    private final String PLAYER_ID = "playerId";

    @Before
    public void init() {
        objectFactory = new ObjectFactory();
        configurationManager = mock(ConfigurationManager.class);
        stateManager = mock(StateManager.class);
        service = new LevelUpActionService(configurationManager, stateManager);
        subscriber = TestSubscriber.create();
        configurations = PublishProcessor.create();
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.empty());
        when(configurationManager.configurationChange(anyString())).thenReturn(configurations);
        service.init();
    }

    @Test
    public void test001_updateConfiguration() {
        assertEquals(Commons.LEVEL_UP_TOTAL_HEALTH_STEP, service.getTotalHealthIncreaseStep());
        final Long newValue = 10l;
        configurations.onNext(createConfiguration("test", newValue));
        assertEquals(newValue, service.getTotalHealthIncreaseStep());
    }

    @Test
    public void test002_no_levelup_points() {
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createState(0L)));
        LevelUpAction action = new LevelUpAction(null, null, PLAYER_ID, LevelUpMode.HEALTH);
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertValueCount(1);
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.LEVEL_UP_NO_POINTS));
    }

    @Test
    public void test003_levelup_health() {
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createState(1L)));
        LevelUpAction action = new LevelUpAction(null, null, PLAYER_ID, LevelUpMode.HEALTH);
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.LEVEL_UP_DONE));
        Map<QName, Object> expectedChanges = new HashMap<>();
        expectedChanges.put(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, -1L);
        expectedChanges.put(ActorStateAttributes.ACTOR_TOTAL_HEALTH, Commons.LEVEL_UP_TOTAL_HEALTH_STEP);
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, e -> TestEventHelper.stateChangeEventMatched(e, PLAYER_ID, expectedChanges));
    }

    @Test
    public void test004_levelup_sailing() {
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createState(1L)));
        LevelUpAction action = new LevelUpAction(null, null, PLAYER_ID, LevelUpMode.SPEED);
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.LEVEL_UP_DONE));
        Map<QName, Object> expectedChanges = new HashMap<>();
        expectedChanges.put(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, -1L);
        expectedChanges.put(ActorStateAttributes.ACTOR_SAILING_SKILL, 1L);
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, e -> TestEventHelper.stateChangeEventMatched(e, PLAYER_ID, expectedChanges));
    }

    @Test
    public void test005_levelup_accuracy() {
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createState(1L)));
        LevelUpAction action = new LevelUpAction(null, null, PLAYER_ID, LevelUpMode.ACCURACY);
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.LEVEL_UP_DONE));
        Map<QName, Object> expectedChanges = new HashMap<>();
        expectedChanges.put(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, -1L);
        expectedChanges.put(ActorStateAttributes.ACTOR_ACCURACY_SKILL, 1L);
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, e -> TestEventHelper.stateChangeEventMatched(e, PLAYER_ID, expectedChanges));
    }

    private Configuration createConfiguration(String oid, Long value) {
        Configuration conf = new Configuration(oid);
        conf.getAttributes().put(LevelUpActionService.PARAM_TOTAL_HEALTH_INCREASE_STEP, objectFactory.createAttribute(oid, LevelUpActionService.PARAM_TOTAL_HEALTH_INCREASE_STEP, value));
        return conf;
    }

    private State createState(Long points) {
        State state = objectFactory.createEmptyState(null, null);
        objectFactory.updateEntityAttribute(state, ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, AttributeChangeMode.REPLACE, points);
        return state;
    }
}