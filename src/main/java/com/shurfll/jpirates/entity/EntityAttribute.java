package com.shurfll.jpirates.entity;

import lombok.Getter;
import lombok.Setter;

import javax.xml.namespace.QName;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * In EAV concept this is Attribute
 */
@Getter
@Setter
public class EntityAttribute {
    private String id;
    private String entityId;
    private QName name;
    private EntityAttributeType type;
    private List<EntityAttributeValue> values;
    private List<Function<Object,Object>> postProcessors;

    public EntityAttribute(String id, String entityId, QName name, EntityAttributeType type, List<EntityAttributeValue> values) {
        this.id = id;
        this.entityId = entityId;
        this.name = name;
        this.type = type;
        this.values = values;
    }

    public EntityAttribute(String id, String entityId, QName name, EntityAttributeType type, List<EntityAttributeValue> values, List<Function<Object, Object>> postProcessors) {
        this(id,entityId,name,type,values);
        this.postProcessors = postProcessors;
    }

    public EntityAttribute(String id, String entityId, QName name, EntityAttributeType type, List<EntityAttributeValue> values, Function<Object, Object> postProcessor) {
        this(id,entityId,name,type,values);
        this.postProcessors = postProcessor !=null ? Collections.singletonList(postProcessor) : Collections.emptyList();
    }

    public Optional<EntityAttributeValue> getValue() {
        return values.stream().findFirst();
    }

    @Override
    public String toString() {
        return "EntityAttribute{" +
                "id='" + id + '\'' +
                ", entityId='" + entityId + '\'' +
                ", name=" + name +
                ", type=" + type +
                ", values=" + values +
                ", postProcessors=" + postProcessors +
                '}';
    }
}
