package com.shurfll.jpirates.entity;

/**
 * In EAV concept this is Value
 */
public class EntityAttributeValue {
    private String id;
    private String attributeId;
    private Object value;

    public EntityAttributeValue(String id, String attributeId, Object value) {
        this.id = id;
        this.attributeId = attributeId;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "EntityAttributeValue{" +
                "id='" + id + '\'' +
                ", attributeId='" + attributeId + '\'' +
                ", value=" + value +
                '}';
    }
}
