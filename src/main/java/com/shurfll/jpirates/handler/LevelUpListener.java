package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Component
public class LevelUpListener extends AbstractConfigurableSelfrefisteringEventHandler<StateChangedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LevelUpListener.class);
    private static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "LevelUpListener";
    public static final QName PARAM_LEVEL_UP_EXPERIENCE_BOUND = new QName(HANDLER_URI, "experienceBound");

    private StateManager stateManager;
    private Long experienceBound;

    @Autowired
    public LevelUpListener(Reactor reactor, ConfigurationManager configurationManager, StateManager stateManager) {
        super(reactor, configurationManager);
        this.stateManager = stateManager;
    }

    @Override
    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_LEVEL_UP_EXPERIENCE_BOUND, Long.class);
    }

    @Override
    protected void configure(Optional<Configuration> nullableConfig) {
        LOGGER.debug("Configuration update: {}", nullableConfig);
        experienceBound = ConfigurationHelper.getParamValue(nullableConfig, PARAM_LEVEL_UP_EXPERIENCE_BOUND, Long.class, Commons.LEVEL_EXPERIENCE_STEP);
        LOGGER.info("Configuration updated. experienceBound={}", experienceBound);
    }

    @Override
    protected String getHandlerUri() {
        return HANDLER_URI;
    }

    @Override
    public Class getEventClass() {
        return StateChangedEvent.class;
    }

    @Override
    public Flowable<Event> handle(StateChangedEvent event) {
        return stateManager.getStateByActorId(event.getTargetActorId())
                .flatMap(state -> checkActorExperience(event, state));
    }

    private Flowable<Event> checkActorExperience(StateChangedEvent event, State state) {
        Long level = ActorHelper.getActorLevel(state);
        Long experience = ActorHelper.getActorExperience(state);
        if (experience >= level * experienceBound) {
            Map<QName, Object> changes = new HashMap<>();
            changes.put(ActorStateAttributes.ACTOR_LEVEL, 1L);
            changes.put(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, 1L);
            return Flowable.just(EventFactory.stateChangeEventModify(event, event.getTargetActorId(), changes)
                    , EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.LEVEL_UP));
        }
        return Flowable.empty();
    }

    public Long getExperienceBound() {
        return experienceBound;
    }
}
