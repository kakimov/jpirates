package com.shurfll.jpirates.event;

import com.shurfll.jpirates.EntityAttributeContainer;
import lombok.Getter;
import lombok.Setter;

import javax.xml.namespace.QName;

/**
 * Created by shurfll on 25.05.17.
 */
public abstract class Event extends EntityAttributeContainer {

    private static final String EVENT_TO_STRING="%s{oid=%s,causeOid=%s}";

    protected static final String BASE_EVENT_NAMESPACE = "http://jpirates.org/event";

    /**
     * Unique event identificator
     */
    @Getter @Setter
    private String oid;

    /**
     * Cause for this event
     */
    @Getter
    private String causeOid;


    public Event(String oid, String causeOid) {
        this.oid = oid;
        this.causeOid = causeOid;
    }


    public static QName attribute(String namespace, String name) {
        return new QName(namespace, name);
    }

    @Override
    public String toString() {
        return String.format(EVENT_TO_STRING, super.toString(), oid,causeOid);
    }

}
