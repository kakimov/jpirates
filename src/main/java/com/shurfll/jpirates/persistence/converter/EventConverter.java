package com.shurfll.jpirates.persistence.converter;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.mapper.AbstractEventMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class EventConverter {

    private List<AbstractEventMapper> mappers = new ArrayList<>();

    private EntityAttributeConverter attributeConverter;

    public EventConverter(List<AbstractEventMapper> mappers, EntityAttributeConverter attributeConverter) {
        this.mappers = mappers;
        this.attributeConverter = attributeConverter;
    }

    public Optional<Event> convert(EventEntity entity){
        return mappers.stream().filter(mapper -> mapper.canHandle(entity))
                .findFirst()
                .map(mapper -> mapper.getEntityToEventFunction(entity));
    }

    public Optional<EventEntity> convert(Event event){
        return mappers.stream().filter(mapper -> mapper.canHandle(event))
                .findFirst()
                .map(mapper -> mapper.getEventToEntityFunction(event));
    }


}
