package com.shurfll.jpirates.action;

import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.FireAction;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import io.reactivex.Flowable;
import org.apache.http.util.Asserts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;

/**
 * The main goal is to prepare action, obtained from transport, to be executed
 * (count meta info and implicit params)
 * Created by kakim on 21.10.2017.
 */
@Component
public class ActionPreparerImpl implements ActionPreparer {

    private PlayersManagerImpl playersManager;

    private ActionMetaProcessor actionMetaProcessor;

    private List<TypedActionMetaProcessor> actionMetaProcessors;

    @Autowired
    public ActionPreparerImpl(PlayersManagerImpl playersManager, ActionMetaProcessor actionMetaProcessor, List<TypedActionMetaProcessor> actionMetaProcessors) {
        this.playersManager = playersManager;
        this.actionMetaProcessor = actionMetaProcessor;
        this.actionMetaProcessors = actionMetaProcessors;
    }

    @Override
    public Action prepare(Action action) {
        prepareAction(action);
        initMetaInfo(action);
        return action;
    }

    private Action prepareAction(Action t) {
        if (t instanceof FireAction && ((FireAction) t).getTargetName().isPresent()) {
            Optional<String> targetName = ((FireAction) t).getTargetName();
            Asserts.check(targetName.isPresent(), "No Target name of FireAction:" + t);
            Actor actor = playersManager.getPlayerByNameFlowable(t.getSourceActorId(),targetName.get()).blockingFirst();
            ((FireAction) t).setTargetId(actor.getActorId());
        }
        return t;
    }

    private Action initMetaInfo(final Action action) {
        actionMetaProcessor.calculateMeta(action);
        actionMetaProcessors.stream()
                .filter(p -> p.canHandle(action))
                .forEach(p -> p.calculateMeta(action));
        return action;
    }
}
