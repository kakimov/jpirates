package com.shurfll.jpirates.persistence.converter.attribute;

import com.google.gson.reflect.TypeToken;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.converter.attribute.util.TypeCollectionItem;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CollectionAttributeValueConverter extends AbstractAttributeValueConverter {

    private Type listType = new TypeToken<List<TypeCollectionItem>>() {}.getType();

    private ValueConverter  valueConverter;

    @Autowired
    public CollectionAttributeValueConverter(ValueConverter valueConverter) {
        super(EntityAttributeType.COLLECTION.name());
        this.valueConverter = valueConverter;
        setDeserializer(attr -> deserializeAttribute(attr, EntityAttributeType.COLLECTION, this::convertAttrToList));
        setSerializer((owner, attr) -> serializeAttribute(owner,attr, this::convertListToAttr));
    }

    private List convertAttrToList(Attribute attr) {
        List<TypeCollectionItem> items = gson.fromJson(attr.getValue(), listType);
        return items.stream().map(i -> i.buildObject(gson)).collect(Collectors.toList());
    }

    private String convertListToAttr(EntityAttributeValue value) {
        List<TypeCollectionItem> items = Stream.of((Object[]) value.getValue())
                .map(o -> new TypeCollectionItem(0, o, valueConverter.serialize(o)))
                .map(TypeCollectionItem.class::cast)
                .collect(Collectors.toList());

        for (int i = 0; i < items.size(); i++) {
            items.get(i).setIndex(i);
        }
        return gson.toJson(items);
    }
}
