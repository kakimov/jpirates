package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.action.Action;
import lombok.Getter;
import lombok.Setter;

import javax.xml.namespace.QName;
import java.util.Map;

@Getter
@Setter
public class TestLongAction extends Action {
    private TestEvent event;
    private long timeout;

    public TestLongAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, TestEvent event, long timeout) {
        super(oid, causeOid, sourceActorId, meta);
        this.event = event;
        this.timeout = timeout;
    }

    public TestLongAction(String oid, String causeOid, String sourceActorId, TestEvent event, long timeout) {
        super(oid, causeOid, sourceActorId);
        this.event = event;
        this.timeout = timeout;
    }
}
