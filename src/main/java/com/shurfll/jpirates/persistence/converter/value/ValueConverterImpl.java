package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ValueConverterImpl implements ValueConverter{

    private List<AbstractValueMapper> mappers;

    @Autowired
    public ValueConverterImpl(List<AbstractValueMapper> mappers){
        this.mappers = mappers;
    }

    private Optional<AbstractValueMapper> findMapper(EntityAttributeType type) {
        return mappers.stream().filter(mapper -> mapper.isApplicable(type)).findFirst();
    }

    private Optional<AbstractValueMapper> findMapper(Class clazz) {
        return mappers.stream().filter(mapper -> mapper.isApplicable(clazz)).findFirst();
    }

    @Override
    public <T> String serialize(T obj) {
        return findMapper(obj.getClass()).map(mapper -> mapper.serialize(obj)).orElseThrow(() -> new IllegalArgumentException("No mapper for object: " + obj));
    }

    @Override
    public <T> T deserialize(EntityAttributeType type, String obj) {
        return findMapper(type).map(mapper -> (T)mapper.deserialize(obj)).orElseThrow(() -> new IllegalArgumentException("No mapper for type: " + type));
    }
}
