package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.event.action.Action;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("admin")
public abstract class AdminActionService<T  extends Action> implements ActionService<T> {
}
