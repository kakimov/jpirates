package com.shurfll.jpirates.telegram.translator;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.telegram.event.QuestEvent;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;

import java.util.Optional;

/**
 * Created by kakim on 18.06.2017.
 */
@Component
public class QuestEventTranslator extends EventTranslator<QuestEvent>{
    @Override
    public boolean canTranslate(Event e) {
        return e instanceof QuestEvent;
    }

    @Override
    public String translate(QuestEvent e) {
        return e.getMessage();
    }

    @Override
    public Optional<ReplyKeyboard> useKeyboard(QuestEvent e) {
        return KeyboardHelper.getKeyboard(e.getKeyboardMode());
    }


}
