package com.shurfll.jpirates.manager;

import com.shurfll.jpirates.action.exception.IllegalActionException;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.Treasure;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.ActorType;
import com.shurfll.jpirates.entity.actor.NPC;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.persistence.dao.ActorDAO;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by shurfll on 26.05.17.
 */
@Component
public class PlayersManagerImpl implements PlayersManager {

    @Autowired
    private ObjectFactory objectFactory;

    @Autowired
    private StateManager stateManager;

    @Autowired
    private ActorDAO actorDAO;



//    private Set<Actor> actors = new HashSet<>();


    public Flowable<Actor> getPlayerByNameFlowable(String actorId, String name) {
        Optional<Actor> playerOpt = getActorByName(name);
        if (playerOpt.isPresent()) {
            return Flowable.just(playerOpt.get());
        } else {
            return Flowable.error(new IllegalActionException(actorId, "Unable to find player with name: " + name));
        }
    }

    private Optional<Actor> getActorByName(String name) {
        return actorDAO.findActorByName(name);
    }

    public Optional<Actor> getPlayerByActorId(String actorId) {
        return actorDAO.getByActorId(actorId);
    }

    public Optional<Actor> getPlayerByOid(String oid) {
        return actorDAO.getByOid(oid);
    }

    @Override
    public Flowable<Actor> getPlayerFlowable(String actorId) {
        return getPlayerFlowable(actorId, actorId);
    }

    @Override
    public Flowable<Actor> getPlayersFlowable() {
        return Flowable.fromIterable(actorDAO.getAllByType(ActorType.PLAYER));
    }


    public List<NPC> getAllNPC(){
        return actorDAO.getAllByType(ActorType.NPC);
    }

    @Override
    public Flowable<Actor> getPlayerFlowable(String actorId, String targetId) {
        Optional<Actor> playerOpt = getPlayerByActorId(targetId);
        if (playerOpt.isPresent()) {
            return Flowable.just(playerOpt.get());
        } else {
            return Flowable.error(new IllegalActionException(actorId, "Unable to find player with id: " + targetId));
        }
    }

    public boolean hasPlayer(String name) {
        return getActorByName(name).isPresent();
    }

    public void addPlayer(Player player) {
        Actor persistedPlayer = actorDAO.add(player);
        stateManager.updateState(objectFactory.createEmptyState(null, persistedPlayer.getOid()));
    }

    public void addPlayer(Player player, Location location) {
        Actor persistedPlayer = actorDAO.add(player);
        stateManager.updateState(objectFactory.createDefaultPlayerState(persistedPlayer.getOid(), location));
    }

    public void addActor(Actor player, State state) {
        Actor persistedPlayer = actorDAO.add(player);
        state.setOwnerOid(persistedPlayer.getOid());
        stateManager.updateState(state);
    }

    private Stream<Actor> getPlayersAtLocation(Location location) {
        return actorDAO.getActorsAtLocation(location).stream()
                .map(id -> getPlayerByOid(id).get());
    }

    public List<Actor> getNeighboursAtLocation(String actorId, Location location) {
        return Optional.ofNullable(location)
                .map(l -> this.getPlayersAtLocation(l))
                .orElse(Stream.empty())
                .filter(p -> !p.getActorId().equals(actorId))
                .collect(Collectors.toList());
    }

    public List<Actor> getNeighbours(String actorId) {
        return getLocation(actorId).map(location -> getNeighboursAtLocation(actorId, location)).orElse(Collections.EMPTY_LIST);
    }

    public void removePlayer(String actorId) {
        Actor p = getPlayerByActorId(actorId).get();
        stateManager.deleteState(p.getOid());
        actorDAO.delete(p.getOid());
    }

    @Override
    public Optional<Location> getLocation(String playerId) {
        return stateManager.getStateByActorId(playerId)
                .map(state -> state.getValue(ActorStateAttributes.ACTOR_LOCATION, Location.class))
                .blockingFirst();
    }

    public ObjectFactory getObjectFactory() {
        return objectFactory;
    }

    public void setObjectFactory(ObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
    }


}
