package com.shurfll.jpirates.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ComponentScan("com.shurfll.jpirates.persistence")
public class JPATest {
    @Autowired
    private TestRepository repo;

    @Autowired
    private TestComplexRepository complexRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void test001_getAll_noElems(){
        assertEquals(0,repo.findAll().size());
    }

    @Test
    public void test002_update(){
        TestEntity entity = new TestEntity(null, "testname");
        repo.save(entity);

        TestEntity repoEntity = repo.getOne(entity.getId());
        assertEquals("testname",repoEntity.getName());

        TestEntity updatedEntity = new TestEntity(entity.getId(),"newname");
        repo.save(updatedEntity);

        repoEntity = repo.getOne(entity.getId());
        assertEquals("newname",repoEntity.getName());
    }

    @Test
    public void test003_createComplex(){
        TestComplexEntity entity = new TestComplexEntity(null, "test");
        complexRepository.save(entity);

        TestComplexEntity persistedEntity = complexRepository.findById(entity.getId()).get();
        assertEquals("test", persistedEntity.getName());

        persistedEntity.getAttributeList().add(new TestAttribute(null, persistedEntity,"attr"));
        persistedEntity.setName("test2");
        complexRepository.save(entity);
        complexRepository.findAll();

        entityManager.find(TestAttribute.class, persistedEntity.getAttributeList().get(0).getId());

        persistedEntity = complexRepository.findById(entity.getId()).get();
        assertEquals("test2", persistedEntity.getName());
        assertEquals("attr", persistedEntity.getAttributeList().get(0).getValue());

    }

}
