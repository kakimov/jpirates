package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.MoveAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class MoveActionMapper extends AbstractEventMapper {

    public MoveActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return MoveAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return MoveAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.moveAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, MoveAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, MoveAction.DIRECTION));
    }


}
