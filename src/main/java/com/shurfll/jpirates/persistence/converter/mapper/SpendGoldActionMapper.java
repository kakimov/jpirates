package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.SpendGoldAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class SpendGoldActionMapper extends AbstractEventMapper {

    public SpendGoldActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return SpendGoldAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return SpendGoldAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.spendGoldAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, SpendGoldAction.SOURCE_ACTOR_ID)
                , mandatoryAttribute(e, SpendGoldAction.MODE));
    }


}
