package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.FireResult;
import com.shurfll.jpirates.entity.GoodsAttributes;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.*;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import com.shurfll.jpirates.reactor.ReactorImpl;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reactivestreams.Subscriber;

import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class DamageReceivedEventHandlerTest {

    private DamageReceivedEventHandler handler;
    private Reactor reactor;
    private PlayersManager playersManager;
    private StateManager stateManager;
    private ObjectFactory objectFactory;
    private PublishProcessor<Event> reactorProcessor;
    private TestSubscriber<Event> subscriber;

    private Actor actor;
    private String actorId = "actorId";
    private String actorName = "actorName";
    private Actor target;
    private String targetId = "targetId";
    private String targetName = "targetName";

    @Before
    public void init() {
        // init reactor
        reactorProcessor = PublishProcessor.create();
        reactor = spy(new ReactorImpl());
        ((ReactorImpl) reactor).init();

        playersManager = mock(PlayersManager.class);
        stateManager = mock(StateManager.class);
        handler = new DamageReceivedEventHandler(reactor, playersManager, stateManager);
        objectFactory = new ObjectFactory();

        // init actors
        actor = actor(actorId, actorName);
        target = actor(targetId, targetName);
        when(playersManager.getPlayerFlowable(eq(actorId))).thenReturn(Flowable.just(actor));
        when(playersManager.getPlayerFlowable(eq(targetId))).thenReturn(Flowable.just(target));

        // init handler
        handler.init();
    }

    @Test
    /**
     * Если пришло одновременно 2 события списания с состояния target, то они должны выполняться последовательно
     */
    public void test001_blocking_access_to_target_state() throws Exception {
        // Для target возвращаем состояние с 5 health и 10 gold
        when(stateManager.getStateByActorId(eq(targetId)))
                .thenReturn(Flowable.timer(100, TimeUnit.MILLISECONDS).map(t -> state(5L, 10L)));
        // Изменяет возвращаемое состояние target после StateChangedEvent
        doAnswer(i -> {
            when(stateManager.getStateByActorId(eq(targetId)))
                    .thenReturn(Flowable.timer(100, TimeUnit.MILLISECONDS).map(t -> state(0L, 0L)));
            return i.callRealMethod();
        }).when(reactor).submitEvent(any(StateChangedEvent.class));
        // генерируем StateChangedEvent на любой StateChangeEvent
        reactor.registerHandler(StateChangeEvent.class, e -> Flowable.just(EventFactory.stateChangedEvent(e, e.getTargetActorId())));

        List<Event> events = new ArrayList<>();
        reactor.events().subscribe(events::add);

        // происходят два асинхронных выстрела с мощностью 10
        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(() -> handler.handle(receivedEvent(10L)).subscribe(reactor::submitEvent));
        executor.submit(() -> handler.handle(receivedEvent(10L)).subscribe(reactor::submitEvent));
        executor.awaitTermination(500, TimeUnit.MILLISECONDS);

        // посчитаем количество событий, полученных из хендлеров

        TestEventHelper.checkEventProduced(events, BattleLoseEvent.class, event -> targetId.equals(((BattleLoseEvent) event).getTargetActorId()));
        TestEventHelper.checkEventProduced(events, BattleWonEvent.class, event -> actorId.equals(((BattleWonEvent) event).getTargetActorId()));
        TestEventHelper.checkEventProduced(events, StateChangeEvent.class, event -> stateChangeEventMatch((StateChangeEvent) event, targetId, ActorStateAttributes.ACTOR_HEALTH, -10L));
        executor.shutdown();
    }

    private Actor actor(String id, String name) {
        return new Player(id, name);
    }

    private State state(Long health, Long gold) {
        State state = objectFactory.createEmptyState(null, null);
        state.putAttribute(null, ActorStateAttributes.ACTOR_HEALTH, health);
        state.putAttribute(null, GoodsAttributes.GOLD, gold);
        state.putAttribute(null, ActorStateAttributes.ACTOR_STATUS, health <= 0 ? "DOWN" : "UP");
        return state;
    }

    private DamageReceivedEvent receivedEvent(Long damage) {
        return EventFactory.damageReceivedEvent(null, actorId, targetId, new FireResult(damage));
    }

    private boolean stateChangeEventMatch(StateChangeEvent event, String targetId, QName name, Object value) {
        return targetId.equals(event.getTargetActorId())
                && event.getChanges().containsKey(name)
                && value.equals(event.getChanges().get(name).getValue());
    }

    private boolean stateChangeEventMatch(StateChangeEvent event, String targetId, Map<QName, Object> changes) {
        return targetId.equals(event.getTargetActorId())
                && changes.entrySet().stream()
                .allMatch(entry -> event.getChanges().containsKey(entry.getKey())
                        && entry.getValue().equals(event.getChanges().get(entry.getKey()).getValue()));
    }

}