package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.action.Action;

public interface TypedActionMetaProcessor<T extends Action> {

    public boolean canHandle(Action action);

    public void calculateMeta(T action);
}
