package com.shurfll.jpirates.effect;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.*;

@Component
public class EffectManagerImpl implements EffectManager {

    protected Map<String, List<Effect>> effects = new HashMap<>();

    private EffectHandlerManager effectHandlerManager;

    @Autowired
    public EffectManagerImpl(EffectHandlerManager effectHandlerManager) {
        this.effectHandlerManager = effectHandlerManager;
    }

    @Override
    public Flowable<Event> handleAction(Action action) {
        return Flowable.fromIterable(effects.entrySet())
                .filter(entry -> action.getSourceActorId().equals(entry.getKey()))
                .map(entry -> entry.getValue())
                .flatMap(effects -> effectHandlerManager.calculateEffects(action, effects))
                .defaultIfEmpty(action);
    }

    @Override
    public void addEffect(String targetId, Effect effect) {
        effects.putIfAbsent(targetId, new ArrayList<>());
        effects.get(targetId).add(effect);
    }

    @Override
    public void removeEffect(String targetId, Effect effect) {
        effects.getOrDefault(targetId, new ArrayList<>()).remove(effect);
    }

    @Override
    public List<Effect> getEffects(String targetId) {
        return effects.getOrDefault(targetId, Collections.emptyList());
    }
}
