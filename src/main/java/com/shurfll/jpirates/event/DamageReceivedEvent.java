package com.shurfll.jpirates.event;

import com.shurfll.jpirates.entity.FireResult;

import javax.xml.namespace.QName;

/**
 * Created by kakim on 04.06.2017.
 */
public class DamageReceivedEvent extends PersonalEvent {

    private static final String QUALIFIER = "damageReceivedEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName FIRE_RESULT = attribute(TYPE, "fireResult");
    private static final QName SOURCE_ACTOR_ID = attribute(TYPE, "sourceActorId");

    public DamageReceivedEvent(String oid, String causeOid,String sourceActorId, String targetActorId, FireResult fireResult) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, SOURCE_ACTOR_ID, sourceActorId);
        putAttribute(oid, FIRE_RESULT, fireResult);
    }

    public FireResult getFireResult() {
        return getValue(FIRE_RESULT, FireResult.class).get();
    }

    public String getSourceActorId(){
        return getValue(SOURCE_ACTOR_ID, String.class).get();
    }


}
