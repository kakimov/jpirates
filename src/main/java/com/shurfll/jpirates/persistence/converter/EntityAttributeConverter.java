package com.shurfll.jpirates.persistence.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.AttributeContainer;
import com.shurfll.jpirates.persistence.converter.attribute.AbstractAttributeValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class EntityAttributeConverter {

    private List<AbstractAttributeValueConverter> attributeConverters;

    private static Gson gson = new GsonBuilder()
            .setLongSerializationPolicy(LongSerializationPolicy.STRING)
            .create();

    @Autowired
    public EntityAttributeConverter(List<AbstractAttributeValueConverter> attributeConverters) {
        this.attributeConverters = attributeConverters;
    }

    public EntityAttribute deserializeEntityAttribute(Attribute attr) {
        return findConverter(attr).map(conv -> conv.deserialize(attr)).orElseThrow(() -> new IllegalArgumentException("Unable to serialize attr: " + attr));
    }

    public Attribute serializeEntityAttribute(AttributeContainer owner, EntityAttribute attr) {
        return findConverter(attr).map(conv -> conv.serialize(owner, attr)).orElseThrow(() -> new IllegalArgumentException("Unable to serialize attr: " + attr));
    }


    private Optional<AbstractAttributeValueConverter> findConverter(Attribute attr) {
        return attributeConverters.stream().filter(conv -> conv.canHandle(attr)).findFirst();
    }

    private Optional<AbstractAttributeValueConverter> findConverter(EntityAttribute attr) {
        return attributeConverters.stream().filter(conv -> conv.canHandle(attr)).findFirst();
    }


}
