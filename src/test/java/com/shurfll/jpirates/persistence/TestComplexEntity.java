package com.shurfll.jpirates.persistence;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TestComplexEntity {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    private List<TestAttribute> attributeList = new ArrayList<>();

    public TestComplexEntity(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TestAttribute> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<TestAttribute> attributeList) {
        this.attributeList = attributeList;
    }


}
