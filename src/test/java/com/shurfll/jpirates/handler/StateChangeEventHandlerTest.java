package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.LongModificationComputer;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentCaptor;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class StateChangeEventHandlerTest {

    private static final String PLAYER_ID = "playerId";

    private StateChangeEventHandler handler;
    private PlayersManager playersManager;
    private StateManager stateManager;
    private ObjectFactory objectFactory;
    private Reactor reactor;
    private State state;
    private TestSubscriber<Event> subscriber;
    private Location location;
    private LongModificationComputer longComputer;


    @Before
    public void init() {
        reactor = mock(Reactor.class);
        stateManager = mock(StateManager.class);
        longComputer = new LongModificationComputer();
        objectFactory = new ObjectFactory(Collections.singletonList(longComputer));
        state = objectFactory.createEmptyState(null, null);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));
        when(stateManager.getStateByActorId(AdditionalMatchers.not(eq(PLAYER_ID)))).thenReturn(Flowable.empty());
        handler = new StateChangeEventHandler(reactor, stateManager, objectFactory);
        subscriber = TestSubscriber.create();
        location = Commons.loc(10L, 10L);
    }

    @Test
    public void test001_no_state_found() {
        handler.handle(EventFactory.stateChangeEventReplace(null, "unknown_pplayer", null))
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertNoValues();
        subscriber = TestSubscriber.create();
        handler.handle(EventFactory.stateChangeEventModify(null, "unknown_pplayer", null))
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertNoValues();
    }

    @Test
    public void test002_no_changes_shouldnt_change_state() {

        handler.handle(EventFactory.stateChangeEventReplace(null, PLAYER_ID, Collections.emptyMap()))
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertNoValues();
        verify(stateManager, never()).updateState(any(State.class));

    }

    @Test
    public void test003_long_modify() {
        ArgumentCaptor<State> stateCaptor = ArgumentCaptor.forClass(State.class);
        final Long defaultHealth = 100L;
        final String stateid = "someid";

        State state = objectFactory.createEmptyState(stateid, null);
        state.putAttribute(objectFactory.createAttribute(stateid, ActorStateAttributes.ACTOR_HEALTH, 10L));
        Long oldHealth = ActorHelper.getActorHealth(state);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));

        // 0 modification ->  nothing changed
        StateChangeEvent event = EventFactory.stateChangeEventModify(null, PLAYER_ID, ActorStateAttributes.ACTOR_HEALTH, 0l);
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        verify(stateManager, times(1)).updateState(stateCaptor.capture());
        assertEquals(Long.valueOf(10L), ActorHelper.getActorHealth(stateCaptor.getValue()));
        reset(stateManager);

        // minus modification
        stateCaptor = ArgumentCaptor.forClass(State.class);
        state = objectFactory.createEmptyState(stateid, null);
        state.putAttribute(objectFactory.createAttribute(stateid, ActorStateAttributes.ACTOR_HEALTH, 10L));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));
        subscriber = new TestSubscriber();
        event = EventFactory.stateChangeEventModify(null, PLAYER_ID, ActorStateAttributes.ACTOR_HEALTH, -1L );
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        // assert correct computation
        verify(stateManager, times(1)).updateState(stateCaptor.capture());
        assertEquals(Long.valueOf(9L), ActorHelper.getActorHealth(stateCaptor.getValue()));
        reset(stateManager);

        stateCaptor = ArgumentCaptor.forClass(State.class);
        state = objectFactory.createEmptyState(stateid, null);
        state.putAttribute(objectFactory.createAttribute(stateid, ActorStateAttributes.ACTOR_HEALTH, 0L));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));
        subscriber = new TestSubscriber();
        event = EventFactory.stateChangeEventReplace(null, PLAYER_ID, ActorStateAttributes.ACTOR_HEALTH, Commons.DEFAULT_HEALTH);
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        // assert correct computation
        verify(stateManager, times(1)).updateState(stateCaptor.capture());
        assertEquals(Commons.DEFAULT_HEALTH, ActorHelper.getActorHealth(stateCaptor.getValue()));
    }

    @Test
    public void test004_location_change() {
        ArgumentCaptor<State> stateCaptor = ArgumentCaptor.forClass(State.class);
        Location oldLocation = Commons.generateLocation();
        final String stateid = "someid";
        state = objectFactory.createEmptyState(stateid, null);
        state.putAttribute(objectFactory.createAttribute(stateid, ActorStateAttributes.ACTOR_LOCATION, oldLocation));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(state));

        Location newLocation = Commons.generateLocation();

        boolean a = true != false;

        StateChangeEvent event = EventFactory.stateChangeEventReplace(null, PLAYER_ID, ActorStateAttributes.ACTOR_LOCATION, newLocation);
        handler.handle(event)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        verify(stateManager, times(1)).updateState(stateCaptor.capture());
        assertEquals(newLocation, ActorHelper.getActorLocation(stateCaptor.getValue()));
    }

    @Test
    public void test005_stateChangedEventProduced() {
        TestSubscriber subscriber = new TestSubscriber();

        Location newLocation = Commons.generateLocation();

        StateChangeEvent event = EventFactory.stateChangeEventReplace(null, PLAYER_ID, ActorStateAttributes.ACTOR_LOCATION, newLocation);
        handler.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, StateChangedEvent.class, event1 -> stateChangedEventProduced(event1, event.getOid()));
    }

    private boolean stateChangedEventProduced(Event event, String causeOid) {
        return event instanceof StateChangedEvent && causeOid.equals(event.getCauseOid());
    }

}