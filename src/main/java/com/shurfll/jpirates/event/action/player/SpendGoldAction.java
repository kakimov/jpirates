package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.entity.GoldSpending;
import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Action of spending gold
 * Created by shurfll on 22.05.17.
 */
public class SpendGoldAction extends PlayerAction {
    private static final String QUALIFIER = "SpemdGoldAction";
    public static final String TYPE = ACTION_BASE_NAMESPACE + "/" + QUALIFIER;

    public static final QName MODE = attribute(TYPE, "mode");

    public SpendGoldAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, GoldSpending goldSpending) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, MODE, goldSpending);
    }

    public SpendGoldAction(String oid, String causeOid, String sourceActorId, GoldSpending goldSpending) {
        this(oid, causeOid, sourceActorId, null, goldSpending);
    }

    public GoldSpending getGoldSpending() {
        return getValue(MODE, GoldSpending.class).get();
    }

}
