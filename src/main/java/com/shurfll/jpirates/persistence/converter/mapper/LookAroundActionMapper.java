package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.LevelUpAction;
import com.shurfll.jpirates.event.action.player.LookAroundAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class LookAroundActionMapper extends AbstractEventMapper {

    public LookAroundActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return LookAroundAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return LookAroundAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return  EventFactory.lookAroundAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, LevelUpAction.SOURCE_ACTOR_ID));
    }


}
