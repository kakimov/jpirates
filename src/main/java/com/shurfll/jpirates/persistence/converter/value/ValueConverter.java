package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;

public interface ValueConverter {

    public <T> String serialize(T obj);

    public <T> T deserialize(EntityAttributeType type, String obj);
}
