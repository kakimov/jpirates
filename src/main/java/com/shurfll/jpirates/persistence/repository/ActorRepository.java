package com.shurfll.jpirates.persistence.repository;

import com.shurfll.jpirates.persistence.ActorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActorRepository extends JpaRepository<ActorEntity,String>, JpaSpecificationExecutor<ActorEntity> {

    @Query("select b from ActorEntity b where b.actorId = :actorId")
    Optional<ActorEntity> findByActorId(@Param("actorId") String actorId);

    @Query("select b from ActorEntity b where b.name = :name")
    Optional<ActorEntity> findByName(@Param("name") String name);

    @Query("select b from ActorEntity b where b.type = :type")
    List<ActorEntity> findAllByType(@Param("type") String type);

    @Query(value = "select a.oid from actor a join state s on s.owner_oid=a.oid join attribute att on att.owner_oid=s.oid where att.value=:loc", nativeQuery = true)
    List<String> findAllAtLocation(@Param("loc") String loc);
}
