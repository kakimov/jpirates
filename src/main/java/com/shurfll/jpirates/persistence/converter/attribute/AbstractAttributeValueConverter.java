package com.shurfll.jpirates.persistence.converter.attribute;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.shurfll.jpirates.entity.AttributeHelper;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.AttributeContainer;
import org.springframework.util.Assert;

import javax.xml.namespace.QName;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public abstract class AbstractAttributeValueConverter {
    protected static Gson gson = new GsonBuilder()
            .setLongSerializationPolicy(LongSerializationPolicy.STRING)
            .create();
    private String type;
    private Function<Attribute, EntityAttribute> deserializer;
    private BiFunction<AttributeContainer, EntityAttribute, Attribute> serializer;

    public AbstractAttributeValueConverter(String type) {
        this.type = type;
    }

    public boolean canHandle(Attribute attr) {
        return Objects.equals(type, attr.getType());
    }

    public boolean canHandle(EntityAttribute attr) {
        return Objects.equals(type, attr.getType().name());
    }

    public EntityAttribute deserialize(Attribute attr) {
        return deserializer.apply(attr);
    }

    public Attribute serialize(AttributeContainer owner, EntityAttribute attr) {
        return serializer.apply(owner, attr);
    }

    protected Function<Attribute, EntityAttribute> getDeserializer() {
        return deserializer;
    }

    protected void setDeserializer(Function<Attribute, EntityAttribute> deserializer) {
        this.deserializer = deserializer;
    }

    protected BiFunction<AttributeContainer, EntityAttribute, Attribute> getSerializer() {
        return serializer;
    }

    protected void setSerializer(BiFunction<AttributeContainer, EntityAttribute, Attribute> serializer) {
        this.serializer = serializer;
    }

    protected EntityAttribute deserializeAttribute(Attribute attr, EntityAttributeType type, Function<Attribute, Object> mapper) {
        return AttributeHelper.createAttribute(Optional.ofNullable(attr.getOwner()).map(AttributeContainer::getOid).orElse(null), attr.getOid(), new QName(attr.getNamespace(), attr.getName()), type, mapper.apply(attr));
    }

    protected Attribute serializeAttribute(AttributeContainer container, EntityAttribute attr, Function<EntityAttributeValue, String> valueMapper) {
        return createAttribute(container, attr, valueMapper);
    }


    private Attribute createAttribute(AttributeContainer container, EntityAttribute attr, Function<EntityAttributeValue, String> valueMapper) {
        Assert.isTrue(attr.getValues().size() == 1, "only singular values are supported for now");
        return new Attribute(attr.getId(), container, attr.getName().getNamespaceURI(), attr.getName().getLocalPart(), attr.getType().name(), attr.getValue().map(val -> valueMapper.apply(val)).orElseThrow(() -> new IllegalArgumentException("Illegal EntityAttribute: " + attr)));
    }
}