package com.shurfll.jpirates.event;

/**
 * Created by kakim on 04.06.2017.
 */
public class BattleLoseEvent extends PersonalEvent {
    private static final String QUALIFIER = "battleLoseEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    public BattleLoseEvent(String oid, String causeOid, String targetActorId) {
        super(oid, causeOid, targetActorId);
    }


}
