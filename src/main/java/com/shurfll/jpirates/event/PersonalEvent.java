package com.shurfll.jpirates.event;

import javax.xml.namespace.QName;

/**
 * Event with a specified target
 */
public abstract class PersonalEvent extends Event {

    private static final String PERSONAL_EVENT_NAMESPACE = "personal";

    private static final QName TARGET_ACTOR_ID = attribute(BASE_EVENT_NAMESPACE + "/" + PERSONAL_EVENT_NAMESPACE, "targetActorId");

    public PersonalEvent(String oid, String causeOid, String targetActorId) {
        super(oid, causeOid);
        putAttribute(oid, TARGET_ACTOR_ID, targetActorId);
    }

    public String getTargetActorId() {
        return getValue(TARGET_ACTOR_ID, String.class).get();
    }

}
