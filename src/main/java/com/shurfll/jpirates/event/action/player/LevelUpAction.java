package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.entity.LevelUpMode;
import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Action of spending gold
 * Created by shurfll on 22.05.17.
 */
public class LevelUpAction extends PlayerAction {
    private static final String QUALIFIER = "levelUpAction";
    public static final String TYPE = ACTION_BASE_NAMESPACE + "/" + QUALIFIER;

    public static final QName MODE = attribute(TYPE, "mode");

    public LevelUpAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, LevelUpMode levelUpMode) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, MODE, levelUpMode);
    }

    public LevelUpAction(String oid, String causeOid, String sourceActorId, LevelUpMode levelUpMode) {
        this(oid, causeOid, sourceActorId, null, levelUpMode);
    }

    public LevelUpMode getLevelUpMode() {
        return getValue(MODE, LevelUpMode.class).get();
    }

}
