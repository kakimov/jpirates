package com.shurfll.jpirates.effect;

import com.shurfll.jpirates.action.TestAction;
import com.shurfll.jpirates.event.action.Action;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import io.reactivex.subscribers.TestSubscriber;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EffectManagerImplTest {

    private EffectManagerImpl effectManager;
    private EffectHandlerManager handlerManager;

    @Before
    public void init(){
        handlerManager = Mockito.mock(EffectHandlerManager.class);
        effectManager = new EffectManagerImpl(handlerManager);
    }

    @Test
    public void test001_handleAction_noeffects() {
        Action action = new TestAction(null, null, "test", null);
        TestSubscriber subscriber = TestSubscriber.create();
        effectManager.handleAction(action).subscribe(subscriber);
        subscriber.assertValue(action);
    }

    @Test
    public void test002_addEffect(){
        Effect effect = Mockito.mock(Effect.class);
        effectManager.addEffect("id", effect);
        assertTrue(effectManager.effects.get("id").contains(effect));
    }

    @Test
    public void test003_removeEffect(){
        Effect effect = Mockito.mock(Effect.class);
        List<Effect> effectList = new ArrayList<>();
        effectList.add(effect);
        effectManager.effects.put("id", effectList);
        effectManager.removeEffect("id", effect);
        assertFalse(effectManager.effects.get("id").contains(effect));
    }
}