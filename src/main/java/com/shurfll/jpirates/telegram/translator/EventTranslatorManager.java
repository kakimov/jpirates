package com.shurfll.jpirates.telegram.translator;

import com.shurfll.jpirates.event.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;

import java.util.List;
import java.util.Optional;

/**
 * Created by kakim on 01.06.2017.
 */
@Component
public class EventTranslatorManager {

    @Autowired
    private List<EventTranslator> translators;

    public String translate(Event e){
        Optional<EventTranslator> translator =  translators.stream().filter(t -> t.canTranslate(e)).findFirst();
        if(translator.isPresent()){
            return translator.get().translate(e);
        }else{
            return e.toString();
        }
    }

    public Optional<ReplyKeyboard> useKeyboard(Event e){
        Optional<EventTranslator> translator =  translators.stream().filter(t -> t.canTranslate(e)).findFirst();
        if(translator.isPresent()){
            return translator.get().useKeyboard(e);
        }else{
            return Optional.empty();
        }
    }
}
