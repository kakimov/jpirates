package com.shurfll.jpirates.helper;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.entity.Direction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LocationHelperTest {

    @Test
    public void getRelativeDirection() {
        assertEquals(Direction.NORTH, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(1L,0L)));
        assertEquals(Direction.SOUTH, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(-1L,0L)));
        assertEquals(Direction.WEST, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(0L,-1L)));
        assertEquals(Direction.EAST, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(0L,1L)));
        assertEquals(Direction.NORTH_WEST, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(1L,-1L)));
        assertEquals(Direction.NORTH_EAST, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(1L,1L)));
        assertEquals(Direction.SOUTH_EAST, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(-1L,1L)));
        assertEquals(Direction.SOUTH_WEST, LocationHelper.getRelativeDirection(Commons.loc(0L,0L), Commons.loc(-1L,-1L)));
    }
}