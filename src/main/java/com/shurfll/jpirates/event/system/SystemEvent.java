package com.shurfll.jpirates.event.system;

import com.shurfll.jpirates.event.Event;

public abstract class SystemEvent extends Event {
    public SystemEvent(String oid, String causeOid) {
        super(oid, causeOid);
    }
}
