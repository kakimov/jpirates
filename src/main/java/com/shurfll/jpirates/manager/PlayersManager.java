package com.shurfll.jpirates.manager;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Treasure;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.NPC;
import com.shurfll.jpirates.entity.actor.Player;
import io.reactivex.Flowable;


import java.util.List;
import java.util.Optional;

public interface PlayersManager {

    List<NPC> getAllNPC();

    Flowable<Actor> getPlayersFlowable();

    Flowable<Actor> getPlayerFlowable(String actorId);

    Flowable<Actor> getPlayerFlowable(String actorId, String targetId);

    Optional<Location> getLocation(String playerId);



    List<Actor> getNeighboursAtLocation(String actorId, Location location);

    void removePlayer(String playerId);

    void addPlayer(Player player, Location location);

    public boolean hasPlayer(String name);
}
