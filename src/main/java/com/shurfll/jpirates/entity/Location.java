package com.shurfll.jpirates.entity;

/**
 * Created by shurfll on 22.05.17.
 */
public class Location {
    private Long latitude;
    private Long longitude;


    public Location(Long latitude, Long longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Location(Location location){
        this(location.getLatitude(),location.getLongitude());
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (longitude != location.longitude) return false;
        return latitude == location.latitude;

    }

    @Override
    public int hashCode() {
        int result = longitude.hashCode();
        result = 31 * result + latitude.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Location{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
