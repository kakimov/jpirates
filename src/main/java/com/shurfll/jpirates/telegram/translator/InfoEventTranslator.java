package com.shurfll.jpirates.telegram.translator;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.InfoEvent;
import org.springframework.stereotype.Component;

/**
 * Created by kakim on 01.06.2017.
 */
@Component
public class InfoEventTranslator extends PropertiesEventTranslator<InfoEvent> {

    private static final String FILE_NAME = "InfoEventTranslator.properties";

    @Override
    public boolean canTranslate(Event e) {
        return e.getClass().equals(InfoEvent.class);
    }

    @Override
    public String getTranslationByKey(InfoEvent infoEvent) {
        return props.getProperty(infoEvent.getMessage());
    }

    @Override
    public String getTranslatorFileName() {
        return FILE_NAME;
    }

    @Override
    public Object[] getArguments(InfoEvent infoEvent) {
        return infoEvent.getArgs();
    }
}
