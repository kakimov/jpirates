package com.shurfll.jpirates.stubs;

import com.shurfll.jpirates.telegram.TelegramBot;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import org.reactivestreams.Subscriber;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;


public class TelegramBotStub implements TelegramBot {

    private io.reactivex.Flowable<Update> Flowable;

    private FlowableEmitter subscriber;

    public TelegramBotStub() {
        Flowable = Flowable.create(subscriber -> TelegramBotStub.this.subscriber = subscriber, BackpressureStrategy.BUFFER);
    }

    @Override
    public Flowable<Update> messagesFlowable() {
        return Flowable;
    }

    public void onUpdateReceived(Update m){
        subscriber.onNext(m);
    }

    @Override
    public Integer sendMessage(Long chatId, String message, ReplyKeyboard replyKeyboard) {
        return 0;
    }

    @Override
    public void removeKeyboard(Long chatId, Integer messageId){

    }
}
