package com.shurfll.jpirates.telegram.operator.resolvers;

import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("admin")
public abstract class AdminActionResolver extends ActionResolver {
}
