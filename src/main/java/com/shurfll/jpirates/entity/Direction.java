package com.shurfll.jpirates.entity;

/**
 * Created by shurfll on 22.05.17.
 */
public enum Direction {
    NORTH,EAST,SOUTH,WEST,
    NORTH_WEST,NORTH_EAST,
    SOUTH_WEST,SOUTH_EAST
}
