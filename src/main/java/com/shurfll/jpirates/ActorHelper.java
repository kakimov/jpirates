package com.shurfll.jpirates;

import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.objectfactory.ObjectFactory;

public class ActorHelper {

    public static boolean isUpStatus(State actorState){
        return actorState.getValue(ActorStateAttributes.ACTOR_STATUS, String.class).map(status -> "UP".equals(status)).orElse(true);
    }

    public static boolean isSameLocation(State stateA, State stateB) {
        return stateA.getValue(ActorStateAttributes.ACTOR_LOCATION, Location.class).equals(stateB.getValue(ActorStateAttributes.ACTOR_LOCATION, Location.class));
    }

    public static Location getActorLocation(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_LOCATION, Location.class).orElseThrow(() -> new IllegalStateException("Actor has no LOCATION:" + actorState.getOid()));
    }

    public static Long getActorExperience(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_EXPERIENCE, Long.class).orElse(0L);
    }

    public static Long getActorLevel(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_LEVEL, Long.class).orElse(1L);
    }

    public static Long getActorLevelUpPoints(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, Long.class).orElse(0L);
    }

    public static Long getActorGold(State actorState) {
        return actorState.getValue(GoodsAttributes.GOLD, Long.class).orElse(0L);
    }

    public static void setActorGold(ObjectFactory objectFactory, State actorState, Long gold) {
        objectFactory.updateEntityAttribute(actorState, GoodsAttributes.GOLD, AttributeChangeMode.REPLACE, gold);
    }

    public static void setActorHeath(ObjectFactory objectFactory, State actorState, Long health) {
        objectFactory.updateEntityAttribute(actorState, ActorStateAttributes.ACTOR_HEALTH, AttributeChangeMode.REPLACE, health);
    }

    public static Long getActorHealth(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_HEALTH, Long.class).orElseThrow(() -> new IllegalStateException("Actor has no HEALTH attribute - strange: " + actorState.getOid()));
    }

    public static Long getActorTotalHealth(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_TOTAL_HEALTH, Long.class).orElseThrow(() -> new IllegalStateException("Actor has no TOTAL_HEALTH attribute - strange: " + actorState.getOid()));
    }

    public static void setActorTotalHeath(ObjectFactory objectFactory, State actorState, Long health) {
        objectFactory.updateEntityAttribute(actorState, ActorStateAttributes.ACTOR_TOTAL_HEALTH, AttributeChangeMode.REPLACE, health);
    }

    public static Long getActorFirePower(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_FIRE_POWER, Long.class).orElse(0L);
    }

    public static boolean isFullyRepaired(State actorState) {
        return getActorHealth(actorState) >= getActorTotalHealth(actorState);
    }

    public static Long getActorHealthRepairingSpeed(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_HEALTH_REPAIRING_SPEED, Long.class).orElseThrow(() -> new IllegalStateException("Actor has no REPAIRING SPEED:" + actorState.getOid()));
    }

    public static void setActorHealthRepairingSpeed(ObjectFactory objectFactory, State actorState, Long value) {
        objectFactory.updateEntityAttribute(actorState, ActorStateAttributes.ACTOR_HEALTH_REPAIRING_SPEED, AttributeChangeMode.REPLACE, value);
    }

    public static void setActorHealthRepairingAmount(ObjectFactory objectFactory, State actorState, Long value) {
        objectFactory.updateEntityAttribute(actorState, ActorStateAttributes.ACTOR_HEALTH_REPAIRING_AMOUNT, AttributeChangeMode.REPLACE, value);
    }

    public static Long getActorHealthRepairingAmount(State actorState) {
        return actorState.getValue(ActorStateAttributes.ACTOR_HEALTH_REPAIRING_AMOUNT, Long.class).orElseThrow(() -> new IllegalStateException("Actor has no REPAIRING AMOUNT:" + actorState.getOid()));
    }

    public static Long getActorSailingSkill(State actorState){
        return actorState.getValue(ActorStateAttributes.ACTOR_SAILING_SKILL, Long.class).orElse(0L);
    }

    public static Long getActorAccuracySkill(State actorState){
        return actorState.getValue(ActorStateAttributes.ACTOR_ACCURACY_SKILL, Long.class).orElse(0L);
    }

}
