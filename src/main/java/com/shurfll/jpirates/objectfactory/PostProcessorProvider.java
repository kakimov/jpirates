package com.shurfll.jpirates.objectfactory;

import com.shurfll.jpirates.entity.State;

import java.util.List;
import java.util.function.Function;

public interface PostProcessorProvider {
    <T> List<Function<T,T>> provide();
}
