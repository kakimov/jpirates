package com.shurfll.jpirates.event;

import com.shurfll.jpirates.entity.Location;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.Map;

/**
 * Created by kakim on 22.06.2017.
 */
public class LookAroundEvent extends PersonalEvent {
    private static final String QUALIFIER = "lookAroundEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName LOCATION = attribute(TYPE, "location");
    private static final QName VICINITY = attribute(TYPE, "vicinity");

    public LookAroundEvent(String oid, String causeOid, String targetActorId, Location actorLocation) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, LOCATION, actorLocation);
        putAttribute(oid, VICINITY, getVicinity());
    }

    public Location getActorLocation() {
        return getValue(LOCATION, Location.class).get();
    }

    public Map<Location, List<Object>> getVicinity() {
        return getValue(VICINITY, Map.class).get();
    }
}
