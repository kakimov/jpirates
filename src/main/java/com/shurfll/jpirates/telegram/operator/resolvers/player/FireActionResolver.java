package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class FireActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/fire) (.+)";

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        if (messages.size() > 1)
            throw new IllegalArgumentException("Wrong arguments of fire action: " + messages.stream().map(message -> message.toString()).collect(Collectors.joining()));
        Message message = messages.get(0);
        Matcher matcher = COMMAND_PATTERN.matcher(message.getText().get());
        if(matcher.find()){
            String name = matcher.group(2);
            return Optional.of(EventFactory.fireAction(null, String.valueOf(message.getChatId()), null, name));
        }else{
            return Optional.empty();
        }

    }
}
