package com.shurfll.jpirates.persistence.dao;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.StateEntity;
import com.shurfll.jpirates.persistence.converter.StateConverter;
import com.shurfll.jpirates.persistence.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StateDAOImpl implements StateDAO {

    private StateRepository stateRepository;
    private StateConverter stateConverter;

    @Autowired
    public StateDAOImpl(StateRepository stateRepository, StateConverter stateConverter) {
        this.stateRepository = stateRepository;
        this.stateConverter = stateConverter;
    }

    @Override
    public State getState(String entityOid) {
        return stateConverter.convert(stateRepository.findById(entityOid).get());
    }

    @Override
    public State getStateByOwnerOid(String ownerOid) {
        return stateConverter.convert(stateRepository.findByOwnerOid(ownerOid).get());
    }

    @Override
    public State updateState(State state) {
        return stateConverter.convert(stateRepository.saveAndFlush(stateConverter.convert(state)));
    }

    @Override
    public List<State> getActorsAtLocation(Location location) {
        return stateRepository.findAll(Specification.where((root, query, builder) -> {
            final Join<StateEntity, Attribute> attrs = root.join("attributes", JoinType.LEFT);
            return builder.equal(attrs.get("value"), LocationHelper.printLocation(location));
        })).stream().map(stateConverter::convert).collect(Collectors.toList());
    }
}
