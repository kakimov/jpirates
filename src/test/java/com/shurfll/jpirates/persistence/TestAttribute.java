package com.shurfll.jpirates.persistence;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class TestAttribute {
    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private TestComplexEntity owner;

    private String value;

    public TestAttribute() {
    }

    public TestAttribute(String id, TestComplexEntity owner, String value) {
        this.id = id;
        this.owner = owner;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TestComplexEntity getOwner() {
        return owner;
    }

    public void setOwner(TestComplexEntity owner) {
        this.owner = owner;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
