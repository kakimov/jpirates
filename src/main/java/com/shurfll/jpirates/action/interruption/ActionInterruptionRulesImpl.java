package com.shurfll.jpirates.action.interruption;

import com.shurfll.jpirates.action.interruption.rule.ActionInterruptionRule;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by kakim on 26.10.2017.
 */
@Component
class ActionInterruptionRulesImpl implements ActionInterruptionRules {

    private List<ActionInterruptionRule> rules;

    @Autowired
    public ActionInterruptionRulesImpl(List<ActionInterruptionRule> rules) {
        this.rules = rules;
    }

    @Override
    public boolean isInterruptedByEvent(Action action, Event event) {
        return rules
                .stream()
                .filter(r -> r.canHandle(action))
                .findFirst()
                .map(r -> r.isInterruptedBy(action,event))
                .orElse(false);
    }


}
