package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.player.RepairAction;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;
import org.springframework.stereotype.Component;

@Component
public class RepairActionMapper extends AbstractEventMapper {

    public RepairActionMapper(EntityAttributeConverter attributeConverter) {
        super(attributeConverter);
    }

    @Override
    protected String getMapperType() {
        return RepairAction.TYPE;
    }

    @Override
    protected Class getMapperClass() {
        return RepairAction.class;
    }

    @Override
    public Event getEntityToEventFunction(EventEntity e) {
        return EventFactory.repairAction(e.getOid()
                , e.getCauseOid()
                , mandatoryAttribute(e, RepairAction.SOURCE_ACTOR_ID));
    }


}
