package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


/**
 * Created by kakim on 02.06.2017.
 */
public interface ActionService<T extends Action> {

    boolean canHandle(Action a);

    default Flowable<Event> doBeforeAction(T t){
        return Flowable.empty();
    }

    Flowable<Event> doAction(T t);

}
