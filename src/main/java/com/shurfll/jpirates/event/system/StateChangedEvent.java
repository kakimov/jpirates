package com.shurfll.jpirates.event.system;

import com.shurfll.jpirates.event.PersonalEvent;

/**
 * Event to change entities state
 */
public class StateChangedEvent extends PersonalEvent {

    private static final String QUALIFIER = "stateChangedEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;


    public StateChangedEvent(String oid, String causeOid, String targetActorId) {
        super(oid, causeOid, targetActorId);
    }


}