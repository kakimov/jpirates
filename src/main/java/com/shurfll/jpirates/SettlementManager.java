package com.shurfll.jpirates;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Settlement;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shurfll on 26.05.17.
 */
@Component
public class SettlementManager {
    public List<Settlement> settlements = new ArrayList<>();

    public final List<Settlement> getSettlementsAtLocation(Location location){
        return settlements.stream().filter(s -> s.equals(location)).collect(Collectors.toList());
    }
}
