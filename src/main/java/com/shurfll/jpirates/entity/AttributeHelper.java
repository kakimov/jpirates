package com.shurfll.jpirates.entity;

import com.shurfll.jpirates.IDGenerator;
import com.shurfll.jpirates.entity.actor.Actor;

import javax.xml.namespace.QName;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AttributeHelper {


    public static EntityAttributeType resolveType(Object o) {
        if (o instanceof String) {
            return EntityAttributeType.STRING;
        } else if (o instanceof Location) {
            return EntityAttributeType.LOCATION;
        } else if (o instanceof Long) {
            return EntityAttributeType.LONG;
        } else if (o instanceof Configuration) {
            return EntityAttributeType.CONFIGURATION;
        } else if (o instanceof LevelUpMode) {
            return EntityAttributeType.LEVELUPMODE;
        } else if (o instanceof Direction) {
            return EntityAttributeType.DIRECTION;
        } else if (o instanceof GoldSpending) {
            return EntityAttributeType.GOLDSPENDING;
        } else if (o instanceof Map) {
            return EntityAttributeType.MAP;
        } else if (o instanceof Treasure) {
            return EntityAttributeType.TREASURE;
        } else if (o instanceof FireResult) {
            return EntityAttributeType.FIRE_RESULT;
        } else if (o instanceof Collection || Object[].class.isInstance(o)) {
            return EntityAttributeType.COLLECTION;
        } else if (o instanceof Actor) {
            return EntityAttributeType.ACTOR;
        }
        throw new IllegalArgumentException("unsupported type: " + o.getClass());
    }

    public static <T> Optional<T> getValue(Map<QName, EntityAttribute> attributes, QName name, Class<T> clazz) {
        return Optional.ofNullable(attributes.get(name))
                .flatMap(EntityAttribute::getValue)
                .map(EntityAttributeValue::getValue)
                .map(clazz::cast);
    }

    public static EntityAttribute createAttribute(String entityOid, QName name, EntityAttributeType type, Object value) {
        return createAttribute(entityOid, name, type, value, Collections.emptyList());
    }

    public static EntityAttribute createAttribute(String entityOid, QName name, EntityAttributeType type, Object value, Function postprocessor) {
        String attributeOid = generateID();
        EntityAttribute entityAttribute = new EntityAttribute(attributeOid, entityOid, name, type, Collections.singletonList(createAttributeValue(attributeOid, value)), postprocessor);
        return entityAttribute;
    }

    public static EntityAttribute createAttribute(String entityOid, QName name, EntityAttributeType type, Object value, List<Function<Object, Object>> postprocessor) {
        return createAttribute(entityOid, null, name, type, value, postprocessor);
    }

    public static EntityAttribute createAttribute(String entityOid, String attributeOid, QName name, EntityAttributeType type, Object value) {
        return createAttribute(entityOid, attributeOid, name, type, value, null);
    }

    public static EntityAttribute createAttribute(String entityOid, String attributeOid, QName name, EntityAttributeType type, EntityAttributeValue value) {
        return createAttribute(entityOid, attributeOid, name, type, value, null);
    }

    public static EntityAttribute createAttribute(String entityOid, String attributeOid, QName name, EntityAttributeType type, Object value, List<Function<Object, Object>> postprocessor) {
        return createAttribute(entityOid, attributeOid, name, type, createAttributeValue(entityOid, value), postprocessor);
    }

    public static EntityAttribute createAttribute(String entityOid, String attributeOid, QName name, EntityAttributeType type, EntityAttributeValue value, List<Function<Object, Object>> postprocessor) {
        return createAttribute(entityOid, attributeOid, name, type, Collections.singletonList(value), postprocessor);
    }

    public static EntityAttribute createAttribute(String entityOid, String attributeOid, QName name, EntityAttributeType type, List<EntityAttributeValue> values) {
        return createAttribute(entityOid, attributeOid, name, type, values, null);
    }


    public static EntityAttribute createAttribute(String entityOid, String attributeOid, QName name, EntityAttributeType type, List<EntityAttributeValue> values, List<Function<Object, Object>> postprocessor) {
        EntityAttribute entityAttribute = new EntityAttribute(attributeOid, entityOid, name, type, values, postprocessor);
        return entityAttribute;
    }

    public static List<EntityAttributeValue> createAttributeValues(String entityAttributeOid, List<Object> values) {
        return values.stream().map(o -> createAttributeValue(entityAttributeOid, o)).collect(Collectors.toList());
    }

    public static EntityAttributeValue createAttributeValue(String entityAttributeOid, Object value) {
        return createAttributeValue(generateID(), entityAttributeOid, value);
    }

    public static EntityAttributeValue createAttributeValue(String id, String entityAttributeOid, Object value) {
        return new EntityAttributeValue(id, entityAttributeOid, value);
    }

    private static String generateID() {
        return IDGenerator.generateID();
    }
}
