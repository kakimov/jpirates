package com.shurfll.jpirates.entity;


import com.shurfll.jpirates.utils.PrettyPrint;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;

public class Configuration {
    private String moduleUri;
    private Map<QName, EntityAttribute> attributes = new HashMap<>();

    public Configuration(String moduleUri) {
        this.moduleUri = moduleUri;
    }

    public Configuration(String moduleUri, Map<QName, EntityAttribute> attributes) {
        this.moduleUri = moduleUri;
        this.attributes = attributes;
    }

    public Map<QName, EntityAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<QName, EntityAttribute> attributes) {
        this.attributes = attributes;
    }

    public String getModuleUri() {
        return moduleUri;
    }

    public void setModuleUri(String moduleUri) {
        this.moduleUri = moduleUri;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "moduleUri=" + moduleUri +
                ", attributes=" + PrettyPrint.print(attributes) +
                '}';
    }
}
