package com.shurfll.jpirates.event;

import javax.xml.namespace.QName;

/**
 * Created by kakim on 04.06.2017.
 */
public class BattleWonEvent extends PersonalEvent{
    private static final String QUALIFIER = "battleWonEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName LOSER_ID = attribute(TYPE, "loserId");
    private static final QName LOSER_NAME = attribute(TYPE, "loserName");
    private static final QName GOLD_EARNED = attribute(TYPE, "goldEarned");

    public BattleWonEvent(String oid, String causeOid, String targetActorId, String loserId, String loserName, Long goldEarned) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, LOSER_ID, loserId);
        putAttribute(oid, LOSER_NAME, loserName);
        putAttribute(oid, GOLD_EARNED, goldEarned);
    }

    public String getLoserId() {
        return getValue(LOSER_ID,String.class).get();
    }

    public String getLoserName() {
        return getValue(LOSER_NAME, String.class).get();
    }

    public Long getGoldEarned() {
        return getValue(GOLD_EARNED, Long.class).get();
    }

}
