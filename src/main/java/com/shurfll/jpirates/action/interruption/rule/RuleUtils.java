package com.shurfll.jpirates.action.interruption.rule;

import com.shurfll.jpirates.event.DamageReceivedEvent;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.PersonalEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.StatusAction;
import org.springframework.util.Assert;

public class RuleUtils {
    static boolean isAction(Event e) {
        return e instanceof Action;
    }

    static boolean isStatus(Event a) {
        Assert.isTrue(a instanceof Action, "Assume that a is Action");
        return a instanceof StatusAction;
    }

    static boolean isPersonalEvent(Event e) {
        return e instanceof PersonalEvent;
    }

    static boolean isEventTargetMe(Action a, Event e) {
        Assert.isTrue(e instanceof PersonalEvent, "Assume that e is PersonalEvent");
        return a.getSourceActorId().equals(((PersonalEvent) e).getTargetActorId());
    }

    static boolean isDamageReceivedEvent(Event e) {
        return e instanceof DamageReceivedEvent;
    }
}
