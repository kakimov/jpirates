package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.exception.IllegalActionException;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.StartAction;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class StartActionService implements ActionService<StartAction> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartActionService.class);

    private PlayersManager playersManager;
    private ObjectFactory objectFactory;

    @Autowired
    public StartActionService(PlayersManager playersManager, ObjectFactory objectFactory) {
        this.playersManager = playersManager;
        this.objectFactory = objectFactory;
    }


    @Override
    public boolean canHandle(Action a) {
        return a instanceof StartAction;
    }


    @Override
    public Flowable<Event> doAction(StartAction action) {
        Location location = action.getLocation().orElseGet(objectFactory::generateLocation);
        return startToPlay(action,action.getSourceActorId(), action.getName(), location)
                .map(player -> EventFactory.actorStartedEvent(action, player, location));

    }

    private Flowable<Player> startToPlay(StartAction action,String id, String name, Location location) {
        if (!playersManager.hasPlayer(name)) {
            Player player = new Player(id, name);
            playersManager.addPlayer(player, location);
            return Flowable.just(player);
        } else {
            return Flowable.error(new IllegalActionException(action.getSourceActorId(), LocalizationKeys.NAME_CONFLICK));
        }
    }
}

