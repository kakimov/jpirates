package com.shurfll.jpirates.component.treasure;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class TreasureManagerImpl implements TreasureManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(TreasureManagerImpl.class);

    private Reactor reactor;

    private ConfigurationManager configurationManager;

    private Map<Location, Treasure> treasures = new HashMap<>();

    private Disposable generationDisposable;
    private Disposable lookupForUpdateDisposable;

    static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "TreasureManagerImpl";

    public static final QName PARAM_TREASURE_INTERVAL = new QName(HANDLER_URI, "treasureGenerateInterval");

    public Long treasureGenerateInterval;


    @Autowired
    public TreasureManagerImpl(Reactor reactor, ConfigurationManager configurationManager) {
        this.reactor = reactor;
        this.configurationManager = configurationManager;
    }

    @PostConstruct
    public void init() {
        lookupForUpdates();
        configure(configurationManager.getConfiguration(HANDLER_URI));
        startGeneration();
    }

    @Override
    public void addTreasure(Treasure treasure, Location location) {
        treasures.put(location, treasure);
    }

    @Override
    public void removeUnfoundTreasure() {
        treasures.clear();
    }

    @Override
    public Optional<Treasure> discoverTreasures(Location location) {
        return Optional.ofNullable(treasures.remove(location));
    }

    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_TREASURE_INTERVAL, Long.class);
    }

    protected void configure(Optional<Configuration> config) {
        LOGGER.debug("Configure: {}", config);
        if(generationDisposable != null) {
            LOGGER.info("Stop generation process because of configuration update");
            generationDisposable.dispose();
        }
        treasureGenerateInterval = ConfigurationHelper.getParamValue(config, PARAM_TREASURE_INTERVAL, Long.class, Commons.TREASURE_GENERATE_DEFAULT_TIMEOUT);
        LOGGER.info("Configuration updated. Treasure generate interval={}", treasureGenerateInterval);
    }


    private Event instantiateNewTreasure(Long timeout) {
        Treasure treasure = generateTreasure(timeout);
        Location location = Commons.generateLocation();
        addTreasure(treasure, location);
        return EventFactory.treasureHiddenEvent(treasure, location);
    }

    private Treasure generateTreasure(Long timeout) {
        Integer gold = new Random().nextInt(50);

        Treasure treasure = new Treasure(String.valueOf(timeout));
        Map<QName, EntityAttribute> items = treasure.getAttributes();
        items.put(GoodsAttributes.GOLD, AttributeHelper.createAttribute(treasure.getOid(), GoodsAttributes.GOLD, EntityAttributeType.LONG, (Long) gold.longValue()));
        return treasure;
    }

    private void startGeneration(){
        LOGGER.info("Start new generation process with params: timeout: {}", treasureGenerateInterval);
        generationDisposable = Flowable.interval(treasureGenerateInterval, TimeUnit.MILLISECONDS)
                .doOnNext(i -> removeUnfoundTreasure())
                .map(this::instantiateNewTreasure)
                .subscribe(reactor::submitEvent);
    }

    private void lookupForUpdates(){
        lookupForUpdateDisposable = configurationManager.configurationChange(HANDLER_URI)
                .map(Optional::ofNullable)
                .filter(this::validateConfiguration)
                .retry()
                .doOnNext(this::configure)
                .doOnNext(conf -> startGeneration())
                .subscribe();
    }
}
