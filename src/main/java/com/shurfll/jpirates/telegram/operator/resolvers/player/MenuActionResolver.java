package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class MenuActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "\\/menu";

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        return Optional.empty();
    }

    @Override
    public Optional<InfoEvent> makeResponse(List<Message> messages) {
        return Optional.of(EventFactory.questEvent(null, "" + messages.get(0).getChatId(), "main_menu", "Базовое меню", null));
    }
}
