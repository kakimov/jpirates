package com.shurfll.jpirates.persistence;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "attribute")
public class Attribute {
    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(length = 32)
    private String oid;

    @ManyToOne
    @JoinColumn(name = "owner_oid")
    private AttributeContainer owner;
    private String namespace;
    private String name;
    private String type;
    private String value;

    public Attribute() {
    }

    public Attribute(String oid, AttributeContainer owner, String namespace, String name, String type, String value) {
        this.oid = oid;
        this.owner = owner;
        this.namespace = namespace;
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public AttributeContainer getOwner() {
        return owner;
    }

    public void setOwner(AttributeContainer owner) {
        this.owner = owner;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "oid='" + oid + '\'' +
                ", owner='" + Optional.ofNullable(owner).map(AttributeContainer::getOid) + '\'' +
                ", namespace='" + namespace + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
