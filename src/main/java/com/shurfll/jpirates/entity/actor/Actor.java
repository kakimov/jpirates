package com.shurfll.jpirates.entity.actor;

public class Actor {
    private String oid;
    private String actorId;
    private ActorType type;
    private String name;

    public Actor(ActorType type, String actorId, String name) {
        this.type = type;
        this.actorId = actorId;
        this.name = name;
    }

    public Actor(ActorType type, String oid, String actorId, String name) {
        this(type, actorId, name);
        this.oid = oid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActorType getType() {
        return type;
    }

    public void setType(ActorType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "actorId='" + actorId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
