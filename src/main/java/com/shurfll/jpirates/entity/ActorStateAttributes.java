package com.shurfll.jpirates.entity;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;

public interface ActorStateAttributes {
    String STATE_NS = "jpirates/state/common";
    QName ACTOR_STATUS = new QName(STATE_NS, "status");
    QName ACTOR_LOCATION = new QName(STATE_NS, "location");
    QName ACTOR_HEALTH = new QName(STATE_NS, "health");
    QName ACTOR_TOTAL_HEALTH = new QName(STATE_NS, "totalHealth");
    QName ACTOR_FIRE_POWER = new QName(STATE_NS, "firePower");
    QName ACTOR_HEALTH_REPAIRING_SPEED = new QName(STATE_NS, "healthRepairingSpeed");
    QName ACTOR_HEALTH_REPAIRING_AMOUNT = new QName(STATE_NS, "healthRepairingAmount");
    QName ACTOR_EXPERIENCE = new QName(STATE_NS, "experience");
    QName ACTOR_LEVEL = new QName(STATE_NS, "level");
    QName ACTOR_LEVEL_UP_POINTS = new QName(STATE_NS, "levelUpPoints");
    QName ACTOR_SAILING_SKILL = new QName(STATE_NS, "sailing_skill");
    QName ACTOR_ACCURACY_SKILL = new QName(STATE_NS, "accuracy_skill");

    Map<QName, EntityAttributeType> ATTRIBUTES = new HashMap() {{
        put(ACTOR_LOCATION, EntityAttributeType.LOCATION);
        put(ACTOR_HEALTH, EntityAttributeType.LONG);
        put(ACTOR_TOTAL_HEALTH, EntityAttributeType.LONG);
        put(GoodsAttributes.GOLD, EntityAttributeType.LONG);
        put(ACTOR_FIRE_POWER, EntityAttributeType.LONG);
        put(ACTOR_HEALTH_REPAIRING_SPEED, EntityAttributeType.LONG);
        put(ACTOR_HEALTH_REPAIRING_AMOUNT, EntityAttributeType.LONG);
        put(ACTOR_EXPERIENCE, EntityAttributeType.LONG);
        put(ACTOR_LEVEL, EntityAttributeType.LONG);
        put(ACTOR_LEVEL_UP_POINTS, EntityAttributeType.LONG);
        put(ACTOR_SAILING_SKILL, EntityAttributeType.LONG);
        put(ACTOR_ACCURACY_SKILL, EntityAttributeType.LONG);
        put(ACTOR_STATUS, EntityAttributeType.STRING);
    }};


}
