package com.shurfll.jpirates.reactor;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.PersonalEvent;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.processors.ReplayProcessor;
import io.reactivex.schedulers.Schedulers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

@Component
public class ReactorImpl implements Reactor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReactorImpl.class);

    private PublishProcessor<Event> events = PublishProcessor.create();

    private Map<Class, List<EventHandler<Event>>> handlers = new HashMap<>();

    @PostConstruct
    public void init() {
        events().flatMap(this::processEvent)
                .retry()
                .subscribe(this::submitEvent);
    }

    @Override
    public Flowable<Event> events() {
        return events.observeOn(Schedulers.computation()).doOnNext(this::initMDCContextIfApplicable);
    }

    private void initMDCContextIfApplicable(Event event){
        if(event instanceof PersonalEvent){
            MDC.put("actorId", ((PersonalEvent) event).getTargetActorId());
        }
    }

    @Override
    public Flowable<Event> events(Predicate<Event> predicate) {
        return events().filter(predicate::test);
    }

    @Override
    public void submitEvent(Event event) {
        events.onNext(event);
    }
    
    @Override
    public Flowable<Event> submitEventAndObserve(Event event){
        ReplayProcessor<Event> subject = ReplayProcessor.create();
        events.subscribe(subject);
        submitEvent(event);
        return subject;
    }

    @Override
    public void registerHandler(Class eventClass, EventHandler handler) {
        if (handlers.containsKey(eventClass)) {
            handlers.get(eventClass).add(handler);
        } else {
            List<EventHandler<Event>> hs = new ArrayList<>();
            hs.add(handler);
            handlers.put(eventClass, hs);
        }
    }

    @Override
    public void unregisterHandler(Class eventClass, EventHandler handler) {
        if (handlers.containsKey(eventClass)) {
            handlers.get(eventClass).remove(handler);
        }
    }

    @Override
    public void unregisterHandler(EventHandler handler) {
        handlers.entrySet().stream().forEach(classListEntry -> classListEntry.getValue().remove(handler));
    }

    private Flowable<Event> processEvent(Event event) {
        return Flowable.fromIterable(handlers.entrySet())
                .filter(entry -> entry.getKey().equals(event.getClass()))
                .flatMap(entry -> Flowable.fromIterable(entry.getValue()))
                .flatMap(handler -> handler.handle(event))
                .doOnNext(e -> LOGGER.debug("Event {} is produced by event {}", e, event.getOid()));
    }
}
