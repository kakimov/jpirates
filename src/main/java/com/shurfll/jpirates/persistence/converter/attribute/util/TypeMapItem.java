package com.shurfll.jpirates.persistence.converter.attribute.util;

import com.google.gson.Gson;

public class TypeMapItem {
    private String name;
    private String type;
    private String value;

    public TypeMapItem(String name, Object o, String serializedValue) {
        this.name = name;
        this.type = o.getClass().getCanonicalName();
        this.value = serializedValue;
    }

    public Object buildObject(Gson gson) {
        try {
            return gson.fromJson(value, Class.forName(type));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}