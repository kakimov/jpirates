package com.shurfll.jpirates.persistence.converter.value;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.shurfll.jpirates.entity.EntityAttributeType;

import java.util.function.Function;

public abstract class AbstractValueMapper {

    protected static Gson gson = new GsonBuilder()
            .setLongSerializationPolicy(LongSerializationPolicy.STRING)
            .create();

    private EntityAttributeType attributeType;
    private Class classType;
    private Function<Object, String> serializeMapper;
    private Function<String, Object> deserializeMapper;

    public AbstractValueMapper(EntityAttributeType attributeType, Class classType, Function<Object, String> serializeMapper, Function<String, Object> deserializeMapper) {
        this.attributeType = attributeType;
        this.classType = classType;
        this.serializeMapper = serializeMapper;
        this.deserializeMapper = deserializeMapper;
    }

    public boolean isApplicable(EntityAttributeType attributeType){
        return this.attributeType.equals(attributeType);
    }

    public boolean isApplicable(Class classType){
        return this.classType.equals(classType);
    }

    public String serialize(Object obj) {
        return serializeMapper.apply(obj);
    }

    public Object deserialize(String obj) {
        return deserializeMapper.apply(obj);
    }
}
