package com.shurfll.jpirates.utils;

import com.shurfll.jpirates.EntityAttributeContainer;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.entity.FireResult;
import com.shurfll.jpirates.entity.Location;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrettyPrint {

    private static final String ATTRIBUTE_TO_STRING = "%s=%s";
    private static final String LOCATION_VALUE_TO_STRING = "{%d,%d}";
    private static final String FIRE_RESULT_VALUE_TO_STRING = "{damage=%d}";

    public static final String print(Map map) {

        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = iter.next();
            sb.append(entry.getKey());
            sb.append('=').append('"');
            sb.append(entry.getValue());
            sb.append('"');
            if (iter.hasNext()) {
                sb.append(',').append(' ');
            }
        }
        return sb.toString();
    }

    public static final String printEntityAttributeContainer(EntityAttributeContainer eac) {
        return eac.getAttributes().values().stream()
                .map(attr -> String.format(ATTRIBUTE_TO_STRING, attr.getName().getLocalPart(), printEntityAttributeValue(attr.getValue())))
                .collect(Collectors.joining(","));
    }

    private static final String printEntityAttributeValue(Optional<EntityAttributeValue> value) {
        return value.map(EntityAttributeValue::getValue)
                .map(PrettyPrint::printValue)
                .orElse(null);
    }

    private static final String printValue(Object value) {
        return value instanceof Object[] ? printValue((Object[]) value) : value.toString();
    }

    private static final String printValue(Location location) {
        return String.format(LOCATION_VALUE_TO_STRING, location.getLatitude(), location.getLongitude());
    }

    private static final String printValue(FireResult fireResult) {
        return String.format(FIRE_RESULT_VALUE_TO_STRING, fireResult.getDamageMade());
    }

    private static final String printValue(Object[] args) {
        return Stream.of(args).map(PrettyPrint::printValue).collect(Collectors.joining(","));
    }
}
