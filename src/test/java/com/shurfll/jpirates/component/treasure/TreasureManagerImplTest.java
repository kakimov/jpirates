package com.shurfll.jpirates.component.treasure;

import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.AttributeHelper;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.event.system.TreasureHiddenEvent;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.xml.namespace.QName;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TreasureManagerImplTest {
    private TreasureManagerImpl manager;
    private Reactor reactor;
    private ConfigurationManager configurationManager;
    private ArgumentCaptor<TreasureHiddenEvent> valueCapture = ArgumentCaptor.forClass(TreasureHiddenEvent.class);
    private PublishProcessor<Configuration> updater = PublishProcessor.create();


    @Before
    public void init(){
        reactor = mock(Reactor.class);
        doNothing().when(reactor).submitEvent(valueCapture.capture());
        configurationManager = mock(ConfigurationManager.class);
        when(configurationManager.getConfiguration(eq(TreasureManagerImpl.HANDLER_URI)))
                .thenReturn(Optional.of(config(100L)));
        manager = new TreasureManagerImpl(reactor,configurationManager);
    }

    @Test
    public void test001_normalBehaviour() throws Exception{
        when(configurationManager.configurationChange(eq(TreasureManagerImpl.HANDLER_URI)))
                .thenReturn(Flowable.empty());
        manager.init();
        Thread.sleep(50);
        assertTrue("No events are generated yet",valueCapture.getAllValues().isEmpty());
        Thread.sleep(60);
        assertEquals("One event is generated",1,valueCapture.getAllValues().size());
    }

    @Test
    /**
     * Тест проверяет, что при обновлении конфигурации старый генератор сокровищ перестает работать
     */
    public void test002_updateConfiguration() throws Exception{

        when(configurationManager.configurationChange(eq(TreasureManagerImpl.HANDLER_URI)))
                .thenReturn(updater);
        manager.init();
        Thread.sleep(50);
        assertTrue("No events are generated yet",valueCapture.getAllValues().isEmpty());
        Thread.sleep(60);
        assertEquals("One event is generated",1,valueCapture.getAllValues().size());
        updater.onNext(config(200));
        Thread.sleep(100);
        assertEquals("No more values are generated",1,valueCapture.getAllValues().size());
        Thread.sleep(110);
        assertEquals("Treasure with updated is created",2,valueCapture.getAllValues().size());

    }


    private Configuration config(long timeoutParam){
        Configuration configuration = new Configuration(TreasureManagerImpl.HANDLER_URI);
        QName attrName = TreasureManagerImpl.PARAM_TREASURE_INTERVAL;
        configuration.getAttributes().put(attrName, AttributeHelper.createAttribute("configOid", attrName, EntityAttributeType.LONG, timeoutParam));
        return  configuration;

    }

}