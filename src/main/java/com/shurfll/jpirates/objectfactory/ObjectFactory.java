package com.shurfll.jpirates.objectfactory;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.IDGenerator;
import com.shurfll.jpirates.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.xml.namespace.QName;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import static com.shurfll.jpirates.entity.ActorStateAttributes.ATTRIBUTES;

/**
 * Created by kakim on 21.06.2017.
 */
@Component
public class ObjectFactory {

    private List<ModificationComputer> modificationComputers;

    @Autowired
    public ObjectFactory(List<ModificationComputer> modificationComputers) {
        this.modificationComputers = modificationComputers;
    }

    public ObjectFactory() {
    }

    public State createEmptyState(String oid, String ownerOid) {
        State state = new State(oid, ownerOid);
        return state;
    }

    public State createState(String oid, String ownerOid, List<EntityAttribute> attrs) {
        State state = createEmptyState(oid, ownerOid);
        attrs.stream()
                .forEach( a -> state.putAttribute(a));
        initPostProcessors(state);
        return state;
    }

    private void initPostProcessors(State state){
        for (EntityAttribute ea : state.getAttributes().values()) {
            if (ActorStateAttributes.ACTOR_HEALTH.equals(ea.getName())) {
                ea.setPostProcessors(new ActorHealthPostProcessor(state).provide());
            }
        }
    }

    public State createState(String oid, String ownerOid, Location location) {
        Assert.notNull(ownerOid, "ownerOid can't be null");
        Assert.notNull(location, "Location can't be null");
        State state = createEmptyState(oid, ownerOid);
        state.putAttribute(createAttribute(oid, ActorStateAttributes.ACTOR_LOCATION, location));
        return state;
    }

    public State createDefaultPlayerState(String entityOid, Location location) {
        return createPlayerState(entityOid, location, Commons.DEFAULT_HEALTH, Commons.DEFAULT_GOLD, Commons.DEFAULT_FIRE_POWER, Commons.DEFAULT_HEALTH_REPAIRING_TIMEOUT, Commons.DEFAULT_HEALTH_REPAIRING_AMOUNT);
    }

    private State createPlayerState(String entityOid, Location location, Long health, Long gold, Long firePower, Long repairingSpeed, Long repairingAmount) {
        State state = createState(null,entityOid, location);
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_HEALTH, health));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_TOTAL_HEALTH, health));
        state.putAttribute(createAttribute(state.getOid(), GoodsAttributes.GOLD, gold));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_FIRE_POWER, firePower));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_HEALTH_REPAIRING_SPEED, repairingSpeed));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_HEALTH_REPAIRING_AMOUNT, repairingAmount));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_EXPERIENCE, 0L));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, 0L));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_LEVEL, 1L));
        initPostProcessors(state);
        return state;
    }

    public State createDefaultNPCState(String entityOid, Location location) {
        State state = createState(null,entityOid, location);
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_HEALTH, Commons.DEFAULT_NPC_HEALTH));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_TOTAL_HEALTH, Commons.DEFAULT_NPC_HEALTH));
        state.putAttribute(createAttribute(state.getOid(), GoodsAttributes.GOLD, Commons.DEFAULT_NPC_GOLD));
        state.putAttribute(createAttribute(state.getOid(), ActorStateAttributes.ACTOR_FIRE_POWER, Commons.DEFAULT_NPC_HEALTH));
        initPostProcessors(state);
        return state;
    }

    public EntityAttribute createAttribute(String stateOid, QName name, Object value) {
        return createAttribute(stateOid, name, value, Collections.emptyList());
    }

    public EntityAttribute createAttribute(String stateOid, QName name, Object value, List<Function<Object, Object>> funcs) {
        return createAttribute(stateOid, name, ATTRIBUTES.get(name), value, funcs);
    }

    public EntityAttribute createAttribute(String stateOid, QName name, Object value, Function<Object, Object>... funcs) {
        return createAttribute(stateOid, name, ATTRIBUTES.get(name), value, Arrays.asList(funcs));
    }

    public EntityAttribute createAttribute(String stateOid, QName name, EntityAttributeType type, Object value) {
        return createAttribute(stateOid, name, type, value, Collections.emptyList());
    }

    public EntityAttribute createAttribute(String stateOid, QName name, EntityAttributeType type, Object value, List<Function<Object, Object>> funcs) {
        return AttributeHelper.createAttribute(stateOid, name, type, value, funcs);
    }

    public EntityAttribute createAttribute(String stateOid, QName name, EntityAttributeType type, Object value, Function<Object, Object> func) {
        return AttributeHelper.createAttribute(stateOid, name, type, value, func);
    }


    private void modifyAttributeValue(EntityAttribute attribute, Object value) {
        if (attribute.getValue().isPresent()) {
            modificationComputers.stream().filter(modificationComputer -> modificationComputer.canCompute(attribute))
                    .findFirst()
                    .map(modificationComputer -> modificationComputer.compute(attribute, value))
                    .ifPresent(newValue -> attribute.getValue().get().setValue(newValue));
        } else {
            attribute.setValues(AttributeHelper.createAttributeValues(attribute.getId(), Collections.singletonList(value)));
        }
    }

    public void updateEntityAttribute(State state, QName name, AttributeChangeMode mode, Object value) {
        state.putAttribute(state.getAttribute(name)
                .map(attribute -> updateAttribute(attribute, mode, value))
                .orElse(createAttribute(state.getOid(), name, value)));
    }

    private EntityAttribute updateAttribute(EntityAttribute attribute, AttributeChangeMode mode, Object value) {
        if (mode == AttributeChangeMode.REPLACE) {
            attribute.setValues(Collections.singletonList(AttributeHelper.createAttributeValue(attribute.getId(), value)));
        } else if (mode == AttributeChangeMode.MODIFY) {
            modifyAttributeValue(attribute, value);
        }
        return attribute;
    }

    private static String generateID() {
        return IDGenerator.generateID();
    }

    public Location generateLocation() {
        Random r = new Random();
        Long longitude = Long.valueOf(r.nextInt(4) - 2);
        Long latitude = Long.valueOf(r.nextInt(4) - 2);
        return new Location(longitude, latitude);
    }
}
