package com.shurfll.jpirates.persistence.converter.attribute.util;

import com.google.gson.Gson;

public class TypeCollectionItem {
    private long index;
    private String type;
    private String value;

    public TypeCollectionItem(long index, Object o, String value) {
        this.index = index;
        this.type = o.getClass().getCanonicalName();
        this.value = value;
    }


    public Object buildObject(Gson gson) {
        try {
            return gson.fromJson(value, Class.forName(type));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}