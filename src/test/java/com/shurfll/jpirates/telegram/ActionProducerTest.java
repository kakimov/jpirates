package com.shurfll.jpirates.telegram;

import com.shurfll.jpirates.Main;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.entity.Direction;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.admin.ConfigureModuleAction;
import com.shurfll.jpirates.event.action.admin.NewsAction;
import com.shurfll.jpirates.event.action.player.*;
import com.shurfll.jpirates.manager.PlayersManagerImpl;
import com.shurfll.jpirates.stubs.TelegramBotStub;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by kakim on 16.06.2017.
 */
@ActiveProfiles("action_producer_test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Main.class)
public class ActionProducerTest {

    private static final Long CHAT_ID_1 = 1l;
    private static final Long CHAT_ID_2 = 2l;

    @Autowired
    private PlayersManagerImpl playersManager;

    @Autowired
    private TelegramManager telegramManager;

    @Autowired
    private ActionProducer actionProducer;

    @Autowired
    @Qualifier("player")
    private TelegramBot bot;

    @Autowired
    @Qualifier("admin")
    private TelegramBot adminBot;

    private TestSubscriber<Action> subscriber;

    @Before
    public void init() {
        subscriber = new TestSubscriber<>();
        actionProducer.actionFlowable().subscribe(subscriber);
    }

    @Test
    public void test001_startCommand() {
        Update u1 = upd(CHAT_ID_1, "/start");
        Update u2 = upd(CHAT_ID_1, "name");
        sendUpdate(u1);
        subscriber.assertNoValues();
        sendUpdate(u2);
        subscriber.assertValueCount(1);
        StartAction action = (StartAction) subscriber.values().get(0);
        assertEquals("name", action.getName());
    }

    @Test
    public void test002_statusCommand() {
        Update u1 = upd(CHAT_ID_1, "/status");
        sendUpdate(u1);
        subscriber.assertValueCount(1);
        assertTrue(subscriber.values().get(0) instanceof StatusAction);
    }

    @Test
    public void test003_sailCommand_goodDirection() {
        Update u1 = upd(CHAT_ID_1, "/sail NoRtH");
        sendUpdate(u1);
        subscriber.assertValueCount(1);
        Action action = subscriber.values().get(0);
        assertTrue(action instanceof MoveAction);
        assertEquals(Direction.NORTH, ((MoveAction) action).getDirection());
    }

    @Test
    public void test004_sailCommand_badDirection() {
        Update u1 = upd(CHAT_ID_1, "/sail nowhere");
        sendUpdate(u1);
        subscriber.assertNoValues();
    }

    @Test
    public void test005_fireCommand() {
        playersManager.addPlayer(new Player("123", "someone"));
        Update u1 = upd(CHAT_ID_1, "/fire someone");
        sendUpdate(u1);
        subscriber.assertValueCount(1);
        Action action = subscriber.values().get(0);
        assertTrue(action instanceof FireAction);
        assertEquals("someone", ((FireAction) action).getTargetName().get());
    }

    @Test
    public void test006_commandsOverlapping() {
        Update u1 = upd(CHAT_ID_1, "/start");
        Update u2 = upd(CHAT_ID_1, "name");
        Update u3 = upd(CHAT_ID_2, "/start");
        Update u4 = upd(CHAT_ID_2, "name2");

        sendUpdate(u1);
        subscriber.assertNoValues();

        sendUpdate(u3);
        subscriber.assertNoValues();

        sendUpdate(u2);
        subscriber.assertValueCount(1);
        Action action1 = subscriber.values().get(0);
        assertTrue(action1 instanceof StartAction);
        assertEquals("name", ((StartAction) action1).getName());

        sendUpdate(u4);
        subscriber.assertValueCount(2);
        Action action2 = subscriber.values().get(1);
        assertTrue(action2 instanceof StartAction);
        assertEquals("name2", ((StartAction) action2).getName());
    }

    @Test
    public void test007_skipInvalidComands() {
        playersManager.addPlayer(new Player("123", "name"));
        Update u1 = upd(CHAT_ID_1, "/start");
        Update u2 = upd(CHAT_ID_1, "/fire 123");

        sendUpdate(u1);
        subscriber.assertNoValues();

        sendUpdate(u2);
        subscriber.assertValueCount(1);
        Action action = subscriber.values().get(0);
        assertTrue(action instanceof FireAction);
        assertEquals("123", ((FireAction) action).getTargetName().get());
    }

    @Test
    public void test008_repairCommand() {
        Update u1 = upd(CHAT_ID_1, "/repair");
        sendUpdate(u1);
        subscriber.assertValueCount(1);
        assertTrue(subscriber.values().get(0) instanceof RepairAction);
    }

    @Test
    //TODO
    public void test009_questCommand_root() {
        Update u1 = upd(CHAT_ID_1, "/quest");
        sendUpdate(u1);
//        subscriber.assertValueCount(1);
//        assertTrue(subscriber.values().getByActorId(0) instanceof QuestAction);
    }

    @Test
    //TODO
    public void test010_questCommand_child() {
        Update u1 = upd(CHAT_ID_1, "/quest questId");
        sendUpdate(u1);
//        subscriber.assertValueCount(1);
//        assertTrue(subscriber.values().getByActorId(0) instanceof QuestAction);
    }

    @Test
    public void test011_admin_configure() {
        Update u1 = upd(CHAT_ID_1, "/config {\"moduleUri\":\"http://test.com\"}");
        sendAdminUpdate(u1);
        assertTrue(subscriber.values().get(0) instanceof ConfigureModuleAction);
    }

    @Test
    public void test0012_admin_notification(){
        final String newsText = "News example";
        Update u1 = upd(CHAT_ID_1,"/news");
        Update u2 = upd(CHAT_ID_1,newsText);
        sendAdminUpdate(u1);
        sendAdminUpdate(u2);
        subscriber.awaitCount(1);
        TestEventHelper.checkEventProduced(subscriber, NewsAction.class, a -> newsText.equals(a.getText()));
    }


    private Update upd(long chatId, String text) {
        Update u = mock(Update.class);
        Message m = msg(chatId, text);
        when(u.getMessage()).thenReturn(m);
        when(u.hasMessage()).thenReturn(true);
        return u;
    }

    private Message msg(long chatId, String text) {
        Message m = mock(Message.class);
        when(m.getChatId()).thenReturn(chatId);
        when(m.getText()).thenReturn(text);
        when(m.hasText()).thenReturn(true);
        return m;
    }

    private void sendUpdate(Update u) {
        ((TelegramBotStub) bot).onUpdateReceived(u);
    }

    private void sendAdminUpdate(Update u) {
        ((TelegramBotStub) adminBot).onUpdateReceived(u);
    }


}