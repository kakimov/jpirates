package com.shurfll.jpirates.component.treasure;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Treasure;

import java.util.Optional;

public interface TreasureManager {
    void init();

    void addTreasure(Treasure treasure, Location location);

    void removeUnfoundTreasure();

    Optional<Treasure> discoverTreasures(Location location);
}
