package com.shurfll.jpirates.telegram.translator;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.quest.QuestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;

import java.util.Optional;

/**
 * Created by kakim on 01.06.2017.
 */
public abstract class EventTranslator<T extends Event> {

    @Autowired
    private QuestManager questManager;

    abstract boolean canTranslate(Event e);

    abstract String translate(T e);

    Optional<ReplyKeyboard> useKeyboard(T e) {
        return KeyboardHelper.getKeyboard(questManager.createDefaultKeyboard());
    }
}
