package com.shurfll.jpirates.telegram.keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kakim on 18.06.2017.
 */
public class CallbackButtonsKeyboard implements Keyboard {
    List<CallbackButton> items = new ArrayList<>();

    public List<CallbackButton> getItems() {
        return items;
    }

    public void setItems(List<CallbackButton> items) {
        this.items = items;
    }

    public static class CallbackButton{
        private String text;
        private String command;

        public CallbackButton(String text, String command) {
            this.text = text;
            this.command = command;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }
    }
}
