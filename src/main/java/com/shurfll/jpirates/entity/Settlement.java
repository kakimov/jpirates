package com.shurfll.jpirates.entity;

/**
 * Created by shurfll on 26.05.17.
 */
public class Settlement extends Location {

    private String name;

    public Settlement(long longitude, long latitude, String name) {
        super(longitude, latitude);
        this.name = name;
    }

    public Settlement(Location location, String name) {
        super(location);
        this.name = name;
    }

    public Settlement(long longitude, long latitude) {
        super(longitude, latitude);
    }

    public Settlement(Location location) {
        super(location);
    }

    @Override
    public String toString() {
        return "Settlement{" +
                "name='" + name + '\'' +
                "} " + super.toString();
    }
}
