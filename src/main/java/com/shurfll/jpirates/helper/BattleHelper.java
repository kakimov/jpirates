package com.shurfll.jpirates.helper;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.entity.FireResult;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by kakim on 04.06.2017.
 */
@Component
public class BattleHelper {

    private StateManager stateManager;

    private ObjectFactory objectFactory;

    @Autowired
    public BattleHelper(StateManager stateManager, ObjectFactory objectFactory) {
        this.stateManager = stateManager;
        this.objectFactory = objectFactory;
    }

    public FireResult fire(String actorId) {
        State actorState = stateManager.getStateByActorId(actorId).blockingFirst();
        Long realFirePower = getRealFirePower(getFirePower(actorState), ActorHelper.getActorAccuracySkill(actorState));
        return new FireResult(realFirePower);
    }

    private Long getFirePower(State actorState) {
        return ActorHelper.getActorFirePower(actorState);
    }

    private Long getRealFirePower(Long power, Long accuracySkill) {
        int res = new Random().nextInt(power.intValue());
        return Math.max((long) res, accuracySkill);
    }
}
