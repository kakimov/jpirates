package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ActionService;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.RepairAction;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.functions.Function3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class RepairActionService implements ActionService<RepairAction> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepairActionService.class);

    private PlayersManager playersManager;
    private StateManager stateManager;
    private Reactor reactor;

    @Autowired
    public RepairActionService(PlayersManager playersManager, StateManager stateManager, Reactor reactor) {
        this.playersManager = playersManager;
        this.stateManager = stateManager;
        this.reactor = reactor;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof RepairAction;
    }


    @Override
    public Flowable<Event> doAction(RepairAction action) {
        return repair(action);
    }

    // TODO not a pure function
    private Flowable<Event> repair(RepairAction action) {
        return Flowable.merge(getZipActorAndState(action, this::repairInternall));
    }

    private Flowable<Event> repairInternall(RepairAction action, Actor actor, State state) {
        if (!ActorHelper.isFullyRepaired(state)) {
            return Flowable.concat(repairing(actor, state, action), completeEvent(action))
                    .doOnNext(event -> LOGGER.debug("Event produced: {}", event));
        } else {
            return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.REPAIR_NO_NEED));
        }
    }

    private Flowable<? extends Event> completeEvent(RepairAction action) {
        return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.REPAIR_COMPLETED));
    }

    private Flowable<? extends Event> repairing(Actor actor, State state, RepairAction action) {
        return Flowable.interval(ActorHelper.getActorHealthRepairingSpeed(state), TimeUnit.MILLISECONDS)
                .map(aLong -> calculateHealthDelta(state))
                .flatMap(delta -> repairStep(action, actor, delta))
                .takeUntil(isFullyRepaired(action));

    }

    private Flowable<Event> repairStep(RepairAction action, Actor actor, Long delta) {
        return Flowable.just(EventFactory.stateChangeEventModify(action, actor.getActorId(), ActorStateAttributes.ACTOR_HEALTH, delta)
                , EventFactory.infoEvent(action, actor.getActorId(), LocalizationKeys.REPAIR_STEP, new Object[]{delta}));
    }

    private Long calculateHealthDelta(State state) {
        return ActorHelper.getActorHealthRepairingAmount(state);
    }

    public <T> Flowable<T> getZipActorAndState(RepairAction action, Function3<RepairAction, Actor, State, T> zip) {
        return Flowable.zip(Flowable.just(action)
                , playersManager.getPlayerFlowable(action.getSourceActorId())
                , stateManager.getStateByActorId(action.getSourceActorId())
                , zip::apply);
    }

    private Flowable<State> isFullyRepaired(RepairAction action){
        return reactor.events().ofType(StateChangedEvent.class)
                .filter(e -> action.getSourceActorId().equals(e.getTargetActorId()))
                .flatMap(e -> stateManager.getStateByActorId(e.getTargetActorId()))
                .filter(ActorHelper::isFullyRepaired);

    }


}
