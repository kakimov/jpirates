package com.shurfll.jpirates.event;

import com.shurfll.jpirates.entity.Direction;

import javax.xml.namespace.QName;
import java.util.List;

/**
 * Created by shurfll on 26.05.17.
 */
public class SailAwayEvent extends PersonalEvent {
    private static final String QUALIFIER = "sailAwayEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName OBJECTS = attribute(TYPE, "objects");
    private static final QName DIRECTION = attribute(TYPE, "direction");

    public SailAwayEvent(String oid, String causeOid, String targetActorId, List<Object> object, Direction direction) {
        super(oid, causeOid, targetActorId);
        putAttribute(oid, OBJECTS, object);
        putAttribute(oid, DIRECTION, direction);
    }

    public List<Object> getObject() {
        return getValue(OBJECTS, List.class).get();
    }

    public Direction getDirection() {
        return getValue(DIRECTION, Direction.class).get();
    }

}
