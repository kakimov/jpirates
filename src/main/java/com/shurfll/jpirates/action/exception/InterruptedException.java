package com.shurfll.jpirates.action.exception;

import com.shurfll.jpirates.ActorException;
import com.shurfll.jpirates.event.Event;

/**
 * Created by kakim on 26.10.2017.
 */
public class InterruptedException extends ActorException {
    private Event causeEvent;

    public InterruptedException(String actorId, Event causeEvent) {
        super(actorId);
        this.causeEvent = causeEvent;
    }

    public Event getCauseEvent() {
        return causeEvent;
    }

    public void setCauseEvent(Event causeEvent) {
        this.causeEvent = causeEvent;
    }

    @Override
    public String toString() {
        return "InterruptedException{actorId=" +getActorId() +",causeEvent="+causeEvent+"} ";
    }
}
