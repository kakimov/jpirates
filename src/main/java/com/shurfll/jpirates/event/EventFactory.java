package com.shurfll.jpirates.event;

import com.shurfll.jpirates.IDGenerator;
import com.shurfll.jpirates.entity.*;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.event.action.admin.ConfigureModuleAction;
import com.shurfll.jpirates.event.action.admin.NewsAction;
import com.shurfll.jpirates.event.action.player.*;
import com.shurfll.jpirates.event.system.*;
import com.shurfll.jpirates.telegram.event.QuestEvent;
import com.shurfll.jpirates.telegram.keyboard.Keyboard;
import org.springframework.util.CollectionUtils;

import javax.xml.namespace.QName;
import java.util.*;
import java.util.stream.Collectors;

public class EventFactory {

    public static RepairAction repairAction(Event causeEvent, String actorId) {
        return repairAction(generateID(), getCauseEventOid(causeEvent), actorId);
    }

    public static RepairAction repairAction(String oid, String causeOid, String actorId) {
        return new RepairAction(oid, causeOid, actorId);
    }

    public static MessageAction messageAction(Event causeEvent, String actorId, String message) {
        return messageAction(generateID(), getCauseEventOid(causeEvent), actorId, message);
    }

    public static MessageAction messageAction(String oid, String causeOid, String actorId, String message) {
        return new MessageAction(oid, causeOid, actorId, message);
    }

    public static LookAroundAction lookAroundAction(Event causeEvent, String actorId) {
        return lookAroundAction(generateID(), getCauseEventOid(causeEvent), actorId);
    }

    public static LookAroundAction lookAroundAction(String oid, String causeOid, String actorId) {
        return new LookAroundAction(oid, causeOid, actorId);
    }

    public static SpendGoldAction spendGoldAction(Event causeEvent, String actorId, GoldSpending goldSpending) {
        return spendGoldAction(generateID(), getCauseEventOid(causeEvent), actorId, goldSpending);
    }

    public static SpendGoldAction spendGoldAction(String oid, String causeOid, String actorId, GoldSpending goldSpending) {
        return new SpendGoldAction(oid, causeOid, actorId, goldSpending);
    }

    public static LevelUpAction levelUpAction(Event causeEvent, String actorId, LevelUpMode mode) {
        return levelUpAction(generateID(), getCauseEventOid(causeEvent), actorId, mode);
    }

    public static LevelUpAction levelUpAction(String oid, String causeOid, String actorId, LevelUpMode mode) {
        return new LevelUpAction(oid, causeOid, actorId, mode);
    }

    public static StatusAction statusAction(Event causeEvent, String actorId) {
        return statusAction(generateID(), getCauseEventOid(causeEvent), actorId);
    }

    public static StatusAction statusAction(String oid, String causeOid, String actorId) {
        return new StatusAction(oid, causeOid, actorId);
    }

    public static SearchAction searchAction(Event causeEvent, String actorId) {
        return searchAction(generateID(), getCauseEventOid(causeEvent), actorId);
    }

    public static SearchAction searchAction(String oid, String causeOid, String actorId) {
        return new SearchAction(oid, causeOid, actorId);
    }

    public static MoveAction moveAction(Event causeEvent, String actorId, Direction direction) {
        return moveAction(generateID(), getCauseEventOid(causeEvent), actorId, direction);
    }

    public static MoveAction moveAction(String oid ,String causeOid, String actorId, Direction direction) {
        return new MoveAction(oid, causeOid, actorId, direction);
    }

    public static QuestAction questAction(Event causeEvent, String actorId, String questId) {
        return questAction(generateID(), getCauseEventOid(causeEvent), actorId, questId);
    }

    public static QuestAction questAction(String oid, String causeOid, String actorId, String questId) {
        return new QuestAction(oid, causeOid, actorId, questId);
    }

    public static StartAction startAction(Event causeEvent, String actorId, String actorName, Location location) {
        return startAction(generateID(), getCauseEventOid(causeEvent), actorId, actorName, location);
    }

    public static NewsAction newsAction(Event causeEvent, String actorId, String text) {
        return new NewsAction(generateID(), getCauseEventOid(causeEvent), actorId, text);
    }

    public static StartAction startAction(String oid, String causeOid, String actorId, String actorName, Location location) {
        return new StartAction(oid, causeOid, actorId, actorName, location);
    }

    public static FireAction fireAction(Event causeEvent, Actor actor, Actor target) {
        return fireAction(generateID(), getCauseEventOid(causeEvent), actor.getActorId(), target.getActorId(), target.getName());
    }

    public static FireAction fireAction(Event causeEvent, String actorId, String targetId, String targetName) {
        return fireAction(generateID(), getCauseEventOid(causeEvent), actorId, targetId, targetName);
    }

    public static FireAction fireAction(String oid,String causeOid, String actorId, String targetId, String targetName) {
        return new FireAction(oid, causeOid, actorId, targetId, targetName);
    }

    public static StateChangeEvent stateChangeEventReplace(Event causeEvent, String actorId, QName key, Object value) {
        Map<QName, Object> map = new HashMap<>();
        map.put(key, value);
        return stateChangeEventReplace(causeEvent, actorId, map);
    }

    public static StateChangeEvent stateChangeEventModify(Event causeEvent, String actorId, QName key, Object value) {
        Map<QName, Object> map = new HashMap<>();
        map.put(key, value);
        return stateChangeEventModify(causeEvent, actorId, map);
    }

    /**
     * Изменения должны применяться в режиме модифицирования текущего значения
     *
     * @param causeEvent
     * @param actorId
     * @param changes
     * @return
     */
    public static StateChangeEvent stateChangeEventModify(Event causeEvent, String actorId, Map<QName, Object> changes) {
        return new StateChangeEvent(generateID(), getCauseEventOid(causeEvent), actorId, convertChanges(changes, AttributeChangeMode.MODIFY));
    }

    public static StateChangeEvent stateChangeEventReplace(Event causeEvent, String actorId, Map<QName, Object> changes) {
        return new StateChangeEvent(generateID(), getCauseEventOid(causeEvent), actorId, convertChanges(changes, AttributeChangeMode.REPLACE));
    }

    public static StateChangedEvent stateChangedEvent(Event causeEvent, String actorId) {
        return new StateChangedEvent(generateID(), getCauseEventOid(causeEvent), actorId);
    }

    private static Map<QName, StateChangeEvent.Change> convertChanges(Map<QName, Object> changes, AttributeChangeMode mode) {
        return !CollectionUtils.isEmpty(changes) ?
                changes.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> new StateChangeEvent.Change(e.getValue(), mode)))
                : Collections.emptyMap();
    }

    public static QuestEvent questEvent(Event causeEvent, String targetId, String questId, String message, Keyboard keyboard) {
        return new QuestEvent(generateID(), getCauseEventOid(causeEvent), targetId, questId, message, keyboard);
    }

    public static SailAwayEvent sailAwayEvent(Event causeEvent, Actor actor, List<Object> objects, Direction direction) {
        return new SailAwayEvent(generateID(), getCauseEventOid(causeEvent), actor.getActorId(), objects, direction);
    }

    public static MeetEvent meetEvent(Event causeEvent, Actor actor, List<Object> objects) {
        return new MeetEvent(generateID(), getCauseEventOid(causeEvent), actor.getActorId(), objects);
    }

    public static BattleLoseEvent battleLoseEvent(Event causeEvent, Actor loser) {
        return new BattleLoseEvent(generateID(), getCauseEventOid(causeEvent), loser.getActorId());
    }

    public static BattleWonEvent battleWonEvent(Event causeEvent, Actor winner, Actor loser, Long goldEarned) {
        return new BattleWonEvent(generateID(), getCauseEventOid(causeEvent), winner.getActorId(), loser.getActorId(), loser.getName(), goldEarned);
    }

    public static DamageReceivedEvent damageReceivedEvent(Event causeEvent,String sourceActorId, String targetActorId, FireResult fireResult) {
        return new DamageReceivedEvent(generateID(), getCauseEventOid(causeEvent), sourceActorId, targetActorId, fireResult);
    }

    public static DamageDeliveredEvent damageDeliveredEvent(Event causeEvent, Actor whoMadeDamageActor, FireResult fireResult) {
        return new DamageDeliveredEvent(generateID(), getCauseEventOid(causeEvent), whoMadeDamageActor.getActorId(), fireResult);
    }

    public static LocationChangeEvent locationChangeEvent(Event causeEvent, String targetActorId, Location prevLocation, Location currentLocation, Direction direction) {
        return new LocationChangeEvent(generateID(), getCauseEventOid(causeEvent), targetActorId, prevLocation, currentLocation, direction);
    }

    public static InfoEvent infoEvent(Event causeEvent, String targetActorId, String message, Object... args) {
        return new InfoEvent(generateID(), getCauseEventOid(causeEvent), targetActorId, message, args);
    }

    public static TreasureFoundEvent treasureFoundEvent(Event causeEvent, String targetActorId, Treasure treasure, Location location) {
        return new TreasureFoundEvent(generateID(), getCauseEventOid(causeEvent), targetActorId, location, treasure);
    }

    public static TreasureHiddenEvent treasureHiddenEvent(Treasure treasure, Location location) {
        return new TreasureHiddenEvent(generateID(), getCauseEventOid(null), location, treasure);
    }

    public static EmergeEvent emergeEvent(Event causeEvent, String actorId, Location location) {
        return new EmergeEvent(generateID(), getCauseEventOid(causeEvent), actorId, location);
    }

    public static ActorStartedEvent actorStartedEvent(Event causeEvent, Actor actor, Location location) {
        return new ActorStartedEvent(generateID(), getCauseEventOid(causeEvent), actor.getActorId(), actor, location);
    }

    public static ConfigureModuleAction configureModuleAction(Event causeEvent, String actorId, Configuration configuration) {
        return configureModuleAction(generateID(), getCauseEventOid(causeEvent), actorId, configuration);
    }

    public static ConfigureModuleAction configureModuleAction(String oid, String causeOid, String actorId, Configuration configuration) {
        return new ConfigureModuleAction(oid, causeOid, actorId, configuration);
    }

    private static String generateID() {
        return IDGenerator.generateID();
    }

    private static String getCauseEventOid(Event event) {
        return Optional.ofNullable(event).map(event1 -> event1.getOid()).orElse(null);
    }
}
