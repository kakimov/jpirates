package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.service.player.RepairActionService;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.player.RepairAction;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.observers.BaseTestConsumer;
import io.reactivex.processors.PublishProcessor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;


import java.util.Collections;

import static com.shurfll.jpirates.Commons.loc;
import static org.mockito.Mockito.*;

public class RepairActionServiceTest {


    private static final String PLAYER_ID = "id";
    private static final String PLAYER_NAME = "name";
    private static final Player PLAYER = new Player(PLAYER_ID, PLAYER_NAME);


    private RepairActionService service;
    private PlayersManager playersManager;
    private StateManager stateManager;
    private TestSubscriber<Event> subscriber;
    private Reactor reactor;
    private PublishProcessor<Event> subject;

    @Before
    public void init() {
        playersManager = mock(PlayersManager.class);
        stateManager = mock(StateManager.class);
        reactor = mock(Reactor.class);
        subject = PublishProcessor.create();
        when(reactor.events()).thenReturn(subject);
        service = new RepairActionService(playersManager,stateManager, reactor);
        subscriber = new TestSubscriber<>();
        initPlayer();
    }

    private void initPlayer() {
        Player player = new Player(PLAYER_ID, PLAYER_NAME);
        playersManager.addPlayer(player, loc(0L, 1L));
    }

    @Test
    public void test001_doAction_no_need_in_repair() {
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID))).thenReturn(Flowable.just(PLAYER));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createHealthState(PLAYER_ID, 10L, 10L, 1L, 1L)));
        RepairAction action = EventFactory.repairAction(null, PLAYER_ID);
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertValueCount(1);
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.REPAIR_NO_NEED));
    }

    @Test
    public void test002_doAction_do_full_repair() {
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID))).thenReturn(Flowable.just(PLAYER));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createHealthState(PLAYER_ID, 1L, 5L, 100L, 1L)));
        RepairAction action = EventFactory.repairAction(null, PLAYER_ID);
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitCount(10, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createHealthState(PLAYER_ID, 5L, 5L, 100L, 1L)));
        subject.onNext(EventFactory.stateChangedEvent(null, PLAYER_ID));
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventsProduced(subscriber, StateChangeEvent.class, e -> TestEventHelper.stateChangeEventMatched(e, PLAYER_ID, ActorStateAttributes.ACTOR_HEALTH, 1L), 5L);
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, e -> TestEventHelper.infoEventMatched(e, PLAYER_ID, LocalizationKeys.REPAIR_COMPLETED));
    }

    @Test
    public void test003_doAction_unsubscribe_in_middle() throws  Exception{
        when(playersManager.getPlayerFlowable(eq(PLAYER_ID))).thenReturn(Flowable.just(PLAYER));
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createHealthState(PLAYER_ID, 1L, 5L, 100L, 1L)));
        RepairAction action = EventFactory.repairAction(null, PLAYER_ID);

        service.doAction(action).subscribe(subscriber);
        subscriber.awaitCount(4, BaseTestConsumer.TestWaitStrategy.SLEEP_10MS, 300);

        subscriber.assertValueCount(4);
    }



    // TODO cover with tests these scenarios, but in another test suite
    // because ActionService knows nothing about interruptions and other stuff
   /* @Test
    void test003_doAction_repair_interruption_event() {
        RepairAction action = new RepairAction(PLAYER_ID);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        playersManager.setState(PLAYER_ID, createHealthState(PLAYER_ID,1, 5,100,1));
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        assertEquals(playersManager.getState(PLAYER_ID).getHealth(),playersManager.getState(PLAYER_ID).getTotalHealth(), "Players health should be fully restored");
    }

    @Test
    void test004_doAction_repair_noninterruption_event() {
        RepairAction action = new RepairAction(PLAYER_ID);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        playersManager.setState(PLAYER_ID, createHealthState(PLAYER_ID,1, 5,100,1));
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        assertEquals(playersManager.getState(PLAYER_ID).getHealth(),playersManager.getState(PLAYER_ID).getTotalHealth(), "Players health should be fully restored");
    }

    @Test
    void test005_doAction_repair_interruption_by_player() {
        RepairAction action = new RepairAction(PLAYER_ID);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
    }

    @Test
    void test006_doAction_full_repair_with_status_command() {
        RepairAction action = new RepairAction(PLAYER_ID);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        service.doAction(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
    }*/


    private State createHealthState(String ownerOid, Long health, Long totalHealth, Long repairingSpeed, Long repairingAmount) {
        ObjectFactory objectFactory = new ObjectFactory(Collections.emptyList());
        State state = objectFactory.createState(null,ownerOid, Commons.generateLocation());
        ActorHelper.setActorHeath(objectFactory, state, health);
        ActorHelper.setActorTotalHeath(objectFactory, state, totalHealth);
        ActorHelper.setActorHealthRepairingSpeed(objectFactory, state, repairingSpeed);
        ActorHelper.setActorHealthRepairingAmount(objectFactory, state, repairingAmount);
        return state;
    }

}