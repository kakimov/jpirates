package com.shurfll.jpirates;

import com.shurfll.jpirates.event.PersonalEvent;
import io.reactivex.Flowable;


/**
 * Maintain the list of active subscribers
 */

public interface SubscriptionManager {

    public Flowable<PersonalEvent> personalEventsFlowable(String playerId);
}
