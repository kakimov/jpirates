package com.shurfll.jpirates;

import java.nio.file.Files;
import java.nio.file.Paths;

public class TestInstruments {
    public static String readResource(String path, String name) {
        try {
            return new String(Files.readAllBytes(Paths.get(TestInstruments.class.getResource(path + "/" + name).toURI())), "UTF-8");
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
