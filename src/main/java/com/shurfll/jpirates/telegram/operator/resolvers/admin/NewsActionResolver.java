package com.shurfll.jpirates.telegram.operator.resolvers.admin;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
@Qualifier("admin")
public class NewsActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/news)";


    public NewsActionResolver() {
    }

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public boolean canResolve(List<Message> messages) {
        boolean result = super.canResolve(messages);
        return result && messages.size() > 1;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        Message message1 = messages.get(0);
        Message message2 = messages.get(1);
        return Optional.of(EventFactory.newsAction(null, String.valueOf(message1.getChatId()), message2.getText().get()));
    }

}
