package com.shurfll.jpirates.persistence.converter.attribute;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.shurfll.jpirates.entity.AttributeHelper;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;

import javax.xml.namespace.QName;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.shurfll.jpirates.entity.EntityAttributeType.valueOf;

public abstract class AbstractComplexAttributeValueConverter extends AbstractAttributeValueConverter{

    private ValueConverter valueConverter;

    public AbstractComplexAttributeValueConverter(String type, ValueConverter valueConverter) {
        super(type);
        this.valueConverter = valueConverter;
    }

    protected Map.Entry<QName, EntityAttribute> convertJsonEntityAttribute(JsonObject jsonObject) {
        String namespace = jsonObject.getAsJsonPrimitive("namespace").getAsString();
        String name = jsonObject.getAsJsonPrimitive("name").getAsString();
        return new AbstractMap.SimpleEntry<>(new QName(namespace, name), convertJsonToEntityAttribute(namespace, name, jsonObject));
    }

    EntityAttribute convertJsonToEntityAttribute(String namespace, String name, JsonObject json) {
        JsonObject attributeContainer = json.getAsJsonObject("attribute");
        String id = attributeContainer.getAsJsonPrimitive("id").getAsString();
        String entityId = attributeContainer.getAsJsonPrimitive("entityId").getAsString();
        String type = attributeContainer.getAsJsonPrimitive("type").getAsString();
        EntityAttributeType attrType = valueOf(type);
        JsonArray values = attributeContainer.getAsJsonArray("values");
        List<EntityAttributeValue> attrValues = StreamSupport.stream(values.spliterator(), true)
                .map(JsonElement::getAsJsonObject)
                .map(jsonElement -> convertJsonToEntityAttributeValue(jsonElement, attrType))
                .collect(Collectors.toList());
        EntityAttribute attr = AttributeHelper.createAttribute(entityId, id, new QName(namespace, name), attrType, attrValues);
        return attr;
    }

    private EntityAttributeValue convertJsonToEntityAttributeValue(JsonObject obj, EntityAttributeType type) {
        String id = obj.getAsJsonPrimitive("id").getAsString();
        String attributeId = obj.getAsJsonPrimitive("attributeId").getAsString();
        Object value = valueConverter.deserialize(type,obj.get("value").getAsString());

        return AttributeHelper.createAttributeValue(id, attributeId, value);
    }

    protected JsonObject convertEntityAttributeToJson(EntityAttribute attribute) {
        JsonObject jsonAttr = new JsonObject();
        jsonAttr.addProperty("name", attribute.getName().getLocalPart());
        jsonAttr.addProperty("namespace", attribute.getName().getNamespaceURI());
        jsonAttr.add("attribute", convertEntityAttributeToJsonAttribute(attribute));
        return jsonAttr;
    }

    private JsonObject convertEntityAttributeToJsonAttribute(EntityAttribute attribute) {
        JsonObject jsonAttr = new JsonObject();
        jsonAttr.addProperty("id", attribute.getId());
        jsonAttr.addProperty("entityId", attribute.getEntityId());
        jsonAttr.addProperty("name", attribute.getName().getLocalPart());
        jsonAttr.addProperty("type", attribute.getType().name());
        jsonAttr.add("values", convertEntityAttributeValuesToJson(attribute.getValues()));
        return jsonAttr;
    }

    private JsonArray convertEntityAttributeValuesToJson(List<EntityAttributeValue> values) {
        JsonArray valuesJson = new JsonArray();
        values.stream().forEach(val -> valuesJson.add(convertEntityAttributeValueToJson(val)));
        return valuesJson;
    }

    private JsonObject convertEntityAttributeValueToJson(EntityAttributeValue value) {
        JsonObject valJson = new JsonObject();
        valJson.addProperty("id", value.getId());
        valJson.addProperty("attributeId", value.getAttributeId());
        valJson.addProperty("value", valueConverter.serialize(value.getValue()));
        return valJson;
    }

}
