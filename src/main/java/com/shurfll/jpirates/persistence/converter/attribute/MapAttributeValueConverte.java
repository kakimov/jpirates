package com.shurfll.jpirates.persistence.converter.attribute;

import com.google.gson.reflect.TypeToken;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.EntityAttributeValue;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.converter.attribute.util.TypeMapItem;
import com.shurfll.jpirates.persistence.converter.value.ValueConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MapAttributeValueConverte extends AbstractAttributeValueConverter {


    private Type mapType = new TypeToken<List<TypeMapItem>>() {}.getType();

    private ValueConverter valueConverter;


    @Autowired
    public MapAttributeValueConverte(ValueConverter valueConverter) {
        super(EntityAttributeType.MAP.name());
        this.valueConverter = valueConverter;
        setDeserializer(attr -> deserializeAttribute(attr, EntityAttributeType.MAP, this::convertAttrToMap));
        setSerializer((owner, attr) -> serializeAttribute(owner, attr, this::convertMapToAttr));
    }

    private String convertMapToAttr(EntityAttributeValue value) {
        Map<String, Object> map = (Map<String, Object>) value.getValue();
        List<TypeMapItem> items = map.entrySet().stream()
                .map(e -> new TypeMapItem(e.getKey(), e.getValue(), valueConverter.serialize(e.getValue())))
                .map(TypeMapItem.class::cast)
                .collect(Collectors.toList());

        return gson.toJson(items);
    }


    private Map convertAttrToMap(Attribute attr) {
        List<TypeMapItem> items = gson.fromJson(attr.getValue(), mapType);
        return items.stream().collect(Collectors.toMap(o -> o.getName(), o -> o.buildObject(gson)));
    }

}
