package com.shurfll.jpirates.effect.handler;

import com.shurfll.jpirates.effect.Effect;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


public interface EffectHandler<T extends Effect> {

    boolean canHandle(Effect effect);

    Flowable<Event> handle(Action action, T effect);
}
