package com.shurfll.jpirates.entity.actor;

/**
 * Created by shurfll on 22.05.17.
 */
public class Player extends Actor {
    public Player(String actorId, String name) {
        super(ActorType.PLAYER, actorId, name);
    }

    public Player(String oid, String actorId, String name) {
        super(ActorType.PLAYER, oid, actorId, name);
    }

    @Override
    public String toString() {
        return getName();
    }
}
