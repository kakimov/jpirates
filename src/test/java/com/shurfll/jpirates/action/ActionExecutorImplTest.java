package com.shurfll.jpirates.action;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.action.interruption.ActionInterruptionRules;
import com.shurfll.jpirates.effect.AbsoluteActionBlockingEffect;
import com.shurfll.jpirates.effect.Effect;
import com.shurfll.jpirates.effect.EffectManager;
import com.shurfll.jpirates.entity.actor.Actor;
import com.shurfll.jpirates.entity.actor.Player;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;

import io.reactivex.subscribers.TestSubscriber;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;


/**
 * Created by kakim on 23.10.2017.
 */
public class ActionExecutorImplTest {

    private static final String PLAYER_ID = "333";

    private Actor actor;

    private ActionExecutor actionExecutor;

    private ActionInterruptionRules interRights;

    private PublishProcessor<Event> eventPublisher;

    private Reactor reactor;

    private EffectManager effectManager;

    @Before
    public void setUp() {
        actor = new Player(PLAYER_ID, PLAYER_ID);
        eventPublisher = PublishProcessor.create();
        reactor = mock(Reactor.class);
        when(reactor.events()).thenReturn(eventPublisher);
        interRights = mock(ActionInterruptionRules.class);
        effectManager = mock(EffectManager.class);
        when(effectManager.handleAction(any(Action.class))).thenAnswer(invocationOnMock -> Flowable.just(invocationOnMock.getArguments()[0]).ofType(Action.class));
        actionExecutor = new ActionExecutorImpl(reactor, interRights, effectManager);
        ((ActionExecutorImpl)actionExecutor).setPlayerActionServices(Arrays.asList(new TestActionService(), new TestLongActionService()));
    }


    @Test
    public void test001_execute_immediate() {
        TestEvent event = createTestEvent();
        TestAction action = createTestAction(PLAYER_ID, 0L, event);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent(100, TimeUnit.MILLISECONDS);
        TestEventHelper.checkEventProduced(subscriber, TestEvent.class, ev -> ev.equals(event));
    }

    @Test
    public void test002_execute_delay_100ms() {
        TestEvent event = createTestEvent();
        TestAction action = createTestAction(PLAYER_ID, 100L, event);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.assertNotComplete();
        subscriber.awaitTerminalEvent(200, TimeUnit.SECONDS);
        TestEventHelper.checkEventProduced(subscriber, TestEvent.class, ev -> ev.equals(event));
    }

    @Test
    public void test003_execute_unsupportedAction() throws Exception {
        UnknownAction action = new UnknownAction(null, null, "test");
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitCount(1);
        assertInfoEvent(subscriber.values().get(0), "test", LocalizationKeys.EXCEPTION_ACTION_IS_UNSUPPORTED);
    }

    @Test
    public void test004_execute_busy_another_action() throws Exception {
        TestAction blockerAction = createTestAction(PLAYER_ID, 100L);
        TestAction action = createTestAction(PLAYER_ID, 0L);
        TestSubscriber<Event> blockerSubscriber = new TestSubscriber<>();
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(blockerAction).subscribe(blockerSubscriber);
        when(effectManager.handleAction(eq(action)))
                .thenReturn(Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.EXCEPTION_ACTION_IN_PROGRESS)));
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent(100, TimeUnit.MILLISECONDS);
        // unable to execute
        subscriber.assertValueCount(1);
        assertInfoEvent(subscriber.values().get(0), PLAYER_ID, LocalizationKeys.EXCEPTION_ACTION_IN_PROGRESS);
        // blocker executed correctly
        blockerSubscriber.awaitTerminalEvent(100, TimeUnit.MILLISECONDS);
    }

    @Test
    public void test005_execute_free_after_another_action_completed() throws Exception {
        TestAction blockerAction = createTestAction(PLAYER_ID, 100L);
        TestAction action = createTestAction(PLAYER_ID, 0L);
        TestSubscriber<Event> blockerSubscriber = new TestSubscriber<>();
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(blockerAction).subscribe(blockerSubscriber);
        blockerSubscriber.awaitTerminalEvent(200, TimeUnit.MILLISECONDS);
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent(200, TimeUnit.MILLISECONDS);
    }

    @Test
    public void test006_execute_action_interrupted_by_event_on_preparing_phase() {
        initMock(TestAction.class, InterruptorEvent.class,true);
        TestAction action = createTestAction(PLAYER_ID, 1000L);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.assertNotComplete();
        eventPublisher.onNext(new InterruptorEvent(null, null, PLAYER_ID));
        subscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);
        subscriber.assertValueCount(1);
        TestEventHelper.checkEventProduced(subscriber,InfoEvent.class,e -> infoEventMatched((InfoEvent) e, PLAYER_ID,LocalizationKeys.EXCEPTION_ACTION_INTERRUPTED));
        verify(effectManager,times(1)).removeEffect(eq(PLAYER_ID),any(Effect.class));
    }

    @Test
    public void test007_execute_action_interrupted_by_event_on_executing_phase() throws Exception{
        initMock(TestLongAction.class, InterruptorEvent.class, true);
        TestLongAction action = createTestLongAction(PLAYER_ID, 1000L, new TestEvent(null, PLAYER_ID));
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.assertNotComplete();
        Thread.sleep(500);
        eventPublisher.onNext(new InterruptorEvent(null, null, PLAYER_ID));
        subscriber.awaitTerminalEvent(1000, TimeUnit.MILLISECONDS);
        subscriber.assertValueCount(1);
        TestEventHelper.checkEventProduced(subscriber,InfoEvent.class,e -> infoEventMatched((InfoEvent) e, PLAYER_ID,LocalizationKeys.EXCEPTION_ACTION_INTERRUPTED));
        verify(effectManager,times(1)).removeEffect(eq(PLAYER_ID),any(Effect.class));
    }

    @Test
    public void test008_execute_action_not_interrupted_by_event() {
        initMock(TestAction.class, InterruptorEvent.class,false);
        TestAction action = createTestAction(PLAYER_ID, 100L);
        action.setEvent(new TestEvent(null, PLAYER_ID));
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.assertNotComplete();
        reactor.submitEvent(new InterruptorEvent(null, null, PLAYER_ID));
        subscriber.awaitTerminalEvent(200, TimeUnit.MILLISECONDS);
        subscriber.assertValueCount(1);
        subscriber.assertNoErrors();
    }

    @Test
    @DisplayName("In case of EXCEPTION_ACTION_IN_PROGRESS the blocking effect shouldn't break on blocking action until it's done")
    public void test009_another_action_in_process() throws Exception {
        TestAction blockerAction = createTestAction(PLAYER_ID, 2000L);
        blockerAction.setOid("blocker_action");
        TestAction action = createTestAction(PLAYER_ID, 0L);
        action.setOid("action");
        TestSubscriber<Event> blockerSubscriber = new TestSubscriber<>();
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(blockerAction).subscribe(blockerSubscriber);
        when(effectManager.handleAction(eq(action)))
                .thenReturn(Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.EXCEPTION_ACTION_IN_PROGRESS)));
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent(100, TimeUnit.MILLISECONDS);
        // unable to execute
        subscriber.assertValueCount(1);
        assertInfoEvent(subscriber.values().get(0), PLAYER_ID, LocalizationKeys.EXCEPTION_ACTION_IN_PROGRESS);

        subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent(100, TimeUnit.MILLISECONDS);
        // unable to execute
        subscriber.assertValueCount(1);
        assertInfoEvent(subscriber.values().get(0), PLAYER_ID, LocalizationKeys.EXCEPTION_ACTION_IN_PROGRESS);
    }

    @Test
    public void test0010_interruption_should_clear_current_action() throws Exception {
        TestAction action = createTestAction(PLAYER_ID, 100L);
        InterruptorEvent event = new InterruptorEvent(null, null, "test");
        when(interRights.isInterruptedByEvent(eq(action), eq(event))).thenReturn(true);
        TestSubscriber<Event> subscriber = new TestSubscriber<>();
        actionExecutor.execute(action).subscribe(subscriber);
        eventPublisher.onNext(event);
        subscriber.awaitTerminalEvent();
        TestEventHelper.checkEventProduced(subscriber, InfoEvent.class, event1 -> infoEventMatched((InfoEvent) event1, PLAYER_ID, LocalizationKeys.EXCEPTION_ACTION_INTERRUPTED));

        // check if the action action block is off
        subscriber = TestSubscriber.create();
        actionExecutor.execute(action).subscribe(subscriber);
        subscriber.awaitTerminalEvent(100, TimeUnit.MILLISECONDS);
        subscriber.assertNoValues();
    }

    @Test
    public void test011_block_is_off_after_action() throws Exception {
        TestSubscriber subscriber = TestSubscriber.create();
        TestAction action = createTestAction(PLAYER_ID, 100L);
        actionExecutor.execute(action).subscribe(subscriber);
        verify(effectManager, times(1)).addEffect(eq(PLAYER_ID), any(AbsoluteActionBlockingEffect.class));
        subscriber.awaitTerminalEvent(200L, TimeUnit.MILLISECONDS);
        verify(effectManager, times(1)).removeEffect(eq(PLAYER_ID), any(AbsoluteActionBlockingEffect.class));

    }


    private boolean infoEventMatched(InfoEvent event, String actorId, String messageKey) {
        return actorId.equals(event.getTargetActorId()) && messageKey.equals(event.getMessage());
    }

    private TestEvent createTestEvent() {
        TestEvent event = new TestEvent("oid", null);
        return event;
    }

    private TestAction createTestAction(String playerId, Long actionDelay, TestEvent event) {
        TestAction action = new TestAction("oid", null, playerId, event);
        action.addMeta(ActionCommons.ACTION_DELAY_META_KEY, actionDelay);
        return action;
    }

    private TestLongAction createTestLongAction(String playerId, Long actionDelay, TestEvent event) {
        TestLongAction action = new TestLongAction("oid", null, playerId, event, actionDelay);
        return action;
    }

    private TestAction createTestAction(String playerId, Long actionDelay) {
        return createTestAction(playerId, actionDelay, null);
    }

    private <T extends Action, V extends Event> void initMock(Class<T> actionClass, Class<V> eventClass,boolean returnValue) {
        when(interRights.isInterruptedByEvent(any(actionClass), any(eventClass))).thenAnswer(i -> returnValue);
    }

    private ArgumentMatcher<Action> createTestActionMatcher(String actorId) {
        return o -> o instanceof TestAction && ((TestAction) o).getSourceActorId().equals(actorId);

    }

    private ArgumentMatcher<Event> createTestEventMatcher(String targetId, Class eventClass) {
        return o -> o instanceof InterruptorEvent && ((InterruptorEvent) o).getTargetActorId().equals(targetId);
    }

    private void assertInfoEvent(Event event, String targetId, String message) {
        Assert.assertTrue(event instanceof InfoEvent);
        Assert.assertEquals(targetId, ((InfoEvent) event).getTargetActorId());
        Assert.assertEquals(message, ((InfoEvent) event).getMessage());
    }


}