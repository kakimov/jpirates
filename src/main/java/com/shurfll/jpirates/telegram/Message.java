package com.shurfll.jpirates.telegram;

import org.telegram.telegrambots.api.objects.CallbackQuery;

import java.util.Optional;

public class Message {
    private org.telegram.telegrambots.api.objects.Update update;
    private Long chatId;
    private Optional<String> text;

    public Message(org.telegram.telegrambots.api.objects.Update update) {
        this.update = update;
        this.chatId = Optional.ofNullable(update.getCallbackQuery()).map(CallbackQuery::getMessage).orElse(update.getMessage()).getChatId();
        this.text = Optional.ofNullable(Optional.ofNullable(update.getCallbackQuery()).map(CallbackQuery::getData)
                .orElse(Optional.ofNullable(update.getMessage()).map(message -> message.getText()).orElse(null)));
    }

    public Message(Long chatId, String text) {
        this.chatId = chatId;
        this.text = Optional.ofNullable(text);
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Optional<String> getText() {
        return text;
    }

    public void setText(Optional<String> text) {
        this.text = text;
    }



    @Override
    public String toString() {
        return "Message{" +
                "chatId=" + chatId +
                ", text='" + text + '\'' +
                '}';
    }
}
