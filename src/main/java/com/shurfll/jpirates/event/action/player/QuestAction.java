package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Created by shurfll on 22.05.17.
 */
public class QuestAction extends PlayerAction {

    private static final String QUALIFIER = "questAction";
    public static final String TYPE = ACTION_BASE_NAMESPACE + "/" + QUALIFIER;

    public static final QName QUEST_ID = attribute(TYPE, "questId");

    public QuestAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, String questId) {
        super(oid, causeOid, sourceActorId, meta);
        putAttribute(oid, QUEST_ID, questId);
    }

    public QuestAction(String oid, String causeOid, String sourceActorId, String questId) {
        this(oid, causeOid, sourceActorId, null, questId);
    }

    public String getQuestId() {
        return getValue(QUEST_ID, String.class).get();
    }

}
