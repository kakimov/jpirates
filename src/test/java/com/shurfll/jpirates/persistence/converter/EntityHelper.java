package com.shurfll.jpirates.persistence.converter;

import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.StateEntity;

import javax.xml.namespace.QName;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.shurfll.jpirates.entity.EntityAttributeType.STRING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EntityHelper {
    public static Map.Entry<QName, Object> entry(QName name, Object value) {
        return new AbstractMap.SimpleEntry<>(name, value);
    }

    public static void stateEntityAttrCheck(StateEntity entity, Map.Entry<QName, Object>  ... attrs) {
        stateEntityAttrCheck(entity, Stream.of(attrs).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
    }

    public static void stateEntityAttrCheck(StateEntity entity, Map<QName, Object> attrs) {
        assertEquals(attrs.size(), entity.getAttributes().size());
        entity.getAttributes().stream().forEach(a -> assertTrue("Assert failed: " + a + " expected in: " + attrs,isInExpected(a, attrs)));
        attrs.entrySet().stream().forEach(e -> assertTrue("Assert failed: " + e.getValue() + " expected in: " + entity.getAttributes(), isInActual(e, entity)));
    }

    public static void stateAttrCheck(StateEntity entity, Map.Entry<QName, Object>  attr) {
        stateEntityAttrCheck(entity, new Map.Entry[] {attr});
    }

    public static void entityAttrCheck(EventEntity entity, Map.Entry<QName, Object>  ... attrs) {
        entityAttrCheck(entity, Stream.of(attrs).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
    }

    public static void entityAttrCheck(EventEntity entity, Map<QName, Object> attrs) {
        assertEquals(attrs.size(), entity.getAttributes().size());
        entity.getAttributes().stream().forEach(a -> assertTrue("Assert failed: " + a + " expected in: " + attrs,isInExpected(a, attrs)));
        attrs.entrySet().stream().forEach(e -> assertTrue("Assert failed: " + e.getValue() + " expected in: " + entity.getAttributes(), isInActual(e, entity)));
    }

    public static boolean isInExpected(Attribute attr, Map<QName, Object> expected) {
        return expected.entrySet().stream().filter(e -> e.getKey().equals(new QName(attr.getNamespace(), attr.getName())) && e.getValue().equals(attr.getValue()))
                .findAny().isPresent();
    }

    public static boolean isInActual(Map.Entry<QName, Object> entry, EventEntity entity) {
        return entity.getAttributes().stream().filter(a -> entry.getKey().equals(new QName(a.getNamespace(), a.getName())) && a.getValue().equals(entry.getValue()))
                .findAny().isPresent();
    }

    public static boolean isInActual(Map.Entry<QName, Object> entry, StateEntity entity) {
        return entity.getAttributes().stream().filter(a -> entry.getKey().equals(new QName(a.getNamespace(), a.getName())) && a.getValue().equals(entry.getValue()))
                .findAny().isPresent();
    }

    public static EventEntity entity(String type, Attribute... attrs) {
        EventEntity entity = new EventEntity(type, Arrays.asList(attrs));
        Arrays.stream(attrs).forEach(a -> a.setOwner(entity));
        return entity;

    }

    public static StateEntity state(String oid,String ownerOid, Attribute... attrs) {
        StateEntity entity =  new StateEntity(oid,ownerOid, null);
        entity.setAttributes(Stream.of(attrs).collect(Collectors.toList()));
        entity.getAttributes().stream().forEach(a -> a.setOwner(entity));

        return entity;
    }

    public static Attribute strAttr(QName name, String value) {
        return attr(name, STRING, value);
    }

    public static Attribute attr(QName name, EntityAttributeType type, String value) {
        return new Attribute(null, null, name.getNamespaceURI(), name.getLocalPart(), type.name(), value);
    }
}
