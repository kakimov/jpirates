package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.entity.Quest;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.quest.QuestManager;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class QuestActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "#(.*)";

    @Autowired
    private QuestManager questManager;

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        return Optional.empty();
    }

    private String getQuestId(Message message1) {

        return message1.getText().map(s -> COMMAND_PATTERN.matcher(s))
                .map(matcher -> matcher.find() && matcher.groupCount() == 1 ? matcher.group(1) : null)
                .map(s -> s.trim()).orElse(null);
    }

    @Override
    public Optional<InfoEvent> makeResponse(List<Message> messages) {

        String questId = getQuestId(messages.get(0));
        Optional<Quest> questOpt = questManager.getQuestInfo(questId);
        InfoEvent event =  questOpt.map(quest -> (InfoEvent)EventFactory.questEvent(null, "" + messages.get(0).getChatId(),quest.getQuestId(), quest.getDescription(), QuestManager.createButtons(questManager.getChildQuests(messages.get(0).getChatId() + "", questId))))
                .orElse(EventFactory.infoEvent(null,""+messages.get(0).getChatId(), LocalizationKeys.EXCEPTION_ACTION_IS_UNSUPPORTED, messages.get(0).getText().orElse("")));
        return Optional.ofNullable(event);
    }
}
