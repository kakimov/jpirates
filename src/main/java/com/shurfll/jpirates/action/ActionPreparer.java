package com.shurfll.jpirates.action;

import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;


/**
 * Created by kakim on 23.10.2017.
 */
public interface ActionPreparer {
    Action prepare(Action action);
}
