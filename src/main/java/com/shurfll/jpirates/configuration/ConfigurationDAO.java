package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.Configuration;
import io.reactivex.Flowable;

import java.util.Optional;

public interface ConfigurationDAO {
    public Optional<Configuration> getConfiguration(String moduleUri);
    public void updateConfiguration(Configuration configuration);
    public Flowable<Configuration> updates();
}
