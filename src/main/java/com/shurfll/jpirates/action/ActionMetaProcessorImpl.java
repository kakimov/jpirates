package com.shurfll.jpirates.action;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.SearchAction;
import com.shurfll.jpirates.manager.PlayersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by kakim on 23.10.2017.
 */
@Component
public class ActionMetaProcessorImpl implements ActionMetaProcessor {

    private PlayersManager playersManager;

    @Autowired
    public ActionMetaProcessorImpl(PlayersManager playersManager) {
        this.playersManager = playersManager;
    }

    public void calculateMeta(Action action) {
        if (action instanceof SearchAction) {
            action.addMeta(ActionCommons.ACTION_DELAY_META_KEY, Commons.ACTION_SEARCH_DELAY);
        }
    }
}
