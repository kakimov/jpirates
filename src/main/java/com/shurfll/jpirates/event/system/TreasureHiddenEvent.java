package com.shurfll.jpirates.event.system;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.Treasure;

import javax.xml.namespace.QName;

public class TreasureHiddenEvent extends SystemEvent {

    private static final String QUALIFIER = "treasureHiddenEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName LOCATION = attribute(TYPE, "location");
    private static final QName TREASURE = attribute(TYPE, "treasure");

    public TreasureHiddenEvent(String oid, String causeOid, Location location, Treasure treasure) {
        super(oid, causeOid);
        putAttribute(oid, LOCATION, location);
        putAttribute(oid, TREASURE, treasure);
    }

    public Location getLocation() {
        return getValue(LOCATION, Location.class).get();
    }

    public Treasure getTreasure() {
        return getValue(TREASURE, Treasure.class).get();
    }

}
