package com.shurfll.jpirates.effect;

import com.shurfll.jpirates.effect.handler.EffectHandler;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;

@Component
public class EffectHandlerManagerImpl implements EffectHandlerManager {

    private List<EffectHandler> effectHandlers;

    @Autowired
    public EffectHandlerManagerImpl(List<EffectHandler> effectHandlers) {
        this.effectHandlers = effectHandlers;
    }

    @Override
    public Flowable<Event> calculateEffects(Action action, List<Effect> effects) {
        return blocking(action,effects).orElse(Flowable.just(nonblocking(action,effects)));
    }

    private Optional<EffectHandler> findHandler(Effect effect) {
        return effectHandlers.stream().filter(handler -> handler.canHandle(effect)).findFirst();
    }

    private Optional<Flowable<Event>> blocking(Action action, List<Effect> effects){
        return effects.stream()
                .filter(Effect::getBlocking)
                .findAny()
                .flatMap(effect -> findHandler(effect).map(effectHandler -> effectHandler.handle(action,effect)));
    }

    private Action nonblocking(Action action, List<Effect> effects){
        effects.stream()
                .filter(effect -> !effect.getBlocking())
                .forEach(effect -> findHandler(effect).ifPresent(effectHandler -> effectHandler.handle(action,effect)));
        return action;
    }

}
