package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.entity.Configuration;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class FileConfigurationDAO implements ConfigurationDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileConfigurationDAO.class);

    private ConfigurationParser configurationParser;
    private List<Configuration> configurations;
    private PublishProcessor<Configuration> updates = PublishProcessor.create();

    @Value("classpath*:/config/*.config")
    private Resource[] configs;

    @Autowired
    public FileConfigurationDAO(ConfigurationParser configurationParser) {
        this.configurationParser = configurationParser;
    }

    @PostConstruct
    public void init() throws Exception {

        configurations = Stream.of(configs)
                .map(this::readModuleConfigToString)
                .map(this::parseConfiguration)
                .collect(Collectors.toList());


    }

    @Override
    public Optional<Configuration> getConfiguration(String moduleId) {
        try {
            return configurations.stream().filter(c -> c.getModuleUri().equals(moduleId)).findFirst();
        } catch (Exception e) {
            LOGGER.error("Unable to read configuration:", e);
        }
        return Optional.empty();
    }

    @Override
    public void updateConfiguration(Configuration configuration) {
        updates.onNext(configuration);
    }

    @Override
    public Flowable<Configuration> updates() {
        return updates;
    }


    protected String readModuleConfigToString(Resource resource) {
        try {
            return IOUtils.toString(resource.getInputStream(), "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected Configuration parseConfiguration(String json) {
        try {
            return configurationParser.parseConfiguration(json);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Resource[] getConfigs() {
        return configs;
    }

    public void setConfigs(Resource[] configs) {
        this.configs = configs;
    }
}
