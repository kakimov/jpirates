package com.shurfll.jpirates.persistence.converter.value;

import com.shurfll.jpirates.entity.EntityAttributeType;
import org.springframework.stereotype.Component;

@Component
public class LongValueMapper extends AbstractValueMapper {
    public LongValueMapper() {
        super(EntityAttributeType.LONG, Long.class, Object::toString, s -> gson.fromJson(s, Long.class));
    }
}
