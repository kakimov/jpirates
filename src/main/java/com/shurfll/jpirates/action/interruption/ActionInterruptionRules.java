package com.shurfll.jpirates.action.interruption;

import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.action.Action;

/**
 * Created by kakim on 27.10.2017.
 */
public interface ActionInterruptionRules {
    boolean isInterruptedByEvent(Action action, Event event);
}
