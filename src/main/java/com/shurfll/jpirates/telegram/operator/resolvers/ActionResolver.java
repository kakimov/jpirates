package com.shurfll.jpirates.telegram.operator.resolvers;

import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Created by k.akimov on 09.06.2017.
 */
public abstract class ActionResolver {
    public static final Integer DEFAULT_PRIORITY = 10;
    public static final Integer HIGH_PRIORITY = 0;
    public static final Integer LOW_PRIORITY = 20;

    protected final Pattern COMMAND_PATTERN = Pattern.compile(getCommandPatternRegexp());

    protected abstract String getCommandPatternRegexp();

    public boolean canResolve(Message command) {
        return command.getText().map(text -> COMMAND_PATTERN.matcher(text).matches()).orElse(false);
    }

    public boolean canResolve(List<Message> messages) {
        return canResolve(messages.get(0));
    }

    public abstract Optional<Action> resolve(List<Message> messages);

    public Integer getPriority() {
        return DEFAULT_PRIORITY;
    }

    public Optional<InfoEvent> makeResponse(List<Message> messages) {
        return Optional.empty();
    }

    /**
     * Checks is the current command is might be accepted by the system
     * @param messageList
     * @return
     */
    public Optional<InfoEvent> validate(List<Message> messageList){
        return Optional.empty();
    }



}
