package com.shurfll.jpirates.telegram.operator.resolvers.player;

import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.PlayerActionResolver;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by k.akimov on 09.06.2017.
 */
@Component
public class RepairActionResolver extends PlayerActionResolver {

    private static final String COMMAND_PATTERN_STR = "(\\/repair)";

    @Override
    protected String getCommandPatternRegexp() {
        return COMMAND_PATTERN_STR;
    }

    @Override
    public Optional<Action> resolve(List<Message> messages) {
        Message message1 = messages.get(0);
        return Optional.of(EventFactory.repairAction(null, String.valueOf(message1.getChatId())));
    }
}
