package com.shurfll.jpirates.event.system;

import com.shurfll.jpirates.entity.AttributeChangeMode;
import com.shurfll.jpirates.event.PersonalEvent;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Event to change entities state
 */
public class StateChangeEvent extends PersonalEvent {
    private static final String QUALIFIER = "stateChangeEvent";
    public static final String TYPE = BASE_EVENT_NAMESPACE + "/" + QUALIFIER;
    private static final QName CHANGES = attribute(TYPE, "changes");

    public StateChangeEvent(String oid, String causeOid, String targetActorId, Map<QName, Change> changes) {
        super(oid, causeOid, targetActorId);
        if (changes != null)
            putAttribute(oid, CHANGES, changes);

    }

    public Map<QName, Change> getChanges() {
        return getValue(CHANGES, Map.class).get();
    }

    public static class Change {
        private Object value;
        private AttributeChangeMode mode;

        public Change(Object value, AttributeChangeMode mode) {
            this.value = value;
            this.mode = mode;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public AttributeChangeMode getMode() {
            return mode;
        }

        public void setMode(AttributeChangeMode mode) {
            this.mode = mode;
        }

        @Override
        public String toString() {
            return "Change{" +
                    "value=" + value +
                    ", mode=" + mode +
                    '}';
        }
    }

}
