package com.shurfll.jpirates.configuration;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;

import static org.junit.Assert.assertEquals;

public class ConfigurationParserImplTest {


    public static final String MODULE_URI = "http://test.com";

    public static final String PARAM_LONG = "param_long";
    public static final String PARAM_LOCATION = "param_location";
    public static final String CONFIG_JSON = "{\n" +
            "  \"moduleUri\": \"http://test.com\",\n" +
            "  \"moduleId\": \"moduleId\",\n" +
            "  \"attributes\": [\n" +
            "    {\n" +
            "      \"name\": \"param_long\",\n" +
            "      \"type\": \"LONG\",\n" +
            "      \"value\": \"7\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"param_location\",\n" +
            "      \"type\": \"LOCATION\",\n" +
            "      \"value\": {\n" +
            "        \"latitude\": \"-3\",\n" +
            "        \"longitude\": \"5\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}";
    private ConfigurationParser parser;
    private ObjectFactory objectFactory;

    @Before
    public void init() {
        objectFactory = new ObjectFactory();
        parser = new ConfigurationParserImpl(objectFactory);
    }

    @Test
    public void test001_parseConfiguration() {
        Configuration configuration = parser.parseConfiguration(CONFIG_JSON);
        assertEquals(MODULE_URI, configuration.getModuleUri());
        assertAttribute(configuration, new QName(MODULE_URI, PARAM_LONG), 7L);
        assertAttribute(configuration, new QName(MODULE_URI, PARAM_LOCATION), Commons.loc(-3l, 5l));
    }

    private void assertAttribute(Configuration conf, QName name, Object value) {
        EntityAttribute attr = conf.getAttributes().get(name);
        assertEquals(value, attr.getValue().get().getValue());
    }
}