package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.ActorStartedEvent;
import com.shurfll.jpirates.helper.LocationHelper;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ActorStartedEventHandler extends AbstractSelfregisteringEventHandler<ActorStartedEvent> {

    @Autowired
    public ActorStartedEventHandler(Reactor reactor) {
        super(reactor);
    }

    @Override
    public Class<ActorStartedEvent> getEventClass() {
        return ActorStartedEvent.class;
    }

    @Override
    public Flowable<Event> handle(ActorStartedEvent event) {
        return Flowable.just(EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.START_INTRO_COMMANDS, event.getActor().getName())
                , EventFactory.infoEvent(event, event.getTargetActorId(), LocalizationKeys.LOCATION_CHANGED_EVENT, LocationHelper.printLocation(event.getLocation()))
                , EventFactory.emergeEvent(event, event.getTargetActorId(), event.getLocation()));
    }
}
