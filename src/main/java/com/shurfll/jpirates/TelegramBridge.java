package com.shurfll.jpirates;

import com.shurfll.jpirates.action.ActionExecutor;
import com.shurfll.jpirates.action.ActionPreparer;
import com.shurfll.jpirates.event.InfoEvent;
import com.shurfll.jpirates.reactor.Reactor;
import com.shurfll.jpirates.telegram.ActionProducer;
import com.shurfll.jpirates.telegram.TelegramManager;
import io.reactivex.disposables.Disposable;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by k.akimov on 08.06.2017.
 */
@Component
public class TelegramBridge {

    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramBridge.class);

    @Autowired
    private ActionProducer actionProducer;
    @Autowired
    private ActionExecutor actionExecutor;
    @Autowired
    private TelegramManager telegramManager;
    @Autowired
    private Reactor reactor;
    @Autowired
    private ActionPreparer actionPreparer;

    @PostConstruct
    public void init() {
        Disposable subscription = actionProducer.actionFlowable()
                .map(actionPreparer::prepare)
                .flatMap(actionExecutor::execute)
                .doOnSubscribe((s) -> LOGGER.debug("bridge subscribed"))
                .doOnError(throwable -> LOGGER.error("Unpredictable error",throwable))
                .retry()
                .subscribe(reactor::submitEvent, t -> LOGGER.error("Unpredictable error", t));


        reactor.events()
                .ofType(InfoEvent.class)
                .doOnSubscribe(s -> LOGGER.info("Bot to player subscription is created"))
                .doOnTerminate(() -> LOGGER.warn("Bot to player subscription is terminated"))
                .retry()
                .subscribe(telegramManager::sendEvent, t -> LOGGER.error("Unpredictable error", t));
    }
}
