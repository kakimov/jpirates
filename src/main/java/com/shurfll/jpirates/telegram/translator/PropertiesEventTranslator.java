package com.shurfll.jpirates.telegram.translator;

import com.shurfll.jpirates.event.Event;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by kakim on 01.06.2017.
 */
public abstract class PropertiesEventTranslator<T extends Event> extends EventTranslator<T> {

    private static final String TRANSLATIONS_BASE_PATH = "/messages/events/";


    protected Properties props;

    @PostConstruct
    public void init() throws IOException {
        props = new Properties();
        props.load(new InputStreamReader(getClass().getResourceAsStream(getResourceFileName()), "UTF-8"));
    }

    @Override
    public String translate(T e) {
        return translate(getTranslationByKey(e), getArguments(e));
    }

    public abstract String getTranslationByKey(T t);

    public String translate(String format, Object... args) {
        return String.format(format, args);
    }

    public String getResourceFileName() {
        return TRANSLATIONS_BASE_PATH + getTranslatorFileName();
    }

    public abstract String getTranslatorFileName();

    public abstract Object[] getArguments(T t);
}
