package com.shurfll.jpirates.configuration;

import com.google.gson.*;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.entity.EntityAttributeType;
import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import java.lang.reflect.Type;

@Component
public class ConfigurationParserImpl implements ConfigurationParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationParserImpl.class);

    private ObjectFactory objectFactory;

    private Gson gson = new GsonBuilder().registerTypeAdapter(SimpleConfiguration.Attribute.class, new JsonDeserializer<SimpleConfiguration.Attribute>() {
        @Override
        public SimpleConfiguration.Attribute deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            JsonObject obj = jsonElement.getAsJsonObject();
            return new SimpleConfiguration.Attribute(obj.get("name").getAsString(), obj.get("type").getAsString(), obj.get("value").toString());
        }
    }).create();


    @Autowired
    public ConfigurationParserImpl(ObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
    }

    public Configuration parseConfiguration(String value) {
        return convert(gson.fromJson(value, SimpleConfiguration.class));
    }

    private Configuration convert(SimpleConfiguration simpleConfiguration) {
        Configuration configuration = new Configuration(simpleConfiguration.getModuleUri());
        if (simpleConfiguration.getAttributes() != null && !simpleConfiguration.getAttributes().isEmpty()) {
            simpleConfiguration.getAttributes().stream()
                    .map(attribute -> convertAttribute(simpleConfiguration.getModuleUri(), attribute))
                    .forEach(attr -> configuration.getAttributes().put(attr.getName(), attr));
        }
        return configuration;
    }

    private EntityAttribute convertAttribute(String moduleUri, SimpleConfiguration.Attribute attribute) {
        QName name = new QName(moduleUri, attribute.getName());
        EntityAttribute entityAttribute = objectFactory.createAttribute(null, name, convertAttributeValue(attribute));
        return entityAttribute;
    }

    private Object convertAttributeValue(SimpleConfiguration.Attribute attribute) {
        switch (EntityAttributeType.valueOf(attribute.getType())) {
            case LONG:
                return new Gson().fromJson(attribute.getValue(), Long.class);
            case LOCATION:
                return new Gson().fromJson(attribute.getValue(), Location.class);
            default:
                throw new IllegalArgumentException("Unsupported attribute: " + attribute);
        }
    }

}
