package com.shurfll.jpirates.action.service.player;

import com.shurfll.jpirates.ActorHelper;
import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.LocalizationKeys;
import com.shurfll.jpirates.action.service.ConfigurableActionService;
import com.shurfll.jpirates.configuration.ConfigurationAttributes;
import com.shurfll.jpirates.configuration.ConfigurationHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.LevelUpAction;
import com.shurfll.jpirates.manager.StateManager;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by shurfll on 28.05.17.
 */
@Component
public class LevelUpActionService extends ConfigurableActionService<LevelUpAction> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LevelUpActionService.class);
    private static final String HANDLER_URI = ConfigurationAttributes.CONFIGURATION_NS + "LevelUpActionService";
    private StateManager stateManager;
    private Long totalHealthIncreaseStep;
    public static final QName PARAM_TOTAL_HEALTH_INCREASE_STEP = new QName(HANDLER_URI, "totalHealthIncreaseStep");

    @Autowired
    public LevelUpActionService(ConfigurationManager configurationManager, StateManager stateManager) {
        super(configurationManager);
        this.stateManager = stateManager;
    }

    @Override
    protected boolean validateConfiguration(Optional<Configuration> nullableConfig) {
        return ConfigurationHelper.hasParam(nullableConfig, PARAM_TOTAL_HEALTH_INCREASE_STEP, Long.class);
    }

    @Override
    protected void configure(Optional<Configuration> nullableConfig) {
        LOGGER.debug("Configuration update: {}", nullableConfig);
        totalHealthIncreaseStep = ConfigurationHelper.getParamValue(nullableConfig, PARAM_TOTAL_HEALTH_INCREASE_STEP, Long.class, Commons.LEVEL_UP_TOTAL_HEALTH_STEP);
        LOGGER.info("Configuration updated. totalHealthIncreaseStep={}", totalHealthIncreaseStep);
    }

    @Override
    protected String getHandlerUri() {
        return HANDLER_URI;
    }

    @Override
    public boolean canHandle(Action a) {
        return a instanceof LevelUpAction;
    }


    @Override
    public Flowable<Event> doAction(LevelUpAction action) {
        return stateManager.getStateByActorId(action.getSourceActorId()).flatMap(state -> actionInternal(action, state));
    }

    private Flowable<Event> actionInternal(LevelUpAction action, State state) {
        Long levelUpPoints = ActorHelper.getActorLevelUpPoints(state);
        if (levelUpPoints == 0) {
            return Flowable.just(EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.LEVEL_UP_NO_POINTS));
        }
        Map<QName, Object> changes = processMode(action);
        changes.put(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, -1L);
        return Flowable.just(EventFactory.stateChangeEventModify(action, action.getSourceActorId(), changes)
                , EventFactory.infoEvent(action, action.getSourceActorId(), LocalizationKeys.LEVEL_UP_DONE));

    }

    private Map<QName, Object> processMode(LevelUpAction action) {
        switch (action.getLevelUpMode()) {
            case HEALTH: {
                return handleHealthMode(action);
            }
            case SPEED: {
                return handleSpeedMode(action);
            }
            case ACCURACY: {
                return handleAccuracyMode(action);
            }
            default: {
                throw new IllegalArgumentException("Unable to process mode: " + action.getLevelUpMode());
            }
        }
    }

    private Map<QName, Object> handleHealthMode(LevelUpAction action) {
        Map<QName, Object> changes = new HashMap<>();
        changes.put(ActorStateAttributes.ACTOR_TOTAL_HEALTH, totalHealthIncreaseStep);
        return changes;
    }

    private Map<QName, Object> handleSpeedMode(LevelUpAction action) {
        Map<QName, Object> changes = new HashMap<>();
        changes.put(ActorStateAttributes.ACTOR_SAILING_SKILL, 1L);
        return changes;
    }

    private Map<QName, Object> handleAccuracyMode(LevelUpAction action) {
        Map<QName, Object> changes = new HashMap<>();
        changes.put(ActorStateAttributes.ACTOR_ACCURACY_SKILL, 1L);
        return changes;
    }

    public Long getTotalHealthIncreaseStep() {
        return totalHealthIncreaseStep;
    }
}
