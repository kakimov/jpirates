package com.shurfll.jpirates;

import com.shurfll.jpirates.entity.Location;
import com.shurfll.jpirates.entity.actor.Player;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by shurfll on 22.05.17.
 */
public class Commons {
    public static final String ACTION_EXPERIENCE="experience";
    public static final Long ACTION_NO_DELAY=0L;
    public static final Long ACTION_DEFAULT_DELAY=30000L;
    public static final Long ACTION_BATTLE_LOSE_DEFAULT_DELAY=300000L;
    public static final Long ACTION_SEARCH_DELAY=45000L;
    public static final Long FIRE_DEFAULT_DELAY=13000L;
    public static final Long FIRE_NPC_DEFAULT_DELAY=17000L;
    public static final Long FIRE_NPC_DEFAULT_START_DELAY=15000L;
    public static final Long DEFAULT_HEALTH = 30L;
    public static final Long DEFAULT_GOLD = 20L;
    public static final Long MOVE_STEP_DELTA=1L;
    public static final Long DEFAULT_FIRE_POWER=10L;
    public static final Long DEFAULT_HEALTH_REPAIRING_AMOUNT = 5L;
    public static final Long DEFAULT_HEALTH_REPAIRING_TIMEOUT = 60000L;
    public static final Long DEFAULT_FIREPOWER_UPGRADE_COST = 30L;
    public static final Long DEFAULT_FIREPOWER_UPGRADE_AMOUNT = 5L;
    public static final Long DEFAULT_REPAIR_UPGRADE_COST = 15L;
    public static final Long DEFAULT_FIRE_POWER_UPGRADE_COST = 50L;
    public static final Long DEFAULT_REPAIR_AMOUNT_UPGRADE_COST = 15L;
    public static final Long DEFAULT_REPAIR_AMOUNT_UPGRADE_AMOUNT = 5L;
    public static final Long DEFAULT_NPC_HEALTH = 10L;
    public static final Long DEFAULT_NPC_GOLD = 5L;
    public static final Long DEFAULT_NPC_FIRE_POWER = DEFAULT_FIRE_POWER;
    public static final Long DEFAULT_MOVE_EXPERIENCE = 1L;
    public static final Long DEFAULT_BATTLE_WON_EXPERIENCE = 20L;
    public static final Long DEFAULT_TREASURE_FOUND_EXPERIENCE = 30L;
    public static final Long LEVEL_EXPERIENCE_STEP = 100L;

    public static final Long LEVEL_UP_TOTAL_HEALTH_STEP = 5L;
    public static final Long TREASURE_GENERATE_DEFAULT_TIMEOUT = 1800000L; // half an hour

    public static Location generateLocation(){
        Random r = new Random();
        Long longitude = Long.valueOf(r.nextInt(4)-2);
        Long latitude = Long.valueOf(r.nextInt(4)-2);
        return new Location(longitude,latitude);
    }

    public static Location restPlace(){
        Random r = new Random();
        Long longitude = 999L;
        Long latitude = 999L;
        return new Location(longitude,latitude);
    }

    public static Location loc(Long lat, Long lon){
        return new Location(lat,lon);
    }

    public static String objectsToString(List<Object> objs){
        return objs.stream().map(o -> {
            if(o instanceof Player) return ((Player) o).getName();
            else return o.toString();
        }).collect(Collectors.joining(", "));
    }

}
