package com.shurfll.jpirates.event.action.player;

import com.shurfll.jpirates.event.action.PlayerAction;

import javax.xml.namespace.QName;
import java.util.Map;
import java.util.Optional;

/**
 * Created by kakim on 04.06.2017.
 */
public class FireAction extends PlayerAction {
    private static final String QUALIFIER = "fireAction";

    public static final String TYPE=ACTION_BASE_NAMESPACE +"/"+QUALIFIER;

    public static final QName TARGET_ID = attribute(TYPE, "targetId");
    public static final QName TARGET_NAME = attribute(TYPE, "targetName");

    public FireAction(String oid, String causeOid, String sourceActorId, Map<QName, Object> meta, String targetId, String targetName) {
        super(oid, causeOid, sourceActorId, meta);
        if (targetId != null)
            putAttribute(oid, TARGET_ID, targetId);
        putAttribute(oid, TARGET_NAME, targetName);
    }

    public FireAction(String oid, String causeOid, String sourceActorId, String targetId, String targetName) {
        this(oid, causeOid, sourceActorId, null, targetId, targetName);
    }

    public String getTargetId() {
        return getValue(TARGET_ID, String.class).get();
    }

    public void setTargetId(String targetId) {
        putAttribute(getOid(), TARGET_ID, targetId);
    }

    public Optional<String> getTargetName() {
        return getValue(TARGET_NAME, String.class);
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FireAction that = (FireAction) o;

        if (getTargetId() != null ? !getTargetId().equals(that.getTargetId()) : that.getTargetId() != null)
            return false;
        return getTargetName() != null ? getTargetName().equals(that.getTargetName()) : that.getTargetName() == null;
    }

    @Override
    public int hashCode() {
        int result = getTargetId() != null ? getTargetId().hashCode() : 0;
        result = 31 * result + (getTargetName() != null ? getTargetName().hashCode() : 0);
        return result;
    }
}
