package com.shurfll.jpirates.handler;

import com.shurfll.jpirates.Commons;
import com.shurfll.jpirates.TestEventHelper;
import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.ActorStateAttributes;
import com.shurfll.jpirates.entity.AttributeChangeMode;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.entity.State;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.event.EventFactory;
import com.shurfll.jpirates.event.system.StateChangeEvent;
import com.shurfll.jpirates.event.system.StateChangedEvent;
import com.shurfll.jpirates.manager.StateManager;
import com.shurfll.jpirates.objectfactory.ObjectFactory;
import com.shurfll.jpirates.reactor.Reactor;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.subscribers.TestSubscriber;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class LevelUpListenerTest {

    private LevelUpListener listener;
    private Reactor reactor;
    private PublishProcessor<Event> subject;
    private PublishProcessor<Configuration> configurations;
    private StateManager stateManager;
    private ConfigurationManager configurationManager;
    private ObjectFactory objectFactory;
    private String PLAYER_ID = "playerId";
    private TestSubscriber<Event> subscriber;

    @Before
    public void init() {
        subscriber = TestSubscriber.create();
        objectFactory = new ObjectFactory();
        subject = PublishProcessor.create();
        configurations = PublishProcessor.create();
        reactor = mock(Reactor.class);
        when(reactor.events()).thenReturn(subject);
        configurationManager = mock(ConfigurationManager.class);
        when(configurationManager.configurationChange(anyString())).thenReturn(configurations);
        when(configurationManager.getConfiguration(anyString())).thenReturn(Optional.empty());
        stateManager = mock(StateManager.class);
        listener = new LevelUpListener(reactor, configurationManager, stateManager);
        listener.init();
    }

    @Test
    public void test001_configurationUpdate() {
        assertEquals(Commons.LEVEL_EXPERIENCE_STEP, listener.getExperienceBound());
        final Long newValue = 200L;
        configurations.onNext(createConfiguration("test", newValue));
        assertEquals(newValue, listener.getExperienceBound());
    }

    @Test
    public void test002_checkBounds() {
        final Long bound = 100L;
        configurations.onNext(createConfiguration("test", bound));
        StateChangedEvent event = EventFactory.stateChangedEvent(null, PLAYER_ID);
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createState(1L, 0L)));
        listener.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertNoValues();


        subscriber = TestSubscriber.create();
        when(stateManager.getStateByActorId(eq(PLAYER_ID))).thenReturn(Flowable.just(createState(1L, 100L)));
        listener.handle(event).subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        Map<QName, Object> expectedChanges = new HashMap<>();
        expectedChanges.put(ActorStateAttributes.ACTOR_LEVEL, 1L);
        expectedChanges.put(ActorStateAttributes.ACTOR_LEVEL_UP_POINTS, 1L);
        TestEventHelper.checkEventProduced(subscriber, StateChangeEvent.class, e -> TestEventHelper.stateChangeEventMatched(e, PLAYER_ID, expectedChanges));
    }

    private Configuration createConfiguration(String oid, Long value) {
        Configuration conf = new Configuration(oid);
        conf.getAttributes().put(LevelUpListener.PARAM_LEVEL_UP_EXPERIENCE_BOUND, objectFactory.createAttribute(oid, LevelUpListener.PARAM_LEVEL_UP_EXPERIENCE_BOUND, value));
        return conf;
    }

    private State createState(Long level, Long experience) {
        State state = objectFactory.createEmptyState(null, null);
        objectFactory.updateEntityAttribute(state, ActorStateAttributes.ACTOR_LEVEL, AttributeChangeMode.REPLACE, level);
        objectFactory.updateEntityAttribute(state, ActorStateAttributes.ACTOR_EXPERIENCE, AttributeChangeMode.REPLACE, experience);
        return state;
    }

}