package com.shurfll.jpirates.telegram.operator;

import com.shurfll.jpirates.event.action.Action;
import com.shurfll.jpirates.event.action.player.MessageAction;
import com.shurfll.jpirates.event.action.player.StartAction;
import com.shurfll.jpirates.manager.PlayersManager;
import com.shurfll.jpirates.telegram.Message;
import com.shurfll.jpirates.telegram.operator.resolvers.ActionResolver;
import com.shurfll.jpirates.telegram.operator.resolvers.player.MessageActionResolver;
import com.shurfll.jpirates.telegram.operator.resolvers.player.StartActionResolver;
import io.reactivex.Flowable;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ActionFactoryImplTest {

    private ActionFactory factory;

    private PlayersManager playersManager;

    @Test
    /**
     * Тест проверять, что конфликта между командами Start и Message не будет
     */
    public void test001_priorityOrder_startVsMessageActionResolver() {
        playersManager = Mockito.mock(PlayersManager.class);
        when(playersManager.getPlayerFlowable(anyString())).thenReturn(Flowable.empty());
        factory = new ActionFactoryImpl(resolvers(new MessageActionResolver(), new StartActionResolver(playersManager)));
        List<Message> messages = messages(msg("/start"), msg("test"));
        Optional<Action> actionOpt = factory.map(messages);
        assertTrue(actionOpt.isPresent());
        assertTrue(actionOpt.get() instanceof StartAction);

        messages = messages(msg("message"));
        actionOpt = factory.map(messages);
        assertTrue(actionOpt.isPresent());
        assertTrue(actionOpt.get() instanceof MessageAction);
    }

    private List<ActionResolver> resolvers(ActionResolver ... resolvers){
        return Arrays.asList(resolvers);
    }

    private List<Message> messages(Message ... msgs){
        return Arrays.asList(msgs);
    }

    private Message msg(String text){
        return new Message(null, text);
    }
}