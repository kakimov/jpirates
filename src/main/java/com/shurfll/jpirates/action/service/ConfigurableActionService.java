package com.shurfll.jpirates.action.service;

import com.shurfll.jpirates.configuration.ConfigurationManager;
import com.shurfll.jpirates.entity.Configuration;
import com.shurfll.jpirates.event.action.Action;

import javax.annotation.PostConstruct;
import java.util.Optional;

public abstract class ConfigurableActionService<T extends Action> extends PlayerActionService<T> {

    private ConfigurationManager configurationManager;

    public ConfigurableActionService(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    @PostConstruct
    public void init() {
        configure(configurationManager.getConfiguration(getHandlerUri()));
        configurationManager.configurationChange(getHandlerUri())
                .map(Optional::ofNullable)
                .filter(this::validateConfiguration)
                .subscribe(this::configure);
    }

    protected abstract boolean validateConfiguration(Optional<Configuration> nullableConfig);

    protected abstract void configure(Optional<Configuration> nullableConfig);

    protected abstract String getHandlerUri();

}
