package com.shurfll.jpirates.quest;

import com.shurfll.jpirates.entity.Quest;
import com.shurfll.jpirates.telegram.keyboard.ButtonsKeyboard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class QuestManager {

    @Autowired
    private List<QuestTypeResolver> resolvers;

    public static List<Quest> QUESTS = Arrays.asList(
            new Quest(StaticQuestTypeResoler.TYPE, "main_quest", null, "", "Выберите действие", "#main_quest")
            , new Quest(StaticQuestTypeResoler.TYPE, "commands", "main_quest", "Слушай мою команду!", "Слушаю, капитан", "#commands")
                , new Quest(StaticQuestTypeResoler.TYPE, "status", "commands", "Статус!", "Сию минуту, капитан!", "/status")
                , new Quest(StaticQuestTypeResoler.TYPE, "repair", "commands", "Ремонт!", "Опытный капитан всегда следит за состоянием своего корабля", "/repair")
                , new Quest(StaticQuestTypeResoler.TYPE, "lookaround", "commands", "Впередсмотрящий!", "Вы осматриваетесь и видете", "/lookaround")
                , new Quest(StaticQuestTypeResoler.TYPE, "search", "commands", "Искать сокровища!", "Вы начинаете искать сокровища...", "/search")
                , new Quest(StaticQuestTypeResoler.TYPE, "sail", "commands", "Поднять паруса!", "Куда собираемся плыть, капитан?", "#sail")
                    , new Quest(StaticQuestTypeResoler.TYPE, "sail_east", "sail", "На восток!", "", "/sail EAST")
                    , new Quest(StaticQuestTypeResoler.TYPE, "sail_west", "sail", "На запад!", "", "/sail WEST")
                    , new Quest(StaticQuestTypeResoler.TYPE, "sail_north", "sail", "На север!", "", "/sail NORTH")
                    , new Quest(StaticQuestTypeResoler.TYPE, "sail_south", "sail", "На юг!", "", "/sail SOUTH")
                , new Quest(FireQuestResoler.TYPE, "fire", "commands", "Огонь!", "Выберите противника", "#fire")
            , new Quest(StaticQuestTypeResoler.TYPE, "person", "main_quest", "Персонаж", "Вы задумываетесь на минуту...", "#person")
                , new Quest(StaticQuestTypeResoler.TYPE, "upgrade", "person", "Купить за голду", "Что хотите купить?", "#upgrade")
                    , new Quest(StaticQuestTypeResoler.TYPE, "upgrade_heal", "upgrade", "Вылечиться", "Вы потратили золото на лечение", "/upgrade HEAL")
                    , new Quest(StaticQuestTypeResoler.TYPE, "upgrade_heal", "upgrade", "Купить пушку!", "Вы купили пушку", "/upgrade FIRE_POWER")
                , new Quest(StaticQuestTypeResoler.TYPE, "levelup", "person", "Прокачать перса", "Выберите характеристику, которую хотите прокачать", "#levelup")
                    , new Quest(StaticQuestTypeResoler.TYPE, "levelup_health", "levelup", "Здоровье", "Ваше максимальное здоровье увеличено", "/levelup HEALTH")
                    , new Quest(StaticQuestTypeResoler.TYPE, "levelup_speed", "levelup", "Скорость", "Ваша скорость передвижения увеличена", "/levelup SPEED")
                    , new Quest(StaticQuestTypeResoler.TYPE, "levelup_accuracy", "levelup", "Точность", "Ваша точность выстрелов увеличена", "/levelup ACCURACY"));

    public Optional<Quest> getQuestInfo(String quest) {
        String questId = StringUtils.isEmpty(quest) ? "main_quest" : quest;
        return QUESTS.stream().filter(q -> q.getQuestId().equals(questId)).findFirst();
    }

    public ButtonsKeyboard createDefaultKeyboard() {
        return createButtons(getDefaultQuests());
    }

    public List<Quest> getChildQuests(String actorId, String quest) {
        String questId = StringUtils.isEmpty(quest) ? "main_quest" : quest;
        List<Quest> quests = getQuestInfo(questId)
                .flatMap(id -> resolvers.stream().filter(resolver -> resolver.canResolve(id)).findFirst())
                .map(res -> res.resolve(actorId, questId))
                .orElse(getDefaultQuests());
        if (!"main_quest".equals(questId))
            quests.add(new Quest(StaticQuestTypeResoler.TYPE, "main_menu", null, "X", "Отставить!", "#"));
        return quests;
    }

    private List<Quest> getDefaultQuests() {
        return QUESTS.stream().filter(quest -> "main_quest".equals(quest.getParentQuestId())).collect(Collectors.toList());
    }

    public static ButtonsKeyboard createButtons(List<Quest> quests) {
        List<String> buttons = new ArrayList<>();
        quests.stream().map(QuestManager::convert).forEach(buttons::add);
        ButtonsKeyboard keyboard = new ButtonsKeyboard(buttons);
        return keyboard;
    }

    public static String convert(Quest quest) {
        return quest.getCommand();
    }

    public Optional<Menu> getMenu(String actorId, String quest){
        return Optional.ofNullable(quest)
                .map(s -> getChildQuests(actorId,s))
                .map(quests -> new Menu(quest,quests));
    }

    public class Menu {
        private String currentMenu;
        private List<Quest> quests;

        public Menu(String currentMenu, List<Quest> quests) {
            this.currentMenu = currentMenu;
            this.quests = quests;
        }

        public String getCurrentMenu() {
            return currentMenu;
        }

        public void setCurrentMenu(String currentMenu) {
            this.currentMenu = currentMenu;
        }

        public List<Quest> getQuests() {
            return quests;
        }

        public void setQuests(List<Quest> quests) {
            this.quests = quests;
        }
    }
}
