package com.shurfll.jpirates.persistence.converter.mapper;

import com.shurfll.jpirates.EntityAttributeContainer;
import com.shurfll.jpirates.entity.EntityAttribute;
import com.shurfll.jpirates.event.Event;
import com.shurfll.jpirates.persistence.Attribute;
import com.shurfll.jpirates.persistence.AttributeContainer;
import com.shurfll.jpirates.persistence.EventEntity;
import com.shurfll.jpirates.persistence.converter.EntityAttributeConverter;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractMapper<T extends EntityAttributeContainer, V extends AttributeContainer> {
    protected EntityAttributeConverter attributeConverter;

    public AbstractMapper(EntityAttributeConverter attributeConverter) {
        this.attributeConverter = attributeConverter;
    }

    public boolean canHandle(EventEntity entity) {
        return Objects.equals(getMapperType(), entity.getType());
    }
    public boolean canHandle(Event entity) {
        return getMapperClass().equals(entity.getClass());
    }

    protected abstract String getMapperType();

    protected abstract Class getMapperClass();

    public abstract V toEntity(T entity);

    public abstract T fromEntity(V entity);

    protected <T> T mandatoryAttribute(EventEntity e, QName name) {
        return (T) findAttribute(e, name).orElseThrow(() -> new IllegalArgumentException("Can't find attr: " + name));
    }

    protected <T> T optionalAttribute(EventEntity e, QName name) {
        return (T) findAttribute(e, name).orElse(null);
    }

    private <T> Optional<T> findAttribute(EventEntity e, QName name) {
        return e.getAttributes().stream()
                .filter(attr -> Objects.equals(name.getLocalPart(), attr.getName()) && Objects.equals(name.getNamespaceURI(), attr.getNamespace()))
                .findFirst()
                .map(attr -> attributeConverter.deserializeEntityAttribute(attr))
                .flatMap(attr -> attr.getValue())
                .map(val -> (T) val.getValue());
    }

    protected List<Attribute> convertEventAttributes(AttributeContainer owner,EntityAttributeContainer e) {
        return e.getAttributes().entrySet().stream()
                .map(entry -> attributeConverter.serializeEntityAttribute(owner,entry.getValue()))
                .collect(Collectors.toList());
    }

    protected List<EntityAttribute> convertAttributes(AttributeContainer e) {
        return e.getAttributes().stream()
                .map(entry -> attributeConverter.deserializeEntityAttribute(entry))
                .collect(Collectors.toList());
    }
}
